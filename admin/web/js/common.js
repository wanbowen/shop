//弹框提示
function myalert(msg) {
    layui.use('layer', function(){
        var layer = layui.layer;
        layer.ready(function(){
            layer.alert(msg,{
                title: '提示信息'
                ,skin: 'layui-layer-molv'
                ,btnAlign: 'c'
                ,closeBtn: 0
                ,anim: 4 //动画类型
            }, function(){
                layer.close(layer.index);
            });
        });
    });
}
//弹窗提示后刷新页面
function reloadalert(msg) {
    layui.use('layer', function(){
        var layer = layui.layer;
        layer.ready(function(){
            layer.alert(msg,{
                title: '提示信息'
                ,skin: 'layui-layer-molv'
                ,btnAlign: 'c'
                ,closeBtn: 0
                ,anim: 4 //动画类型
            }, function(){
                layer.close(layer.index);
                location.reload();
            });
        });
    });
}