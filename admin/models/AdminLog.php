<?php

namespace admin\models;

use Yii;

/**
 * This is the model class for table "hm_admin_log".
 *
 * @property int $id 序号
 * @property int $admin_id 管理员id
 * @property string $client_ip 登陆IP
 * @property string $record 操作记录
 * @property string $data 数据id
 * @property string $created_at 创建时间
 */
class AdminLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hm_admin_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['admin_id'], 'integer'],
            [['created_at'], 'safe'],
            [['client_ip'], 'string', 'max' => 20],
            [['record'], 'string', 'max' => 200],
            [['data'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'admin_id' => 'Admin ID',
            'client_ip' => 'Client Ip',
            'record' => 'Record',
            'data' => 'Data',
            'created_at' => 'Created At',
        ];
    }
}
