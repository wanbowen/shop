<?php
namespace admin\models;

use Yii;
use yii\base\Model;
use common\components\VerifyHelper;
use admin\models\AdminRole;
/**
 * Login form
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;
	public $verify;
    private $_user;
	private $_user2;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
			[['verify'], 'required'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }
	public function attributeLabels()
    {
        return [
            'username' => '用户名',
            'password' => '密码',
			'verify' => '验证码',
            'rememberMe' => '下次记住我'
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
			$user2 = $this->getUser2();
			if (!$user2) {
                $this->addError($attribute, '用户名不正确！');
				return false;
            }
			if (isset($user2->status)&&$user2->status==1) {
                $this->addError($attribute, '用户被禁用！');
				return false;
            }
			if (!$user->validatePassword($this->password)) {
                $this->addError($attribute, '密码不正确！');
				return false;
            }
			$role=AdminRole::findOne($user->roleid);
			if (isset($role->disabled)&&$role->disabled==1) {
                $this->addError($attribute, '角色被禁用！');
				return false;
            }
			$ceckstatus=$this->check_verify();
			if(!$ceckstatus){
				$this->addError('verify','验证码不正确');
				return false;
			}
        }
    }

    /**
     * Logs in a user using the provided username and password.
     *
     * @return bool whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 2 : 0);
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = Admin::findByUsername($this->username);
        }

        return $this->_user;
    }
	/**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser2()
    {
        if ($this->_user2 === null) {
            $this->_user2 = Admin::find()->where(['username'=>$this->username])->one();
        }

        return $this->_user2;
    }
	/** 
	* 验证码检查 
	*/  
	public function check_verify(){  
		$verify2 = new VerifyHelper(); 
		return $verify2->check($this->verify, $id="");  
	}
}
