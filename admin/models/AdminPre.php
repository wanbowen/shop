<?php

namespace admin\models;

use Yii;

/**
 * This is the model class for table "nlo_admin_pre".
 *
 * @property string $id 序号
 * @property int $pid 父级id
 * @property string $name 权限名称
 * @property string $m 模块名
 * @property string $c 控制器
 * @property string $a 方法
 * @property string $data 数据
 */
class AdminPre extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hm_admin_pre';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pId'], 'integer'],
            [['name'], 'string', 'max' => 40],
            [['m', 'c', 'a'], 'string', 'max' => 20],
            [['data'], 'string', 'max' => 30],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pId' => 'Pid',
            'name' => 'Name',
            'm' => 'M',
            'c' => 'C',
            'a' => 'A',
            'data' => 'Data',
        ];
    }
}
