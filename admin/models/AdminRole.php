<?php

namespace admin\models;

use Yii;

/**
 * This is the model class for table "nlo_admin_role".
 *
 * @property int $roleid 角色id
 * @property string $rolename 角色名
 * @property string $description 描述
 * @property int $listorder 排序
 * @property int $disabled 是否禁用. 0正常 1.禁用
 * @property int $admin_id 创建人
 * @property string $created_at 创建时间
 * @property string $updated_at 更新时间
 * @property string $deleted_at 删除时间
 */
class AdminRole extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hm_admin_role';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['description'], 'required'],
            [['description'], 'string'],
            [['listorder', 'disabled', 'admin_id'], 'integer'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['rolename'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'roleid' => 'Roleid',
            'rolename' => 'Rolename',
            'description' => 'Description',
            'listorder' => 'Listorder',
            'disabled' => 'Disabled',
            'admin_id' => 'Admin ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'deleted_at' => 'Deleted At',
        ];
    }
	public function getAdmins()
    {
        return $this->hasMany(Admin::className(), ['roleid' => 'roleid']);
    }
	public function getAdminones()
	{
		return $this->hasOne(Admin::className(), ['id' => 'admin_id']);
	}
}
