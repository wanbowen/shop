<?php

namespace admin\models;

use Yii;

/**
 * This is the model class for table "nlo_admin_role_pre".
 *
 * @property int $roleid 角色id
 * @property int $preid 权限id
 */
class AdminRolePre extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hm_admin_role_pre';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['roleid', 'preid'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'roleid' => 'Roleid',
            'preid' => 'Preid',
        ];
    }
}
