<?php
use yii\helpers\Html;
$this->title = '订单管理';
?>
<span class="layui-breadcrumb">
  <a href="/">首页</a>
  <a><cite><?=$this->title?></cite></a>
</span>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>订单管理</legend>
</fieldset>

<style>
    .layui-form-label{width: 90px;}
    .layui-input-block {margin-left: 120px;}
</style>

<form class="layui-form" id="search_form" action="/order/index" method="get">
    <div class="layui-form-item">
        <input type="hidden" name="limit" value="<?=$limit?>">
        <label class="layui-form-label" style="width: 60px;" >订单号</label>
        <div class="layui-input-inline" style="width: 200px;">
            <input type="text" name="order_code" value="<?= !empty($getForm['order_code'])?$getForm['order_code']:'';?>" lay-verify="title" autocomplete="off" placeholder="请输入单号" class="layui-input">
        </div>
        <label class="layui-form-label" style="width: 70px;">下单时间:</label>
        <div class="layui-input-inline" style="width: 200px;">
            <input type="text" name="create_time" id="created_at" value="<?= !empty($getForm['create_time'])?$getForm['create_time']:'' ?>" class="layui-input" placeholder="起始时间">
        </div>
        <div class="layui-input-inline" style="width: 200px;">
            <input type="text" name="end_time" id="end_at" value="<?= !empty($getForm['end_time'])?$getForm['end_time']:'' ?>" class="layui-input" placeholder="结束时间">
        </div>
        <div style="height: 10px;clear: both;"></div>
        <label class="layui-form-label" style="width: 60px;">订单状态</label>
        <div class="layui-input-inline" style="width: 150px;">
            <select name="status">
                <option value="-1">请选择状态</option>
                <option value="0" <?= isset($getForm['status']) && $getForm['status']=='0' ? 'selected' : ''?>>待派单</option>
                <option value="1" <?= isset($getForm['status']) && $getForm['status']=='1' ? 'selected' : ''?>>已派单</option>
                <option value="2" <?= isset($getForm['status']) && $getForm['status']=='2' ? 'selected' : ''?>>订单已修改</option>
                <option value="3" <?= isset($getForm['status']) && $getForm['status']=='3' ? 'selected' : ''?>>待付款</option>
                <option value="4" <?= isset($getForm['status']) && $getForm['status']=='4' ? 'selected' : ''?>>已付款(待评价)</option>
                <option value="5" <?= isset($getForm['status']) && $getForm['status']=='5' ? 'selected' : ''?>>已评价(已完成)</option>
                <option value="8" <?= isset($getForm['status']) && $getForm['status']=='8' ? 'selected' : ''?>>已取消</option>
            </select>
        </div>
        <label class="layui-form-label" style="width: 100px;" >用户电话</label>
        <div class="layui-input-inline" style="width: 200px;">
            <input type="text" name="mobile" value="<?= !empty($getForm['mobile'])?$getForm['mobile']:'';?>" lay-verify="title" autocomplete="off" placeholder="请输入电话" class="layui-input">
        </div>
        <div class="layui-input-inline" style="width:50px;">
            <button class="layui-btn" lay-submit="" lay-filter="demo1">筛选</button>
        </div>
    </div>
</form>

<table class="layui-table">
    <thead>
    <tr>
        <th>序号</th>
        <th>订单号</th>
        <th>商品名称</th>
        <th>订单金额</th>
		<th>状态</th>
		<th>用户手机号</th>
		<th>下单时间</th>
        <th>操作</th>
    </tr>
    </thead>
    <tbody>
    <?php if(isset($datas) && !empty($datas)){ ?>
        <?php foreach($datas as $key=>$value){?>
            <tr>
                <td><?=$value['id']?></td>
				<td><?=$value['order_code']?></td>
				<td><?=$value['goods_name']?></td>
				<td>￥<?=$value['order_total']?></td>
			    <td>
                    <?php
                        $sta = '';
                        switch ($value['order_status']){
                            case 0:
                                $sta = '待派单';
                                break;
                            case 1:
                                $sta = '已派单';
                                break;
                            case 2:
                                $sta = '订单已修改';
                                break;
                            case 3:
                                $sta = '待付款';
                                break;
                            case 4:
                                $sta = '已付款';
                                break;
                            case 5:
                                $sta = '已评价';
                                break;
                            case 8:
                                $sta = '已取消';
                                break;
                        }
                        echo $sta;
                    ?>
                </td>
			    <td><?=$value['mobile']?></td>
			    <td><?=$value['created_at']?></td>
                <td>
                    <?php if($value['order_status'] == 0){ ?>
                        <a class="layui-btn layui-btn-xs" href="javascript:;" onclick="order(<?=$value['id']?>,'<?=$value['book_date']?>')" >派单</a>
                    <?php }?>
                    <?php if(in_array($value['order_status'],[1,2,3])){ ?>
                        <a class="layui-btn layui-btn-xs" href="/order/edit?id=<?=$value['id']?>" >修改订单</a>
                    <?php }?>
                    <a class="layui-btn layui-btn-xs" href="javascript:;" onclick="remark(<?=$value['id']?>,'<?=$value['order_remarks']?>')">备注</a>
                    <a class="layui-btn layui-btn-xs" href="/order/view?id=<?=$value['id']?>" >查看</a>
                    <a class="layui-btn layui-btn-xs" href="javascript:;" onclick="cancelorder(<?=$value['id']?>)">取消</a>
                    <a class="layui-btn layui-btn-xs" href="javascript:;" onclick="vsremark(<?=$value['id']?>,'<?=$value['visit_remarks']?>')">回访备注</a>
                </td>
            </tr>
        <?php }}else{ ?>
        <tr>
            <td colspan='7' style="text-align: center">暂无数据</td>
        </tr>

    <?php }?>

    </tbody>
</table>
<div id="demo3"></div>

<script>
    layui.use(['form', 'layedit', 'laydate','laypage'], function(){
        var form = layui.form
            ,layer = layui.layer
            ,layedit = layui.layedit
            ,laydate = layui.laydate
            ,laypage = layui.laypage;
        laypage.render({
            elem: 'demo3'
            ,count: <?=$count?>
            ,layout:['count','prev', 'page', 'next','limit','skip']
            ,curr:<?=$page?>
			,limit:<?=$limit?>
            ,jump: function(obj, first){
                var page=obj.curr; //得到当前页，以便向服务端请求对应页的数据。
                var limit=obj.limit; //得到每页显示的条数
                var js_str = 'page='+page+'&limit='+limit;
                var form_str = $("#search_form").serialize();
                var form_arr = form_str.split('&');
                form_arr.splice(0,1);
                form_str = form_arr.join("&");
                form_str = js_str+'&'+form_str;
                //首次不执行
                if(!first){
                    window.location.href = "/order/index?"+form_str;
                }
            }
        });
        laydate.render({
            elem: '#created_at' //指定元素
        });
        laydate.render({
            elem: '#end_at' //指定元素
        });
        form.on('select(pro)', function(data){
            var this_pro_id = data.value;
            var new_staff_data = getStaff(this_pro_id);
            var nhtm = '';
            if(new_staff_data.length > 0) {
                $.each(new_staff_data, function (m, n) {
                    nhtm += '<option value="' + n.id + '">' + n.name + '</option>';
                });
            }else{
                nhtm += '<option value="0">此职业无人员</option>';
            }
            $("#c-staff").html(nhtm);
            form.render('select', 'staf');
        });
    });

    //派单
    function order(id,book_date) {
        layer.load();
        var pro_data = getPro();

        var htm = '<form class="layui-form" action="/order/save" method="post"><div class="layui-form-item" style="padding: 15px 30px 0px 0px;"><label class="layui-form-label">选择职业</label>';
        htm += '<input type="hidden" name="order_id" value="'+id+'">';
        htm += '<div class="layui-input-block"><select name="pro_id" lay-filter="pro">';
        var pro_id = 0;
        $.each(pro_data, function (x,y) {
            if(x == 0){
                pro_id = y.id;
            }
            htm += '<option value="'+y.id+'">'+y.name+'</option>';
        });
        htm += '</select></div></div>';

        var staff_data = getStaff(pro_id);
        htm += '<div class="layui-form-item" style="padding: 5px 30px 0px 0px;"><label class="layui-form-label">选择人员</label>';
        htm += '<div class="layui-input-block layui-form" lay-filter="staf"><select name="staff_id" lay-filter="staff" id="c-staff">';
        $.each(staff_data, function (m,n) {
            htm += '<option value="'+n.id+'">'+n.name+'</option>';
        });
        htm += '</select></div></div>';
        htm += '<div class="layui-form-item"><label class="layui-form-label">上门时间</label>';
        htm += '<div class="layui-input-inline" style="width: 25%">';
        htm += '<input type="text" name="home_date" id="home_date" value="" required  lay-verify="required" class="layui-input">';
        htm += '</div><div class="layui-form-mid">  </div>';
        htm += '<div class="layui-input-inline" style="width: 15%">';
        htm += '<input type="text" name="strat_time" id="strat_time" value="" required  lay-verify="required" class="layui-input">';
        htm += '</div><div class="layui-form-mid">至</div>';
        htm += '<div class="layui-input-inline" style="width: 15%">';
        htm += '<input type="text" name="end_time" id="end_time" value="" required  lay-verify="required" class="layui-input">';
        htm += '</div></div>';
        htm += '<div class="layui-form-item"><label class="layui-form-label">订单预约时间</label>';
        htm += '<div class="layui-input-inline">';
        htm += '<input type="text" name="" value="'+book_date+'" required class="layui-input">';
        htm += '</div></div>';
        htm += '<div style="text-align: center;margin-top: 40px;"><button class="layui-btn" lay-submit>确定</button>';
        htm += '<a class="layui-btn layui-btn-primary" style="margin-left: 60px;" href="javascript:;" onclick="cancle()">取消</a></div></form>';

        layer.open({
            type: 1,
            title: '选择接单人',
            content: htm,
            area: ['600px', '400px']
        });
        layui.form.render();
        layui.laydate.render({
            elem: '#home_date' //指定元素
            ,format: 'yyyy年MM月dd日'
        });
        layui.laydate.render({
            elem: '#strat_time' //指定元素
            ,type: 'time'
            ,format: 'HH:mm'
        });
        layui.laydate.render({
            elem: '#end_time' //指定元素
            ,type: 'time'
            ,format: 'HH:mm'
        });

        layer.closeAll('loading');
    }
    //获取员工职业
    function getPro() {
        var data = '';
        $.ajaxSettings.async = false;
        $.get('/repair/get-profession',{},function (ret) {
            if(ret.code == '200'){
                data = ret.data;
            }else{
                myalert(ret.msg);
                layer.closeAll('loading');
                return false;
            }
        },'json');
        return data;
    }
    //获取员工
    function getStaff(pro_id) {
        var data = '';
        $.ajaxSettings.async = false;
        $.get('/repair/get-staff',{'pro_id':pro_id},function (ret) {
            if(ret.code == '200'){
                data = ret.data;
            }
        },'json');
        return data;
    }

    //订单备注
    function remark(order_id,msg){
        var htm = '<form class="layui-form" action="" method="post" style="margin-top: 20px;"><div class="layui-form-item"><label class="layui-form-label">备注</label>';
        htm += '<input type="hidden" value="'+order_id+'" name="order_id" >';
        htm += '<div class="layui-input-inline" style="width: 60%;">';
        htm += '<textarea  name="order_remarks" required lay-verify="required" placeholder="请输入" class="layui-textarea">'+msg+'</textarea></div></div>';
        htm += '<div style="text-align: center;margin-top: 40px;"><a class="layui-btn" onclick="tjremarks(this)">确定</a>';
        htm += '<a class="layui-btn layui-btn-primary" style="margin-left: 60px;" href="javascript:;" onclick="canre()">取消</a></div></form>';
        layer.open({
            type: 1,
            title: '填写订单备注',
            content: htm,
            area: ['500px', '300px']
        });
        layui.form.render();
    }
    
    //提交备注
    function tjremarks(obj) {
        layer.load();
        var formobj = $(obj).parent().parent();
        var order_id = formobj.find('input[name="order_id"]').val();
        var order_remarks = formobj.find('textarea').val();
        if(order_id == ''){
            myalert('参数错误，请刷新页面重试');
            return false;
        }
        if(order_remarks == ''){
            myalert('请输入备注');
            return false;
        }
        $.post('/order/remarks',{'order_id':order_id,'order_remarks':order_remarks},function (ret) {
            if(ret.code == '200'){
                layer.alert('提交成功', function(index){
                    layer.closeAll();
                    window.location.reload();
                });
            }else{
                myalert(ret.msg);
                return false;
            }
        },'json');
    }

    //回访备注
    function vsremark(order_id,msg){
        var htm = '<form class="layui-form" action="" method="post" style="margin-top: 20px;"><div class="layui-form-item"><label class="layui-form-label">回访备注</label>';
        htm += '<input type="hidden" value="'+order_id+'" name="order_id" >';
        htm += '<div class="layui-input-inline" style="width: 60%;">';
        htm += '<textarea  name="order_remarks" required lay-verify="required" placeholder="请输入" class="layui-textarea">'+msg+'</textarea></div></div>';
        htm += '<div style="text-align: center;margin-top: 40px;"><a class="layui-btn" onclick="tjvsremarks(this)">确定</a>';
        htm += '<a class="layui-btn layui-btn-primary" style="margin-left: 60px;" href="javascript:;" onclick="canre()">取消</a></div></form>';
        layer.open({
            type: 1,
            title: '填写回访备注',
            content: htm,
            area: ['500px', '300px']
        });
        layui.form.render();
    }

    //提交回访备注
    function tjvsremarks(obj) {
        layer.load();
        var formobj = $(obj).parent().parent();
        var order_id = formobj.find('input[name="order_id"]').val();
        var visit_remarks = formobj.find('textarea').val();
        if(order_id == ''){
            myalert('参数错误，请刷新页面重试');
            return false;
        }
        if(visit_remarks == ''){
            myalert('请输入备注');
            return false;
        }
        $.post('/order/vsremarks',{'order_id':order_id,'visit_remarks':visit_remarks},function (ret) {
            if(ret.code == '200'){
                layer.alert('提交成功', function(index){
                    layer.closeAll();
                    window.location.reload();
                });
            }else{
                myalert(ret.msg);
                return false;
            }
        },'json');
    }

    //取消订单
    function cancelorder(order_id) {
        layer.confirm('订单确定要取消吗？', function(index){
            layer.load();
            $.post('/order/cancel',{'order_id':order_id},function (ret) {
                if(ret.code == '200'){
                    layer.alert('操作成功', function(index){
                        layer.closeAll();
                        window.location.reload();
                    });
                }else{
                    myalert(ret.msg);
                    return false;
                }
            },'json');
        });
    }

    //取消派单
    function cancle() {
        layer.closeAll();
    }

    //取消备注
    function canre() {
        layer.closeAll();
    }
</script>