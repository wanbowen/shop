<?php
use yii\helpers\Html;
$this->title = '订单管理';
?>
<span class="layui-breadcrumb">
  <a href="/">首页</a>
  <a><cite><?=$this->title?></cite></a>
</span>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>查看订单</legend>
</fieldset>

<style>
    .layui-form-label {width: 100px;}
    .layui-input-block {margin-left: 130px;}
    .layui-card {margin-left: 130px;width: 50%;}
</style>
<div style="width: 70%">
    <div class="layui-fluid">
        <div class="layui-row" style="line-height: 60px;font-size: 26px;">订单基本信息</div>
        <div class="layui-form-item">
            <label class="layui-form-label">订单号</label>
            <div class="layui-input-block">
                <input type="text" name="title" value="<?=$data['order_code']?>" readonly autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">下单时间</label>
            <div class="layui-input-block">
                <input type="text" name="title" value="<?=$data['created_at']?>" readonly autocomplete="off" class="layui-input">
            </div>
        </div>
        <?php if(!empty($goods)){ foreach($goods as $key=>$val){?>
            <div class="layui-card">
                <div class="layui-card-header">商品<?=$key+1?></div>
                <div class="layui-card-body">
                    <div class="layui-input-inline">商品名称：<?=$val['goods_name']?></div><br>
                    <div class="layui-input-inline">商品价格：￥<?=$val['price']?>/<?=$val['unit']?></div><br>
                    <div class="layui-input-inline">商品数量：<?=$val['amount']?></div>
                </div>
            </div>
        <?php }} ?>
        <div class="layui-form-item">
            <label class="layui-form-label">状态</label>
            <div class="layui-input-block">
                <?php
                    $sta = '';
                    switch ($data['order_status']){
                        case 0:
                            $sta = '待派单';
                            break;
                        case 1:
                            $sta = '已派单';
                            break;
                        case 2:
                            $sta = '订单已修改';
                            break;
                        case 3:
                            $sta = '待付款';
                            break;
                        case 4:
                            $sta = '已付款';
                            break;
                        case 5:
                            $sta = '已评价';
                            break;
                        case 8:
                            $sta = '已取消';
                            break;
                    }
                ?>
                <input type="text" name="title" value="<?=$sta?>" readonly autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">预约时间</label>
            <div class="layui-input-block">
                <input type="text" name="title" value="<?=$data['book_date']?>" readonly autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">上门时间</label>
            <div class="layui-input-block">
                <input type="text" name="title" value="<?=$data['home_date']?>" readonly autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">订单备注</label>
            <div class="layui-input-block">
                <textarea name="" class="layui-textarea" readonly><?=$data['order_remarks']?></textarea>
            </div>
        </div>
    </div>
    <div class="layui-fluid">
        <div class="layui-row" style="line-height: 60px;font-size: 26px;">服务人员基本信息</div>
        <div class="layui-form-item">
            <label class="layui-form-label">服务人员</label>
            <div class="layui-input-block">
                <input type="text" name="title" value="<?=$staff_data['name']?>" readonly autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">服务人员手机号</label>
            <div class="layui-input-block">
                <input type="text" name="title" value="<?=$staff_data['mobile']?>" readonly autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">派单时间</label>
            <div class="layui-input-block">
                <input type="text" name="title" value="<?=$data['reply_at']?>" readonly autocomplete="off" class="layui-input">
            </div>
        </div>
    </div>
    <div class="layui-fluid">
        <div class="layui-row" style="line-height: 60px;font-size: 26px;">客户基本信息</div>
        <div class="layui-form-item">
            <label class="layui-form-label">客户电话</label>
            <div class="layui-input-block">
                <input type="text" name="title" value="<?=$data['mobile']?>" readonly autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">客户地址</label>
            <div class="layui-input-block">
                <input type="text" name="title" value="<?=$data['address']?>" readonly autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">评价</label>
            <div class="layui-input-block">
                <?php for($i=1;$i<=5;$i++){ if($i<=$data['appraisal']){  ?>
                    <img src="/images/red.png" height="30px;" />
                <?php }else{?>
                    <img src="/images/grey.png" height="30px;" />
                <?php }}?>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">备注</label>
            <div class="layui-input-block">
                <textarea name="" class="layui-textarea" readonly><?=$data['remarks']?></textarea>
            </div>
        </div>
    </div>
    <div class="layui-form-item"  style="margin-left: 10%;margin-top: 20px;">
        <div class="layui-input-block">
            <a href="javascript:history.back(-1)" class="layui-btn">返回</a>
        </div>
    </div>
</div>
