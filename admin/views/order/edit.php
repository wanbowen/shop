<?php
use yii\helpers\Html;
$this->title = '订单管理';
?>
<span class="layui-breadcrumb">
  <a href="/">首页</a>
  <a><cite><?=$this->title?></cite></a>
</span>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>修改订单</legend>
</fieldset>

<style>
    .layui-form-label {width: 100px;}
    .layui-input-block {margin-left: 130px;}
    .layui-card {margin-left: 130px;width: 50%;}
</style>
<div style="width: 70%">
    <div class="layui-fluid">
        <div class="layui-row" style="line-height: 60px;font-size: 26px;">订单基本信息</div>
        <div class="layui-form-item">
            <label class="layui-form-label">订单号</label>
            <div class="layui-input-block">
                <input type="text" name="title" value="<?=$data['order_code']?>" readonly autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">下单时间</label>
            <div class="layui-input-block">
                <input type="text" name="title" value="<?=$data['created_at']?>" readonly autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-input-inline" style="position: absolute;right: 15px;">
            <a class="layui-btn" onclick="checkcate()" lay-filter="">新增项目</a>
        </div>
        <?php if(!empty($goods)){ foreach($goods as $key=>$val){?>
            <div class="layui-card">
                <div class="layui-card-header">商品<?=$key+1?></div>
                <div class="layui-card-body">
                    <div class="layui-input-inline">商品名称：<?=$val['goods_name']?></div><br>
                    <div class="layui-input-inline">商品价格：￥<?=$val['price']?>/<?=$val['unit']?></div><br>
                    <div class="layui-input-inline">商品数量：<?=$val['amount']?></div>
                </div>
            </div>
        <?php }} ?>

        <div id="new_card"></div>

        <div class="layui-form-item" style="margin-top: 20px;">
            <label class="layui-form-label">订单总金额</label>
            <div class="layui-input-inline" style="width: 20%">
                <input type="text" name="title" id="order_total" value="<?=$data['order_total']?>" readonly autocomplete="off" class="layui-input">
            </div>
            <div class="layui-form-mid">元</div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">状态</label>
            <div class="layui-input-block">
                <?php
                    $sta = '';
                    switch ($data['order_status']){
                        case 0:
                            $sta = '待派单';
                            break;
                        case 1:
                            $sta = '已派单';
                            break;
                        case 2:
                            $sta = '订单已修改';
                            break;
                        case 3:
                            $sta = '待付款';
                            break;
                        case 4:
                            $sta = '已付款';
                            break;
                        case 5:
                            $sta = '已评价';
                            break;
                        case 8:
                            $sta = '已取消';
                            break;
                    }
                ?>
                <input type="text" name="title" value="<?=$sta?>" readonly autocomplete="off" class="layui-input">
            </div>
        </div>
    </div>
    <div class="layui-fluid">
        <div class="layui-row" style="line-height: 60px;font-size: 26px;">服务人员基本信息</div>
        <div class="layui-form-item">
            <label class="layui-form-label">服务人员</label>
            <div class="layui-input-block">
                <input type="text" name="title" value="<?=$staff_data['name']?>" readonly autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">服务人员手机号</label>
            <div class="layui-input-block">
                <input type="text" name="title" value="<?=$staff_data['mobile']?>" readonly autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">派单时间</label>
            <div class="layui-input-block">
                <input type="text" name="title" value="<?=$data['reply_at']?>" readonly autocomplete="off" class="layui-input">
            </div>
        </div>
    </div>
    <div class="layui-fluid">
        <div class="layui-row" style="line-height: 60px;font-size: 26px;">客户基本信息</div>
        <div class="layui-form-item">
            <label class="layui-form-label">客户电话</label>
            <div class="layui-input-block">
                <input type="text" name="title" value="<?=$data['mobile']?>" readonly autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">客户地址</label>
            <div class="layui-input-block">
                <input type="text" name="title" value="<?=$data['address']?>" readonly autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">评价</label>
            <div class="layui-input-block">
                <?php for($i=1;$i<=5;$i++){ if($i<=$data['appraisal']){  ?>
                    <img src="/images/red.png" height="30px;" />
                <?php }else{?>
                    <img src="/images/grey.png" height="30px;" />
                <?php }}?>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">备注</label>
            <div class="layui-input-block">
                <textarea name="" class="layui-textarea" readonly><?=$data['remarks']?></textarea>
            </div>
        </div>
    </div>
    <div class="layui-form-item"  style="margin-left: 10%;margin-top: 20px;">
        <div class="layui-input-block">
            <a href="javascript:;" class="layui-btn" onclick="sub()">提交</a>
            <a href="javascript:history.back(-1)" class="layui-btn">返回</a>
        </div>
    </div>
</div>

<div id="add_goods" style="display: none">
    <div class="layui-card">
        <div class="layui-card-header">新商品</div>
        <div class="layui-card-body">
            <input type="hidden" class="goods_id" name="goods_id">
            <div class="layui-input-inline">商品名称：<span class="goods_name"></span></div><br>
            <div class="layui-input-inline">商品价格：￥<span class="goods_price"></span>/<span class="goods_unit"></span></div><br>
            <div class="layui-input-inline">商品数量：<span class="goods_amount"></span></div>
            <a class="layui-btn" onclick="delgoods(this)" style="float: right;" lay-filter="">删除</a>
        </div>
    </div>
</div>

<script type="text/javascript" src="/js/math.js"></script>
<script>
    var order_id = "<?=$data['id']?>";
    layui.use(['form', 'element', 'laydate', 'layer'], function(){
        var form = layui.form
            ,layer = layui.layer
            ,laydate = layui.laydate
            ,element = layui.element;

        form.on('select(cate)', function(data){
            var this_cate_id = data.value;
            var new_cate_data = getCate(this_cate_id);
            var nhtm = '<option value="0">请选择</option>';
            if(new_cate_data.length > 0) {
                $.each(new_cate_data, function (m, n) {
                    nhtm += '<option value="' + n.id + '">' + n.name + '</option>';
                });
            }

            $("#cate_two").html(nhtm);
            form.render('select', 'staf');
        });

        form.on('select(cate_two)', function(data){
            var this_cate_id = data.value;
            var goods_data = getGoods(this_cate_id);
            var nhtm = '<option value="0">请选择</option>';
            if(goods_data.length > 0) {
                $.each(goods_data, function (m, n) {
                    nhtm += '<option value="' + n.id + '" price="'+n.price+'" unit="'+n.unit+'">' + n.name + '</option>';
                });
            }

            $("#goods").html(nhtm);
            form.render('select', 'goods');
        });

        form.on('select(goods)', function(data){
            var gdom = data.elem;
            var goods_id = data.value;
            var name = $(gdom).find("option:selected").text();
            var price = $(gdom).find("option:selected").attr('price');
            var unit = $(gdom).find("option:selected").attr('unit');
            $("#add_goods").find(".goods_id").val(goods_id);
            $("#add_goods").find(".goods_name").text(name);
            $("#add_goods").find(".goods_price").text(price);
            $("#add_goods").find(".goods_unit").text(unit);
        });
    });

    function checkcate() {
        layer.load();
        var cate_data = getCate(0);
        if(cate_data == ''){
            myalert('获取分类失败');return false;
        }
        //一级分类
        var htm = '<form class="layui-form" action="" method="post"><div class="layui-form-item" style="padding: 15px 30px 0px 0px;"><label class="layui-form-label">选择分类</label>';
        htm += '<div class="layui-input-block"><select name="cate_id" lay-filter="cate">';
        var cate_id = 0;
        $.each(cate_data, function (x,y) {
            if(x == 0){
                cate_id = y.id;
            }
            htm += '<option value="'+y.id+'">'+y.name+'</option>';
        });
        htm += '</select></div></div>';
        //二级分类
        var cate_data_2 = getCate(cate_id);
        htm += '<div class="layui-form-item" style="padding: 5px 30px 0px 0px;"><label class="layui-form-label"></label>';
        htm += '<div class="layui-input-block layui-form" lay-filter="staf"><select name="staff_id" lay-filter="cate_two" id="cate_two">';
        htm += '<option value="0">请选择</option>';
        if(cate_data_2 != '') {
            $.each(cate_data_2, function (m, n) {
                htm += '<option value="' + n.id + '">' + n.name + '</option>';
            });
        }
        htm += '</select></div></div>';
        //商品
        htm += '<div class="layui-form-item" style="padding: 5px 30px 0px 0px;"><label class="layui-form-label">选择商品</label>';
        htm += '<div class="layui-input-block layui-form" lay-filter="goods"><select name="goods_id" lay-filter="goods" id="goods">';
        htm += '<option value="0"></option>';
        htm += '</select></div></div>';
        //商品数量
        htm += '<div class="layui-form-item" style="padding: 5px 30px 0px 0px;"><label class="layui-form-label">商品数量</label>';
        htm += '<div class="layui-input-block layui-form" lay-filter="">';
        htm += '<input type="text" name="amount" id="amount" required lay-verify="required" placeholder="请输入商品数量" autocomplete="off" class="layui-input">';
        htm += '</div></div></form>';
        htm += '<div style="text-align: center;margin-top: 40px;"><button class="layui-btn" onclick="sure()">确定</button>';
        htm += '<a class="layui-btn layui-btn-primary" style="margin-left: 60px;" href="javascript:;" onclick="cancle()">取消</a></div>';

        layer.open({
            type: 1,
            title: '选择项目',
            content: htm,
            area: ['500px', '500px']
        });
        layui.form.render();
        layer.closeAll('loading');
    }
    
    //项目添加
    function sure() {
        var goods_id = $("#add_goods").find(".goods_id").val();
        if(goods_id == ''){
            myalert('请选择商品');return false;
        }
        var goods_price = $("#add_goods").find(".goods_price").text();
        var amount = $("#amount").val();
        if(amount == ''){
            myalert('请输入数量');return false;
        }
        $("#add_goods").find(".goods_amount").text(amount);
        //总金额修改
        var old = $("#order_total").val();
        var now = floatMul(goods_price, amount);
        var total = floatAdd(old, now);
        $("#order_total").val(total);

        $("#new_card").append($("#add_goods").html());
        layer.closeAll();
    }
    
    //删除
    function delgoods(obj) {
        var cardobj = $(obj).parent().parent();
        var goods_price = cardobj.find(".goods_price").text();
        var amount = cardobj.find(".goods_amount").text();
        //总金额修改
        var old = $("#order_total").val();
        var now = floatMul(goods_price, amount);
        var total = floatSub(old, now);
        $("#order_total").val(total);
        $(obj).parent().parent().remove();
    }

    //获取分类
    function getCate(pid) {
        var data = '';
        $.ajaxSettings.async = false;
        $.get('/order/get-category',{'pid':pid},function (ret) {
            if(ret.code == '200'){
                data = ret.data;
            }
        },'json');
        return data;
    }
    //获取商品
    function getGoods(cate_id) {
        var data = '';
        $.ajaxSettings.async = false;
        $.get('/order/get-goods',{'cate_id':cate_id},function (ret) {
            if(ret.code == '200'){
                data = ret.data;
            }
        },'json');
        return data;
    }
    //取消
    function cancle() {
        layer.closeAll();
    }
    
    //提交数据
    function sub() {
        var domdata = $("#new_card").html();
        var ids = '';
        var amounts = '';
        if(domdata != ''){
            $.each($("#new_card").find(".layui-card"),function (x,y) {
                var id = $(y).find('.goods_id').val();
                ids += id + ',';
                var amount = $(y).find('.goods_amount').text();
                amounts += amount + ',';
            });
        }

        layer.load();
        $.post('/order/edit-order',{'order_id':order_id,'ids':ids,'amounts':amounts},function (ret) {
            layer.closeAll('loading');
            if(ret.code == '200'){
                reloadalert(ret.msg);
            }else{
                myalert(ret.msg);
            }
        },'json');
    }
</script>
