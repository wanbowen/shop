<?php
use yii\helpers\Html;
$this->title = '操作日志';
?>
<span class="layui-breadcrumb">
  <a href="/">首页</a>
  <a><cite><?=$this->title?></cite></a>
</span>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>操作日志</legend>
</fieldset>

<form class="layui-form" id="search_form" action="/adminlog/index" method="get">
    <div class="layui-form-item">
        <input type="hidden" name="limit" value="<?=$limit?>">
        <label class="layui-form-label" style="width: 50px;" >用户名</label>
        <div class="layui-input-inline" style="width: 150px;">
            <input type="text" name="username" value="<?= !empty($getForm['username'])?$getForm['username']:'';?>" lay-verify="title" autocomplete="off" placeholder="请输入名称" class="layui-input">
        </div>
        <label class="layui-form-label" style="width: 50px;" >IP地址</label>
        <div class="layui-input-inline" style="width: 150px;">
            <input type="text" name="client_ip" value="<?= !empty($getForm['client_ip'])?$getForm['client_ip']:'';?>" lay-verify="title" autocomplete="off" placeholder="请输入IP" class="layui-input">
        </div>
        <label class="layui-form-label" style="width: 70px;">操作时间:</label>
        <div class="layui-input-inline" style="width: 200px;">
            <input type="text" name="create_time" id="created_at" value="<?= !empty($getForm['create_time'])?$getForm['create_time']:'' ?>" class="layui-input" placeholder="起始时间">
        </div>
        <div class="layui-input-inline" style="width: 200px;">
            <input type="text" name="end_time" id="end_at" value="<?= !empty($getForm['end_time'])?$getForm['end_time']:'' ?>" class="layui-input" placeholder="结束时间">
        </div>
        <div class="layui-input-inline" style="width:50px;">
            <button class="layui-btn" lay-submit="" lay-filter="demo1">筛选</button>
        </div>
    </div>
</form>

<table class="layui-table">
    <thead>
    <tr>
        <th>序号</th>
        <th>用户名</th>
        <th>IP地址</th>
        <th width="500px">操作说明</th>
        <th>操作时间</th>
    </tr>
    </thead>
    <tbody>
    <?php if(isset($datas) && !empty($datas)){ ?>
        <?php foreach($datas as $key=>$value){?>
            <tr>
                <td><?=$value['id']?></td>
                <td><?=$value['username']?></td>
                <td><?=$value['client_ip']?></td>
                <td><?=$value['record']?></td>
                <td><?=$value['created_at']?></td>
            </tr>
        <?php }}else{ ?>
        <tr>
            <td colspan='5' style="text-align: center">暂无数据</td>
        </tr>

    <?php }?>

    </tbody>
</table>
<div id="demo3"></div>

<script>
    layui.use(['form', 'layedit', 'laydate','laypage'], function(){
        var form = layui.form
            ,layer = layui.layer
            ,layedit = layui.layedit
            ,laydate = layui.laydate
            ,laypage = layui.laypage;
        laypage.render({
            elem: 'demo3'
            ,count: <?=$count?>
            ,layout:['count','prev', 'page', 'next','limit','skip']
            ,curr:<?=$page?>
            ,limit:<?=$limit?>
            ,jump: function(obj, first){
                var page=obj.curr; //得到当前页，以便向服务端请求对应页的数据。
                var limit=obj.limit; //得到每页显示的条数
                var js_str = 'page='+page+'&limit='+limit;
                var form_str = $("#search_form").serialize();
                var form_arr = form_str.split('&');
                form_arr.splice(0,1);
                form_str = form_arr.join("&");
                form_str = js_str+'&'+form_str;
                //首次不执行
                if(!first){
                    window.location.href = "/adminlog/index?"+form_str;
                }
            }
        });

        laydate.render({
            elem: '#created_at' //指定元素
        });
        laydate.render({
            elem: '#end_at' //指定元素
        });
    });
</script>