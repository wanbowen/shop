<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = '登陆';
?>
<div class="row" style="margin-top: 100px;">
        <div class="col-md-4 col-md-offset-4">
            <div class="login-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">登录</h3>
                </div>
                <div class="panel-body">
                    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                    <fieldset>
                        <?= $form->field($model, 'username')->textInput(['value' => 'admin']) ?>
                        <?= $form->field($model, 'password')->passwordInput(['value' => '&admin123']) ?>
						<?= $form->field($model, 'verify') ?>
						 <img class="admin-captcha" src="/site/verify" id="code" onclick="updateVerify()">
                        <?= $form->field($model, 'rememberMe')->checkbox() ?>
                        <div class="form-group">
                            <?= Html::submitButton('登录', ['class' => 'btn btn-primary']) ?>
                        </div>
                    </fieldset>
                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
</div>
<script>
function updateVerify() {
  document.getElementById('code').src = "/site/verify?"+Math.random();
}
</script>