<?php
use common\models\CxcRegion;
use common\models\CxcDepartment;
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <title>修改信息</title>
  <meta name="renderer" content="webkit">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <link rel="stylesheet" href="/layui/css/layui.css">
   <script src='/layui/layui.js'></script>
   <script type="text/javascript" src="/tree/js/jquery-1.4.4.min.js"></script>
  <!-- 注意：如果你直接复制所有代码到本地，上述css路径需要改成你本地的 -->
</head>
<body>
<div class="layui-row" style="margin-top:10px;margin-right:10px" >    
<form class="layui-form" action="#" method='post'>
<input type="hidden" value="<?php echo Yii::$app->request->csrfToken; ?>" name="_csrf" >
  <input type="hidden" name='id' value="<?=$admin->id?>">
  <div class="layui-form-item">
    <label class="layui-form-label">用户名</label>
    <div class="layui-input-inline">
      <input type="text" value="<?=$admin->username?>" disabled name="username" required  lay-verify="required|username" placeholder="请输入用户名" autocomplete="off" class="layui-input">
    </div>
  </div>
 <div class="layui-form-item">
    <label class="layui-form-label">原密码</label>
    <div class="layui-input-inline">
      <input type="password" name="yuanpassword"  placeholder="请输入原密码" lay-verify="required|pass" autocomplete="off" class="layui-input">
    </div>
  </div>
  <div class="layui-form-item">
    <label class="layui-form-label">密码</label>
    <div class="layui-input-inline">
      <input type="password" name="password"  placeholder="请输入密码" lay-verify="required|pass" autocomplete="off" class="layui-input">
    </div>
  </div>
	<div class="layui-form-item" style="margin-right:10px">
    <div class="layui-input-block">
      <button class="layui-btn" lay-submit="" lay-filter="demo1">立即提交</button>
    </div>
  </div>
</form>
</div>
<script>
    //Demo
    layui.use(['form', 'layedit', 'laydate','laypage'], function(){
        var form = layui.form
            ,layer = layui.layer
            ,layedit = layui.layedit
            ,laydate = layui.laydate
            ,laypage = layui.laypage;
		//监听提交
		form.verify({
		  username: function(value, item){ //value：表单的值、item：表单的DOM对象
			if(!new RegExp("^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$").test(value)){
			  return '用户名不能有特殊字符';
			}
			if(/(^\_)|(\__)|(\_+$)/.test(value)){
			  return '用户名首尾不能出现下划线\'_\'';
			}
			if(/^\d+\d+\d$/.test(value)){
			  return '用户名不能全为数字';
			}
		  }
		  
		  //我们既支持上述函数式的方式，也支持下述数组的形式
		  //数组的两个值分别代表：[正则匹配、匹配不符时的提示文字]
		  ,pass: [
			/^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[^a-zA-Z0-9]).{8,30}$/
			,'密码至少为8位的字母、数字和特殊符号的组合'
		  ] 
		});
		form.on('submit(demo1)', function(data){
		$.post('/site/personalsetpost/'+data.field.id,data.field,function(data){
			if(data == '')
			{
				layui.use('layer', function(){
				var $ = layui.jquery, layer = layui.layer; 
				layer.ready(function(){
					layer.alert(data.msg,{
					  title: '提示信息'
					  ,skin: 'layui-layer-molv'
					  ,btnAlign: 'c'
					  ,closeBtn: 0
					  ,anim: 4 //动画类型
					}, function(){
					   layer.close(layer.index);
						var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
						parent.layer.close(index); //再执行关闭 
					});
					});
				});
				return false;
			}
			else if (data.ret == 200)
			{
				layui.use('layer', function(){
				var $ = layui.jquery, layer = layui.layer; 
				layer.ready(function(){
					layer.alert(data.msg,{
					  title: '提示信息'
					  ,skin: 'layui-layer-molv'
					  ,btnAlign: 'c'
					  ,closeBtn: 0
					  ,anim: 4 //动画类型
					}, function(){
						layer.close(layer.index);
						window.parent.location.href='/site/logout';
						/* var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
						parent.layer.close(index); //再执行关闭  */
					});
					});
				});
				
			}
			else
			{
				layui.use('layer', function(){
				var $ = layui.jquery, layer = layui.layer; 
				layer.ready(function(){
					layer.alert(data.msg,{
					  title: '提示信息'
					  ,skin: 'layui-layer-molv'
					  ,btnAlign: 'c'
					  ,closeBtn: 0
					  ,anim: 4 //动画类型
					}, function(){
					   layer.close(layer.index);
						//var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
						//parent.layer.close(index); //再执行关闭 
					});
					});
				});
				return false;
			}
		},'json');
		return false;
		});
		});
</script>

</body>
</html>