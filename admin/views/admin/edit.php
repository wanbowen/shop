<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = '修改用户';
$this->params['breadcrumbs'][] = $this->title;
?>
<span class="layui-breadcrumb">
  <a href="/">首页</a>
  <a href="/admin/">用户列表</a>
  <a><cite><?=$this->title?></cite></a>
</span>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
  <legend>修改用户</legend>
</fieldset>
<form class="layui-form" action="/admin/editpost/<?=$admin->id?>" method='post'>
  <div class="layui-form-item">
    <label class="layui-form-label">用户名</label>
    <div class="layui-input-inline">
      <input type="text" value="<?=$admin->username?>" name="username" required  lay-verify="required|username" placeholder="请输入用户名" autocomplete="off" class="layui-input">
    </div>
  </div>
  <div class="layui-form-item">
    <label class="layui-form-label">密码</label>
    <div class="layui-input-inline">
      <input type="text" name="password" value="<?=$admin->mpassword?>" lay-verify="required|pass" placeholder="请输入密码" autocomplete="off" class="layui-input">
    </div>
  </div>
  <div class="layui-form-item">
    <label class="layui-form-label">状态</label>
    <div class="layui-input-inline">
      <select name="status" lay-verify="required">
        <option value="0" <?=$admin->status=='0' ? 'selected' : ''?>>已激活</option>
		<option value="1" <?=$admin->status=='1' ? 'selected' : ''?>>未激活</option>
      </select>
    </div>
  </div>
  <div class="layui-form-item">
    <label class="layui-form-label">选择角色</label>
    <div class="layui-input-inline">
      <select name="roleid" lay-verify="required">
        <option value=""></option>
		<?php if(!empty($role)&&isset($role)){ foreach($role as $key=>$value){ ?>
        <option value="<?=$value['roleid']?>" <?php if($admin->roleid==$value['roleid']){?> selected <?php }?>><?=$value['rolename']?></option>
		<?php }} ?>
      </select>
    </div>
  </div>
<!-- <div class="layui-form-item">-->
<!--      <label class="layui-form-label">验证邮箱</label>-->
<!--      <div class="layui-input-inline">-->
<!--        <input type="text" name="email" value="--><!--" lay-verify="email" autocomplete="off" class="layui-input">-->
<!--      </div>-->
<!--    </div>-->
<!---->
<!--  <div class="layui-form-item">-->
<!--    <label class="layui-form-label">真实姓名</label>-->
<!--    <div class="layui-input-inline">-->
<!--	   <input type="text" name="realname" value="--><!--" required  lay-verify="required" placeholder="请输入真实姓名" autocomplete="off" class="layui-input">-->
<!--    </div>-->
<!--  </div>-->
  <div class="layui-form-item">
    <div class="layui-input-block">
      <button class="layui-btn" lay-submit lay-filter="formDemo">修改</button>
    </div>
  </div>
</form>
<script>
//Demo
layui.use('form', function(){
  var form = layui.form;
  form.verify({
  username: function(value, item){ //value：表单的值、item：表单的DOM对象
    if(!new RegExp("^[a-zA-Z0-9_\u4e00-\u9fa5\\s·]+$").test(value)){
      return '用户名不能有特殊字符';
    }
    if(/(^\_)|(\__)|(\_+$)/.test(value)){
      return '用户名首尾不能出现下划线\'_\'';
    }
    if(/^\d+\d+\d$/.test(value)){
      return '用户名不能全为数字';
    }
  }

  //我们既支持上述函数式的方式，也支持下述数组的形式
  //数组的两个值分别代表：[正则匹配、匹配不符时的提示文字]
  ,pass: [
    /^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[^a-zA-Z0-9]).{8,30}$/
    ,'密码至少为8位的字母、数字和特殊符号的组合'
  ]
});
});
</script>
