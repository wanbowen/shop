<?php

use yii\helpers\Html;

$this->title = '工作时间设置';
?>
<span class="layui-breadcrumb">
  <a href="/">首页</a>
  <a><cite><?=$this->title?></cite></a>
</span>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>工作时间设置</legend>
</fieldset>

<style>
    .layui-form-label{width: 110px;}
1</style>

<form class="layui-form" action="/setting/workhours" method='post'>
    <input type="hidden" value="<?php echo Yii::$app->request->csrfToken; ?>" name="_csrf" >
    <div class="layui-form-item">
        <label class="layui-form-label">开始时间</label>
        <div class="layui-input-inline" style="width: 20%">
            <input type="text" name="start" id="strat_time" value="<?= $hours_data['start'] ?>" required  lay-verify="required" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">结束时间</label>
        <div class="layui-input-inline" style="width: 20%">
            <input type="text" name="end" id="end_time" value="<?= $hours_data['end'] ?>" required  lay-verify="required" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label"></label>
        <div class="layui-input-inline" style="width: 20%">
            注：本时间设置为24小时制
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block" style="margin-left: 150px;">
            <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
        </div>
    </div>
</form>

<script>
    //Demo
    layui.use(['form', 'layedit', 'laydate'], function(){
        var form = layui.form
            ,layer = layui.layer
            ,layedit = layui.layedit
            ,laydate = layui.laydate;

        laydate.render({
            elem: '#strat_time' //指定元素
            ,type: 'time'
            ,format: 'HH:mm'
        });
        laydate.render({
            elem: '#end_time' //指定元素
            ,type: 'time'
            ,format: 'HH:mm'
        });
    });
</script>


