<?php

use yii\helpers\Html;

$this->title = '预约金管理';
?>
<span class="layui-breadcrumb">
  <a href="/">首页</a>
  <a><cite><?=$this->title?></cite></a>
</span>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>预约金管理</legend>
</fieldset>

<style>
    .layui-form-label{width: 110px;}
1</style>

<form class="layui-form" action="/setting/money" method='post'>
    <input type="hidden" value="<?php echo Yii::$app->request->csrfToken; ?>" name="_csrf" >
    <div class="layui-form-item">
        <label class="layui-form-label">金额</label>
        <div class="layui-input-inline" style="width: 20%">
            <input type="text" name="money" value="<?= $data['money'] ?>"  lay-verify="required|number" placeholder="请输入金额" class="layui-input">
        </div>
        <div class="layui-form-mid">元</div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block" style="margin-left: 150px;">
            <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
        </div>
    </div>
</form>

<script>
    //Demo
    layui.use(['form', 'layedit', 'laydate'], function(){
        var form = layui.form
            ,layer = layui.layer
            ,layedit = layui.layedit
            ,laydate = layui.laydate;
    });
</script>


