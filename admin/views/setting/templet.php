<?php

use yii\helpers\Html;

$this->title = '短信模板设置';
?>
<span class="layui-breadcrumb">
  <a href="/">首页</a>
  <a><cite><?=$this->title?></cite></a>
</span>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>短信模板设置</legend>
</fieldset>

<style>
    .layui-form-label{width: 110px;}
    .ycang{display: none}
    .layui-textarea{height: 150px;}
1</style>

<div class="layui-tab layui-tab-brief" lay-filter="docDemoTabBrief">
    <ul class="layui-tab-title">
        <li class="layui-this">验证码模板</li>
        <li>员工通知模板</li>
        <li>客户通知模板</li>
        <li>报事报修员工通知模板</li>
        <li>报事报修客户通知模板</li>
        <li>工作时间外短信通知</li>
    </ul>
    <div class="layui-tab-content"></div>
</div>

<form id="tab-one" class="layui-form" action="/setting/smstemplet" method='post'>
    <input type="hidden" value="<?php echo Yii::$app->request->csrfToken; ?>" name="_csrf" >
    <input type="hidden" value="1" name="type">
    <div class="layui-form-item">
        <label class="layui-form-label">短信内容</label>
        <div class="layui-input-inline" style="width: 50%">
            <textarea name="desc" placeholder="请输入内容" class="layui-textarea"><?=$tem_one_data['content']?></textarea>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">示例：</label>
        <div class="layui-input-inline" style="width: 50%">
            【金安网络科技有限公司】验证码：321456，您正在使用短信验证码登录功能，该验证码仅用于身份验证，请勿泄露给他人使用。
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block" style="margin-left: 150px;">
            <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
        </div>
    </div>
</form>

<form id="tab-two" class="layui-form ycang" action="/setting/smstemplet" method='post'>
    <input type="hidden" value="<?php echo Yii::$app->request->csrfToken; ?>" name="_csrf" >
    <input type="hidden" value="2" name="type">
    <div class="layui-form-item">
        <label class="layui-form-label">短信内容</label>
        <div class="layui-input-inline" style="width: 50%">
            <textarea name="desc" placeholder="请输入内容" class="layui-textarea"><?=$tem_two_data['content']?></textarea>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">示例：</label>
        <div class="layui-input-inline" style="width: 50%">
            【金安网络科技有限公司】员工张三您好，请于2019年8月12日14:30至15:00期间到达“中街138号1403”进行上门服务， 服务内容“擦玻璃”，“修水龙头”,请您带好服务所需设备按时到达服务地点。 客户备注：“xxxxxx”
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block" style="margin-left: 150px;">
            <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
        </div>
    </div>
</form>

<form id="tab-three" class="layui-form ycang" action="/setting/smstemplet" method='post'>
    <input type="hidden" value="<?php echo Yii::$app->request->csrfToken; ?>" name="_csrf" >
    <input type="hidden" value="3" name="type">
    <div class="layui-form-item">
        <label class="layui-form-label">短信内容</label>
        <div class="layui-input-inline" style="width: 50%">
            <textarea name="desc" placeholder="请输入内容" class="layui-textarea"><?=$tem_three_data['content']?></textarea>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">示例：</label>
        <div class="layui-input-inline" style="width: 50%">
            【金安网络科技有限公司】尊敬的AAA您好，您的订单XXXXXXXXXXXXX已被受理，服务人员张三将于2019年7月12日14:30至15:30期间进行上门服务。员工电话：xxxxxxxxxxx 客服电话：024-xxxxxxxx
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block" style="margin-left: 150px;">
            <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
        </div>
    </div>
</form>

<form id="tab-four" class="layui-form ycang" action="/setting/smstemplet" method='post'>
    <input type="hidden" value="<?php echo Yii::$app->request->csrfToken; ?>" name="_csrf" >
    <input type="hidden" value="4" name="type">
    <div class="layui-form-item">
        <label class="layui-form-label">短信内容</label>
        <div class="layui-input-inline" style="width: 50%">
            <textarea name="desc" placeholder="请输入内容" class="layui-textarea"><?=$tem_four_data['content']?></textarea>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">示例：</label>
        <div class="layui-input-inline" style="width: 50%">
            【辽宁金安物业有限公司】员工张三您好，请到达“地点”进行问题查看，问题描述“水管坏了”，请您带好所需设备到达地点。
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block" style="margin-left: 150px;">
            <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
        </div>
    </div>
</form>

<form id="tab-five" class="layui-form ycang" action="/setting/smstemplet" method='post'>
    <input type="hidden" value="<?php echo Yii::$app->request->csrfToken; ?>" name="_csrf" >
    <input type="hidden" value="5" name="type">
    <div class="layui-form-item">
        <label class="layui-form-label">短信内容</label>
        <div class="layui-input-inline" style="width: 50%">
            <textarea name="desc" placeholder="请输入内容" class="layui-textarea"><?=$tem_five_data['content']?></textarea>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">示例：</label>
        <div class="layui-input-inline" style="width: 50%">
            【辽宁金安物业有限公司】尊敬的业主您好，您反馈的问题已经得到受理，我们会尽快派出服务人员张三为您进行服务。
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block" style="margin-left: 150px;">
            <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
        </div>
    </div>
</form>

<form id="tab-six" class="layui-form ycang" action="/setting/smstemplet" method='post'>
    <input type="hidden" value="<?php echo Yii::$app->request->csrfToken; ?>" name="_csrf" >
    <input type="hidden" value="6" name="type">
    <div class="layui-form-item">
        <label class="layui-form-label">短信内容</label>
        <div class="layui-input-inline" style="width: 50%">
            <textarea name="desc" placeholder="请输入内容" class="layui-textarea"><?=$tem_six_data['content']?></textarea>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">示例：</label>
        <div class="layui-input-inline" style="width: 50%">
            【金安网络科技有限公司】尊敬的用户您好，我们的工作时间为8：30至16：30，您的订单我们会在工作时间为您尽快处理，为您带来的不便请您谅解。
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block" style="margin-left: 150px;">
            <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
        </div>
    </div>
</form>

<script>
    //Demo
    layui.use(['form', 'element'], function(){
        var form = layui.form;
        var element = layui.element;

        element.on('tab(docDemoTabBrief)', function(data){
            var index = data.index;
            if(index == 0){
                $("#tab-one").show();
                $("#tab-two").hide();
                $("#tab-three").hide();
                $("#tab-four").hide();
                $("#tab-five").hide();
                $("#tab-six").hide();
            }
            if(index == 1){
                $("#tab-one").hide();
                $("#tab-two").show();
                $("#tab-three").hide();
                $("#tab-four").hide();
                $("#tab-five").hide();
                $("#tab-six").hide();
            }
            if(index == 2){
                $("#tab-one").hide();
                $("#tab-two").hide();
                $("#tab-three").show();
                $("#tab-four").hide();
                $("#tab-five").hide();
                $("#tab-six").hide();
            }
            if(index == 3){
                $("#tab-one").hide();
                $("#tab-two").hide();
                $("#tab-three").hide();
                $("#tab-four").show();
                $("#tab-five").hide();
                $("#tab-six").hide();
            }
            if(index == 4){
                $("#tab-one").hide();
                $("#tab-two").hide();
                $("#tab-three").hide();
                $("#tab-four").hide();
                $("#tab-five").show();
                $("#tab-six").hide();
            }
            if(index == 5){
                $("#tab-one").hide();
                $("#tab-two").hide();
                $("#tab-three").hide();
                $("#tab-four").hide();
                $("#tab-five").hide();
                $("#tab-six").show();
            }
        });
    });
</script>


