<?php

use yii\helpers\Html;

$this->title = '预约标签管理';
?>
<span class="layui-breadcrumb">
  <a href="/">首页</a>
  <a><cite><?=$this->title?></cite></a>
</span>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>添加标签</legend>
</fieldset>

<form class="layui-form" action="/setting/save?id=<?=$data->id?>" method='post'>
    <input type="hidden" value="<?php echo Yii::$app->request->csrfToken; ?>" name="_csrf" >
    <div class="layui-form-item">
        <label class="layui-form-label">标签名称</label>
        <div class="layui-input-inline" style="width: 50%">
            <input type="text" name="name" value="<?=$data['name']?>" required  lay-verify="required" placeholder="请输入名称" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>

<script>
    //Demo
    layui.use(['form', 'layedit', 'laydate'], function(){
        var form = layui.form
            ,layer = layui.layer
            ,layedit = layui.layedit
            ,laydate = layui.laydate;
        //日期
        laydate.render({
            elem: '#date',
            type: 'date'
        });
    });
</script>


