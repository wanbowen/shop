<?php

use yii\helpers\Html;

$this->title = '预约时间管理';
?>
<span class="layui-breadcrumb">
  <a href="/">首页</a>
  <a><cite><?=$this->title?></cite></a>
</span>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>预约时间管理</legend>
</fieldset>

<style>
    .layui-form-label{width: 110px;}
1</style>

<form class="layui-form" action="/setting/book" method='post'>
    <input type="hidden" value="<?php echo Yii::$app->request->csrfToken; ?>" name="_csrf" >
    <div class="layui-form-item">
        <label class="layui-form-label">预约服务开关</label>
        <div class="layui-input-inline" style="width: 50%">
            <input type="checkbox" name="switch" lay-skin="switch" value="0" lay-text="开启|关闭" <?= $data['switch']=='0' ? 'checked' : ''?>>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">开始接单时间</label>
        <div class="layui-input-inline" style="width: 24%">
            <input type="text" name="start_at" id="created_at" value="<?= $data['start_at'] ?>" required  lay-verify="required" placeholder="开始时间" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">结束接单时间</label>
        <div class="layui-input-inline" style="width: 24%;">
            <input type="text" name="end_at" id="end_at" value="<?= $data['end_at'] ?>" required  lay-verify="required" placeholder="结束时间" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">接单天数</label>
        <div class="layui-input-inline" style="width: 100px">
            <input type="number" name="days" value="<?= $data['days'] ?>" autocomplete="off" class="layui-input">
        </div>
        <div class="layui-form-mid">天可以接单（包含当天）</div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>

<script>
    //Demo
    layui.use(['form', 'layedit', 'laydate'], function(){
        var form = layui.form
            ,layer = layui.layer
            ,layedit = layui.layedit
            ,laydate = layui.laydate;
        //日期
        laydate.render({
            elem: '#created_at' //指定元素
            ,type: 'time'
            ,format: 'HH:mm:ss'
        });
        laydate.render({
            elem: '#end_at' //指定元素
            ,type: 'time'
            ,format: 'HH:mm:ss'
        });
        form.on('radio(rest)', function(data){
            if(data.value == 1){
                $("#rest").show();
            }else{
                $("#rest").hide();
            }
        });
        form.on('radio(limit)', function(data){
            if(data.value == 1){
                $("#limit").show();
            }else{
                $("#limit").hide();
            }
        });
    });
</script>


