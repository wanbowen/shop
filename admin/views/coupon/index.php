<?php
use yii\helpers\Html;
$this->title = '优惠券管理';
?>
<span class="layui-breadcrumb">
  <a href="/">首页</a>
  <a><cite><?=$this->title?></cite></a>
</span>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>优惠券管理</legend>
</fieldset>

<form class="layui-form" id="search_form" action="/coupon/index" method="get">
    <div class="layui-form-item">
        <input type="hidden" name="limit" value="<?=$limit?>">
        <label class="layui-form-label" style="width: 50px;" >名称</label>
        <div class="layui-input-inline" style="width: 150px;">
            <input type="text" name="name" value="<?= !empty($getForm['name'])?$getForm['name']:'';?>" lay-verify="title" autocomplete="off" placeholder="请输入名称" class="layui-input">
        </div>
        <label class="layui-form-label" style="width: 30px;">状态</label>
        <div class="layui-input-inline" style="width: 150px;">
            <select name="status">
                <option value="-1">请选择状态</option>
                <option value="0" <?= isset($getForm['status']) && $getForm['status']=='0' ? 'selected' : ''?>>上架中</option>
                <option value="1" <?= isset($getForm['status']) && $getForm['status']=='1' ? 'selected' : ''?>>已下架</option>
            </select>
        </div>
        <div class="layui-input-inline" style="width:50px;">
            <button class="layui-btn" lay-submit="" lay-filter="demo1">筛选</button>
        </div>
    </div>
</form>

<a href="/coupon/add" class="layui-btn layui-btn-sm"><i class="layui-icon"></i>新增</a>

<a href="javascript:;" onclick="swit(<?=$switch?>)" class="layui-btn layui-btn-sm"><?=$switch==0 ? '关闭':'开启'?>优惠券</a>

<a href="javascript:;" onclick="share()" class="layui-btn layui-btn-sm">分享给优惠券选择</a>

<table class="layui-table">
    <thead>
    <tr>
        <th>序号</th>
        <th>优惠券名称</th>
        <th>优惠券金额</th>
        <th>有效期</th>
        <th>状态</th>
        <th>创建时间</th>
        <th>操作</th>
    </tr>
    </thead>
    <tbody>
    <?php if(isset($datas) && !empty($datas)){ ?>
        <?php foreach($datas as $key=>$value){?>
            <tr>
                <td><?=$value['id']?></td>
                <td><?=$value['name']?></td>
                <td>¥<?=$value['money']?></td>
                <td><?=substr($value['start_at'],0,10)?>---<?=substr($value['end_at'],0,10)?></td>
                <td><?=$value['status']==0 ? '上架中':'已下架'?></td>
                <td><?=$value['created_at']?></td>
                <td>
                    <a  class="layui-btn layui-btn-danger layui-btn-xs" id="<?=$value['id']?>" onclick="disable(this.id,<?=$value['status']?>)" ><?=$value['status']==0 ? '下架':'上架'?></a>
                    <a class="layui-btn layui-btn-xs" href="/coupon/edit?id=<?=$value['id']?>" >编辑</a>
                    <a class="layui-btn layui-btn-danger layui-btn-xs" id="<?=$value['id']?>" onclick="Deleteone(this.id)" >删除</a>
                </td>
            </tr>
        <?php }}else{ ?>
        <tr>
            <td colspan='7' style="text-align: center">暂无数据</td>
        </tr>

    <?php }?>

    </tbody>
</table>
<div id="demo3"></div>

<script>
    var share_name = "<?=$share_name?>";
    layui.use(['form', 'layedit', 'laydate','laypage'], function(){
        var form = layui.form
            ,layer = layui.layer
            ,layedit = layui.layedit
            ,laydate = layui.laydate
            ,laypage = layui.laypage;
        laypage.render({
            elem: 'demo3'
            ,count: <?=$count?>
            ,layout:['count','prev', 'page', 'next','limit','skip']
            ,curr:<?=$page?>
            ,limit:<?=$limit?>
            ,jump: function(obj, first){
                var page=obj.curr; //得到当前页，以便向服务端请求对应页的数据。
                var limit=obj.limit; //得到每页显示的条数
                var js_str = 'page='+page+'&limit='+limit;
                var form_str = $("#search_form").serialize();
                var form_arr = form_str.split('&');
                form_arr.splice(0,1);
                form_str = form_arr.join("&");
                form_str = js_str+'&'+form_str;
                //首次不执行
                if(!first){
                    window.location.href = "/coupon/index?"+form_str;
                }
            }
        });
    });
    
    function share() {
        layer.load();
        var pro_data = getCoupon();
        if(!pro_data){
            myalert('没有可用优惠券，请先添加优惠券');
            layer.closeAll('loading');
            return false;
        }

        var htm = '<form class="layui-form" action="/coupon/save-share" method="post">';
        if(share_name != ''){
            htm += '<div class="layui-input-block" style="line-height: 50px;">已选择优惠券：'+share_name+'</div>';
        }
        htm += '<div class="layui-form-item" style="padding: 15px 30px 0px 0px;"><label class="layui-form-label">选择优惠券</label>';
        htm += '<div class="layui-input-block"><select name="coupon_id" lay-filter="pro">';
        $.each(pro_data, function (x,y) {
            htm += '<option value="'+y.id+'">'+y.name+'</option>';
        });
        htm += '</select></div></div>';
        htm += '<div style="text-align: center;margin-top: 40px;"><button class="layui-btn" lay-submit>确定</button>';
        htm += '<a class="layui-btn layui-btn-primary" style="margin-left: 60px;" href="javascript:;" onclick="cancle()">取消</a></div></form>';

        layer.open({
            type: 1,
            title: '选择优惠券',
            content: htm,
            area: ['500px', '300px']
        });
        layui.form.render();
        layer.closeAll('loading');
    }

    //取消
    function cancle() {
        layer.closeAll();
    }

    //获取优惠券
    function getCoupon() {
        var data = '';
        $.ajaxSettings.async = false;
        $.get('/coupon/get-coupon',{},function (ret) {
            if(ret.code == '200'){
                data = ret.data;
            }else{
                data  = false;
            }
        },'json');
        return data;
    }

    //开启 / 关闭
    function swit(tsta){
        var msg = '开启';
        var sta = 0;
        if(tsta == '0'){
            msg = '关闭';
            sta = 1;
        }
        layer.confirm('确认'+msg+'优惠券吗？', function(index){
            $.get('/coupon/switch',{'sta':sta},function (ret) {
                layer.close(index);
                if(ret.code == '200'){
                    reloadalert(ret.msg);
                }else{
                    myalert(ret.msg);
                }
            },'json');
        });
    }

    //上架 / 下架
    function disable(thisid,thissta){
        var msg = '上架';
        var sta = 0;
        if(thissta == '0'){
            msg = '下架';
            sta = 1;
        }
        layer.confirm('确认'+msg+'吗？', function(index){
            $.get('/coupon/disable',{'id':thisid,'sta':sta},function (ret) {
                layer.close(index);
                if(ret.code == '200'){
                    reloadalert(ret.msg);
                }else{
                    myalert(ret.msg);
                }
            },'json');
        });
    }

    //删除
    function Deleteone(obj){
        layer.confirm('确认删除吗？', function(index){
            $.get('/coupon/delete',{'id':obj},function (ret) {
                layer.close(index);
                if(ret.code == '200'){
                    reloadalert(ret.msg);
                }else{
                    myalert(ret.msg);
                }
            },'json');
        });
    }
</script>