<?php

use yii\helpers\Html;

$this->title = '优惠券管理';
?>
<span class="layui-breadcrumb">
  <a href="/">首页</a>
  <a><cite><?=$this->title?></cite></a>
</span>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>添加优惠券</legend>
</fieldset>

<style>
    .layui-form-label{width: 110px;}
1</style>

<form class="layui-form" action="/coupon/save" method='post'>
    <input type="hidden" value="<?php echo Yii::$app->request->csrfToken; ?>" name="_csrf" >
    <div class="layui-form-item">
        <label class="layui-form-label">优惠券名称</label>
        <div class="layui-input-inline" style="width: 50%">
            <input type="text" name="name" required  lay-verify="required" placeholder="请输入名称" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">优惠券有效期</label>
        <div class="layui-input-inline" style="width: 24%">
            <input type="text" name="start_at" id="created_at" required  lay-verify="required" placeholder="起始时间" autocomplete="off" class="layui-input">
        </div>
        <div class="layui-form-mid">-</div>
        <div class="layui-input-inline" style="width: 24%;">
            <input type="text" name="end_at" id="end_at" required  lay-verify="required" placeholder="起始时间" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">优惠券金额</label>
        <div class="layui-input-inline" style="width: 50%">
            <input type="number" name="money" required  lay-verify="required" placeholder="请输入金额" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">优惠券使用条件</label>
        <div class="layui-input-inline" style="width: 15%">
            <input type="radio" name="restriction" lay-filter="rest" value="0" title="无限制" checked>
            <input type="radio" name="restriction" lay-filter="rest" value="1" title="有限制">
        </div>
        <div id="rest" style="display: none">
            <div class="layui-form-mid">满</div>
            <div class="layui-input-inline" style="width: 100px">
                <input type="number" name="rest_num" autocomplete="off" class="layui-input">
            </div>
            <div class="layui-form-mid">元可用</div>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">优惠券领取限制</label>
        <div class="layui-input-inline" style="width: 15%">
            <input type="radio" name="limit" lay-filter="limit" value="0" title="无限制" checked>
            <input type="radio" name="limit" lay-filter="limit" value="1" title="有限制">
        </div>
        <div id="limit" style="display: none">
            <div class="layui-form-mid">一个用户领取</div>
            <div class="layui-input-inline" style="width: 100px">
                <input type="number" name="limit_num" autocomplete="off" class="layui-input">
            </div>
            <div class="layui-form-mid">张</div>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>

<script>
    //Demo
    layui.use(['form', 'layedit', 'laydate'], function(){
        var form = layui.form
            ,layer = layui.layer
            ,layedit = layui.layedit
            ,laydate = layui.laydate;
        //日期
        laydate.render({
            elem: '#created_at' //指定元素
            ,type: 'date'
            ,format: 'yyyy-MM-dd'
        });
        laydate.render({
            elem: '#end_at' //指定元素
            ,type: 'date'
            ,format: 'yyyy-MM-dd'
        });
        form.on('radio(rest)', function(data){
            if(data.value == 1){
                $("#rest").show();
            }else{
                $("#rest").hide();
            }
        });
        form.on('radio(limit)', function(data){
            if(data.value == 1){
                $("#limit").show();
            }else{
                $("#limit").hide();
            }
        });
    });
</script>


