<?php

use yii\helpers\Html;

$this->title = '员工管理';
?>
<span class="layui-breadcrumb">
  <a href="/">首页</a>
  <a><cite><?=$this->title?></cite></a>
</span>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>编辑员工</legend>
</fieldset>

<style>
    .layui-form-label{width: 110px;}
1</style>

<form class="layui-form" action="/staff/save" method='post'>
    <input type="hidden" value="<?php echo Yii::$app->request->csrfToken; ?>" name="_csrf" >
    <input type="hidden" value="<?=$data['id']?>" name="id" >
    <div class="layui-form-item">
        <label class="layui-form-label">姓名</label>
        <div class="layui-input-inline" style="width: 50%">
            <input type="text" name="name" value="<?=$data['name']?>" required  lay-verify="required" placeholder="请输入姓名" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">年龄</label>
        <div class="layui-input-inline" style="width: 50%">
            <input type="text" name="age" value="<?=$data['age']?>" required  lay-verify="required" placeholder="请输入年龄" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">性别</label>
        <div class="layui-input-inline" style="width: 24%">
            <input type="radio" name="sex" value="0" title="男" <?= $data['sex']==0?'checked':'';?>>
            <input type="radio" name="sex" value="1" title="女" <?= $data['sex']==1?'checked':'';?>>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">手机号</label>
        <div class="layui-input-inline" style="width: 50%">
            <input type="number" name="mobile" value="<?=$data['mobile']?>" required  lay-verify="required" placeholder="请输入手机号" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">职业</label>
        <div class="layui-input-inline" style="width: 30%">
            <select name="profession_id" lay-verify="">
                <option value="">请选择职业</option>
                <?php if(!empty($pro_data)){ ?>
                    <?php foreach($pro_data as $key=>$val){ ?>
                        <option value="<?=$val['id']?>" <?= $data['profession_id']==$val['id']?'selected':'';?>><?=$val['name']?></option>
                    <?php }}?>
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">地址</label>
        <div class="layui-input-inline" style="width: 50%">
            <input type="text" name="address" value="<?=$data['address']?>" required  lay-verify="required" placeholder="请输入地址" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">员工账号</label>
        <div class="layui-input-inline" style="width: 50%">
            <input type="text" name="admin" value="<?=$admin_data['username']?>" required  lay-verify="required" placeholder="请输入员工账号" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">密码</label>
        <div class="layui-input-inline" style="width: 50%">
            <input type="text" name="password" value="<?=$admin_data['mpassword']?>" required  lay-verify="required" placeholder="请输入密码" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>

<script>
    //Demo
    layui.use(['form', 'layedit', 'laydate'], function(){
        var form = layui.form
            ,layer = layui.layer
            ,layedit = layui.layedit
            ,laydate = layui.laydate;
    });
</script>


