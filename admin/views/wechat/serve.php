<?php
use yii\helpers\Html;
$this->title = '客服电话';
?>
<span class="layui-breadcrumb">
  <a href="/">首页</a>
  <a><cite><?=$this->title?></cite></a>
</span>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>客服电话</legend>
</fieldset>

<div style="width: 100%;height: 400px;">
    <div style="width: 80%;margin-left: 10%;">
        <!-- 加载编辑器的容器 -->
        <textarea name="text"  id="container" placeholder="请输入内容" style="width:100%;height:400px"><?=$content?></textarea>
    </div>
    <div class="layui-form-item"  style="margin-left: 10%;margin-top: 20px;">
        <div class="layui-input-block">
            <button class="layui-btn" id="sub" lay-filter="formDemo">立即提交</button>
            <button type="reset" id="resetue" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</div>

<!-- 上传文件地址 -->
<script>
    var uploadFileUrl = '/base/uploadfile';
</script>
<!-- 配置文件 -->
<script type="text/javascript" src="/utf/ueditor.config.js"></script>
<!-- 编辑器源码文件 -->
<script type="text/javascript" src="/utf/ueditor.all.js"></script>
<script>
    <!-- 实例化编辑器 -->
    var ue = UE.getEditor('container',{
        autoHeightEnabled: false,
        autoFloatEnabled:false //是否保持toolbar位置不动
    });
    ue.ready(function() {
        //清空编辑器的内容
        $("#resetue").click(function () {
            ue.setContent('');
        });
        //提交数据
        $("#sub").click(function () {
            var cont = ue.getContent();
            $.post('/wechat/serve',{'cont':cont},function (ret) {
                myalert(ret.msg);
            },'json')
        });
    });

</script>