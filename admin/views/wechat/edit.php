<?php
use yii\helpers\Html;
$this->title = $title;
?>
<span class="layui-breadcrumb">
  <a href="/">首页</a>
  <a><cite><?=$this->title?></cite></a>
</span>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>编辑<?=$title?></legend>
</fieldset>

<div style="width: 100%;height: 400px;">
    <form class="layui-form" action="/wechat/save" method='post'>
        <input type="hidden" value="<?php echo Yii::$app->request->csrfToken; ?>" name="_csrf" >
        <input type="hidden" name="mid" value="<?=$mid?>">
        <input type="hidden" name="type" value="<?=$type?>">
        <input type="hidden" name="url" value="<?=$url?>">
        <div class="layui-form-item">
            <label class="layui-form-label">标题</label>
            <div class="layui-input-inline" style="width: 50%;">
                <input type="text" name="title" value="<?=$data['title']?>" required  lay-verify="required" autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">内容</label>
            <div class="layui-input-inline" style="width: 80%;">
                <!-- 加载编辑器的容器 -->
                <textarea name="content" id="container" style="width:100%;height:400px"><?=$data['content']?></textarea>
            </div>
        </div>
        <div class="layui-form-item"  style="margin-left: 10%;margin-top: 20px;">
            <div class="layui-input-block">
                <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
                <button type="reset" id="resetue" class="layui-btn layui-btn-primary">重置</button>
            </div>
        </div>
    </form>
</div>

<!-- 上传文件地址 -->
<script>
    var uploadFileUrl = '/base/uploadfile';
</script>
<!-- 配置文件 -->
<script type="text/javascript" src="/utf/ueditor.config.js"></script>
<!-- 编辑器源码文件 -->
<script type="text/javascript" src="/utf/ueditor.all.js"></script>
<script>
    <!-- 实例化编辑器 -->
    var ue = UE.getEditor('container',{
        autoHeightEnabled: false,
        autoFloatEnabled:false //是否保持toolbar位置不动
    });
    ue.ready(function() {
        //清空编辑器的内容
        $("#resetue").click(function () {
            ue.setContent('');
        });
    });

</script>