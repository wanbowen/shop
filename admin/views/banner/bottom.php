<?php
use yii\helpers\Html;
$this->title = '首页底部图';
?>
<span class="layui-breadcrumb">
  <a href="/">首页</a>
  <a><cite><?=$this->title?></cite></a>
</span>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>首页底部图</legend>
</fieldset>

<div style="width: 100%;height: 400px;">
    <div class="layui-form-item">
        <label class="layui-form-label">图片</label>
        <div class="layui-inline">
            <div class="layui-upload">
                <button type="button" class="layui-btn" id="levelimg">点击上传</button>
                <div class="layui-upload-list" id="img_show" style="width: 400px;height: 200px">
                    <img src="<?=$img?>" style="width: 100%;height: 100%;" />
                </div>
            </div>
            <input type="hidden" id='img_val' name="img" value="<?=$img?>" required  lay-verify="required">
        </div>
    </div>
    <div class="layui-form-item"  style="margin-left: 10%;margin-top: 20px;">
        <div class="layui-input-block">
            <button class="layui-btn" id="sub" lay-filter="formDemo">立即提交</button>
        </div>
    </div>
</div>

<script>
    <!-- 上传文件地址 -->
    var uploadFileUrl = '/base/uploadfile';
    layui.use('upload', function(){
        var $ = layui.jquery
            ,upload = layui.upload;

        //普通图片上传
        var uploadInst = upload.render({
            elem: '#levelimg'
            ,field: 'upfile'
            ,url: uploadFileUrl
            ,done: function(res){
                if(res.state == 'SUCCESS'){
                    $("#img_show").html('<img src="'+res.url+'" style="width: 100%;height: 100%;" />');
                    $("#img_val").val(res.url);
                }else{
                    myalert(res.state);
                }
            }
            ,error: function(){
                myalert('上传失败，请刷新页面重试');
            }
        });
    });

    $(function () {
        //提交数据
        $("#sub").click(function () {
            var img = $("#img_val").val();
            if(img == ''){
                myalert('请上传图片');
                return false;
            }
            $.post('/banner/bottom',{'img':img},function (ret) {
                myalert(ret.msg);
            },'json')
        });
    })
</script>