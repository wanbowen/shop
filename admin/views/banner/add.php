<?php

use yii\helpers\Html;

$this->title = '首页banner图管理';
?>
<span class="layui-breadcrumb">
  <a href="/">首页</a>
  <a><cite><?=$this->title?></cite></a>
</span>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>添加banner图</legend>
</fieldset>

<form class="layui-form" action="/banner/save" method='post'>
    <input type="hidden" value="<?php echo Yii::$app->request->csrfToken; ?>" name="_csrf" >
    <input type="hidden" name="type" value="<?=$type?>">
    <div class="layui-form-item">
        <label class="layui-form-label">图片名称</label>
        <div class="layui-input-inline">
            <input type="text" name="name" required  lay-verify="required" placeholder="请输入名称" autocomplete="off" class="layui-input">
        </div>
    </div>
	<!--
    <div class="layui-form-item">
        <label class="layui-form-label">描述</label>
        <div class="layui-input-inline">
			<textarea name="description" placeholder="请输入描述" class="layui-textarea"></textarea>
        </div>
    </div>
	<div class="layui-form-item">
        <label class="layui-form-label">链接</label>
        <div class="layui-input-inline">
            <input type="text" name="url" required  lay-verify="required|url" placeholder="请输入网址" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">排序</label>
        <div class="layui-input-inline">
            <input type="text" name="sorts"  required lay-verify="required|number" autocomplete="off" placeholder="请输入排序" class="layui-input">
        </div>
    </div>
	-->
	<div class="layui-form-item">
        <label class="layui-form-label">状态</label>
        <div class="layui-input-inline">
            <select name="status">
                <option value="0">启用</option>
                <option value="1">禁用</option>
            </select>
        </div>
    </div>
<!--	<div class="layui-form-item">-->
<!--        <label class="layui-form-label"><span style="color:red">★</span>banner内链文章id</label>-->
<!--        <div class="layui-input-inline">-->
<!--            <input type="text" name="document_id"  required lay-verify="required|number" autocomplete="off" placeholder="请输入内链文章id" class="layui-input">-->
<!--        </div>-->
<!--    </div>-->
    <div class="layui-form-item">
        <label class="layui-form-label">图片</label>
        <div class="layui-inline">
            <div class="layui-upload">
                <button type="button" class="layui-btn" id="levelimg">点击上传</button>
                <div class="layui-upload-list" id="img_show" style="width: 400px;height: 200px">

                </div>
            </div>
            <input type="hidden" id='img_val' name="img" value='' required  lay-verify="required">
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>

<script>
    //Demo
    layui.use(['form', 'layedit', 'laydate'], function(){
        var form = layui.form
            ,layer = layui.layer
            ,layedit = layui.layedit
            ,laydate = layui.laydate;
        //日期
        laydate.render({
            elem: '#date',
            type: 'date'
        });
    });
    layui.use('upload', function(){
        var $ = layui.jquery
            ,upload = layui.upload;

        //普通图片上传
        var uploadInst = upload.render({
            elem: '#levelimg'
            ,field: 'upfile'
            ,url: '/base/uploadfile'
            ,done: function(res){
                if(res.state == 'SUCCESS'){
                    $("#img_show").html('<img src="'+res.url+'" style="width: 100%;height: 100%;" />');
                    $("#img_val").val(res.url);
                }else{
                    myalert(res.state);
                }
            }
            ,error: function(){
                myalert('上传失败，请刷新页面重试');
            }
        });
    });
</script>


