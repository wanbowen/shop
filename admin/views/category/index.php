<?php
use yii\helpers\Html;
$this->title = '首页分类管理';
?>
<span class="layui-breadcrumb">
  <a href="/">首页</a>
  <a><cite><?=$this->title?></cite></a>
</span>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>首页分类管理</legend>
</fieldset>

<form class="layui-form" id="search_form" action="/category/index" method="get">
    <div class="layui-form-item">
        <input type="hidden" name="limit" value="<?=$limit?>">
        <label class="layui-form-label" style="width: 50px;" >名称</label>
        <div class="layui-input-inline" style="width: 150px;">
            <input type="text" name="name" value="<?= !empty($getForm['name'])?$getForm['name']:'';?>" lay-verify="title" autocomplete="off" placeholder="请输入名称" class="layui-input">
        </div>
        <div class="layui-input-inline" style="width:50px;">
            <button class="layui-btn" lay-submit="" lay-filter="demo1">筛选</button>
        </div>
    </div>
</form>

<a href="/category/save-one" class="layui-btn layui-btn-sm"><i class="layui-icon"></i>新增</a>

<table class="layui-table">
    <thead>
    <tr>
        <th>序号</th>
        <th>分类名称</th>
        <th>操作</th>
    </tr>
    </thead>
    <tbody>
    <?php if(isset($datas) && !empty($datas)){ ?>
        <?php foreach($datas as $key=>$value){?>
            <tr>
                <td><?=$value['id']?></td>
                <td><?=$value['name']?></td>
                <td>
                    <a class="layui-btn layui-btn-xs" href="/category/attachment?pid=<?=$value['id']?>" >查看分类</a>
                    <a class="layui-btn layui-btn-xs" href="/category/save-one?id=<?=$value['id']?>" >编辑</a>
                    <a class="layui-btn layui-btn-danger layui-btn-xs" id="<?=$value['id']?>" onclick="Deleteone(this.id)" >删除</a>
                </td>
            </tr>
        <?php }}else{ ?>
        <tr>
            <td colspan='3' style="text-align: center">暂无数据</td>
        </tr>

    <?php }?>

    </tbody>
</table>
<div id="demo3"></div>

<script>
    layui.use(['form', 'layedit', 'laydate','laypage'], function(){
        var form = layui.form
            ,layer = layui.layer
            ,layedit = layui.layedit
            ,laydate = layui.laydate
            ,laypage = layui.laypage;
        laypage.render({
            elem: 'demo3'
            ,count: <?=$count?>
            ,layout:['count','prev', 'page', 'next','limit','skip']
            ,curr:<?=$page?>
            ,limit:<?=$limit?>
            ,jump: function(obj, first){
                var page=obj.curr; //得到当前页，以便向服务端请求对应页的数据。
                var limit=obj.limit; //得到每页显示的条数
                var js_str = 'page='+page+'&limit='+limit;
                var form_str = $("#search_form").serialize();
                var form_arr = form_str.split('&');
                form_arr.splice(0,1);
                form_str = form_arr.join("&");
                form_str = js_str+'&'+form_str;
                //首次不执行
                if(!first){
                    window.location.href = "/category/index?"+form_str;
                }
            }
        });
    });

    //删除
    function Deleteone(obj){
        layer.confirm('确认删除吗？', function(index){
            $.get('/category/delete-one',{'id':obj},function (ret) {
                layer.close(index);
                if(ret.code == '200'){
                    reloadalert(ret.msg);
                }else{
                    myalert(ret.msg);
                }
            },'json');
        });
    }
</script>