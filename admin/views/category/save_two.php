<?php

use yii\helpers\Html;

$this->title = '首页分类管理';
?>
<span class="layui-breadcrumb">
  <a href="/">首页</a>
  <a><cite><?=$this->title?></cite></a>
</span>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>保存二级分类</legend>
</fieldset>

<form class="layui-form" action="/category/save-two?id=<?=$data->id?>" method='post'>
    <input type="hidden" value="<?php echo Yii::$app->request->csrfToken; ?>" name="_csrf" >
    <input type="hidden" value="<?= $pid ?>" name="pid" >
    <div class="layui-form-item">
        <label class="layui-form-label">分类名称</label>
        <div class="layui-input-inline" style="width: 50%">
            <input type="text" name="name" value="<?=$data->name?>" required  lay-verify="required" placeholder="请输入名称" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">上传图标</label>
        <div class="layui-inline">
            <div class="layui-upload">
                <button type="button" class="layui-btn" id="icon_img">点击上传</button>
                <div class="layui-upload-list" id="img_show_icon" style="width: 100px;height: 100px">
                    <img src="<?=$data['icon_img']?>" style="width: 100%;height: 100%;" />
                </div>
            </div>
            <input type="hidden" id='img_icon' name="icon_img" value='<?=$data->icon_img?>' required  lay-verify="required">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">banner图片</label>
        <div class="layui-inline">
            <div class="layui-upload">
                <button type="button" class="layui-btn" id="levelimg">点击上传</button>
                <div class="layui-upload-list" id="img_show" style="width: 400px;height: 200px">
                    <img src="<?=$data['banner_img']?>" style="width: 100%;height: 100%;" />
                </div>
            </div>
            <input type="hidden" id='img_val' name="banner_img" value='<?=$data->banner_img?>' required  lay-verify="required">
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>

<script>
    //Demo
    layui.use(['form', 'layedit', 'laydate'], function(){
        var form = layui.form
            ,layer = layui.layer
            ,layedit = layui.layedit
            ,laydate = layui.laydate;
        //日期
        laydate.render({
            elem: '#date',
            type: 'date'
        });
    });
    layui.use('upload', function(){
        var $ = layui.jquery
            ,upload = layui.upload;

        //普通图片上传
        var uploadInst = upload.render({
            elem: '#levelimg'
            ,field: 'upfile'
            ,url: '/base/uploadfile'
            ,done: function(res){
                if(res.state == 'SUCCESS'){
                    $("#img_show").html('<img src="'+res.url+'" style="width: 100%;height: 100%;" />');
                    $("#img_val").val(res.url);
                }else{
                    myalert(res.state);
                }
            }
            ,error: function(){
                myalert('上传失败，请刷新页面重试');
            }
        });

        //普通图片上传
        var uploadInst2 = upload.render({
            elem: '#icon_img'
            ,field: 'upfile'
            ,url: '/base/uploadfile'
            ,done: function(res){
                if(res.state == 'SUCCESS'){
                    $("#img_show_icon").html('<img src="'+res.url+'" style="width: 100%;height: 100%;" />');
                    $("#img_icon").val(res.url);
                }else{
                    myalert(res.state);
                }
            }
            ,error: function(){
                myalert('上传失败，请刷新页面重试');
            }
        });
    });
</script>


