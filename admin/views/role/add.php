<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = '添加角色';
$this->params['breadcrumbs'][] = $this->title;
?>
<span class="layui-breadcrumb">
  <a href="/">首页</a>
  <a href="/role/">角色列表</a>
  <a><cite><?=$this->title?></cite></a>
</span>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
  <legend>添加角色</legend>
</fieldset>
<form class="layui-form" action="/role/addpost" method='post'>
  <input type="hidden" value="<?php echo Yii::$app->request->csrfToken; ?>" name="_csrf" >
  <div class="layui-form-item">
    <label class="layui-form-label">角色名</label>
    <div class="layui-input-inline">
      <input type="text" name="rolename" required  lay-verify="rolename" placeholder="请输入角色名" autocomplete="off" class="layui-input">
    </div>
  </div>
  <div class="layui-form-item">
    <label class="layui-form-label">状态</label>
    <div class="layui-input-inline">
      <select name="disabled" lay-verify="required">
        <option value="0">启用</option>
		<option value="1">禁用</option>
      </select>
    </div>
  </div>
 <div class="layui-form-item">
      <label class="layui-form-label">描述</label>
      <div class="layui-input-inline">
        <textarea class="layui-textarea" name="description" lay-verify="description"></textarea>
      </div>
    </div>
  <div class="layui-form-item">
    <label class="layui-form-label">排序</label>
    <div class="layui-input-inline">
      <input type="text" name="listorder" required lay-verify="required" placeholder="请输排序" autocomplete="off" class="layui-input">
    </div>
  </div>
  <div class="layui-form-item">
    <div class="layui-input-block">
      <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
      <button type="reset" class="layui-btn layui-btn-primary">重置</button>
    </div>
  </div>
</form>
<script>
//Demo
layui.use(['form', 'layedit', 'laydate'], function(){
  var form = layui.form
  ,layer = layui.layer
  ,layedit = layui.layedit
  ,laydate = layui.laydate;
 
  //自定义验证规则
  form.verify({
    rolename: function(value){
      if(value.length < 2){
        return '标题至少得2个字符啊';
      }
    }
  });
});
</script>
