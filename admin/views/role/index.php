<?php

/* @var $this yii\web\View */

$this->title = '角色列表';
?>
<span class="layui-breadcrumb">
  <a href="/">首页</a>
  <a><cite><?=$this->title?></cite></a>
</span>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
  <legend>角色列表</legend>
</fieldset>
<form class="layui-form" action="/role/index" method="get">
  <div class="layui-form-item">
	<input type="hidden" name="limit" value="<?=$limit?>">
    <label class="layui-form-label" style="width: 50px;" >角色名</label>
    <div class="layui-input-inline" style="width: 150px;">
      <input type="text" name="rolename" value="<?=$rolename?>" lay-verify="title" autocomplete="off" placeholder="请输入角色名" class="layui-input">
    </div>
	<label class="layui-form-label" style="width: 30px;">状态</label>
    <div class="layui-input-inline" style="width: 150px;">
        <select name="disabled">
          <option value="">请选择状态</option>
          <option value="0" <?=$disabled=='0' ? 'selected' : ''?>>启用</option>
		  <option value="1" <?=$disabled=='1' ? 'selected' : ''?>>禁用</option>
        </select>
    </div>
    <div class="layui-input-inline" style="width:50px;">
      <button class="layui-btn" lay-submit="" lay-filter="demo1">筛选</button>
    </div>
  </div>
</form>
<a href="/role/add/" ><button class="layui-btn">添加角色</button></a>
<table class="layui-table">
  <thead>
    <tr>
      <th>编号</th>
	  <th>角色名</th>
      <th>状态</th>
      <th>排序</th>
	  <th>添加时间</th>
	  <th>添加人</th>
	  <th>管理操作</th>
    </tr> 
  </thead>
  <tbody>
  <?php if(isset($dataProvider)&&!empty($dataProvider)){ ?>
	  <?php foreach($dataProvider as $key=>$value){?>
    <tr>
      <td><?=$value['roleid']?></td>
	  <td><?=$value['rolename']?></td>
	  <td><?=$value['disabled']==0 ? '启用':'禁用'?></td>
	  <td><?=$value['listorder']?></td>
	  <td><?=$value['created_at']?></td>
	  <td><?=$value['username']?></td>
	  <td>
          <a  class="layui-btn layui-btn-danger layui-btn-xs" id="<?=$value['roleid']?>" onclick="disable(this.id,<?=$value['disabled']?>)" ><?=$value['disabled']==0 ? '禁用':'启用'?></a>
          <a class="layui-btn layui-btn-xs" href="/role/edit/<?=$value['roleid']?>" >编辑</a>
          <a  class="layui-btn layui-btn-xs" id="<?=$value['roleid']?>" onclick="preset(this.id)" >权限分配</a>
          <a  class="layui-btn layui-btn-danger layui-btn-xs" id="<?=$value['roleid']?>" onclick="Deleteone(this.id)" >删除</a>
	  </td>
    </tr>
	<?php }}else{ ?> 
	 <tr>
      <td colspan='7'>暂无数据</td>
     </tr>
	
	<?php }?>
	
  </tbody>
</table>
<div id="demo3"></div>
<script>
    //Demo
    layui.use(['form', 'layedit', 'laydate','laypage'], function(){
        var form = layui.form
            ,layer = layui.layer
            ,layedit = layui.layedit
			,laypage = layui.laypage;

        laypage.render({
            elem: 'demo3'
            ,count: <?=$count?>
            ,limit:<?=$limit?>
			,layout:['count','prev', 'page', 'next','limit','skip']
            ,curr:<?=$page?>
            
            ,jump: function(obj, first){
                var page=obj.curr; //得到当前页，以便向服务端请求对应页的数据。
                var limit=obj.limit; //得到每页显示的条数

                //首次不执行
                if(!first){
                    window.location.href="/role/index?page="+page+"&limit="+limit+"&rolename=<?=$rolename?>&disabled=<?=$disabled?>";
                }
            }
        });
    });
    function disable(thisid,thissta){
        var msg = '启用';
        var sta = 0;
        if(thissta == '0'){
            msg = '禁用';
            sta = 1;
        }
        layer.confirm('此用户确认'+msg+'吗？', function(index){
            window.location.href="/role/disable?id="+thisid+"&sta="+sta;
            layer.close(index);
        });
    }
    function Deleteone(obj){

        layer.confirm('确认删除吗？', function(index){
            window.location.href="/role/delete/"+obj;
            layer.close(index);
        });
    }
	function preset(obj){

         layer.open({
		  title: '权限设置',
		  type: 2,
		  area: ['450px', '520px'],
		  skin: 'layui-layer-rim', //加上边框
		  content: '/role/preset/'+obj, //iframe的url，no代表不显示滚动条
		});
    }
</script>