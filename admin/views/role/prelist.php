<!DOCTYPE html>
<HTML>
<HEAD>
	<TITLE>权限设置</TITLE>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<link rel="stylesheet" href="/tree/css/demo.css" type="text/css">
	<link rel="stylesheet" href="/tree/css/zTreeStyle/zTreeStyle.css" type="text/css">
	<script type="text/javascript" src="/tree/js/jquery-1.4.4.min.js"></script>
	<script type="text/javascript" src="/tree/js/jquery.ztree.core.js"></script>
	<script type="text/javascript" src="/tree/js/jquery.ztree.excheck.js"></script>
	<script src='/layui/layui.js'></script>
	<SCRIPT type="text/javascript">
		<!--
		var setting = {
			view: {
				selectedMulti: false
			},
			check: {
				enable: true
			},
			data: {
				simpleData: {
					enable: true
				}
			}
		};

		var zNodes =<?=$json?>;

		var clearFlag = false;
		function tijiao(obj){
			var treeObj=$.fn.zTree.getZTreeObj("treeDemo"),
				nodes=treeObj.getCheckedNodes(true);
				key="";
				value="";
				for(var i=0;i<nodes.length;i++){
					key+=nodes[i].id + ",";
				}
			$.post('/role/adminpreset/'+obj,{key:key},function(data){
				if(data == '')
				{
					layui.use('layer', function(){
					var $ = layui.jquery, layer = layui.layer; 
				    layer.ready(function(){
						layer.alert(data.msg,{
						  title: '提示信息'
						  ,skin: 'layui-layer-molv'
						  ,btnAlign: 'c'
						  ,closeBtn: 0
						  ,anim: 4 //动画类型
						}, function(){
						   layer.close(layer.index);
							var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
							parent.layer.close(index); //再执行关闭 
						});
						});
					});
					return false;
				}
				else if (data.ret == 200)
				{
					layui.use('layer', function(){
					var $ = layui.jquery, layer = layui.layer; 
				    layer.ready(function(){
						layer.alert(data.msg,{
						  title: '提示信息'
						  ,skin: 'layui-layer-molv'
						  ,btnAlign: 'c'
						  ,closeBtn: 0
						  ,anim: 4 //动画类型
						}, function(){
							layer.close(layer.index);
							var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
							parent.layer.close(index); //再执行关闭 
						});
						});
					});
				}
				else
				{
					layui.use('layer', function(){
					var $ = layui.jquery, layer = layui.layer; 
				    layer.ready(function(){
						layer.alert(data.msg,{
						  title: '提示信息'
						  ,skin: 'layui-layer-molv'
						  ,btnAlign: 'c'
						  ,closeBtn: 0
						  ,anim: 4 //动画类型
						}, function(){
						   layer.close(layer.index);
							var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
							parent.layer.close(index); //再执行关闭 
						});
						});
					});
					return false;
				}
			},'json');
		}
		function clearCheckedOldNodes() {
			var zTree = $.fn.zTree.getZTreeObj("treeDemo"),
			nodes = zTree.getChangeCheckedNodes();
			for (var i=0, l=nodes.length; i<l; i++) {
				nodes[i].checkedOld = nodes[i].checked;
			}
		}
		function count() {
			var zTree = $.fn.zTree.getZTreeObj("treeDemo"),
			checkCount = zTree.getCheckedNodes(true).length,
			nocheckCount = zTree.getCheckedNodes(false).length,
			changeCount = zTree.getChangeCheckedNodes().length;
			$("#checkCount").text(checkCount);
			$("#nocheckCount").text(nocheckCount);
			$("#changeCount").text(changeCount);

		}
		function createTree() {
			$.fn.zTree.init($("#treeDemo"), setting, zNodes);
			count();
			clearFlag = $("#last").attr("checked");
		}

		$(document).ready(function(){
			createTree();			
			$("#init").bind("change", createTree);
			$("#last").bind("change", createTree);
		});
		//-->
	</SCRIPT>
</HEAD>

<BODY>
<div class="zTreeDemoBackground">	
	<ul id="treeDemo" class="ztree"></ul>
</div>
<div class="zTreeDemoBackground">
<button onclick="tijiao(this.id)" id="<?=$roleid?>" type="button" style="display: inline-block;height: 30px;line-height: 30px;padding: 0 18px;background-color: #009688;color: #fff;white-space: nowrap;text-align: center;font-size: 14px;border: none;border-radius: 2px;cursor: pointer;margin-top:40px">设置权限</button>	
</div>
</BODY>
</HTML>