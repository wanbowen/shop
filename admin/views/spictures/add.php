<?php

use yii\helpers\Html;

$this->title = '商城banner图管理';
?>
<span class="layui-breadcrumb">
  <a href="/">首页</a>
  <a><cite><?=$this->title?></cite></a>
</span>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>添加banner图</legend>
</fieldset>

<form class="layui-form" action="/spictures/save" method='post'>
    <input type="hidden" value="<?php echo Yii::$app->request->csrfToken; ?>" name="_csrf" >
    <input type="hidden" name="status" value="<?=$status?>">

	<div class="layui-form-item">
        <label class="layui-form-label">状态</label>
        <div class="layui-input-inline">
            <select name="status">
                <option value="0">启用</option>
                <option value="1">禁用</option>
            </select>
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">图片</label>
        <div class="layui-inline">
            <div class="layui-upload">
                <button type="button" class="layui-btn" id="levelimg">点击上传</button>
                <div class="layui-upload-list" id="img_show" style="width: 400px;height: 200px">

                </div>
            </div>
            <input type="hidden" id='img_val' name="img" value='' required  lay-verify="required">
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>

<script>
    //Demo
    layui.use(['form', 'layedit', 'laydate'], function(){
        var form = layui.form
            ,layer = layui.layer
            ,layedit = layui.layedit
            ,laydate = layui.laydate;
        //日期
        laydate.render({
            elem: '#date',
            type: 'date'
        });
    });
    layui.use('upload', function(){
        var $ = layui.jquery
            ,upload = layui.upload;

        //普通图片上传
        var uploadInst = upload.render({
            elem: '#levelimg'
            ,field: 'upfile'
            ,url: '/base/uploadfile'
            ,done: function(res){
                if(res.state == 'SUCCESS'){
                    $("#img_show").html('<img src="'+res.url+'" style="width: 100%;height: 100%;" />');
                    $("#img_val").val(res.url);
                }else{
                    myalert(res.state);
                }
            }
            ,error: function(){
                myalert('上传失败，请刷新页面重试');
            }
        });
    });
</script>


