<?php
use yii\helpers\Html;
$this->title = '投诉建议';
?>
<span class="layui-breadcrumb">
  <a href="/">首页</a>
  <a><cite><?=$this->title?></cite></a>
</span>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>查看投诉建议</legend>
</fieldset>

<div style="width: 70%">
    <div class="layui-form-item">
        <label class="layui-form-label">标题</label>
        <div class="layui-input-block">
            <input type="text" name="title" value="<?=$data['title']?>" readonly autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">内容</label>
        <div class="layui-input-block">
            <textarea name="" placeholder="请输入" class="layui-textarea" readonly><?=$data['content']?></textarea>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">投诉人</label>
        <div class="layui-input-block">
            <input type="text" name="title" value="<?=$staff_data['name']?>" readonly autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">手机号</label>
        <div class="layui-input-block">
            <input type="text" name="title" value="<?=$staff_data['mobile']?>" readonly autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">回复内容</label>
        <div class="layui-input-block">
            <textarea name="" class="layui-textarea" readonly><?=$data['reply']?></textarea>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">投诉时间</label>
        <div class="layui-input-block">
            <input type="text" name="title" value="<?=$data['created_at']?>" readonly autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">回复时间</label>
        <div class="layui-input-block">
            <input type="text" name="title" value="<?=$data['reply_at']?>" readonly autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item"  style="margin-left: 10%;margin-top: 20px;">
        <div class="layui-input-block">
            <a href="javascript:history.back(-1)" class="layui-btn">返回</a>
        </div>
    </div>
</div>
