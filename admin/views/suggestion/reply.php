<?php
use yii\helpers\Html;
$this->title = '投诉建议';
?>
<span class="layui-breadcrumb">
  <a href="/">首页</a>
  <a><cite><?=$this->title?></cite></a>
</span>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>回复投诉建议</legend>
</fieldset>

<div style="width: 70%">
    <form class="layui-form" action="/suggestion/save" method='post'>
        <input type="hidden" value="<?php echo Yii::$app->request->csrfToken; ?>" name="_csrf" >
        <input type="hidden" name="id" value="<?=$data['id']?>">
        <div class="layui-form-item">
            <label class="layui-form-label">标题</label>
            <div class="layui-input-block">
                <input type="text" value="<?=$data['title']?>" readonly autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">内容</label>
            <div class="layui-input-block">
                <textarea placeholder="请输入" class="layui-textarea" readonly><?=$data['content']?></textarea>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">回复</label>
            <div class="layui-input-block">
                <textarea name="reply" class="layui-textarea"></textarea>
            </div>
        </div>
        <div class="layui-form-item"  style="margin-left: 10%;margin-top: 20px;">
            <div class="layui-input-block">
                <button class="layui-btn" lay-submit lay-filter="formDemo">确定回复</button>
                <a href="javascript:history.back(-1)" class="layui-btn">返回</a>
            </div>
        </div>
    </form>
</div>
