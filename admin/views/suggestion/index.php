<?php
use yii\helpers\Html;
$this->title = '投诉建议';
?>
<span class="layui-breadcrumb">
  <a href="/">首页</a>
  <a><cite><?=$this->title?></cite></a>
</span>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>投诉建议</legend>
</fieldset>

<form class="layui-form" id="search_form" action="/suggestion/index" method="get">
    <div class="layui-form-item">
        <input type="hidden" name="limit" value="<?=$limit?>">
        <label class="layui-form-label" style="width: 70px;">投诉时间:</label>
        <div class="layui-input-inline" style="width: 150px;">
            <input type="text" name="create_time" id="created_at" value="<?= !empty($getForm['create_time'])?$getForm['create_time']:'' ?>" class="layui-input" placeholder="起始时间">
        </div>
        <div class="layui-input-inline" style="width: 150px;">
            <input type="text" name="end_time" id="end_at" value="<?= !empty($getForm['end_time'])?$getForm['end_time']:'' ?>" class="layui-input" placeholder="结束时间">
        </div>
        <label class="layui-form-label" style="width: 30px;">状态</label>
        <div class="layui-input-inline" style="width: 150px;">
            <select name="status">
                <option value="-1">请选择状态</option>
                <option value="0" <?= isset($getForm['status']) && $getForm['status']=='0' ? 'selected' : ''?>>未回复</option>
                <option value="1" <?= isset($getForm['status']) && $getForm['status']=='1' ? 'selected' : ''?>>已反馈</option>
            </select>
        </div>
        <div class="layui-input-inline" style="width:50px;">
            <button class="layui-btn" lay-submit="" lay-filter="demo1">筛选</button>
        </div>
    </div>
</form>

<table class="layui-table">
    <thead>
    <tr>
        <th>序号</th>
        <th>标题</th>
        <th>投诉人</th>
        <th>手机号</th>
		<th>状态</th>
		<th>投诉时间</th>
        <th>操作</th>
    </tr>
    </thead>
    <tbody>
    <?php if(isset($datas) && !empty($datas)){ ?>
        <?php foreach($datas as $key=>$value){?>
            <tr>
                <td><?=$value['id']?></td>
				<td><?=$value['title']?></td>
				<td><?=$value['name']?></td>
				<td><?=$value['mobile']?></td>
			    <td><?=$value['status']==0 ? '未回复':'已反馈'?></td>
			    <td><?=$value['created_at']?></td>
                <td>
                    <?php if($value['status'] == 0){ ?>
                        <a class="layui-btn layui-btn-xs" href="/suggestion/reply?id=<?=$value['id']?>" >回复</a>
                    <?php }?>
                    <a class="layui-btn layui-btn-xs" href="/suggestion/view?id=<?=$value['id']?>" >查看</a>
                    <a class="layui-btn layui-btn-danger layui-btn-xs" id="<?=$value['id']?>" onclick="Deleteone(this.id)" >删除</a>
                </td>
            </tr>
        <?php }}else{ ?>
        <tr>
            <td colspan='7' style="text-align: center">暂无数据</td>
        </tr>

    <?php }?>

    </tbody>
</table>
<div id="demo3"></div>

<script>
    layui.use(['form', 'layedit', 'laydate','laypage'], function(){
        var form = layui.form
            ,layer = layui.layer
            ,layedit = layui.layedit
            ,laydate = layui.laydate
            ,laypage = layui.laypage;
        laypage.render({
            elem: 'demo3'
            ,count: <?=$count?>
            ,layout:['count','prev', 'page', 'next','limit','skip']
            ,curr:<?=$page?>
			,limit:<?=$limit?>
            ,jump: function(obj, first){
                var page=obj.curr; //得到当前页，以便向服务端请求对应页的数据。
                var limit=obj.limit; //得到每页显示的条数
                var js_str = 'page='+page+'&limit='+limit;
                var form_str = $("#search_form").serialize();
                var form_arr = form_str.split('&');
                form_arr.splice(0,1);
                form_str = form_arr.join("&");
                form_str = js_str+'&'+form_str;
                //首次不执行
                if(!first){
                    window.location.href = "/suggestion/index?"+form_str;
                }
            }
        });
        laydate.render({
            elem: '#created_at' //指定元素
        });
        laydate.render({
            elem: '#end_at' //指定元素
        });
    });

    //删除
    function Deleteone(obj){
        layer.confirm('确认删除吗？', function(index){
            $.get('/suggestion/delete',{'id':obj},function (ret) {
                layer.close(index);
                if(ret.code == '200'){
                    reloadalert(ret.msg);
                }else{
                    myalert(ret.msg);
                }
            },'json');
        });
    }
</script>