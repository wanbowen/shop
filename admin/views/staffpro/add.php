<?php

use yii\helpers\Html;

$this->title = '员工职业管理';
?>
<span class="layui-breadcrumb">
  <a href="/">首页</a>
  <a><cite><?=$this->title?></cite></a>
</span>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>添加职业</legend>
</fieldset>

<style>
    .layui-form-label{width: 110px;}
1</style>

<form class="layui-form" action="/staffpro/save" method='post'>
    <input type="hidden" value="<?php echo Yii::$app->request->csrfToken; ?>" name="_csrf" >
    <div class="layui-form-item">
        <label class="layui-form-label">职业名称</label>
        <div class="layui-input-inline" style="width: 50%">
            <input type="text" name="name" required  lay-verify="required" placeholder="请输入职业名称" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
            <button type="reset" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>

<script>
    //Demo
    layui.use(['form', 'layedit', 'laydate'], function(){
        var form = layui.form
            ,layer = layui.layer
            ,layedit = layui.layedit
            ,laydate = layui.laydate;
    });
</script>


