<?php
use yii\helpers\Html;
$this->title = '报时报修';
?>
<span class="layui-breadcrumb">
  <a href="/">首页</a>
  <a><cite><?=$this->title?></cite></a>
</span>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>查看报时报修</legend>
</fieldset>

<style>
    .layui-form-label {width: 100px;}
    .layui-input-block {margin-left: 130px;}
</style>
<div style="width: 70%">
    <div class="layui-fluid">
        <div class="layui-row" style="line-height: 60px;font-size: 26px;">订单基本信息</div>
        <div class="layui-form-item">
            <label class="layui-form-label">报修单号</label>
            <div class="layui-input-block">
                <input type="text" name="title" value="<?=$data['repair_code']?>" readonly autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">下单时间</label>
            <div class="layui-input-block">
                <input type="text" name="title" value="<?=$data['created_at']?>" readonly autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">项目名称</label>
            <div class="layui-input-block">
                <textarea name="" placeholder="请输入" class="layui-textarea" readonly><?=$data['name']?></textarea>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">状态</label>
            <div class="layui-input-block">
                <input type="text" name="title" value="<?=$data['status']==0 ? '待派单':'已派单'?>" readonly autocomplete="off" class="layui-input">
            </div>
        </div>
    </div>
    <div class="layui-fluid">
        <div class="layui-row" style="line-height: 60px;font-size: 26px;">服务人员基本信息</div>
        <div class="layui-form-item">
            <label class="layui-form-label">接单人</label>
            <div class="layui-input-block">
                <input type="text" name="title" value="<?=$staff_data['name']?>" readonly autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">接单人手机号</label>
            <div class="layui-input-block">
                <input type="text" name="title" value="<?=$staff_data['mobile']?>" readonly autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">派单时间</label>
            <div class="layui-input-block">
                <input type="text" name="title" value="<?=$data['reply_at']?>" readonly autocomplete="off" class="layui-input">
            </div>
        </div>
    </div>
    <div class="layui-fluid">
        <div class="layui-row" style="line-height: 60px;font-size: 26px;">客户基本信息</div>
        <div class="layui-form-item">
            <label class="layui-form-label">联系人</label>
            <div class="layui-input-block">
                <input type="text" name="title" value="<?=$data['contact']?>" readonly autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">联系人电话</label>
            <div class="layui-input-block">
                <input type="text" name="title" value="<?=$data['contact_tel']?>" readonly autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">问题描述</label>
            <div class="layui-input-block">
                <textarea name="" class="layui-textarea" readonly><?=$data['content']?></textarea>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">具体位置</label>
            <div class="layui-input-block">
                <input type="text" name="title" value="<?=$data['position']?>" readonly autocomplete="off" class="layui-input">
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label">图片</label>
            <div class="layui-input-block">
                <?php if(!empty($imgs_arr)){ foreach ($imgs_arr as $val){?>
                    <div class="layui-input-inline" style="width: 150px;height: 150px;">
                        <a href="<?=$val?>" target="_blank"><img style="width: 100%;height: 100%" src="<?=$val?>"></a>
                    </div>
                <?php }}?>
            </div>
        </div>
    </div>
    <div class="layui-form-item"  style="margin-left: 10%;margin-top: 20px;">
        <div class="layui-input-block">
            <a href="javascript:history.back(-1)" class="layui-btn">返回</a>
        </div>
    </div>
</div>
