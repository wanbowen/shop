<?php
use yii\helpers\Html;
$this->title = '报时报修';
?>
<span class="layui-breadcrumb">
  <a href="/">首页</a>
  <a><cite><?=$this->title?></cite></a>
</span>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>报时报修</legend>
</fieldset>

<form class="layui-form" id="search_form" action="/repair/index" method="get">
    <div class="layui-form-item">
        <input type="hidden" name="limit" value="<?=$limit?>">
        <label class="layui-form-label" style="width: 60px;" >报修单号</label>
        <div class="layui-input-inline" style="width: 200px;">
            <input type="text" name="repair_code" value="<?= !empty($getForm['repair_code'])?$getForm['repair_code']:'';?>" lay-verify="title" autocomplete="off" placeholder="请输入单号" class="layui-input">
        </div>
        <label class="layui-form-label" style="width: 70px;">下单时间:</label>
        <div class="layui-input-inline" style="width: 200px;">
            <input type="text" name="create_time" id="created_at" value="<?= !empty($getForm['create_time'])?$getForm['create_time']:'' ?>" class="layui-input" placeholder="起始时间">
        </div>
        <div class="layui-input-inline" style="width: 200px;">
            <input type="text" name="end_time" id="end_at" value="<?= !empty($getForm['end_time'])?$getForm['end_time']:'' ?>" class="layui-input" placeholder="结束时间">
        </div>
        <div style="height: 10px;clear: both;"></div>
        <label class="layui-form-label" style="width: 60px;">订单状态</label>
        <div class="layui-input-inline" style="width: 150px;">
            <select name="status">
                <option value="-1">请选择状态</option>
                <option value="0" <?= isset($getForm['status']) && $getForm['status']=='0' ? 'selected' : ''?>>待派单</option>
                <option value="1" <?= isset($getForm['status']) && $getForm['status']=='1' ? 'selected' : ''?>>已派单</option>
            </select>
        </div>
        <label class="layui-form-label" style="width: 100px;" >联系人电话</label>
        <div class="layui-input-inline" style="width: 200px;">
            <input type="text" name="contact_tel" value="<?= !empty($getForm['contact_tel'])?$getForm['contact_tel']:'';?>" lay-verify="title" autocomplete="off" placeholder="请输入电话" class="layui-input">
        </div>
        <div class="layui-input-inline" style="width:50px;">
            <button class="layui-btn" lay-submit="" lay-filter="demo1">筛选</button>
        </div>
    </div>
</form>

<table class="layui-table">
    <thead>
    <tr>
        <th>序号</th>
        <th>报修单号</th>
        <th>项目名称</th>
        <th>联系人</th>
        <th>联系人电话</th>
		<th>状态</th>
		<th>下单时间</th>
        <th>操作</th>
    </tr>
    </thead>
    <tbody>
    <?php if(isset($datas) && !empty($datas)){ ?>
        <?php foreach($datas as $key=>$value){?>
            <tr>
                <td><?=$value['id']?></td>
				<td><?=$value['repair_code']?></td>
				<td><?=$value['name']?></td>
				<td><?=$value['contact']?></td>
				<td><?=$value['contact_tel']?></td>
			    <td><?=$value['status']==0 ? '待派单':'已派单'?></td>
			    <td><?=$value['created_at']?></td>
                <td>
                    <?php if($value['status'] == 0){ ?>
                        <a class="layui-btn layui-btn-xs" href="javascript:;"  onclick="order(<?=$value['id']?>)" >派单</a>
                    <?php }?>
                    <a class="layui-btn layui-btn-xs" href="/repair/view?id=<?=$value['id']?>" >查看</a>
                    <a class="layui-btn layui-btn-danger layui-btn-xs" id="<?=$value['id']?>" onclick="Deleteone(this.id)" >删除</a>
                </td>
            </tr>
        <?php }}else{ ?>
        <tr>
            <td colspan='7' style="text-align: center">暂无数据</td>
        </tr>

    <?php }?>

    </tbody>
</table>
<div id="demo3"></div>

<script>
    layui.use(['form', 'layedit', 'laydate','laypage'], function(){
        var form = layui.form
            ,layer = layui.layer
            ,layedit = layui.layedit
            ,laydate = layui.laydate
            ,laypage = layui.laypage;
        laypage.render({
            elem: 'demo3'
            ,count: <?=$count?>
            ,layout:['count','prev', 'page', 'next','limit','skip']
            ,curr:<?=$page?>
			,limit:<?=$limit?>
            ,jump: function(obj, first){
                var page=obj.curr; //得到当前页，以便向服务端请求对应页的数据。
                var limit=obj.limit; //得到每页显示的条数
                var js_str = 'page='+page+'&limit='+limit;
                var form_str = $("#search_form").serialize();
                var form_arr = form_str.split('&');
                form_arr.splice(0,1);
                form_str = form_arr.join("&");
                form_str = js_str+'&'+form_str;
                //首次不执行
                if(!first){
                    window.location.href = "/repair/index?"+form_str;
                }
            }
        });
        laydate.render({
            elem: '#created_at' //指定元素
        });
        laydate.render({
            elem: '#end_at' //指定元素
        });
        form.on('select(pro)', function(data){
            var this_pro_id = data.value;
            var new_staff_data = getStaff(this_pro_id);
            var nhtm = '';
            if(new_staff_data.length > 0) {
                $.each(new_staff_data, function (m, n) {
                    nhtm += '<option value="' + n.id + '">' + n.name + '</option>';
                });
            }else{
                nhtm += '<option value="0">此职业无人员</option>';
            }
            $("#c-staff").html(nhtm);
            form.render('select', 'staf');
        });
    });

    //派单
    function order(id) {
        layer.load();
        var pro_data = getPro();

        var htm = '<form class="layui-form" action="/repair/save" method="post"><div class="layui-form-item" style="padding: 15px 30px 0px 0px;"><label class="layui-form-label">选择职业</label>';
        htm += '<input type="hidden" name="rid" value="'+id+'">';
        htm += '<div class="layui-input-block"><select name="pro_id" lay-filter="pro">';
        var pro_id = 0;
        $.each(pro_data, function (x,y) {
            if(x == 0){
                pro_id = y.id;
            }
            htm += '<option value="'+y.id+'">'+y.name+'</option>';
        });
        htm += '</select></div></div>';

        var staff_data = getStaff(pro_id);
        htm += '<div class="layui-form-item" style="padding: 5px 30px 0px 0px;"><label class="layui-form-label">选择人员</label>';
        htm += '<div class="layui-input-block layui-form" lay-filter="staf"><select name="staff_id" lay-filter="staff" id="c-staff">';
        $.each(staff_data, function (m,n) {
            htm += '<option value="'+n.id+'">'+n.name+'</option>';
        });
        htm += '</select></div></div>';
        htm += '<div style="text-align: center;margin-top: 40px;"><button class="layui-btn" lay-submit>确定</button>';
        htm += '<a class="layui-btn layui-btn-primary" style="margin-left: 60px;" href="javascript:;" onclick="cancle()">取消</a></div></form>';

        layer.open({
            type: 1,
            title: '选择接单人',
            content: htm,
            area: ['500px', '300px']
        });
        layui.form.render();
        layer.closeAll('loading');
    }
    //获取员工职业
    function getPro() {
        var data = '';
        $.ajaxSettings.async = false;
        $.get('/repair/get-profession',{},function (ret) {
            if(ret.code == '200'){
                data = ret.data;
            }else{
                myalert(ret.msg);
                layer.closeAll('loading');
                return false;
            }
        },'json');
        return data;
    }
    //获取员工
    function getStaff(pro_id) {
        var data = '';
        $.ajaxSettings.async = false;
        $.get('/repair/get-staff',{'pro_id':pro_id},function (ret) {
            if(ret.code == '200'){
                data = ret.data;
            }
        },'json');
        return data;
    }
    //取消派单
    function cancle() {
        layer.closeAll();
    }

    //删除
    function Deleteone(obj){
        layer.confirm('确认删除吗？', function(index){
            $.get('/repair/delete',{'id':obj},function (ret) {
                layer.close(index);
                if(ret.code == '200'){
                    reloadalert(ret.msg);
                }else{
                    myalert(ret.msg);
                }
            },'json');
        });
    }
</script>