<?php
use yii\helpers\Html;
$this->title = '用户管理';
?>
<span class="layui-breadcrumb">
  <a href="/">首页</a>
  <a><cite><?=$this->title?></cite></a>
</span>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>黑名单管理</legend>
</fieldset>

<form class="layui-form" id="search_form" action="/user/blacklist" method="get">
    <div class="layui-form-item">
        <input type="hidden" name="page" value="<?=$page?>">
        <input type="hidden" name="limit" value="<?=$limit?>">
        <label class="layui-form-label" style="width: 60px;" >用户姓名</label>
        <div class="layui-input-inline" style="width: 200px;">
            <input type="text" name="name" value="<?= !empty($getForm['name'])?$getForm['name']:'';?>" autocomplete="off" placeholder="请输入用户姓名" class="layui-input">
        </div>
        <label class="layui-form-label" style="width: 60px;" >手机号</label>
        <div class="layui-input-inline" style="width: 200px;">
            <input type="text" name="mobile" value="<?= !empty($getForm['mobile'])?$getForm['mobile']:'';?>" autocomplete="off" placeholder="请输入手机号" class="layui-input">
        </div>
        <div class="layui-input-inline" style="width:50px;">
            <button class="layui-btn" lay-submit="" lay-filter="demo1">筛选</button>
        </div>
    </div>
</form>

<table class="layui-table">
    <thead>
    <tr>
        <th>序号</th>
        <th>头像</th>
        <th>姓名</th>
        <th>手机号</th>
        <th>地址</th>
		<th>创建时间</th>
        <th>操作</th>
    </tr>
    </thead>
    <tbody>
    <?php if(isset($datas) && !empty($datas)){ ?>
        <?php foreach($datas as $key=>$value){?>
            <tr>
                <td><?=$value['id']?></td>
				<td><img src="<?=$value['avatar']?>" width="50px" height="50px" style="border-radius: 50%;" /></td>
				<td><?=$value['name']?></td>
				<td><?=$value['mobile']?></td>
			    <td><?=$value['address']?></td>
			    <td><?=$value['created_at']?></td>
                <td>
                    <a class="layui-btn layui-btn-danger layui-btn-xs" id="<?=$value['id']?>" onclick="Deleteone(this.id)" >撤销黑名单</a>
                </td>
            </tr>
        <?php }}else{ ?>
        <tr>
            <td colspan='7' style="text-align: center">暂无数据</td>
        </tr>

    <?php }?>

    </tbody>
</table>
<div id="demo3"></div>

<script>
    layui.use(['form', 'layedit', 'laydate','laypage'], function(){
        var form = layui.form
            ,layer = layui.layer
            ,layedit = layui.layedit
            ,laydate = layui.laydate
            ,laypage = layui.laypage;
        laypage.render({
            elem: 'demo3'
            ,count: <?=$count?>
            ,layout:['count','prev', 'page', 'next','limit','skip']
            ,curr:<?=$page?>
			,limit:<?=$limit?>
            ,jump: function(obj, first){
                var page=obj.curr; //得到当前页，以便向服务端请求对应页的数据。
                var limit=obj.limit; //得到每页显示的条数
                var js_str = 'page='+page+'&limit='+limit;
                var form_str = $("#search_form").serialize();
                var form_arr = form_str.split('&');
                form_arr.splice(0,1);
                form_str = form_arr.join("&");
                form_str = js_str+'&'+form_str;
                //首次不执行
                if(!first){
                    window.location.href = "/user/blacklist?"+form_str;
                }
            }
        });
        laydate.render({
            elem: '#created_at' //指定元素
        });
        laydate.render({
            elem: '#end_at' //指定元素
        });
    });

    //取消黑名单
    function Deleteone(obj){
        layer.confirm('确认要撤销黑名单吗？', function(index){
            $.get('/user/delete',{'id':obj,'sta':0},function (ret) {
                layer.close(index);
                if(ret.code == '200'){
                    reloadalert(ret.msg);
                }else{
                    myalert(ret.msg);
                }
            },'json');
        });
    }
</script>