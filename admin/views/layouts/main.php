<?php
use yii\helpers\Html;
$controller = Yii::$app->controller->id;
$action =Yii::$app->controller->action->id;
use admin\controllers\CheckpreController;
$menus=CheckpreController::menu();
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>

  <meta charset="<?= Yii::$app->charset ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
  <?= Html::csrfMetaTags() ?>
  <title><?= Html::encode($this->title) ?></title>
  <link rel="stylesheet" href="/layui/css/layui.css">
  <script src="/layui/layui.js"></script>
    <script type="text/javascript" src="/js/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="/js/common.js"></script>
  <?php $this->head() ?>
</head>
<body class="layui-layout-body">
<?php $this->beginBody() ?>
<div class="layui-layout layui-layout-admin">
  <div class="layui-header">
    <div class="layui-logo">金安云物业管理后台</div>
    <ul class="layui-nav layui-layout-right">
		<li class="layui-nav-item">
        <a href="javascript:;">
          <img src="/images/5db11ff4gw1e77d3nqrv8j203b03cweg.jpg" class="layui-nav-img">
         <?=!empty(Yii::$app->user->identity->username)? Yii::$app->user->identity->username:""?>
        </a>
		<dl class="layui-nav-child">
		  <dd><a id="<?=!empty(Yii::$app->user->identity->id)? Yii::$app->user->identity->id:"0"?>" onclick="personalset(this.id)">修改信息</a></dd>
		  <dd><a href="/site/logout">安全退出</a></dd>
		</dl>
      </li>
    </ul>
  </div>

  <div class="layui-side layui-bg-black">
    <div class="layui-side-scroll">
      <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
      <ul class="layui-nav layui-nav-tree"  lay-filter="test">
	  <?php if(!empty($menus)){ foreach($menus as $key=>$value){ ?>
        <li class="layui-nav-item <?php if (in_array($controller,$value['active'])) echo ' layui-nav-itemed'?>">
          <a class="" href="javascript:;"><?=$value['name']?></a>

          <dl class="layui-nav-child">
		   <?php if(!empty($value['menu'])){ foreach($value['menu'] as $keys=>$values){ ?>
		   <dd><a href="<?='/'.$values['c'].'/'.$values['a']?>" style="<?php if ($controller == $values['m'] && $action == $values['a']) echo 'color:#009688'?>"><?=$values['name']?></a>
		   </dd>
			<?php }}?>
          </dl>
        </li>
	  <?php }}?>


      </ul>
    </div>
  </div>

  <div class="layui-body">
    <!-- 内容主体区域 -->
    <div style="padding: 15px;"> <?= $content ?></div>
  </div>

  <div class="layui-footer">
    <!-- 底部固定区域 -->
      <div style="text-align: center">© 2019 沈阳华茂鑫玺科技有限公司</div>
  </div>
</div>
<script>
//JavaScript代码区域
layui.use(['element','jquery'], function(){
	var element = layui.element;
	var $ = layui.jquery;
});
function personalset(obj){
	layer.open({
	  title: '修改个人信息',
	  type: 2,
	  area: ['400px', '520px'],
	  skin: 'layui-layer-rim', //加上边框
	  content: '/site/personalset/'+obj, //iframe的url，no代表不显示滚动条
	});
}
</script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>