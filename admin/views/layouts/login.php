<?php
use yii\helpers\Url;
use yii\helpers\Html;
use admin\assets\AppAsset;
/* @var $this \yii\web\View */
/* @var $content string */
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>

</head>

<body style="">
<div class="container-fluid">
    <?php $this->beginBody() ?>
     <?= $content ?>
    <?php $this->endBody() ?>
</div>
</body>
<?php $this->endPage() ?>
</html>