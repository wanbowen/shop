<?php

use yii\helpers\Html;

$this->title = '商城商品管理';
?>
<span class="layui-breadcrumb">
  <a href="/">首页</a>
  <a><cite><?=$this->title?></cite></a>
</span>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>编辑商品</legend>
</fieldset>

<style>
    .layui-form-label{width: 110px;}
    .thumbnail_close{font-size: 26px;position: absolute;top: 37px;right: -12px;}
    .detail_close{font-size: 26px;position: absolute;top: -12px;right: -12px;}
    .layui-form-select dl{z-index: 1000;}
</style>

<form class="layui-form" action="/sgoods/save" method='post'>
    <input type="hidden" value="<?php echo Yii::$app->request->csrfToken; ?>" name="_csrf" >
    <input type="hidden" value="<?=$data['id']?>" name="id" >
    <div class="layui-form-item">
        <label class="layui-form-label">商品名称</label>
        <div class="layui-input-inline" style="width: 50%">
            <input type="text" name="name" value="<?=$data['name']?>" required  lay-verify="required" placeholder="请输入商品名称" autocomplete="off" class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">分类</label>
        <div class="layui-input-inline" style="width: 6%">
            <a class="layui-btn" onclick="checkcate()" lay-filter="">选择</a>
        </div>
        <div class="layui-input-inline" id="category" style="width: 40%;display: block">
            <input type="hidden" name="category_id" value="<?=$data['category_id']?>" autocomplete="off" class="layui-input">
            <input type="text" name="cate_name" value="<?=$cate_data['name']?>" autocomplete="off" readonly class="layui-input">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">价格</label>
        <div class="layui-input-inline" style="width: 10%">
            <input type="text" name="price" value="<?=$data['price']?>" required  lay-verify="required" placeholder="￥" autocomplete="off" class="layui-input">
        </div>
        <div class="layui-form-mid">单位</div>
        <div class="layui-input-inline" style="width: 100px;">
            <select name="unit" lay-verify="required">
                <?php if(!empty($unit)){ ?>
                    <?php foreach($unit as $key=>$val){ ?>
                        <option value="<?=$val['name']?>" <?= $data['unit']==$val['name']?'selected':'';?>><?=$val['name']?></option>
                    <?php }}?>
            </select>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">内容</label>
        <div class="layui-input-inline" style="width: 60%;">
            <!-- 加载编辑器的容器 -->
            <textarea name="content" id="container" style="width:100%;height:300px"><?=$data['content']?></textarea>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">缩略图片</label>
        <div class="layui-inline">
            <div class="layui-upload">
                <button type="button" class="layui-btn" id="levelimg">点击上传</button>
                <div class="layui-upload-list" id="thumbnail_show" style="width: 200px;height: 200px;">
                    <img src="<?=$data['thumbnail']?>" style="width: 100%;height: 100%;" />
                    <i class="layui-icon thumbnail_close" onclick="thumbnail_close()">&#x1007;</i>
                </div>
            </div>
            <input type="hidden" id='thumbnail' value="<?=$data['thumbnail']?>" name="thumbnail" required  lay-verify="required">
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">详情图片</label>
        <div class="layui-upload" style="width: 80%;margin-left: 140px;">
            <button type="button" class="layui-btn" id="detailimg">点击上传</button>
            <div class="layui-upload-list" id="detail_show" style="width: 100%;">
                <?php if(!empty($detail_imgs)){ ?>
                    <?php foreach($detail_imgs as $key=>$val){ ?>
                        <div class="layui-input-inline" style="width: 200px;height: 200px;margin-bottom: 10px;">
                            <img src="<?=$val?>" style="width: 100%;height: 100%;">
                            <i class="layui-icon detail_close" onclick="detail_close(this)">&#x1007;</i>
                            <input type="hidden" value="<?=$val?>" name="detail_imgs[]" >
                        </div>
                    <?php }}?>
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">状态</label>
        <div class="layui-input-inline" style="width: 24%">
            <input type="radio" name="status" value="0" title="上架" <?= $data['status']==0?'checked':'';?>>
            <input type="radio" name="status" value="1" title="下架" <?= $data['status']==1?'checked':'';?>>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <button class="layui-btn" lay-submit lay-filter="formDemo">立即提交</button>
            <button type="reset" id="resetue" class="layui-btn layui-btn-primary">重置</button>
        </div>
    </div>
</form>

<!-- 上传文件地址 -->
<script>
    var uploadFileUrl = '/base/uploadfile';
    var filedomin = "<?= Yii::$app->params['fileDomain'] ?>";
</script>
<!-- 配置文件 -->
<script type="text/javascript" src="/utf/ueditor.config.js"></script>
<!-- 编辑器源码文件 -->
<script type="text/javascript" src="/utf/ueditor.all.js"></script>
<script>
    <!-- 实例化编辑器 -->
    var ue = UE.getEditor('container',{
        autoHeightEnabled: false,
        autoFloatEnabled:false //是否保持toolbar位置不动
    });
    ue.ready(function() {
        //清空编辑器的内容
        $("#resetue").click(function () {
            ue.setContent('');
        });
    });

    //Demo
    layui.use(['form', 'upload', 'laydate', 'element'], function(){
        var form = layui.form
            ,layer = layui.layer
            ,upload = layui.upload
            ,element = layui.element
            ,laydate = layui.laydate;

        //缩略图片上传
        var uploadInst = upload.render({
            elem: '#levelimg'
            ,field: 'upfile'
            ,accept: 'images'
            ,url: uploadFileUrl
            ,done: function(res){
                if(res.state == 'SUCCESS'){
                    var htm = '<img src="'+res.url+'" style="width: 100%;height: 100%;" />';
                    htm += '<i class="layui-icon thumbnail_close" onclick="thumbnail_close()">&#x1007;</i>';
                    var imgurl = res.url.replace(filedomin,"");
                    $("#thumbnail_show").html(htm);
                    $("#thumbnail_show").show();
                    $("#thumbnail").val(imgurl);
                }else{
                    myalert(res.state);
                }
            }
            ,error: function(){
                myalert('上传失败，请刷新页面重试');
            }
        });
        //详情图片上传
        var uploadInst = upload.render({
            elem: '#detailimg'
            ,field: 'upfile'
            ,accept: 'images'
            ,url: uploadFileUrl
            ,done: function(res){
                if(res.state == 'SUCCESS'){
                    var imgurl = res.url.replace(filedomin,"");
                    var htm = '<div class="layui-input-inline" style="width: 200px;height: 200px;margin-bottom: 10px;">';
                    htm += '<img src="'+res.url+'" style="width: 100%;height: 100%;">';
                    htm += '<i class="layui-icon detail_close" onclick="detail_close(this)">&#x1007;</i>';
                    htm += '<input type="hidden" value="'+imgurl+'" name="detail_imgs[]" >';
                    htm += '</div>';
                    $("#detail_show").append(htm);
                    $("#detail_show").show();
                }else{
                    myalert(res.state);
                }
            }
            ,error: function(){
                myalert('上传失败，请刷新页面重试');
            }
        });
    });

    function checkcate() {
        layer.load();
        $.get('/sgoods/get-category',{},function (ret) {
            layer.closeAll('loading');
            if(ret.code == '200'){
                var htm = '<div class="layui-collapse" lay-accordion>';
                $.each(ret.data, function (x,y) {
                    htm += '<div class="layui-colla-item"><h2 class="layui-colla-title">'+y.name+'</h2>';
                    htm += '<div class="layui-colla-content">';
                    htm += '<table class="layui-table" lay-skin="nob" style="margin: 0">';
                    if(y.son != '') {
                        $.each(y.son, function (m, n) {
                            htm += '<tr onclick="checkthis('+n.id+',\''+n.name+'\')"><td>'+n.name+'</td></tr>';
                        });
                    }else{
                        htm += '<tr><td>暂无数据</td></tr>';
                    }
                    htm += '</table></div></div>';
                });
                htm += '</div>';

                layer.open({
                    type: 1,
                    title: '选择商品分类',
                    content: htm,
                    area: ['500px', '500px']
                });
                layui.element.init();
                layer.closeAll('loading');
            }else{
                myalert(ret.msg);
                return false;
            }
        },'json');
    }

    function checkthis(cid,cname) {
        $("#category").find('input[name="category_id"]').val(cid);
        $("#category").find('input[name="cate_name"]').val(cname);
        layer.closeAll();
        $("#category").show();
    }

    function thumbnail_close() {
        $("#thumbnail").val('');
        $("#thumbnail_show").hide();
    }

    function detail_close(obj) {
        $(obj).parent().remove();
    }
</script>


