<?php
use yii\helpers\Html;
$this->title = '商城商品管理';
?>
<span class="layui-breadcrumb">
  <a href="/">首页</a>
  <a><cite><?=$this->title?></cite></a>
</span>
<fieldset class="layui-elem-field layui-field-title" style="margin-top: 20px;">
    <legend>商城商品管理</legend>
</fieldset>

<form class="layui-form" id="search_form" action="/sgoods/index" method="get">
    <div class="layui-form-item">
        <input type="hidden" name="limit" value="<?=$limit?>">
        <label class="layui-form-label" style="width: 60px;" >商品名称</label>
        <div class="layui-input-inline" style="width: 200px;">
            <input type="text" name="name" value="<?= !empty($getForm['name'])?$getForm['name']:'';?>" autocomplete="off" placeholder="请输入商品名称" class="layui-input">
        </div>
        <!-- <label class="layui-form-label" style="width: 60px;" >分类</label>
        <div class="layui-input-inline" style="width: 200px;">
            <select name="category_id" lay-verify="">
                <option value="">请选择分类</option>
                <?php if(!empty($cate_data)){ ?>
                <?php foreach($cate_data as $key=>$val){ ?>
                    <option value="<?=$val['id']?>" <?= !empty($getForm['category_id'])&&$getForm['category_id']==$val['id']?'selected':'';?>><?=$val['name']?></option>
                <?php }}?>
            </select>
        </div> -->
        <label class="layui-form-label">
            价格区间
        </label>
        <div class="layui-input-inline" style="width: 100px;">
            <input type="text" name="price_min" value="<?= !empty($getForm['price_min'])?$getForm['price_min']:'';?>" placeholder="￥" autocomplete="off"
            class="layui-input">
        </div>
        <div class="layui-form-mid">
            -
        </div>
        <div class="layui-input-inline" style="width: 100px;">
            <input type="text" name="price_max" value="<?= !empty($getForm['price_max'])?$getForm['price_max']:'';?>" placeholder="￥" autocomplete="off"
            class="layui-input">
        </div>


    </div>
    <div class="layui-form-item">
        <label class="layui-form-label" style="width: 60px;" >是否推荐</label>
        <div class="layui-input-inline" style="width: 200px;">
            <select name="is_recommend" lay-verify="">
                <option value="">请选择是否推荐</option>
                <option value="0" <?= isset($getForm['is_recommend'])&&$getForm['is_recommend']=='0'?'selected':'';?>>已推荐</option>
                <option value="1" <?= isset($getForm['is_recommend'])&&$getForm['is_recommend']==1?'selected':'';?>>未推荐</option>
            </select>
        </div>
        <label class="layui-form-label" style="width: 60px;margin-left:20px" >是否上架</label>
        <div class="layui-input-inline" style="width: 224px;">
            <select name="status" lay-verify="">
                <option value="">请选择是否上架</option>
                <option value="0" <?= isset($getForm['status'])&&$getForm['status']=='0'?'selected':'';?>>已上架</option>
                <option value="1" <?= isset($getForm['status'])&&$getForm['status']==1?'selected':'';?>>已下架</option>
            </select>
        </div>
        <div class="layui-input-inline" style="width:50px;">
            <button class="layui-btn" lay-submit="" lay-filter="demo1">筛选</button>
        </div>
    </div>
</form>

<a href="/sgoods/add" class="layui-btn layui-btn-sm"><i class="layui-icon"></i>新增</a>

<table class="layui-table">
    <thead>
    <tr>
        <th>序号</th>
        <th>所属分类</th>
        <th>商品名称</th>
        <th>价格与库存</th>
        <th>状态</th>
        <th>是否推荐</th>
		<th>创建时间</th>
        <th>操作</th>
    </tr>
    </thead>
    <tbody>
    <?php if(isset($datas) && !empty($datas)){ ?>
        <?php foreach($datas as $key=>$value){?>
            <tr>
                <td><?=$value['id']?></td>
				<td><?=$value['category_name']?></td>
                <td><?=$value['goods_name']?></td>
				<td><?php
                    // 判断是否有规格
                    if ($value['is_specs'] == 0) {
                        // 无规格
                        $price_Arr = explode(':!' , $value['price']);
                        echo "价格：".$price_Arr[0] .' ; 库存：'.$price_Arr[1] ;
                    } else {
                        $price_Arr = explode('%!' , $value['price']);
                        foreach ($price_Arr as $k => $v) {
                            $price_Spece_Arr = explode(':!', $v);
                            echo $price_Spece_Arr[1].";价格：".$price_Spece_Arr[2] .' ; 库存：'.$price_Spece_Arr[3] ."<br />";
                        }
                    }
                    ?>
                </td>
                <td><?=$value['status']==0 ? '<span style="color:green">上架中</span>':'<span style="color:red">已下架</span>'?></td>
				<td><?=$value['is_recommend']==0 ? '<span style="color:red">已推荐</span>':'未推荐'?></td>
			    <td><?=$value['created_at']?></td>
                <td>
                    <a class="layui-btn layui-btn-xs" href="/sgoods/edit?id=<?=$value['id']?>" >编辑</a>
                    <a class="layui-btn layui-btn-danger layui-btn-xs" id="<?=$value['id']?>" onclick="is_status(this.id,<?=$value['status']==0 ? 1:0?>,'<?=$value['status']==0 ? '下架':'上架'?>')" ><?=$value['status']==0 ? '下架':'上架'?></a>
                    <a class="layui-btn layui-btn-danger layui-btn-xs" id="<?=$value['id']?>" onclick="recommend(this.id,<?=$value['is_recommend']==0 ? 1:0?>,'<?=$value['is_recommend']==0 ? '取消推荐':'推荐'?>')" ><?=$value['is_recommend']==0 ? '取消推荐':'推荐'?></a>
                    <a class="layui-btn layui-btn-danger layui-btn-xs" id="<?=$value['id']?>" onclick="Deleteone(this.id)" >删除</a>
                </td>
            </tr>
        <?php }}else{ ?>
        <tr>
            <td colspan='7' style="text-align: center">暂无数据</td>
        </tr>

    <?php }?>

    </tbody>
</table>
<div id="demo3"></div>

<script>
    layui.use(['form', 'layedit', 'laydate','laypage'], function(){
        var form = layui.form
            ,layer = layui.layer
            ,layedit = layui.layedit
            ,laydate = layui.laydate
            ,laypage = layui.laypage;
        laypage.render({
            elem: 'demo3'
            ,count: <?=$count?>
            ,layout:['count','prev', 'page', 'next','limit','skip']
            ,curr:<?=$page?>
			,limit:<?=$limit?>
            ,jump: function(obj, first){
                var page=obj.curr; //得到当前页，以便向服务端请求对应页的数据。
                var limit=obj.limit; //得到每页显示的条数
                var js_str = 'page='+page+'&limit='+limit;
                var form_str = $("#search_form").serialize();
                var form_arr = form_str.split('&');
                form_arr.splice(0,1);
                form_str = form_arr.join("&");
                form_str = js_str+'&'+form_str;
                //首次不执行
                if(!first){
                    window.location.href = "/sgoods/index?"+form_str;
                }
            }
        });
        laydate.render({
            elem: '#created_at' //指定元素
        });
        laydate.render({
            elem: '#end_at' //指定元素
        });
    });

    //删除
    function Deleteone(obj){
        layer.confirm('确认要删除吗？', function(index){
            $.get('/sgoods/delete',{'id':obj},function (ret) {
                layer.close(index);
                if(ret.code == '200'){
                    reloadalert(ret.msg);
                }else{
                    myalert(ret.msg);
                }
            },'json');
        });
    }

    //上架下架
    function is_status(obj,num,msg1){
        layer.confirm('确认要' + msg1 + '吗？', function(index){
            $.get('/sgoods/is_status',{'id':obj,'val':num},function (ret) {
                layer.close(index);
                if(ret.code == '200'){
                    reloadalert(ret.msg);
                }else{
                    myalert(ret.msg);
                }
            },'json');
        });
    }

    //上架下架
    function recommend(obj,num,msg1){
        layer.confirm('确认要' + msg1 + '吗？', function(index){
            $.get('/sgoods/recommend',{'id':obj,'val':num},function (ret) {
                layer.close(index);
                if(ret.code == '200'){
                    reloadalert(ret.msg);
                }else{
                    myalert(ret.msg);
                }
            },'json');
        });
    }

</script>