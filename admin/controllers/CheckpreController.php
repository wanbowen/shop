<?php
namespace admin\controllers;
use  Yii;
use admin\models\AdminPre;
use admin\models\AdminRolePre;
class CheckpreController
{
    
	/**
     * 返回权限id数组
     * @param  $roleid=>角色id,
     * @return array
     * @author ycl
     * @date 2018/4/17
     */
	public  static function returnpre($roleid)
    {
		 $AdminRolePre=AdminRolePre::find()->select(['preid'])->where(['roleid'=>$roleid])->asArray()->all();
		 $array=array();
		 foreach($AdminRolePre as $key=>$value){
			 $array[$key]=$value['preid'];
		 }
		 return $array;
    }
	/**
     * 检查权限
     * @param  $controller=>控制器id,$action=>方法id,
     * @return html
     * @author ycl
     * @date 2018/4/17
     */
	public  static function checkpre($controller,$action)
    {
		$roleid=!empty(Yii::$app->user->identity->roleid)? Yii::$app->user->identity->roleid:"";
		if(empty($roleid)){
			self::alert('未分配角色,请联系管理员','/site/login');exit;
		}
		$pre=self::returnpre($roleid);
		$AdminPre=AdminPre::find()->where(['c'=>$controller,'a'=>$action])->one();
		if (!isset($AdminPre)|| !in_array($AdminPre->id, $pre)){
			self::alert('没有权限');exit;
		}
    }
	/**
     * 菜单
     * @return array
     * @author ycl
     * @date 2018/4/17
     */
	public  static function menu()
    {
		$roleid=!empty(Yii::$app->user->identity->roleid)? Yii::$app->user->identity->roleid:"";
		if(empty($roleid)){
			self::alert('未分配角色或登录失败,请联系管理员','/site/login');exit;
		}
		$pre=self::returnpre($roleid);
		$AdminPre=AdminPre::find()->where(['in', 'id', $pre])->orderBy('sort asc')->asArray()->all();
		$menus=array();
		if(!empty($AdminPre)){
			foreach($AdminPre as $key=>$value){
				if($value['pId']=='0'){
					$menus[$value['id']]=$value;
					$AdminPretwo=AdminPre::find()->where(['pId'=>$value['id']])->orderBy('sort asc')->asArray()->all();
					if(!empty($AdminPretwo)){
						foreach($AdminPretwo as $keyo=>$valueo){
							if (in_array($valueo['id'], $pre)){
							  $menus[$value['id']]['menu'][$valueo['id']]=$valueo;
							  $menus[$value['id']]['active'][]=$valueo['c'];
							}			
						}
					}
				}
			}
		}
		return $menus;
    }
	public static function alert($message,$url='')
    {
        echo "<!DOCTYPE html>
			   <html>
               <head> 
				   <meta charset='UTF-8'>
                   <meta name='viewport' content='width=device-width, height=device-height, initial-scale=1,minimum-scale=1, maximum-scale=1, user-scalable=0'>
				   <link rel='stylesheet' href='/layui/css/layui.css'>
				   <script src='/layui/layui.js'></script>
               </head>
               <script type='text/javascript'>
			        layui.use('layer', function(){
					var $ = layui.jquery, layer = layui.layer; 
				    layer.ready(function(){
						layer.alert('".$message."',{
						  title: '提示信息'
						  ,skin: 'layui-layer-molv'
						  ,btnAlign: 'c'
						  ,closeBtn: 0
						  ,anim: 4 //动画类型
						}, function(){
						    if('".$url."' == ''){
						        window.history.go(-1);
						    }else{
						        window.location.href = '".$url."';
						   }
						});
						});
					}); 					
                </script>
               </html>";exit;
    }
}
