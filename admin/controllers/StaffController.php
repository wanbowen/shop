<?php
namespace admin\controllers;

use Yii;
use yii\data\Pagination;
use admin\models\Admin;
use common\models\Staff;
use common\models\StaffProfession;

/*
 * 员工信息管理
 */
class StaffController extends BaseController{

    public function init()
    {
        parent::init();
    }

    /*
     * 首页
     */
    public function actionIndex(){
        //筛选条件
        $getForm = Yii::$app->request->get();
        $query = new \yii\db\Query();
        $query = $query->select('s.*,p.name as pro_name');
        $query = $query->from('hm_staff as s');
        $query = $query->leftJoin('hm_staff_profession as p', 'p.id = s.profession_id');

        $where['s.is_del'] = 0;   //正常
        if (isset($getForm['profession_id']) && $getForm['profession_id'] != '') {      //职业
            $where['s.profession_id'] = $getForm['profession_id'];
        }
        $query = $query->where($where);
        if (isset($getForm['name']) && $getForm['name'] != '') {      //姓名
            $query = $query->andFilterWhere(['like', 's.name', $getForm['name']]);
        }
        if (isset($getForm['mobile']) && $getForm['mobile'] != '') {      //手机号
            $query = $query->andFilterWhere(['like', 's.mobile', $getForm['mobile']]);
        }

        $page = (isset($getForm['page']) && $getForm['page']) ? $getForm['page'] : 1;
        $limit = (isset($getForm['limit']) && $getForm['limit']) ? $getForm['limit'] : 10;

        $allcount = $query->count();
        $pagination = new Pagination([
            'defaultPageSize' => $limit,
            'totalCount' => $allcount
        ]);
        $allData = $query->orderBy('id DESC')->offset($pagination->offset)->limit($pagination->limit)->all();

        //职业数据
        $pro_data = StaffProfession::find()->select('id,name')->where(['is_del'=>0])->asArray()->all();

        return $this->render('index', [
            'datas'=>$allData,
            'pro_data'=>$pro_data,
            'getForm' => $getForm,//筛选条件
            'page' => $page,
            'limit' => $limit,
            'count' => $allcount
        ]);
    }

    /*
     * 添加员工
     */
    public function actionAdd(){
        $pro_data = StaffProfession::find()->select('id,name')->where(['is_del'=>0])->asArray()->all();
        if(empty($pro_data)){
            $this->alert('请先添加员工分类！');exit;
        }

        return $this->render('add',[
            'pro_data' => $pro_data
        ]);
    }

    /*
     * 编辑员工
     */
    public function actionEdit(){
        $id = Yii::$app->request->get('id');
        if(empty($id)){
            $this->alert('系统错误，请重试！');exit;
        }

        $data = Staff::find()->where(['id' => $id, 'is_del'=> 0])->asArray()->one();
        if(empty($data)){
            $this->alert('系统错误，请重试！');exit;
        }

        $admin_data = Admin::find()->where(['id'=>$data['admin_id']])->asArray()->one();

        $pro_data = StaffProfession::find()->select('id,name')->where(['is_del'=>0])->asArray()->all();
        if(empty($pro_data)){
            $this->alert('请先添加员工分类！');exit;
        }

        return $this->render('edit',[
            'data' => $data,
            'admin_data' => $admin_data,
            'pro_data' => $pro_data
        ]);
    }

    /*
     * 保存数据
     */
    public function actionSave()
    {
        if(Yii::$app->request->post()){
            $form = Yii::$app->request->post();

            $record = '';
            if(isset($form['id']) && !empty($form['id'])){
                $_model = Staff::find()->where(['id' => $form['id']])->one();
                $admin_model = Admin::find()->where(['id' => $_model->admin_id])->one();
                if($admin_model->username != $form['admin']){
                    $yanzheng = Admin::find()->where(['username'=>$form['admin']])->count();
                    if($yanzheng > 0){
                        $this->alert('员工账号名称已经存在！');exit;
                    }
                    $admin_model->username = $form['admin'];
                }
                if($admin_model->mpassword != $form['password']){
                    $admin_model->password_hash = $admin_model->setPassword($form['password']);
                    $admin_model->auth_key = $admin_model->generateAuthKey();
                    $admin_model->mpassword = $form['password'];
                }
                $admin_model->save();
                $this->addAdminLog('管理员账号编辑','表:hm_admin,id:'.$admin_model->id);
                $record = '员工编辑';
            }else{
                $_model = new Staff();
                //添加为管理员（员工角色）
                $user = new Admin();
                $yanzheng = Admin::find()->where(['username'=>$form['admin']])->count();
                if($yanzheng > 0){
                    $this->alert('员工账号名称已经存在！');exit;
                }
                $user->username = $form['admin'];
                $user->password_hash = $user->setPassword($form['password']);
                $user->mpassword = $form['password'];
                $user->auth_key = $user->generateAuthKey();
                $user->roleid = 2; //员工角色
                $user->created_at = date('Y-m-d H:i:s');
                $user->updated_at = date('Y-m-d H:i:s');
                if($user->save()){
                    $this->addAdminLog('管理员账号添加','表:hm_admin,id:'.$user->id);
                    $_model->admin_id = $user->id;
                }
                $_model->created_at = date('Y-m-d H:i:s');
                $record = '员工添加';
            }
            $_model->name = $form['name'];
            $_model->age = $form['age'];
            $_model->sex = $form['sex'];
            $_model->mobile = $form['mobile'];
            $_model->profession_id = $form['profession_id'];
            $_model->address = $form['address'];
            if($_model->save()){
                $this->addAdminLog($record,'表:hm_staff,id:'.$_model->id);
                $this->alert('提交成功', '/staff/index');exit;
            }else{
                $this->alert('提交失败，请重试！');exit;
            }
        }
    }

    /*
     * 删除
     */
    public function actionDelete()
    {
        $id = Yii::$app->request->get('id');
        if(empty($id)){
            $this->json('400','系统错误，请重试！');
        }

        $model = Staff::find()->where(['id' => $id])->one();
        $model->is_del = 1;
        $model->save();

        $this->addAdminLog('员工删除','表:hm_staff,id:'.$model->id);

        $this->json('200','操作成功');
    }
}