<?php
namespace admin\controllers;

use Yii;
use yii\data\Pagination;
use common\models\Goods;
use common\models\GoodsCategory;

/*
 * 小程序  首页分类管理
 */
class CategoryController extends BaseController{

    public function init()
    {
        parent::init();
    }

    /*
     * 首页(一级分类)
     */
    public function actionIndex()
    {
        //筛选条件
        $getForm = Yii::$app->request->get();
        $query = GoodsCategory::find();

        $where['is_del'] = 0;
        $where['pid'] = 0;
        $query = $query->where($where);
        if (isset($getForm['name']) && $getForm['name'] != '') {      //名称
            $query = $query->andFilterWhere(['like', 'name', $getForm['name']]);
        }

        $page = (isset($getForm['page']) && $getForm['page']) ? $getForm['page'] : 1;
        $limit = (isset($getForm['limit']) && $getForm['limit']) ? $getForm['limit'] : 10;

        $allcount = $query->count();
        $pagination = new Pagination([
            'defaultPageSize' => $limit,
            'totalCount' => $allcount
        ]);
        $allData = $query->orderBy('id ASC')->offset($pagination->offset)->limit($pagination->limit)->all();

        return $this->render('index', [
            'datas'=>$allData,
            'getForm' => $getForm,//筛选条件
            'page' => $page,
            'limit' => $limit,
            'count' => $allcount
        ]);
    }

    /*
     * 添加(编辑)一级分类
     */
    public function actionSaveOne()
    {
        $id = Yii::$app->request->get('id');
        $record = '';
        if(isset($id) && !empty($id)){
            $model = GoodsCategory::find()->where(['id'=>$id])->one();
            $record = '分类编辑';
        }else{
            $model = new GoodsCategory();
            $model->created_at = date('Y-m-d H:i:s');
            $record = '分类添加';
        }
        if(Yii::$app->request->post()){
            $model->name = Yii::$app->request->post('name');
            if(!isset($id) || empty($id)) {
                //查询分类名称是否已存在
                $cate = GoodsCategory::find()->where(['name' => $model->name, 'is_del' => 0])->one();
                if (!empty($cate)) {
                    $this->alert('分类名称已存在');
                    exit;
                }
            }
            if($model->save()){
                $this->addAdminLog($record,'表:hm_goods_category,id:'.$model->id);
                $this->alert('提交成功','/category/index');exit;
            }else{
                $this->alert('提交失败，请重试！');exit;
            }
        }else {
            return $this->render('save', [
                'data' => $model
            ]);
        }
    }

    /*
     * 删除 一级分类
     */
    public function actionDeleteOne()
    {
        $id = Yii::$app->request->get('id');
        if(empty($id)){
            $this->json('400','系统错误，请重试！');
        }
        //查找是否存在二级分类
        $two = GoodsCategory::find()->where(['pid'=>$id, 'is_del'=> 0])->one();
        if(!empty($two)){
            $this->json('400','此分类下存在二级分类，不可删除！');
        }
        //查找分类下是否存在商品
        $data = Goods::find()->where(['category_id' => $id, 'is_del'=> 0])->asArray()->one();
        if(!empty($data)){
            $this->json('400','此分类下存在商品，不可删除！');
        }

        $model = GoodsCategory::find()->where(['id' => $id])->one();
        $model->is_del = 1;
        $model->deleted_at = date('Y-m-d H:i:s');
        $model->save();
        $this->addAdminLog('分类删除','表:hm_goods_category,id:'.$model->id);

        $this->json('200','操作成功');
    }

    /*
     * 查看二级分类
     */
    public function actionAttachment()
    {
        //筛选条件
        $getForm = Yii::$app->request->get();
        $query = GoodsCategory::find();

        $where['is_del'] = 0;
        if (isset($getForm['pid']) && $getForm['pid'] >= 0) {    //状态
            $where['pid'] = $getForm['pid'];
        }else{
            $this->alert('系统错误，请重试！');exit;
        }
        $query = $query->where($where);
        if (isset($getForm['name']) && $getForm['name'] != '') {      //名称
            $query = $query->andFilterWhere(['like', 'name', $getForm['name']]);
        }

        $page = (isset($getForm['page']) && $getForm['page']) ? $getForm['page'] : 1;
        $limit = (isset($getForm['limit']) && $getForm['limit']) ? $getForm['limit'] : 10;

        $allcount = $query->count();
        $pagination = new Pagination([
            'defaultPageSize' => $limit,
            'totalCount' => $allcount
        ]);
        $allData = $query->orderBy('id ASC')->offset($pagination->offset)->limit($pagination->limit)->all();

        $pcate = GoodsCategory::find()->where(['id' => $getForm['pid']])->one();
        $title = $pcate->name;

        return $this->render('attachment', [
            'datas'=>$allData,
            'getForm' => $getForm,//筛选条件
            'ptitle' => $title,
            'pid' => $getForm['pid'],
            'page' => $page,
            'limit' => $limit,
            'count' => $allcount
        ]);
    }

    /*
     * 添加(编辑)二级分类
     */
    public function actionSaveTwo()
    {
        $id = Yii::$app->request->get('id');
        $record = '';
        if(isset($id) && !empty($id)){
            $model = GoodsCategory::find()->where(['id'=>$id])->one();
            $record = '分类编辑';
        }else{
            $model = new GoodsCategory();
            $model->created_at = date('Y-m-d H:i:s');
            $record = '分类添加';
        }
        if(Yii::$app->request->post()){
            $form = Yii::$app->request->post();
            if(empty($form['banner_img'])){
                $this->alert('请上传图片');exit;
            }
            if(empty($form['icon_img'])){
                $this->alert('请上传图标');exit;
            }
            if(!isset($id) || empty($id)) {
                //查询分类名称是否已存在
                $cate = GoodsCategory::find()->where(['name' => $form['name'], 'is_del' => 0])->one();
                if (!empty($cate)) {
                    $this->alert('分类名称已存在');
                    exit;
                }
            }
            $icon_img = str_replace(Yii::$app->params['fileDomain'], '', $form['icon_img']);
            $banner_img = str_replace(Yii::$app->params['fileDomain'], '', $form['banner_img']);
            $model->pid = $form['pid'];
            $model->name = $form['name'];
            $model->icon_img = $icon_img;
            $model->banner_img = $banner_img;
            if($model->save()){
                $this->addAdminLog($record,'表:hm_goods_category,id:'.$model->id);
                $this->alert('提交成功','/category/attachment?pid='.$form['pid']);exit;
            }else{
                $this->alert('提交失败，请重试！');exit;
            }
        }else {
            $pid = Yii::$app->request->get('pid');
            return $this->render('save_two', [
                'data' => $model,
                'pid' => $pid
            ]);
        }
    }
}