<?php
namespace admin\controllers;

use Yii;
use yii\data\Pagination;
use common\models\Coupon;
use common\models\CouponShare;
use common\models\CouponConfig;

/*
 * 小程序  首页分类管理
 */
class CouponController extends BaseController{

    public function init()
    {
        parent::init();
    }

    /*
     * 首页
     */
    public function actionIndex()
    {
        //筛选条件
        $getForm = Yii::$app->request->get();
        $query = Coupon::find();

        $where['is_del'] = 0;
        if (isset($getForm['status']) && $getForm['status'] >= 0) {    //状态
            $where['status'] = $getForm['status'];
        }
        $query = $query->where($where);
        if (isset($getForm['name']) && $getForm['name'] != '') {      //名称
            $query = $query->andFilterWhere(['like', 'name', $getForm['name']]);
        }

        $page = (isset($getForm['page']) && $getForm['page']) ? $getForm['page'] : 1;
        $limit = (isset($getForm['limit']) && $getForm['limit']) ? $getForm['limit'] : 10;

        $allcount = $query->count();
        $pagination = new Pagination([
            'defaultPageSize' => $limit,
            'totalCount' => $allcount
        ]);
        $allData = $query->orderBy('id ASC')->offset($pagination->offset)->limit($pagination->limit)->all();

        //优惠券开关配置
        $config = CouponConfig::find()->one();
        $switch = !empty($config) ? $config->switch : 0;
        //分享优惠券
        $share = CouponShare::find()->one();
        $share_name = '';
        if(!empty($share->coupon_id)){
            $coupon = Coupon::find()->select('id,name')->where(['id'=>$share->coupon_id])->one();
            $share_name = $coupon->name;
        }

        return $this->render('index', [
            'datas'=>$allData,
            'switch'=>$switch,
            'share_name'=>$share_name,
            'getForm' => $getForm,//筛选条件
            'page' => $page,
            'limit' => $limit,
            'count' => $allcount
        ]);
    }

    /*
     * 优惠券开关
     */
    public function actionSwitch()
    {
        $sta = Yii::$app->request->get('sta');
        $model = CouponConfig::find()->one();
        if(empty($model)){
            $model = new CouponConfig();
        }
        $model->switch = $sta;
        $model->save();
        $record = $sta == 1 ? '关闭' : '打开';
        $this->addAdminLog('优惠券'.$record,'表:hm_coupon_config,id:'.$model->id);

        $this->json('200','操作成功');
    }

    /*
     * 添加优惠券
     */
    public function actionAdd(){
        return $this->render('add',[]);
    }

    /*
     * 编辑优惠券
     */
    public function actionEdit(){
        $id = Yii::$app->request->get('id');
        if(empty($id)){
            $this->alert('系统错误，请重试！');exit;
        }

        $data = Coupon::find()->where(['id' => $id, 'is_del'=> 0])->one();
        if(empty($data)){
            $this->alert('系统错误，请重试！');exit;
        }

        return $this->render('edit',[
            'data' => $data,
            'id' => $id,
        ]);
    }

    /*
     * 保存数据
     */
    public function actionSave()
    {
        if(Yii::$app->request->post()){
            $form = Yii::$app->request->post();
            if(empty($form['name'])){
                $this->alert('请输入优惠券名称');exit;
            }
            if(empty($form['start_at']) || empty($form['end_at'])){
                $this->alert('请选择有效期');exit;
            }
            if(empty($form['money'])){
                $this->alert('请输入优惠券金额');exit;
            }

            $record = '';
            if(isset($form['id']) && !empty($form['id'])){
                $_model = Coupon::find()->where(['id' => $form['id']])->one();
                $record = '编辑';
            }else{
                $_model = new Coupon();
                $_model->created_at = date('Y-m-d H:i:s');
                $record = '添加';
            }
            $_model->name = $form['name'];
            $_model->money = $form['money'];
            $_model->start_at = $form['start_at'];
            $_model->end_at = $form['end_at'];
            if($form['restriction'] == 1){
                if($form['rest_num'] > 0) {
                    $_model->restriction = $form['rest_num'];
                }else{
                    $this->alert('请输入优惠券使用条件');exit;
                }
            }
            if($form['limit'] == 1){
                if($form['limit_num'] > 0) {
                    $_model->limit = $form['limit_num'];
                }else{
                    $this->alert('请输入优惠券领取限制');exit;
                }
            }

            if($_model->save()){
                $this->addAdminLog('优惠券'.$record,'表:hm_coupon,id:'.$_model->id);
                $this->alert('提交成功', '/coupon/index');exit;
            }else{
                $this->alert('提交失败，请重试！');exit;
            }
        }
    }

    /*
     * 获取优惠券
     */
    public function actionGetCoupon()
    {
        $query = Coupon::find();

        $where['is_del'] = 0;
        $where['status'] = 0;
        $query = $query->where($where);
        $nowd = date('Y-m-d H:i:s');
        $query = $query->andFilterWhere(['<=', 'start_at', $nowd]);
        $query = $query->andFilterWhere(['>', 'end_at', $nowd]);
        $datas = $query->asArray()->all();
        if(empty($datas)){
            $this->json('400','没有可用优惠券，请先添加优惠券');
        }
        $this->json('200','操作成功',$datas);
    }

    /*
     * 分享获得优惠券配置
     */
    public function actionSaveShare()
    {
        $id = Yii::$app->request->post('coupon_id');
        $model = CouponShare::find()->one();
        if(empty($model)){
            $model = new CouponShare();
        }
        $model->coupon_id = $id;
        $model->save();
        //把当前选择得优惠券设置为已选择分享
        $coupon_model1 = Coupon::find()->where(['is_share'=>1])->one();
        if(!empty($coupon_model1)) {
            $coupon_model1->is_share = 0;
            $coupon_model1->save();
        }
        $coupon_model = Coupon::find()->where(['id'=>$id])->one();
        $coupon_model->is_share = 1;
        $coupon_model->save();

        $this->addAdminLog('分享获得优惠券配置','表:hm_coupon_share,id:'.$model->id);

        $this->alert('提交成功', '/coupon/index');exit;
    }

    /*
     * 上架/下架
     */
    public function actionDisable()
    {
        $id = Yii::$app->request->get('id');
        if(empty($id)){
            $this->json('400','系统错误，请重试！');
        }
        $sta = Yii::$app->request->get('sta');

        $model = Coupon::find()->where(['id' => $id])->one();
        $model->status = $sta;
        $model->save();

        $record = $sta == 1 ? '下架' : '上架';
        $this->addAdminLog('优惠券'.$record,'表:hm_coupon,id:'.$model->id);

        $this->json('200','操作成功');
    }

    /*
     * 删除
     */
    public function actionDelete()
    {
        $id = Yii::$app->request->get('id');
        if(empty($id)){
            $this->json('400','系统错误，请重试！');
        }

        $model = Coupon::find()->where(['id' => $id])->one();
        $model->is_del = 1;
        $model->deleted_at = date('Y-m-d H:i:s');
        $model->save();

        $this->addAdminLog('优惠券删除','表:hm_coupon,id:'.$model->id);

        $this->json('200','操作成功');
    }
}