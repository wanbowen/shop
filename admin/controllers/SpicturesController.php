<?php

namespace admin\controllers;

use Yii;
use yii\data\Pagination;
use common\models\SPictures;

// 商城banner管理
class SpicturesController extends BaseController{

    /*
     * 商城banner图
     *
     * @return array
     */
    public function actionIndex(){
        //筛选条件
        $getForm = Yii::$app->request->get();
        $query = SPictures::find();

        $where['is_del'] = 0;
        // $where['status'] = 0;
        if (isset($getForm['status']) && $getForm['status'] >= 0) {    //状态
            $where['status'] = $getForm['status'];
        }
        $query = $query->where($where);

        $page = (isset($getForm['page']) && $getForm['page']) ? $getForm['page'] : 1;
        $limit = (isset($getForm['limit']) && $getForm['limit']) ? $getForm['limit'] : 10;

        $allcount = $query->count();
        $pagination = new Pagination([
            'defaultPageSize' => $limit,
            'totalCount' => $allcount
        ]);
        $allData = $query->orderBy('id DESC')->offset($pagination->offset)->limit($pagination->limit)->all();

        return $this->render('index', [
            'datas'=>$allData,
            'getForm' => $getForm,//筛选条件
            'page' => $page,
            'limit' => $limit,
            'count' => $allcount
        ]);
    }

    /*
     * 商城添加banner图页
     */
    public function actionAdd(){
        $status = Yii::$app->request->get('status');
        if(empty($status)){
            $this->alert('系统错误，请重试！');exit;
        }

        return $this->render('add',[
            'status' => $status
        ]);
    }

    /*
     * 商城编辑banner图页
     *
     * @param int    $id     商城bannerId
     * @return array
     */
    public function actionEdit()
    {
        $status = Yii::$app->request->get('status');
        $pid = Yii::$app->request->get('pid');
        if(empty($status) || empty($pid)){
            $this->alert('系统错误，请重试！');exit;
        }

        $data = SPictures::find()->where(['id' => $pid, 'is_del'=> 0])->one();
        if(empty($data)){
            $this->alert('系统错误，请重试！');exit;
        }

        return $this->render('edit',[
            'data' => $data,
            'status' => $status,
            'pid' => $pid,
        ]);
    }

    /**
     * 保存数据
     *
     * @param int    $id         bannerId
     * @param str    $status     banner状态
     * @param str    $icon_img   banner图标
     * @return string
     */
    public function actionSave()
    {
        if(Yii::$app->request->post()){
            $form = Yii::$app->request->post();
            if($form['status'] != '0' && $form['status'] != '1'){
                $this->alert('系统错误，请重试！');exit;
            }
            if(empty($form['img'])){
                $this->alert('请上传图片');exit;
            }
            $img = str_replace(Yii::$app->params['fileDomain'], '', $form['img']);

            $record = '';
            if(isset($form['pid']) && !empty($form['pid'])){
                $_model = SPictures::find()->where(['id' => $form['pid']])->one();
                $record = '商城banner图片编辑';
            }else{
                $_model = new SPictures();
                $_model->status = $form['status'];
                $record = '商城banner图片添加';
            }
            if($_model->img != $img){
                $_model->img = $img;
            }
            $_model->status = $form['status'];
            if($_model->save()){
                $this->addAdminLog($record,'表:hm_pictures,id:'.$_model->id);
                $this->alert('提交成功', '/spictures/index');exit;
            }else{
                $this->alert('提交失败，请重试！');exit;
            }
        }
    }

    /**
     * 启用禁用
     *
     * @param int    $pid         bannerId
     * @return string
     */
    public function actionDisable()
    {
        $pid = Yii::$app->request->get('pid');
        if(empty($pid)){
            $this->json('400','系统错误，请重试！');
        }
        $sta = Yii::$app->request->get('sta');

        $model = SPictures::find()->where(['id' => $pid])->one();
        $model->status = $sta;
        $model->save();
        $record = $sta == 1 ? '禁用' : '启用';
        $this->addAdminLog('商城banner图片'.$record,'表:hm_pictures,id:'.$model->id);

        $this->json('200','操作成功');
    }

    /**
     * 删除
     *
     * @param int    $pid         bannerId
     * @return string
     */
    public function actionDelete()
    {
        $pid = Yii::$app->request->get('pid');
        if(empty($pid)){
            $this->json('400','系统错误，请重试！');
        }

        $model = SPictures::find()->where(['id' => $pid])->one();
        $model->is_del = 1;
        $model->deleted_at = date('Y-m-d H:i:s');
        $model->save();
        $this->addAdminLog('商城banner图片删除','表:hm_pictures,id:'.$model->id);

        $this->json('200','操作成功');
    }

}
