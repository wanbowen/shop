<?php
namespace admin\controllers;

use Yii;
use yii\data\Pagination;
use common\models\Pictures;

/*
 * 小程序  banner图管理
 */
class BannerController extends BaseController{

    public function init()
    {
        parent::init();
    }

    /*
     * 首页banner图
     */
    public function actionIndex(){
        //筛选条件
        $getForm = Yii::$app->request->get();
        $query = Pictures::find();

        $where['is_del'] = 0;
        $where['type'] = 1;
        if (isset($getForm['status']) && $getForm['status'] >= 0) {    //状态
            $where['status'] = $getForm['status'];
        }
        $query = $query->where($where);
        if (isset($getForm['name']) && $getForm['name'] != '') {      //名称
            $query = $query->andFilterWhere(['like', 'name', $getForm['name']]);
        }

        $page = (isset($getForm['page']) && $getForm['page']) ? $getForm['page'] : 1;
        $limit = (isset($getForm['limit']) && $getForm['limit']) ? $getForm['limit'] : 10;

        $allcount = $query->count();
        $pagination = new Pagination([
            'defaultPageSize' => $limit,
            'totalCount' => $allcount
        ]);
        $allData = $query->orderBy('id DESC')->offset($pagination->offset)->limit($pagination->limit)->all();

        return $this->render('index', [
            'datas'=>$allData,
            'getForm' => $getForm,//筛选条件
            'page' => $page,
            'limit' => $limit,
            'count' => $allcount
        ]);
    }

    /*
     * 首页底部图
     */
    public function actionBottom()
    {
        $model = Pictures::find()->where(['type' => 2])->one();
        if(Yii::$app->request->post()){
            $img = Yii::$app->request->post('img');
            if(empty($img)){
                $this->json('400','请上传图片！');
            }
            if(!empty($model)){
                $img = str_replace(Yii::$app->params['fileDomain'], '', $img);
                if($model->img != $img){
                    $model->img = $img;
                    $model->save();
                    $this->addAdminLog('首页底部图片修改','表:hm_pictures,id:'.$model->id);
                }
                $this->json('200','提交成功');
            }else{
                $_model = new Pictures();
                $_model->type = 2;
                $_model->name = '首页底部图片';
                $_model->img = $img;
                $_model->created_at = date('Y-m-d H:i:s');
                if($_model->save()){
                    $this->addAdminLog('首页底部图片添加','表:hm_pictures,id:'.$_model->id);
                    $this->json('200','提交成功');
                }
            }
            $this->json('401','提交失败');
        }else {
            $img = !empty($model) ? $model->img : '';
            return $this->render('bottom', [
                'img' => $img,
            ]);
        }
    }

    /*
     * 添加banner图
     */
    public function actionAdd(){
        $type = Yii::$app->request->get('type');
        if(empty($type)){
            $this->alert('系统错误，请重试！');exit;
        }

        return $this->render('add',[
            'type' => $type
        ]);
    }

    /*
     * 编辑banner图
     */
    public function actionEdit()
    {
        $type = Yii::$app->request->get('type');
        $pid = Yii::$app->request->get('pid');
        if(empty($type) || empty($pid)){
            $this->alert('系统错误，请重试！');exit;
        }

        $data = Pictures::find()->where(['id' => $pid, 'is_del'=> 0])->one();
        if(empty($data)){
            $this->alert('系统错误，请重试！');exit;
        }

        return $this->render('edit',[
            'data' => $data,
            'type' => $type,
            'pid' => $pid,
        ]);
    }

    /*
     * 保存数据
     */
    public function actionSave()
    {
        if(Yii::$app->request->post()){
            $form = Yii::$app->request->post();
            if(empty($form['type'])){
                $this->alert('系统错误，请重试！');exit;
            }
            if(empty($form['name'])){
                $this->alert('请输入图片名称');exit;
            }
            if(empty($form['img'])){
                $this->alert('请上传图片');exit;
            }
            $img = str_replace(Yii::$app->params['fileDomain'], '', $form['img']);

            $record = '';
            if(isset($form['pid']) && !empty($form['pid'])){
                $_model = Pictures::find()->where(['id' => $form['pid']])->one();
                $record = 'banner图片编辑';
            }else{
                $_model = new Pictures();
                $_model->type = $form['type'];
                $_model->created_at = date('Y-m-d H:i:s');
                $record = 'banner图片添加';
            }
            if($_model->img != $img){
                $_model->img = $img;
            }
            $_model->status = $form['status'];
            $_model->name = $form['name'];
            if($_model->save()){
                $this->addAdminLog($record,'表:hm_pictures,id:'.$_model->id);
                $this->alert('提交成功', '/banner/index');exit;
            }else{
                $this->alert('提交失败，请重试！');exit;
            }
        }
    }

    /*
     * 禁用/启用
     */
    public function actionDisable()
    {
        $pid = Yii::$app->request->get('pid');
        if(empty($pid)){
            $this->json('400','系统错误，请重试！');
        }
        $sta = Yii::$app->request->get('sta');

        $model = Pictures::find()->where(['id' => $pid])->one();
        $model->status = $sta;
        $model->save();
        $record = $sta == 1 ? '禁用' : '启用';
        $this->addAdminLog('banner图片'.$record,'表:hm_pictures,id:'.$model->id);

        $this->json('200','操作成功');
    }

    /*
     * 删除
     */
    public function actionDelete()
    {
        $pid = Yii::$app->request->get('pid');
        if(empty($pid)){
            $this->json('400','系统错误，请重试！');
        }

        $model = Pictures::find()->where(['id' => $pid])->one();
        $model->is_del = 1;
        $model->deleted_at = date('Y-m-d H:i:s');
        $model->save();
        $this->addAdminLog('banner图片删除','表:hm_pictures,id:'.$model->id);

        $this->json('200','操作成功');
    }
}