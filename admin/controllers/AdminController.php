<?php

namespace admin\controllers;

use Yii;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use admin\models\Admin;
use yii\data\ActiveDataProvider;
use admin\models\AdminRole;

class AdminController extends BaseController
{

    public $enableCsrfValidation = false;
	//管理员列表
	public function actionIndex()
    {
        $this->checkrolepre();
		$page=isset($_GET['page'])?$_GET['page']:'1';
		$limit=isset($_GET['limit'])?$_GET['limit']:'10';
		$limit2=($page-1)*$limit;
		$username=isset($_GET['username']) ? $_GET['username']:'';
		$status=isset($_GET['status']) ? $_GET['status']:'';

		$dataProvider =Admin::find()->joinWith('roles a')->select(['id','username','hm_admin.created_at','a.rolename','status','a.roleid']);
		$count =Admin::find()->joinWith('roles a')->select(['id','username','hm_admin.created_at','a.rolename','status','a.roleid']);
		if(!empty($username)){
			$dataProvider=$dataProvider->andFilterWhere(['like', 'username', $username]);
			$count =$count->andFilterWhere(['like', 'username', $username]);
		}
		if((!empty($status)&&$status!='0') || $status=='0'){
			$dataProvider=$dataProvider->andFilterWhere(['status' => $status]);
			$count =$count->andFilterWhere(['status' => $status]);
		}
		$dataProvider=$dataProvider->orderBy('id DESC')->limit($limit)->offset($limit2)->asArray()->all();
		$count =$count->count();
        return $this->render('index', [
            'dataProvider' => $dataProvider,
			'username'=>$username,
			'status'=>$status,
			'count'=>$count,
			'page'=>$page,
			'limit'=>$limit,
        ]);
    }
	//添加管理员
    public function actionAdd()
    {
		$AdminRole=AdminRole::find()->select(['roleid','rolename'])->where(['disabled'=>'0'])->asArray()->all();
		return $this->render('add',['role'=>$AdminRole]);
    }
	//修改管理员
	public function actionEdit($id)
    {
		$model=$this->findModel($id);
		$AdminRole=AdminRole::find()->select(['roleid','rolename'])->where(['disabled'=>'0'])->asArray()->all();
		return $this->render('edit',['admin'=>$model,'role'=>$AdminRole]);
    }
	//修改管理员
	public function actionEditpost($id)
    {
       if(Yii::$app->request->post()){
            $username = Yii::$app->request->post('username');
			$password = Yii::$app->request->post('password');
			$email = Yii::$app->request->post('email');
			$status = Yii::$app->request->post('status');
			$realname = Yii::$app->request->post('realname');
			$roleid = Yii::$app->request->post('roleid');
			$yanzheng = Admin::find()->where(['username'=>$username])->count();
			if($yanzheng > 1){
				$this->alert('用户名已经存在！');exit;
			}
			$user = Admin::findOne($id);
			$user->username = $username;
			$user->email = $email;
			$user->status = $status;
			if(isset($password)&&!empty($password)){
				$user->password_hash=$user->setPassword($password);
				$user->auth_key=$user->generateAuthKey();
				$user->mpassword=$password;
			}

			$user->roleid = $roleid;
			$user->realname = $realname;
			$user->updated_at = date('Y-m-d H:i:s');
			if($user->save()){
                $this->addAdminLog('修改管理员','表:hm_admin,id:'.$user->id);
				$this->alert('修改成功','/admin/index/');exit;

			}
			$this->alert('提交失败','/admin/index/');exit;
        }else{
			$this->alert('系统错误','/admin/index/');exit;
		}
    }
	//添加管理员
	public function actionAddpost()
    {
		if(Yii::$app->request->post()){
            $username = Yii::$app->request->post('username');
			$password = Yii::$app->request->post('password');
			$email = Yii::$app->request->post('email');
			$roleid = Yii::$app->request->post('roleid');
			$status = Yii::$app->request->post('status');
			$realname = Yii::$app->request->post('realname');
			$yanzheng = Admin::find()->where(['username'=>$username])->one();
			if(!empty($yanzheng)){
				$this->alert('用户名已经存在！');exit;
			}
			$user = new Admin();
			$user->username = $username;
			$user->email = $email;
			$user->password_hash=$user->setPassword($password);
			$user->mpassword=$password;
			$user->auth_key=$user->generateAuthKey();
			$user->roleid = $roleid;
			$user->status = $status;
			$user->realname =$realname;
			$user->created_at = date('Y-m-d H:i:s');
			$user->updated_at = date('Y-m-d H:i:s');
			if($user->save()){
                $this->addAdminLog('添加管理员','表:hm_admin,id:'.$user->id);
				$this->alert('添加成功','/admin/index/');exit;

			}
			$this->alert('提交失败','/admin/index/');exit;
        }else{
		    $this->alert('系统错误','/admin/index/');exit;
		}
    }
    //禁用/启用 管理员
    public function actionDisable()
    {
        $id = Yii::$app->request->get('id');
        if($id==1){
            $this->alert('初始超级管理员禁止禁用','/admin/index/');exit;
        }
        $sta = Yii::$app->request->get('sta');

        $model = $this->findModel($id);
        $model->status = $sta;
        $model->save();

        $record = $sta == 1 ? '禁用' : '启用';
        $this->addAdminLog('管理员'.$record,'表:hm_admin,id:'.$model->id);

        $this->alert('操作成功','/admin/index/');exit;
    }
	//删除管理员
	public function actionDelete($id)
    {
		if($id==1){
			$this->alert('初始超级管理员禁止删除','/admin/index/');exit;
		}
	   $this->findModel($id)->delete();

        $this->addAdminLog('删除管理员','表:hm_admin,id:'.$id);

       $this->alert('删除成功','/admin/index/');exit;
    }
	protected function findModel($id)
    {
        if (($model = Admin::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
