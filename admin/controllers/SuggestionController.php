<?php
namespace admin\controllers;

use Yii;
use yii\data\Pagination;
use common\models\User;
use common\models\Staff;
use common\models\Suggestion;

/*
 * 公众号  投诉建议
 */
class SuggestionController extends BaseController{

    public function init()
    {
        parent::init();
    }

    /*
     * 首页
     */
    public function actionIndex()
    {
        //筛选条件
        $getForm = Yii::$app->request->get();
        $query = new \yii\db\Query();
        $query = $query->select('s.id,s.title,s.status,s.created_at,s.user_id,t.name,t.mobile');
        $query = $query->from('hm_suggestion as s');
        $query = $query->leftJoin('hm_user as t', 't.id = s.user_id');

        $where['s.is_del'] = 0;
        if (isset($getForm['status']) && $getForm['status'] >= 0) {    //状态
            $where['s.status'] = $getForm['status'];
        }
        $query = $query->where($where);
        if (isset($getForm['create_time']) && $getForm['create_time'] != '') {  // 模糊查询时间
            $query = $query->andFilterWhere(['>', 's.created_at', $getForm['create_time'].' 00:00:00']);
        }
        if (isset($getForm['end_time']) && $getForm['end_time'] != '') {
            $query = $query->andFilterWhere(['<=', 's.created_at', $getForm['end_time'].' 23:59:59']);
        }

        $page = (isset($getForm['page']) && $getForm['page']) ? $getForm['page'] : 1;
        $limit = (isset($getForm['limit']) && $getForm['limit']) ? $getForm['limit'] : 10;

        $allcount = $query->count();
        $pagination = new Pagination([
            'defaultPageSize' => $limit,
            'totalCount' => $allcount
        ]);
        $allData = $query->orderBy('id DESC')->offset($pagination->offset)->limit($pagination->limit)->all();

        return $this->render('index', [
            'datas'=>$allData,
            'getForm' => $getForm,//筛选条件
            'page' => $page,
            'limit' => $limit,
            'count' => $allcount
        ]);
    }

    /*
     * 查看投诉建议
     */
    public function actionView()
    {
        $id = Yii::$app->request->get('id');
        if(empty($id)){
            $this->alert('系统错误，请重试！');exit;
        }

        $data = Suggestion::find()->where(['id'=>$id])->asArray()->one();
        $staff_data = User::find()->select('name,mobile')->where(['id'=>$data['user_id']])->asArray()->one();
        if(empty($data) || empty($staff_data)){
            $this->alert('系统错误，请重试！');exit;
        }

        return $this->render('view',[
            'data' => $data,
            'staff_data' => $staff_data
        ]);
    }

    /*
     * 回复投诉建议
     */
    public function actionReply()
    {
        $id = Yii::$app->request->get('id');
        if(empty($id)){
            $this->alert('系统错误，请重试！');exit;
        }

        $data = Suggestion::find()->where(['id'=>$id])->asArray()->one();
        if(empty($data)){
            $this->alert('系统错误，请重试！');exit;
        }

        return $this->render('reply',[
            'data' => $data
        ]);
    }

    /*
     * 保存回复
     */
    public function actionSave()
    {
        if(Yii::$app->request->post()){
            $form = Yii::$app->request->post();
            if(empty($form['id'])){
                $this->alert('系统错误，请重试！');exit;
            }
            if(empty($form['reply'])){
                $this->alert('请输入回复内容');exit;
            }

            $_model = Suggestion::find()->where(['id' => $form['id']])->one();
            if($_model->status == 1){
                $this->alert('已经回复过，不可重复提交');exit;
            }
            $_model->reply = $form['reply'];
            $_model->status = 1;
            $_model->reply_at = date('Y-m-d H:i:s');
            if($_model->save()){
                $this->addAdminLog('回复投诉建议','表:hm_suggestion,id:'.$_model->id);
                $this->alert('提交成功','/suggestion/index');exit;
            }else{
                $this->alert('提交失败，请重试！');exit;
            }
        }
    }
    /*
     * 删除
     */
    public function actionDelete()
    {
        $id = Yii::$app->request->get('id');
        if(empty($id)){
            $this->json('400','系统错误，请重试！');
        }

        $model = Suggestion::find()->where(['id' => $id])->one();
        $model->is_del = 1;
        $model->deleted_at = date('Y-m-d H:i:s');
        $model->save();
        $this->addAdminLog('删除投诉建议','表:hm_suggestion,id:'.$model->id);

        $this->json('200','操作成功');
    }
}