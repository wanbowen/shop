<?php
namespace admin\controllers;

use Yii;
use yii\data\Pagination;
use common\models\BookConfig;
use common\models\BookMoney;
use common\models\OrderLabel;
use common\models\SmsTemplet;
use common\models\WorkHours;

/*
 * 小程序  系统设置管理
 */
class SettingController extends BaseController{

    public function init()
    {
        parent::init();
    }

    /*
     * 预约时间管理
     */
    public function actionBook(){
        $model = BookConfig::find()->one();
        if(empty($model)){
            $model = new BookConfig();
        }

        if(Yii::$app->request->post()){
            $form = Yii::$app->request->post();
            if(empty($form['start_at']) || empty($form['end_at'])){
                $this->alert('请选择预约时间');exit;
            }

            $model->start_at = $form['start_at'];
            $model->end_at = $form['end_at'];
            $model->days = $form['days'];
            $model->switch = isset($form['switch']) ? 0 : 1;
            if($model->save()){
                $this->addAdminLog('预约时间编辑','表:hm_book_config,id:'.$model->id);
                $this->alert('提交成功', '/setting/book');exit;
            }else{
                $this->alert('提交失败，请重试！');exit;
            }
        }else{
            return $this->render('book', [
                'data'=>$model,
            ]);
        }
    }

    /*
     * 预约金管理
     */
    public function actionMoney(){
        $model = BookMoney::find()->one();
        if(empty($model)){
            $model = new BookMoney();
        }

        if(Yii::$app->request->post()){
            $form = Yii::$app->request->post();

            $model->money = !empty($form['money']) ? $form['money'] : 0;
            if($model->save()){
                $this->addAdminLog('预约金编辑','表:hm_book_money');
                $this->alert('提交成功', '/setting/money');exit;
            }else{
                $this->alert('提交失败，请重试！');exit;
            }
        }else{
            return $this->render('money', [
                'data'=>$model,
            ]);
        }
    }

    /*
     * 预约标签管理
     */
    public function actionLabel()
    {
        //筛选条件
        $getForm = Yii::$app->request->get();
        $query = OrderLabel::find();

        $where['is_del'] = 0;
        $query = $query->where($where);
        if (isset($getForm['name']) && $getForm['name'] != '') {      //名称
            $query = $query->andFilterWhere(['like', 'name', $getForm['name']]);
        }

        $page = (isset($getForm['page']) && $getForm['page']) ? $getForm['page'] : 1;
        $limit = (isset($getForm['limit']) && $getForm['limit']) ? $getForm['limit'] : 10;

        $allcount = $query->count();
        $pagination = new Pagination([
            'defaultPageSize' => $limit,
            'totalCount' => $allcount
        ]);
        $allData = $query->orderBy('id ASC')->offset($pagination->offset)->limit($pagination->limit)->all();

        return $this->render('label', [
            'datas'=>$allData,
            'getForm' => $getForm,//筛选条件
            'page' => $page,
            'limit' => $limit,
            'count' => $allcount
        ]);
    }

    /*
     * 添加(编辑)标签
     */
    public function actionSave()
    {
        $id = Yii::$app->request->get('id');
        $record = '';
        if(isset($id) && !empty($id)){
            $model = OrderLabel::find()->where(['id'=>$id])->one();
            $record = '标签编辑';
        }else{
            $model = new OrderLabel();
            $model->created_at = date('Y-m-d H:i:s');
            $record = '标签添加';
        }
        if(Yii::$app->request->post()){
            $model->name = Yii::$app->request->post('name');
            if($model->save()){
                $this->addAdminLog($record,'表:hm_order_label,id:'.$model->id);
                $this->alert('提交成功','/setting/label');exit;
            }else{
                $this->alert('提交失败，请重试！');exit;
            }
        }else {
            return $this->render('save', [
                'data' => $model
            ]);
        }
    }

    /*
     * 删除标签
     */
    public function actionDelete()
    {
        $id = Yii::$app->request->get('id');
        if(empty($id)){
            $this->json('400','系统错误，请重试！');
        }

        $model = OrderLabel::find()->where(['id' => $id])->one();
        $model->is_del = 1;
        $model->save();

        $this->addAdminLog('标签删除','表:hm_order_label,id:'.$model->id);

        $this->json('200','操作成功');
    }

    /*
     * 短信模板设置
     */
    public function actionSmstemplet()
    {
        if(Yii::$app->request->post()){
            $form = Yii::$app->request->post();
            $model = SmsTemplet::find()->where(['type'=>$form['type']])->one();
            if(empty($model)){
                $model = new SmsTemplet();
                $model->type = $form['type'];
            }

            $model->content = $form['desc'];
            $model->created_at = date('Y-m-d H:i:s');
            if($model->save()){
                $this->addAdminLog('短信模板设置','表:hm_sms_templet,id:'.$model->id);
                $this->alert('提交成功', '/setting/smstemplet');exit;
            }else{
                $this->alert('提交失败，请重试！');exit;
            }
        }

        $tem_one_data = SmsTemplet::find()->where(['type'=>1])->asArray()->one();
        $tem_two_data = SmsTemplet::find()->where(['type'=>2])->asArray()->one();
        $tem_three_data = SmsTemplet::find()->where(['type'=>3])->asArray()->one();
        $tem_four_data = SmsTemplet::find()->where(['type'=>4])->asArray()->one();
        $tem_five_data = SmsTemplet::find()->where(['type'=>5])->asArray()->one();
        $tem_six_data = SmsTemplet::find()->where(['type'=>6])->asArray()->one();
        return $this->render('templet', [
            'tem_one_data' => $tem_one_data,
            'tem_two_data' => $tem_two_data,
            'tem_three_data' => $tem_three_data,
            'tem_four_data' => $tem_four_data,
            'tem_five_data' => $tem_five_data,
            'tem_six_data' => $tem_six_data
        ]);
    }

    /*
     * 工作时间设置
     */
    public function actionWorkhours()
    {
        if(Yii::$app->request->post()){
            $form = Yii::$app->request->post();
            $model = WorkHours::find()->one();
            if(empty($model)){
                $model = new WorkHours();
            }

            $model->start = $form['start'];
            $model->end = $form['end'];
            if($model->save()){
                $this->addAdminLog('工作时间设置','表:hm_work_hours,id:'.$model->id);
                $this->alert('提交成功', '/setting/workhours');exit;
            }else{
                $this->alert('提交失败，请重试！');exit;
            }
        }

        $hours_data = WorkHours::find()->asArray()->one();
        return $this->render('workhours', [
            'hours_data' => $hours_data
        ]);
    }
}