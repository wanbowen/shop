<?php

namespace admin\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use admin\models\AdminRole;
use admin\models\AdminPre;
use admin\models\AdminRolePre;
use yii\data\ActiveDataProvider;
use yii\web\NotFoundHttpException;

class RoleController extends BaseController
{
	//角色列表
	public function actionIndex()
    {
        $this->checkrolepre();
		$page=isset($_GET['page'])?$_GET['page']:'1';
		$limit=isset($_GET['limit'])?$_GET['limit']:'10';
		$limit2=($page-1)*$limit;
		$rolename=isset($_GET['rolename']) ? $_GET['rolename']:'';
		$disabled=isset($_GET['disabled']) ? $_GET['disabled']:'';

		$dataProvider =AdminRole::find()->joinWith('adminones a')->select(['rolename','hm_admin_role.created_at','listorder','disabled','a.username','hm_admin_role.roleid','hm_admin_role.admin_id']);
		$count =AdminRole::find()->joinWith('adminones a')->select(['rolename','hm_admin_role.created_at','listorder','disabled','a.username','hm_admin_role.roleid','hm_admin_role.admin_id']);
		if(!empty($rolename)){
			$dataProvider=$dataProvider->andFilterWhere(['like', 'rolename', $rolename]);
			$count =$count->andFilterWhere(['like', 'rolename', $rolename]);
		}
		if((!empty($disabled)&&$disabled!='0') || $disabled=='0'){
			$dataProvider=$dataProvider->andFilterWhere(['disabled' => $disabled]);
			$count =$count->andFilterWhere(['disabled' => $disabled]);
		}
		$dataProvider=$dataProvider->orderBy('listorder ASC')->limit($limit)->offset($limit2)->asArray()->all();
		$count =$count->count();
        return $this->render('index', [
            'dataProvider' => $dataProvider,
			'rolename'=>$rolename,
			'disabled'=>$disabled,
			'count'=>$count,
			'page'=>$page,
			'limit'=>$limit,
        ]);
    }
	//添加角色
    public function actionAdd()
    {
	   return $this->render('add');
    }
	//修改角色
	public function actionEdit($id)
    {
		$model=$this->findModel($id);
		return $this->render('edit',['role'=>$model]);
    }
	//修改角色
	public function actionEditpost($id)
    {
       if(Yii::$app->request->post()){
            $rolename = Yii::$app->request->post('rolename');
			$description = Yii::$app->request->post('description');
			$listorder = Yii::$app->request->post('listorder');
			$disabled = Yii::$app->request->post('disabled');
			$role = adminRole::findOne($id);
			$role->rolename = $rolename;
			$role->description = $description;
			$role->listorder=$listorder;
			$role->disabled =$disabled;
			$role->updated_at = date('Y-m-d H:i:s');
			if($role->save()){
                $this->addAdminLog('修改角色','表:hm_admin_role,id:'.$role->roleid);
				$this->alert('修改成功','/role/index/');exit;
				
			} 
			$this->alert('提交失败','/role/index/');exit;
        }else{
			
			$this->alert('系统错误','/role/index/');exit;
		}
    }
	//添加角色
	public function actionAddpost()
    {
		if(Yii::$app->request->post()){
            $rolename = Yii::$app->request->post('rolename');
			$description = Yii::$app->request->post('description');
			$listorder = Yii::$app->request->post('listorder');
			$disabled = Yii::$app->request->post('disabled');
			$role = new AdminRole();
			$role->rolename = $rolename;
			$role->description = $description;
			$role->listorder=$listorder;
			$role->disabled =$disabled;
			$role->admin_id =!empty(Yii::$app->user->identity->id)? Yii::$app->user->identity->id:"0";
			$role->created_at = date('Y-m-d H:i:s');
			$role->updated_at = date('Y-m-d H:i:s');
			if($role->save()){
                $this->addAdminLog('添加角色','表:hm_admin_role,id:'.$role->roleid);
				$this->alert('添加成功','/role/index/');exit;
				
			} 
			$this->alert('提交失败','/role/index/');exit;
        }else{
		    $this->alert('系统错误','/role/index/');exit;
		}
    }
    //禁用/启用 角色
    public function actionDisable()
    {
        $id = Yii::$app->request->get('id');
        if($id==1){
            $this->alert('初始超级管理员角色禁止禁用','/role/index/');exit;
        }
        $sta = Yii::$app->request->get('sta');

        $model = $this->findModel($id);
        $model->disabled = $sta;
        $model->save();

        $record = $sta == 1 ? '禁用' : '启用';
        $this->addAdminLog('角色'.$record,'表:hm_admin_role,id:'.$model->roleid);

        $this->alert('操作成功','/role/index/');exit;
    }
	//删除角色
	public function actionDelete($id)
    {
        if($id==1){
			$this->alert('初始超级管理员角色禁止删除','/role/index/');exit;
		}
        if($id==2){
            $this->alert('员工角色禁止删除','/role/index/');exit;
        }
		$this->findModel($id)->delete();

        $this->addAdminLog('删除角色','表:hm_admin_role,id:'.$id);

        $this->alert('删除成功','/role/index/');exit;
    }
	protected function findModel($id)
    {
        if (($model = AdminRole::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
	//权限分配
	public function actionPreset($id)
    {
		 $this->layout = false;
		 $AdminRolePre=AdminRolePre::find()->select(['preid'])->where(['roleid'=>$id])->asArray()->all();
		 $array=array();
		 foreach($AdminRolePre as $key=>$value){
			 $array[$key]=$value['preid'];
		 }
		 $AdminPre=AdminPre::find()->select(['id','pId','name'])->asArray()->all();
		 foreach($AdminPre as $keys=>$values){
			if (in_array($values['id'], $array))
			{
				$AdminPre[$keys]['checked']='true';
				if($values['pId']=='0'){
					$AdminPre[$keys]['open']='true';
				}
			}
		 }
		 $json=json_encode($AdminPre);
		 return $this->render('prelist',['json'=>$json,'roleid'=>$id]);
    }
	//权限分配
	public function actionAdminpreset($id)
    {
         $this->layout = false;
		 if(Yii::$app->request->isAjax){
			 $keys=Yii::$app->request->post('key');
			 $keys=substr($keys,0,-1);
			 if(empty($keys)){
				 $this->json(201,'提交数据不能为空');exit;
			 }
			 $ids=explode(',',$keys);
			 $add=array();
			 foreach($ids as $key=>$value){
				 $add[$key]['roleid']=$id;
				 $add[$key]['preid']=$value;
			 }
			 $connection = \Yii::$app->db;
			 $connection ->createCommand()
				->delete('hm_admin_role_pre', 'roleid ='.$id)
				->execute();
			  //数据批量入库  
			 $connection->createCommand()->batchInsert(  
				'hm_admin_role_pre',
				['roleid','preid'],//字段  
				$add  
			  )->execute();
			  $this->json(200,'设置成功'); exit;
		 }else{
			$this->json(203,'程序错误'); exit;
		 }
		 
    }
}
