<?php
namespace admin\controllers;

use Yii;
use yii\data\Pagination;
use common\models\Materials;

/*
 * 公众号相关内容管理
 */
class WechatController extends BaseController{

    public function init()
    {
        parent::init();
    }

    /*
     * 企业介绍
     */
    public function actionIntroduction()
    {
        $model = Materials::find()->where(['type' => 1])->one();
        if(Yii::$app->request->post()){
            $cont = Yii::$app->request->post('cont');
            if(empty($cont)){
                $this->json('401','请填写企业介绍内容！');
            }
            if(!empty($model)){
                $model->content = $cont;
                if($model->save()){
                    $this->addAdminLog('编辑企业介绍','表:hm_materials,id:'.$model->id);
                    $this->json('200','提交成功');
                }
            }else{
                $_model = new Materials();
                $_model->type = 1;
                $_model->title = '企业介绍';
                $_model->content = $cont;
                $_model->created_at = date('Y-m-d H:i:s');
                if($_model->save()){
                    $this->addAdminLog('新建企业介绍','表:hm_materials,id:'.$_model->id);
                    $this->json('200','提交成功');
                }
            }
            $this->json('401','提交失败');
        }else {
            $content = !empty($model) ? $model->content : '';
            return $this->render('introduction', [
                'content' => $content,
            ]);
        }
    }

    /*
     * 客服电话
     */
    public function actionServe()
    {
        $model = Materials::find()->where(['type' => 5])->one();
        if(Yii::$app->request->post()){
            $cont = Yii::$app->request->post('cont');
            if(empty($cont)){
                $this->json('401','请填写客服电话内容！');
            }
            if(!empty($model)){
                $model->content = $cont;
                if($model->save()){
                    $this->addAdminLog('编辑客服电话','表:hm_materials,id:'.$model->id);
                    $this->json('200','提交成功');
                }
            }else{
                $_model = new Materials();
                $_model->type = 5;
                $_model->title = '客服电话';
                $_model->content = $cont;
                $_model->created_at = date('Y-m-d H:i:s');
                if($_model->save()){
                    $this->addAdminLog('新建客服电话','表:hm_materials,id:'.$_model->id);
                    $this->json('200','提交成功');
                }
            }
            $this->json('401','提交失败');
        }else {
            $content = !empty($model) ? $model->content : '';
            return $this->render('serve', [
                'content' => $content,
            ]);
        }
    }

    /*
     * 企业动态
     */
    public function actionTrend()
    {
        $data = $this->getList(2);
        return $this->render('list',$data);
    }

    /*
     * 项目信息
     */
    public function actionProject()
    {
        $data = $this->getList(3);
        return $this->render('list',$data);
    }

    /*
     * 企业动态
     */
    public function actionLife()
    {
        $data = $this->getList(4);
        return $this->render('list',$data);
    }

    /*
     * 图文列表
     */
    private function getList($type)
    {
        //筛选条件
        $getForm = Yii::$app->request->get();
        $query = Materials::find()->select('id,title,status,created_at');

        $where['type'] = $type;
        $where['is_del'] = 0;
        if (isset($getForm['status']) && $getForm['status'] >= 0) {    //状态
            $where['status'] = $getForm['status'];
        }
        $query = $query->where($where);
        if (isset($getForm['title']) && $getForm['title'] != '') {  //标题
            $query = $query->andFilterWhere(['like', 'title', $getForm['title']]);
        }

        $page = (isset($getForm['page']) && $getForm['page']) ? $getForm['page'] : 1;
        $limit = (isset($getForm['limit']) && $getForm['limit']) ? $getForm['limit'] : 10;

        $allcount = $query->count();
        $pagination = new Pagination([
            'defaultPageSize' => $limit,
            'totalCount' => $allcount
        ]);
        $allData = $query->orderBy('id DESC')->offset($pagination->offset)->limit($pagination->limit)->asArray()->all();

        $cate = $this->getCate($type);

        return [
            'datas'=>$allData,
            'getForm' => $getForm,//筛选条件
            'page' => $page,
            'limit' => $limit,
            'count' => $allcount,
            'title' => $cate['t'],
            'url' => $cate['u'],
            'type' => $type,
        ];
    }

    /*
     * 添加图文数据
     */
    public function actionAdd()
    {
        $type = Yii::$app->request->get('type');
        if(empty($type)){
            $this->alert('系统错误，请重试！');exit;
        }
        $cate = $this->getCate($type);

        return $this->render('add',[
            'type' => $type,
            'url' => $cate['u'],
            'title' => $cate['t']
        ]);
    }

    /*
     * 编辑图文数据
     */
    public function actionEdit()
    {
        $type = Yii::$app->request->get('type');
        $mid = Yii::$app->request->get('mid');
        if(empty($type) || empty($mid)){
            $this->alert('系统错误，请重试！');exit;
        }
        $data = Materials::find()->select('id,title,content')->where(['id' => $mid])->one();
        $cate = $this->getCate($type);

        return $this->render('edit',[
            'data' => $data,
            'type' => $type,
            'mid' => $mid,
            'url' => $cate['u'],
            'title' => $cate['t']
        ]);
    }

    /*
     * 根据分类获取不同信息
     */
    private function getCate($type)
    {
        $title = '';
        $url = '';
        switch ($type){
            case 2:
                $title = '企业动态';
                $url = '/wechat/trend';
                break;
            case 3:
                $title = '项目信息';
                $url = '/wechat/project';
                break;
            case 4:
                $title = '生活小百科';
                $url = '/wechat/life';
                break;
        }

        return ['t'=>$title,'u'=>$url];
    }

    /*
     * 保存数据
     */
    public function actionSave()
    {
        if(Yii::$app->request->post()){
            $form = Yii::$app->request->post();
            if(empty($form['type']) || empty($form['url'])){
                $this->alert('系统错误，请重试！');exit;
            }
            if(empty($form['title'])){
                $this->alert('请输入标题');exit;
            }
            if(empty($form['content'])){
                $this->alert('请输入内容');exit;
            }

            if(isset($form['mid']) && !empty($form['mid'])){
                $_model = Materials::find()->where(['id' => $form['mid']])->one();
            }else{
                $_model = new Materials();
                $_model->type = $form['type'];
                $_model->created_at = date('Y-m-d H:i:s');
            }
            $_model->title = $form['title'];
            $_model->content = $form['content'];
            if($_model->save()){
                $this->addAdminLog('保存图文数据','表:hm_materials,id:'.$_model->id);
                $this->alert('提交成功',$form['url']);exit;
            }else{
                $this->alert('提交失败，请重试！');exit;
            }
        }
    }
    /*
     * 禁用/启用
     */
    public function actionDisable()
    {
        $mid = Yii::$app->request->get('mid');
        if(empty($mid)){
            $this->json('400','系统错误，请重试！');
        }
        $sta = Yii::$app->request->get('sta');

        $model = Materials::find()->where(['id' => $mid])->one();
        $model->status = $sta;
        $model->save();

        $record = $sta == 1 ? '禁用' : '启用';
        $this->addAdminLog('图文数据'.$record,'表:hm_materials,id:'.$model->id);

        $this->json('200','操作成功');
    }
    /*
     * 删除
     */
    public function actionDelete()
    {
        $mid = Yii::$app->request->get('mid');
        if(empty($mid)){
            $this->json('400','系统错误，请重试！');
        }

        $model = Materials::find()->where(['id' => $mid])->one();
        $model->is_del = 1;
        $model->deleted_at = date('Y-m-d H:i:s');
        $model->save();
        $this->addAdminLog('删除图文数据','表:hm_materials,id:'.$model->id);

        $this->json('200','操作成功');
    }
}