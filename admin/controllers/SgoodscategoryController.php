<?php

namespace admin\controllers;

use Yii;
use yii\data\Pagination;
use common\models\SGoodsCategory;
// 商品分类
class SgoodscategoryController extends BaseController{

    /**
     * 商品分类列表
     *
     * @return array
     */
    public function actionIndex(){
        $getForm = Yii::$app->request->get();
        $query = SGoodsCategory::find();
        $where['is_del'] = 0;
        $query = $query->where($where);
        if (isset($getForm['name']) && $getForm['name'] != '') {      // 名称
            $query = $query->andFilterWhere(['like', 'name', $getForm['name']]);
        }

        $page = (isset($getForm['page']) && $getForm['page']) ? $getForm['page'] : 1;
        $limit = (isset($getForm['limit']) && $getForm['limit']) ? $getForm['limit'] : 10;

        $allcount = $query->count();
        $pagination = new Pagination([
            'defaultPageSize' => $limit,
            'totalCount' => $allcount
        ]);
        $allData = $query->orderBy('id ASC')->offset($pagination->offset)->limit($pagination->limit)->all();

        return $this->render('index', [
            'datas'=>$allData,
            'getForm' => $getForm,// 筛选条件
            'page' => $page,
            'limit' => $limit,
            'count' => $allcount
        ]);
    }

    /*
     * 添加分类页面
     */
    public function actionAdd(){
        return $this->render('add');
    }

    /**
     * 修改分类页
     *
     * @param int    $id     分类Id
     * @return array
     */
    public function actionEdit(){
        $id = Yii::$app->request->get('id');
        if(empty($id)){
            $this->alert('系统错误，请重试！');exit;
        }
        $data = SGoodsCategory::find()->where(['id' => $id, 'is_del'=> 0])->one();
        if(empty($data)){
            $this->alert('系统错误，请重试！');exit;
        }
        return $this->render('edit',[
            'data' => $data,
            'id' => $id,
        ]);
    }
    /**
     * 保存数据
     *
     * @param int    $id         分类Id
     * @param str    $name       分类名称
     * @param str    $icon_img   分类图标
     * @return string
     */
    public function actionSave(){
        if(Yii::$app->request->post()){
            $form = Yii::$app->request->post();
            if(empty($form['name'])){
                $this->alert('请输入分类名称');exit;
            }
            if(empty($form['icon_img'])){
                $this->alert('请上传分类图标');exit;
            }
            $icon_img = str_replace(Yii::$app->params['fileDomain'], '', $form['icon_img']);

            $record = '';
            if(isset($form['id']) && !empty($form['id'])){
                $_model = SGoodsCategory::find()->where(['id' => $form['id']])->one();
                $record = '商城分类编辑';
            }else{
                $_model = new SGoodsCategory();
                $record = '商城分类添加';
            }
            if($_model->icon_img != $icon_img){
                $_model->icon_img = $icon_img;
            }
            $_model->name = $form['name'];
            if($_model->save()){
                $this->addAdminLog($record,'表:hm_s_goods_category,id:'.$_model->id);
                $this->alert('提交成功', '/sgoodscategory/index');exit;
            }else{
                $this->alert('提交失败，请重试！');exit;
            }
        }
    }
    /*
     * 删除
     *
     * @param int    $id         分类Id
     */
    public function actionDelete(){
        $id = Yii::$app->request->get('id');
        if(empty($id)){
            $this->json('400','系统错误，请重试！');
        }

        $model = SGoodsCategory::find()->where(['id' => $id])->one();
        $model->is_del = 1;
        $model->deleted_at = date('Y-m-d H:i:s');
        $model->save();
        $this->addAdminLog('商城分类删除','表:hm_s_goods_category,id:'.$model->id);

        $this->json('200','操作成功');
    }

}
