<?php
namespace admin\controllers;

use Yii;
use yii\data\Pagination;
use common\models\Goods;
use common\models\GoodsUnit;
use common\models\GoodsCategory;

/*
 * 商品管理
 */
class GoodsController extends BaseController{

    public function init()
    {
        parent::init();
    }

    /*
     * 首页
     */
    public function actionIndex(){
        //筛选条件
        $getForm = Yii::$app->request->get();
        $query = new \yii\db\Query();
        $query = $query->select('s.*,p.name as cate_name');
        $query = $query->from('hm_goods as s');
        $query = $query->leftJoin('hm_goods_category as p', 'p.id = s.category_id');

        $where['s.is_del'] = 0;   //正常
        if (isset($getForm['category_id']) && $getForm['category_id'] != '') {      //分类
            $where['s.category_id'] = $getForm['category_id'];
        }
        $query = $query->where($where);
        if (isset($getForm['name']) && $getForm['name'] != '') {      //商品名称
            $query = $query->andFilterWhere(['like', 's.name', $getForm['name']]);
        }

        $page = (isset($getForm['page']) && $getForm['page']) ? $getForm['page'] : 1;
        $limit = (isset($getForm['limit']) && $getForm['limit']) ? $getForm['limit'] : 10;

        $allcount = $query->count();
        $pagination = new Pagination([
            'defaultPageSize' => $limit,
            'totalCount' => $allcount
        ]);
        $allData = $query->orderBy('s.id DESC')->offset($pagination->offset)->limit($pagination->limit)->all();

        //分类数据
        $cate_data = GoodsCategory::find()->select('id,name')->where(['is_del'=>0])->andWhere(['not', 'pid = 0'])->asArray()->all();

        return $this->render('index', [
            'datas'=>$allData,
            'cate_data'=>$cate_data,
            'getForm' => $getForm,//筛选条件
            'page' => $page,
            'limit' => $limit,
            'count' => $allcount
        ]);
    }

    /*
     * 添加商品
     */
    public function actionAdd(){
        //商品价格单位
        $unit = GoodsUnit::find()->select('id,name')->asArray()->all();

        return $this->render('add',[
            'unit' => $unit
        ]);
    }

    /*
     * 获取商品分类
     */
    public function actionGetCategory(){
        //商品分类
        $pdata = GoodsCategory::find()->select('id,name')->where(['is_del'=>0,'pid'=>0])->asArray()->all();
        if(empty($pdata)){
            $this->json('400','请先添加分类！');
        }
        $cate_data = [];
        foreach ($pdata as $key=>$val){
            $data = GoodsCategory::find()->select('id,name')->where(['is_del'=>0,'pid'=>$val['id']])->asArray()->all();
            $val['son'] = !empty($data) ? $data : '';
            $cate_data[] = $val;
        }

        $this->json('200','操作成功',$cate_data);
    }

    /*
     * 编辑商品
     */
    public function actionEdit(){
        $id = Yii::$app->request->get('id');
        if(empty($id)){
            $this->alert('系统错误，请重试！');exit;
        }

        $data = Goods::find()->where(['id' => $id, 'is_del'=> 0])->asArray()->one();
        if(empty($data)){
            $this->alert('系统错误，请重试！');exit;
        }
        $detail_imgs = explode(';',$data['detail_imgs']);

        $cate_data = GoodsCategory::find()->select('id,name')->where(['is_del'=>0,'id'=>$data['category_id']])->asArray()->one();
        //商品价格单位
        $unit = GoodsUnit::find()->select('id,name')->asArray()->all();

        return $this->render('edit',[
            'data' => $data,
            'detail_imgs' => $detail_imgs,
            'cate_data' => $cate_data,
            'unit' => $unit
        ]);
    }

    /*
     * 保存数据
     */
    public function actionSave()
    {
        if(Yii::$app->request->post()){
            $form = Yii::$app->request->post();
            if(empty($form['category_id'])){
                $this->alert('请选择分类！');exit;
            }

            $record = '';
            if(isset($form['id']) && !empty($form['id'])){
                $_model = Goods::find()->where(['id' => $form['id']])->one();
                $record = '编辑商品';
            }else{
                $_model = new Goods();
                $_model->created_at = date('Y-m-d H:i:s');
                $record = '添加商品';
            }
            $_model->name = $form['name'];
            $_model->category_id = $form['category_id'];
            $_model->price = $form['price'];
            $_model->unit = $form['unit'];
            $_model->content = $form['content'];
            $_model->thumbnail = $form['thumbnail'];
            $_model->status = $form['status'];
            $details_arr = $form['detail_imgs'];
            $detail_imgs = '';
            if(!empty($details_arr)){
                foreach ($details_arr as $key=>$val){
                    $detail_imgs .= $val . ';';
                }
            }
            $detail_imgs = trim($detail_imgs,';');
            $_model->detail_imgs = $detail_imgs;

            if($_model->save()){
                $this->addAdminLog($record,'表:hm_goods,id:'.$_model->id);
                $this->alert('提交成功', '/goods/index');exit;
            }else{
                $this->alert('提交失败，请重试！');exit;
            }
        }
    }

    /*
     * 商品删除
     */
    public function actionDelete()
    {
        $id = Yii::$app->request->get('id');
        if(empty($id)){
            $this->json('400','系统错误，请重试！');
        }

        $model = Goods::find()->where(['id' => $id])->one();
        $model->is_del = 1;
        $model->deleted_at = date('Y-m-d H:i:s');
        $model->save();

        $this->addAdminLog('商品删除','表:hm_goods,id:'.$model->id);

        $this->json('200','操作成功');
    }
}