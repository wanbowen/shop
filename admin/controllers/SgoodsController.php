<?php

namespace admin\controllers;

use Yii;
use yii\data\Pagination;
use common\models\SGoods;
use common\models\SSpecs;
use common\models\SGoodsCategory;

/*
 * 商品管理
 */
class SgoodsController extends BaseController{

    public function init(){
        parent::init();
    }

    /*
     * 首页
     */
    public function actionIndex(){
        // 筛选条件
        $getForm = Yii::$app->request->get();
        $where = '';
        // 是否上架
        if (isset($getForm['status']) && $getForm['status'] != '') {
            $where .= ' AND t1.status = ' . $getForm['status'];
        }
        // 是否推荐
        if (isset($getForm['is_recommend']) && $getForm['is_recommend'] != '') {
            $where .= ' AND t1.is_recommend = ' . $getForm['is_recommend'];
        }
        // 价格区间
        if (isset($getForm['price_min']) && $getForm['price_min'] != '') {
            $where .= ' AND CASE t1.`is_specs` WHEN 0 THEN t1.`price` >= ' . $getForm['price_min']*100 . ' WHEN 1 THEN t2.`price` >= '.$getForm['price_min']*100 . ' END ';
        }
        if (isset($getForm['price_max']) && $getForm['price_max'] != '') {
            $where .= ' AND CASE t1.`is_specs` WHEN 0 THEN t1.`price` <= ' . $getForm['price_max']*100 . ' WHEN 1 THEN t2.`price` <= ' . $getForm['price_max']*100 . ' END ';
        }
        // 高效检索
        if (isset($getForm['name']) && $getForm['name'] != '') {
            $where .= ' AND locate("' . $getForm['name'] . '", REPLACE(t1.`name`, " ", "")) > 0 ';
        }
        // 处理分页
        $page = (isset($getForm['page']) && $getForm['page']) ? $getForm['page'] : 1;
        $limit = (isset($getForm['limit']) && $getForm['limit']) ? $getForm['limit'] : 10;
        $offset = ($page-1) * $limit;
        $sql = "SELECT t1.`id`, t1.`name` AS goods_name, t1.`is_recommend`, t1.`status`, t1.`is_specs`, case t1.`is_specs` WHEN 1 THEN GROUP_CONCAT(t2.`id`,':!',t2.`name`,':!',round(t2.`price`/100,2),':!',t2.`stock` separator '%!') WHEN 0 THEN CONCAT(round(t1.`price`/100,2),':!',t1.`stock`) END AS `price`, t3.`name` AS category_name, t3.`id` AS category_id,t1.`created_at` FROM hm_s_goods t1 LEFT JOIN hm_s_specs t2 ON t1.`is_specs` = 1 AND t1.`id` = t2.`goods_id` AND t2.`is_del` = 0, hm_s_goods_category t3 WHERE t1.`is_del` = 0 AND t3.`is_del` = 0 AND t3.`id` = t1.`category_id` $where GROUP BY t1.`id` LIMIT {$offset},{$limit}";
        $allData = Yii::$app->getDb()->createCommand($sql)->query();
        // 统计总条数
        $sql = 'SELECT COUNT(num) AS num from(SELECT count(t1.`id`) AS num FROM hm_s_goods t1 LEFT JOIN hm_s_specs t2 ON t1.`is_specs` = 1 AND t1.`id` = t2.`goods_id` AND t2.`is_del` = 0, hm_s_goods_category t3 WHERE t1.`is_del` = 0 AND t3.`is_del` = 0 AND t3.`id` = t1.`category_id` '.$where.' GROUP BY t1.`id`) a';
        $allcount = Yii::$app->getDb()->createCommand($sql)->queryAll()[0]['num'];

        // 分类数据
        // $cate_data = SGoodsCategory::find()->select('id,name')->where(['is_del'=>0])->asArray()->all();

        return $this->render('index', [
            'datas'=>$allData,
            // 'cate_data'=>$cate_data,
            'getForm' => $getForm,// 筛选条件
            'page' => $page,
            'limit' => $limit,
            'count' => $allcount
        ]);
    }

    /*
     * 添加商品
     */
    public function actionAdd(){
        return $this->render('add');
    }

    /*
     * 获取商品分类
     */
    public function actionGetCategory(){
        //商品分类
        $data = SGoodsCategory::find()->select('id,name')->where(['is_del'=>0])->asArray()->all();
        if(empty($data)){
            $this->json('400','请先添加分类！');
        }
        $this->json('200','操作成功',$data);
    }

    /*
     * 编辑商品
     */
    public function actionEdit(){
        $id = Yii::$app->request->get('id');
        if(empty($id)){
            $this->alert('系统错误，请重试！');exit;
        }

        $data = SGoods::find()->where(['id' => $id, 'is_del'=> 0])->asArray()->one();
        if(empty($data)){
            $this->alert('系统错误，请重试！');exit;
        }
        $detail_imgs = explode(';',$data['detail_imgs']);

        $cate_data = SGoodsCategory::find()->select('id,name')->where(['is_del'=>0,'id'=>$data['category_id']])->asArray()->one();
        //商品价格单位
        $unit = SSpecs::find()->select('id,name')->asArray()->all();

        return $this->render('edit',[
            'data' => $data,
            'detail_imgs' => $detail_imgs,
            'cate_data' => $cate_data,
            'unit' => $unit
        ]);
    }

    /*
     * 保存数据
     */
    public function actionSave()
    {
        if(Yii::$app->request->post()){
            $form = Yii::$app->request->post();
            if(empty($form['category_id'])){
                $this->alert('请选择分类！');exit;
            }

            $record = '';
            if(isset($form['id']) && !empty($form['id'])){
                $_model = SGoods::find()->where(['id' => $form['id']])->one();
                $record = '编辑商品';
            }else{
                $_model = new Goods();
                $_model->created_at = date('Y-m-d H:i:s');
                $record = '添加商品';
            }
            $_model->name = $form['name'];
            $_model->category_id = $form['category_id'];
            $_model->price = $form['price'];
            $_model->unit = $form['unit'];
            $_model->content = $form['content'];
            $_model->thumbnail = $form['thumbnail'];
            $_model->status = $form['status'];
            $details_arr = $form['detail_imgs'];
            $detail_imgs = '';
            if(!empty($details_arr)){
                foreach ($details_arr as $key=>$val){
                    $detail_imgs .= $val . ';';
                }
            }
            $detail_imgs = trim($detail_imgs,';');
            $_model->detail_imgs = $detail_imgs;

            if($_model->save()){
                $this->addAdminLog($record,'表:hm_s_goods,id:'.$_model->id);
                $this->alert('提交成功', '/goods/index');exit;
            }else{
                $this->alert('提交失败，请重试！');exit;
            }
        }
    }

    /*
     * 商品删除
     */
    public function actionDelete()
    {
        $id = Yii::$app->request->get('id');
        if(empty($id)){
            $this->json('400','系统错误，请重试！');die;
        }
        // 启用事物
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $model = SGoods::find()->where(['id' => $id])->one();
            $model->is_del = 1;
            $model->deleted_at = date('Y-m-d H:i:s');
            $res = $model->save();
            if (!$res) {
                throw new \Exception('商品删除失败！');
            }

            $sql = "UPDATE `hm_s_specs` SET `is_del`='1' WHERE `goods_id`='$id'";
            $res = Yii::$app->getDb()->createCommand($sql)->execute();
            // $model = SSpecs::find()->where(['goods_id' => ])->one();
            // $model->is_del = 1;
            // $res = $model->save();
            if (!is_int($res)) {
                throw new \Exception('商品删除失败！');
            }

            $transaction->commit();
            $this->addAdminLog('商品删除','表:hm_s_goods,id:'.$model->id);
            $this->addAdminLog('商品规格删除','表:hm_s_specs,goods_id:'.$model->id);
            $this->json('200','操作成功');
        } catch (\Exception $e) {
            $transaction->rollBack();
            $this->json('200',$e->getMessage());
        }
    }

    /*
     * 商品上下架
     */
    public function actionIs_status()
    {
        $id = Yii::$app->request->get('id');
        $val = Yii::$app->request->get('val');
        if(empty($id) || ($val != 1 && $val != 0)){
            $this->json('400','系统错误，请重试！');die;
        }
        $model = SGoods::find()->where(['id' => $id])->one();
        $model->status = $val;
        $res = $model->save();
        if ($res) {
            if ($val == 1) {
                $this->addAdminLog('商品下架','表:hm_s_goods,id:'.$model->id);
            }else {
                $this->addAdminLog('商品上架','表:hm_s_goods,id:'.$model->id);
            }
            $this->json('200','操作成功');
        }else{
            $this->json('200','执行失败');
        }
    }

    /*
     * 商品推荐
     */
    public function actionRecommend()
    {
        $id = Yii::$app->request->get('id');
        $val = Yii::$app->request->get('val');
        if(empty($id) || ($val != 1 && $val != 0)){
            $this->json('400','系统错误，请重试！');die;
        }
        $model = SGoods::find()->where(['id' => $id])->one();
        $model->is_recommend = $val;
        $res = $model->save();
        if ($res) {
            if ($val == 1) {
                $this->addAdminLog('商品取消推荐','表:hm_s_goods,id:'.$model->id);
            }else {
                $this->addAdminLog('商品推荐','表:hm_s_goods,id:'.$model->id);
            }
            $this->json('200','操作成功');
        }else{
            $this->json('200','执行失败');
        }
    }
}
