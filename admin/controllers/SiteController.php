<?php
namespace admin\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use admin\models\LoginForm;
use admin\models\Admin;
use common\components\VerifyHelper;

/**
 * Site controller
 */
class SiteController  extends \yii\web\Controller
{
    public $enableCsrfValidation = false;
	/**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error','verify'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index','personalset','personalsetpost'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
		return $this->render('index');
    }
	 public function actionPersonalset($id)
    {
        $this->layout = false;
		$admin=Admin::findOne($id);
		return $this->render('personalset',['admin'=>$admin]);
    }
	/**
	 * 个人信息修改
	 */
	 public function actionPersonalsetpost($id){
        if(Yii::$app->request->isAjax&&Yii::$app->request->post()){
            $post=Yii::$app->request->post();
            $username=$post['username'];
            $yuanpassword=$post['yuanpassword'];
            $password=$post['password'];
			if(!preg_match("/^(?=.*[0-9])(?=.*[a-zA-Z])(?=.*[^a-zA-Z0-9]).{8,30}$/",$password)){
				$this->alert('密码至少为8位的字母、数字和特殊符号的组合！');exit;
			}
            $model=Admin::findOne($id);
			$bool = Yii::$app->security->validatePassword($yuanpassword, $model->password_hash);
			if(!$bool){
				 $this->json(204,'原密码不正确'); exit;
			}
            if(isset($password)&&!empty($password)){
				$model->password_hash=$model->setPassword($password);
				$model->auth_key=$model->generateAuthKey();
			}
            if($model->save()){
                $this->json(200,'修改成功'); exit;
            }else{
               $this->json(201,'修改失败'); exit;
            }
        }else{
            $this->json(203,'程序错误'); exit;
        }
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        $this->layout = 'login';
		if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
			$username=Yii::$app->user->identity->username ? Yii::$app->user->identity->username:"";
			if(!empty($username)){
				$admin=Admin::find()->where(['username'=>$username])->one();
				$admin->lastlogintime=time();
				$admin->lastloginip=$this->get_client_ip();
				$admin->save();
			}
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
	/**
	 * 获取客户端IP地址
	 * @param integer $type 返回类型 0 返回IP地址 1 返回IPV4地址数字
	 * @param boolean $adv 是否进行高级模式获取（有可能被伪装） 
	 * @return mixed
	 */
	public function get_client_ip($type = 0,$adv=false) {
		$type       =  $type ? 1 : 0;
		static $ip  =   NULL;
		if ($ip !== NULL) return $ip[$type];
		if($adv){
			if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
				$arr    =   explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
				$pos    =   array_search('unknown',$arr);
				if(false !== $pos) unset($arr[$pos]);
				$ip     =   trim($arr[0]);
			}elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
				$ip     =   $_SERVER['HTTP_CLIENT_IP'];
			}elseif (isset($_SERVER['REMOTE_ADDR'])) {
				$ip     =   $_SERVER['REMOTE_ADDR'];
			}
		}elseif (isset($_SERVER['REMOTE_ADDR'])) {
			$ip     =   $_SERVER['REMOTE_ADDR'];
		}
		// IP地址合法验证
		$long = sprintf("%u",ip2long($ip));
		$ip   = $long ? array($ip, $long) : array('0.0.0.0', 0);
		return $ip[$type];
	}
	/**
     * json转换
     * @param  $ret =>状态码,$msg=>包含信息,$data=>数据,
     * @return json
     * @author ycl
     * @date 2018/4/17
     */
    public function json($ret, $msg = '', $data = array())
    {
        $dataRet = array('ret' => $ret);
        $dataRet['msg'] = $msg;
        $dataRet['data'] = $data;
        $dataJson = json_encode($dataRet, JSON_UNESCAPED_SLASHES);
        echo $dataJson;
    }
	/**
     * 生成验证码.
     * author yuchenglong
     * @return img
     */
	 public function actionVerify()
    {
		$Verify = new VerifyHelper();
		$Verify->useImgBg = true;
		$Verify->fontSize = 36;
		$Verify->length   = 6;
		$Verify->useNoise = true;
		$Verify->entry();
	}
	
}
