<?php
namespace admin\controllers;

use Yii;
use yii\data\Pagination;
use admin\models\AdminLog;

/*
 * 操作日志管理
 */
class AdminlogController extends BaseController{

    public function init()
    {
        parent::init();
    }

    /*
     * 首页
     */
    public function actionIndex()
    {
        //筛选条件
        $getForm = Yii::$app->request->get();
        $query = new \yii\db\Query();
        $query = $query->select('o.*,g.username');
        $query = $query->from('hm_admin_log as o');
        $query = $query->leftJoin('hm_admin as g', 'g.id = o.admin_id');

        if (isset($getForm['username']) && $getForm['username'] != '') {      //名称
            $query = $query->andFilterWhere(['like', 'g.username', $getForm['username']]);
        }
        if (isset($getForm['client_ip']) && $getForm['client_ip'] != '') {      //IP
            $query = $query->andFilterWhere(['like', 'o.client_ip', $getForm['client_ip']]);
        }
        if (isset($getForm['create_time']) && $getForm['create_time'] != '') {  // 模糊查询时间
            $query = $query->andFilterWhere(['>', 'o.created_at', $getForm['create_time'].' 00:00:00']);
        }
        if (isset($getForm['end_time']) && $getForm['end_time'] != '') {
            $query = $query->andFilterWhere(['<=', 'o.created_at', $getForm['end_time'].' 23:59:59']);
        }

        $page = (isset($getForm['page']) && $getForm['page']) ? $getForm['page'] : 1;
        $limit = (isset($getForm['limit']) && $getForm['limit']) ? $getForm['limit'] : 10;

        $allcount = $query->count();
        $pagination = new Pagination([
            'defaultPageSize' => $limit,
            'totalCount' => $allcount
        ]);
        $allData = $query->orderBy('id DESC')->offset($pagination->offset)->limit($pagination->limit)->all();

        return $this->render('index', [
            'datas'=>$allData,
            'getForm' => $getForm,//筛选条件
            'page' => $page,
            'limit' => $limit,
            'count' => $allcount
        ]);
    }
}