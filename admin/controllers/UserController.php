<?php
namespace admin\controllers;

use Yii;
use yii\data\Pagination;
use common\models\User;

/*
 * 用户管理
 */
class UserController extends BaseController{

    public function init()
    {
        parent::init();
    }

    /*
     * 首页
     */
    public function actionIndex(){
        //筛选条件
        $getForm = Yii::$app->request->get();
        $query = User::find();

        $where['status'] = 0;   //正常用户
        $query = $query->where($where);
        if (isset($getForm['name']) && $getForm['name'] != '') {      //用户姓名
            $query = $query->andFilterWhere(['like', 'name', $getForm['name']]);
        }
        if (isset($getForm['mobile']) && $getForm['mobile'] != '') {      //手机号
            $query = $query->andFilterWhere(['like', 'mobile', $getForm['mobile']]);
        }

        $page = (isset($getForm['page']) && $getForm['page']) ? $getForm['page'] : 1;
        $limit = (isset($getForm['limit']) && $getForm['limit']) ? $getForm['limit'] : 10;

        $allcount = $query->count();
        $pagination = new Pagination([
            'defaultPageSize' => $limit,
            'totalCount' => $allcount
        ]);
        $allData = $query->orderBy('id DESC')->offset($pagination->offset)->limit($pagination->limit)->all();

        return $this->render('index', [
            'datas'=>$allData,
            'getForm' => $getForm,//筛选条件
            'page' => $page,
            'limit' => $limit,
            'count' => $allcount
        ]);
    }

    /*
     * 黑名单
     */
    public function actionBlacklist(){
        //筛选条件
        $getForm = Yii::$app->request->get();
        $query = User::find();

        $where['status'] = 1;   //黑名单
        $query = $query->where($where);
        if (isset($getForm['name']) && $getForm['name'] != '') {      //用户姓名
            $query = $query->andFilterWhere(['like', 'name', $getForm['name']]);
        }
        if (isset($getForm['mobile']) && $getForm['mobile'] != '') {      //手机号
            $query = $query->andFilterWhere(['like', 'mobile', $getForm['mobile']]);
        }

        $page = (isset($getForm['page']) && $getForm['page']) ? $getForm['page'] : 1;
        $limit = (isset($getForm['limit']) && $getForm['limit']) ? $getForm['limit'] : 10;

        $allcount = $query->count();
        $pagination = new Pagination([
            'defaultPageSize' => $limit,
            'totalCount' => $allcount
        ]);
        $allData = $query->orderBy('id DESC')->offset($pagination->offset)->limit($pagination->limit)->all();

        return $this->render('blacklist', [
            'datas'=>$allData,
            'getForm' => $getForm,//筛选条件
            'page' => $page,
            'limit' => $limit,
            'count' => $allcount
        ]);
    }

    /*
     * 加入/撤销 黑名单
     */
    public function actionDelete()
    {
        $id = Yii::$app->request->get('id');
        $sta = Yii::$app->request->get('sta');
        if(empty($id)){
            $this->json('400','系统错误，请重试！');
        }

        $model = User::find()->where(['id' => $id])->one();
        $model->status = $sta;
        $model->save();

        $record = $sta == 1 ? '加入黑名单' : '撤销黑名单';
        $this->addAdminLog('用户'.$record,'表:hm_user,id:'.$model->id);

        $this->json('200','操作成功');
    }
}