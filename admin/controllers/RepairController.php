<?php
namespace admin\controllers;

use Yii;
use yii\data\Pagination;
use common\models\Staff;
use common\models\Repair;
use common\models\StaffProfession;

/*
 * 公众号  报时报修
 */
class RepairController extends BaseController{

    public function init()
    {
        parent::init();
    }

    /*
     * 首页
     */
    public function actionIndex()
    {
        //筛选条件
        $getForm = Yii::$app->request->get();
        $query = Repair::find();

        $where['is_del'] = 0;
        if (isset($getForm['status']) && $getForm['status'] >= 0) {    //状态
            $where['status'] = $getForm['status'];
        }
        $query = $query->where($where);
        if (isset($getForm['repair_code']) && $getForm['repair_code'] != '') {      //订单号
            $query = $query->andFilterWhere(['like', 'repair_code', $getForm['repair_code']]);
        }
        if (isset($getForm['contact_tel']) && $getForm['contact_tel'] != '') {      //联系人电话
            $query = $query->andFilterWhere(['like', 'contact_tel', $getForm['contact_tel']]);
        }
        if (isset($getForm['create_time']) && $getForm['create_time'] != '') {  // 模糊查询时间
            $query = $query->andFilterWhere(['>', 'created_at', $getForm['create_time'].' 00:00:00']);
        }
        if (isset($getForm['end_time']) && $getForm['end_time'] != '') {
            $query = $query->andFilterWhere(['<=', 'created_at', $getForm['end_time'].' 23:59:59']);
        }

        $page = (isset($getForm['page']) && $getForm['page']) ? $getForm['page'] : 1;
        $limit = (isset($getForm['limit']) && $getForm['limit']) ? $getForm['limit'] : 10;

        $allcount = $query->count();
        $pagination = new Pagination([
            'defaultPageSize' => $limit,
            'totalCount' => $allcount
        ]);
        $allData = $query->orderBy('id DESC')->offset($pagination->offset)->limit($pagination->limit)->all();

        return $this->render('index', [
            'datas'=>$allData,
            'getForm' => $getForm,//筛选条件
            'page' => $page,
            'limit' => $limit,
            'count' => $allcount
        ]);
    }

    /*
     * 查看报修单
     */
    public function actionView()
    {
        $id = Yii::$app->request->get('id');
        if(empty($id)){
            $this->alert('系统错误，请重试！');exit;
        }

        $data = Repair::find()->where(['id'=>$id])->asArray()->one();
        if(empty($data)){
            $this->alert('系统错误，请重试！');exit;
        }
        //详情图片
        $imgs_arr = [];
        if(!empty($data['detail_imgs'])){
            $arr = explode(';',$data['detail_imgs']);
            foreach ($arr as $val){
                $url = Yii::$app->params['fileDomain'].$val;
                $imgs_arr[] = $url;
            }
        }
        $staff_data = Staff::find()->select('name,mobile')->where(['id'=>$data['staff_id']])->asArray()->one();

        return $this->render('view',[
            'data' => $data,
            'imgs_arr' => $imgs_arr,
            'staff_data' => $staff_data
        ]);
    }

    /*
     * 获取员工职业
     */
    public function actionGetProfession()
    {
        //获取
        $profession_data = StaffProfession::find()->select('id,name')->asArray()->all();
        if(empty($profession_data)){
            $this->json('400','无数据，请先添加员工职业');
        }

        $this->json('200', '成功', $profession_data);
    }

    /*
     * 获取员工
     */
    public function actionGetStaff()
    {
        $pro_id = Yii::$app->request->get('pro_id');
        if(empty($pro_id)){
            $this->json('400','系统错误，请重试！');
        }
        //获取
        $staff_data = Staff::find()->select('id,name')->where(['profession_id'=>$pro_id, 'is_del'=>0])->asArray()->all();

        $this->json('200', '成功', $staff_data);
    }

    /*
     * 派单
     */
    public function actionSave()
    {
        if(Yii::$app->request->post()){
            $form = Yii::$app->request->post();
            if(empty($form['rid'])){
                $this->alert('系统错误，请重试！');exit;
            }
            if(empty($form['staff_id'])){
                $this->alert('请选择接单人员');exit;
            }
            //接单人信息
            $staff_data = Staff::find()->select('id,name,mobile')->where(['id'=>$form['staff_id'], 'is_del'=>0])->asArray()->one();
            if(empty($staff_data)){
                $this->alert('不存在此员工！');exit;
            }

            $_model = Repair::find()->where(['id' => $form['rid']])->one();
            if($_model->status > 0){
                $this->alert('已经派单，不可重复派单');exit;
            }
            if($_model->is_del == 1){
                $this->alert('此订单已删除');exit;
            }
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $_model->staff_id = $form['staff_id'];
                $_model->status = 1;
                $_model->reply_at = date('Y-m-d H:i:s');
                if($_model->save()){
                    $this->addAdminLog('报时报修派单','表:hm_repair,id:'.$_model->id);
                    //给员工 发短信
                    $tel1 = $staff_data['mobile'];
                    $content1 = $this->repairTem($staff_data['name'],$_model->position,$_model->content);
                    $ret1 = $this->sendSms($tel1, $content1);
                    //给联系人 发短信
                    $tel2 = $_model->contact_tel;
                    $content2 = $this->repairTem2($staff_data['name']);
                    $ret2 = $this->sendSms($tel2, $content2);
                    if($ret1 && $ret2){
                        $transaction->commit();
                        $this->alert('派单成功','/repair/index');exit;
                    }else{
                        $this->alert('短信发送失败，请重试！');exit;
                    }
                }else{
                    $this->alert('派单失败，请重试！');exit;
                }
            } catch (Exception $e) {
                //回滚
                $transaction->rollBack();
                $this->alert('派单失败，请重试！');exit;
            }
        }
    }
    /*
     * 删除
     */
    public function actionDelete()
    {
        $id = Yii::$app->request->get('id');
        if(empty($id)){
            $this->json('400','系统错误，请重试！');
        }

        $model = Repair::find()->where(['id' => $id])->one();
        $model->is_del = 1;
        $model->deleted_at = date('Y-m-d H:i:s');
        $model->save();
        $this->addAdminLog('报时报修删除','表:hm_repair,id:'.$model->id);

        $this->json('200','操作成功');
    }
}