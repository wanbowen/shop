<?php
namespace admin\controllers;

use Yii;
use yii\data\Pagination;
use common\models\User;
use common\models\Staff;
use common\models\Goods;
use common\models\Order;
use common\models\OrderGoods;
use common\models\GoodsCategory;

/*
 * 订单管理
 */
class OrderController extends BaseController{

    public function init()
    {
        parent::init();
    }

    /*
     * 首页
     */
    public function actionIndex()
    {
        //筛选条件
        $getForm = Yii::$app->request->get();
        $query = new \yii\db\Query();
        $query = $query->select('o.id,o.order_code,o.mobile,o.order_total,o.order_status,o.order_remarks,o.visit_remarks,o.book_date,o.created_at,g.goods_name');
        $query = $query->from('hm_order as o');
        $query = $query->leftJoin('hm_order_goods as g', 'g.order_code = o.order_code');

        $where['g.is_edit'] = 0;    //最先购买的商品
        if (isset($getForm['status']) && $getForm['status'] >= 0) {    //状态
            $where['o.order_status'] = $getForm['status'];
            $query = $query->where($where);
        } else {
            $query = $query->where($where);
            $query = $query->andWhere(['not in', 'order_status', [6, 7]]);
        }

        if (isset($getForm['order_code']) && $getForm['order_code'] != '') {      //订单号
            $query = $query->andFilterWhere(['like', 'o.order_code', $getForm['order_code']]);
        }
        if (isset($getForm['mobile']) && $getForm['mobile'] != '') {      //联系人电话
            $query = $query->andFilterWhere(['like', 'o.mobile', $getForm['mobile']]);
        }
        if (isset($getForm['create_time']) && $getForm['create_time'] != '') {  // 模糊查询时间
            $query = $query->andFilterWhere(['>', 'o.created_at', $getForm['create_time'].' 00:00:00']);
        }
        if (isset($getForm['end_time']) && $getForm['end_time'] != '') {
            $query = $query->andFilterWhere(['<=', 'o.created_at', $getForm['end_time'].' 23:59:59']);
        }

        $page = (isset($getForm['page']) && $getForm['page']) ? $getForm['page'] : 1;
        $limit = (isset($getForm['limit']) && $getForm['limit']) ? $getForm['limit'] : 10;

        $allcount = $query->count();
        $pagination = new Pagination([
            'defaultPageSize' => $limit,
            'totalCount' => $allcount
        ]);
        $allData = $query->orderBy('o.id DESC')->offset($pagination->offset)->limit($pagination->limit)->all();


        return $this->render('index', [
            'datas'=>$allData,
            'getForm' => $getForm,//筛选条件
            'page' => $page,
            'limit' => $limit,
            'count' => $allcount
        ]);
    }

    /*
     * 查看订单
     */
    public function actionView()
    {
        $id = Yii::$app->request->get('id');
        if(empty($id)){
            $this->alert('系统错误，请重试！');exit;
        }
        //订单信息
        $data = Order::find()->where(['id'=>$id])->asArray()->one();
        if(empty($data)){
            $this->alert('系统错误，请重试！');exit;
        }
        //订单商品信息
        $goods = OrderGoods::find()->where(['order_code'=>$data['order_code']])->asArray()->all();

        $staff_data = Staff::find()->select('name,mobile')->where(['id'=>$data['staff_id']])->asArray()->one();

        return $this->render('view',[
            'data' => $data,
            'goods' => $goods,
            'staff_data' => $staff_data
        ]);
    }

    /*
     * 修改订单
     */
    public function actionEdit()
    {
        $id = Yii::$app->request->get('id');
        if(empty($id)){
            $this->alert('系统错误，请重试！');exit;
        }
        //订单信息
        $data = Order::find()->where(['id'=>$id])->asArray()->one();
        if(empty($data)){
            $this->alert('系统错误，请重试！');exit;
        }
        //订单商品信息
        $goods = OrderGoods::find()->where(['order_code'=>$data['order_code']])->asArray()->all();

        $staff_data = Staff::find()->select('name,mobile')->where(['id'=>$data['staff_id']])->asArray()->one();

        return $this->render('edit',[
            'data' => $data,
            'goods' => $goods,
            'staff_data' => $staff_data
        ]);
    }

    /*
     * 派单
     */
    public function actionSave()
    {
        if(Yii::$app->request->post()){
            $form = Yii::$app->request->post();
            if(empty($form['order_id'])){
                $this->alert('系统错误，请重试！');exit;
            }
            if(empty($form['staff_id'])){
                $this->alert('请选择接单人员');exit;
            }
            if(empty($form['home_date']) || empty($form['strat_time']) || empty($form['end_time'])){
                $this->alert('请选择上门时间');exit;
            }
            //接单人信息
            $staff_data = Staff::find()->select('id,name,mobile')->where(['id'=>$form['staff_id'], 'is_del'=>0])->asArray()->one();
            if(empty($staff_data)){
                $this->alert('不存在此员工！');exit;
            }

            $_model = Order::find()->where(['id' => $form['order_id']])->one();
            if($_model->order_status > 0){
                $this->alert('已经派单，不可重复派单');exit;
            }
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $_model->staff_id = $form['staff_id'];
                $_model->order_status = 2;
                $_model->home_date = $form['home_date'].$form['strat_time'].'至'.$form['end_time'];
                $_model->reply_at = date('Y-m-d H:i:s');
                if($_model->save()){
                    //给服务人员发短信
                    $goods = OrderGoods::find()->select('goods_name')->where(['order_code'=>$_model->order_code,'is_edit'=>0])->asArray()->one();
                    $content1 = $this->serverTem($staff_data['name'],$_model->home_date,$_model->address,$goods['goods_name'],$_model->remarks);
                    $tel1 = $staff_data['mobile'];
                    $ret1 = $this->sendSms($tel1, $content1);
                    //给客户发短信
                    $tel2 = $_model->mobile;
                    $user = User::find()->select('name')->where(['id'=>$_model->user_id])->asArray()->one();
                    $content2 = $this->userTem($user['name'],$_model->order_code,$staff_data['name'],$_model->home_date,$staff_data['mobile'],Yii::$app->params['customerPhone']);
                    $ret2 = $this->sendSms($tel2, $content2);
                    if($ret1 && $ret2){
                        $this->addAdminLog('订单号：'.$_model->order_code.'，派单','表:hm_order,id:'.$_model->id);
                        $transaction->commit();
                        $this->alert('派单成功','/order/index');exit;
                    }else{
                        $this->alert('短信发送失败，请重试！');exit;
                    }
                }else{
                    $this->alert('派单失败，请重试！');exit;
                }
            } catch (Exception $e) {
                //回滚
                $transaction->rollBack();
                $this->alert('派单失败，请重试！');exit;
            }
        }
    }

    /*
     * 订单备注
     */
    public function actionRemarks()
    {
        $id = Yii::$app->request->post('order_id');
        $order_remarks = Yii::$app->request->post('order_remarks');
        if(empty($id)){
            $this->json('400','系统错误，请重试！');
        }
        if(empty($order_remarks)){
            $this->json('400','请输入备注');
        }

        $model = Order::find()->where(['id' => $id])->one();
        $model->order_remarks = $order_remarks;
        $model->save();

        $this->addAdminLog('订单号：'.$model->order_code.'，填写订单备注','表:hm_order,id:'.$model->id);

        $this->json('200','操作成功');
    }

    /*
     * 订单回访备注
     */
    public function actionVsremarks()
    {
        $id = Yii::$app->request->post('order_id');
        $visit_remarks = Yii::$app->request->post('visit_remarks');
        if(empty($id)){
            $this->json('400','系统错误，请重试！');
        }
        if(empty($visit_remarks)){
            $this->json('400','请输入备注');
        }

        $model = Order::find()->where(['id' => $id])->one();
        $model->visit_remarks = $visit_remarks;
        $model->save();

        $this->addAdminLog('订单号：'.$model->order_code.'，填写订单回访备注','表:hm_order,id:'.$model->id);

        $this->json('200','操作成功');
    }

    /*
     * 取消订单
     */
    public function actionCancel()
    {
        $id = Yii::$app->request->post('order_id');
        if(empty($id)){
            $this->json('400','系统错误，请重试！');
        }

        $model = Order::find()->where(['id' => $id])->one();
        $model->order_status = 8;
        $model->save();

        $this->addAdminLog('订单号：'.$model->order_code.'，订单取消','表:hm_order,id:'.$model->id);

        $this->json('200','操作成功');
    }

    /*
     * 获取分类
     */
    public function actionGetCategory(){
        $pid = Yii::$app->request->get('pid');
        $pid = !empty($pid) ? $pid : 0;
        //商品分类
        $pdata = GoodsCategory::find()->select('id,name')->where(['is_del'=>0,'pid'=>$pid])->asArray()->all();
        $this->json('200','操作成功',$pdata);
    }

    /*
     * 获取商品
     */
    public function actionGetGoods(){
        $cid = Yii::$app->request->get('cate_id');
        //商品
        $data = Goods::find()->select('id,name,price,unit')->where(['category_id' => $cid, 'is_del'=> 0])->asArray()->all();
        $this->json('200','操作成功',$data);
    }

    /*
     * 修改订单
     */
    public function actionEditOrder(){
        $order_id = Yii::$app->request->post('order_id');
        $ids = Yii::$app->request->post('ids');
        $amounts = Yii::$app->request->post('amounts');
        if(empty($order_id)){
            $this->json('400','系统错误，请重试！');
        }

        $order_model = Order::find()->where(['id' => $order_id])->one();
        if(empty($order_model)){
            $this->json('400','订单不存在！');
        }

        //如果没有新增加商品项目，只修改订单状态
        if(empty($ids)){
            $order_model->order_status = 2;
            if($order_model->save()){
                $this->addAdminLog('订单号：'.$order_model->order_code.'，修改订单(没有增加服务项目，只修改订单状态)','表:hm_order,id:'.$order_model->id);
                $this->json('200','提交成功');
            }else{
                $this->json('402','提交失败，请重试！');
            }
        }
        $id_arr = explode(',',trim($ids,','));
        $amount_arr = explode(',',trim($amounts,','));

        $transaction = Yii::$app->db->beginTransaction();
        try {
            $total = 0;
            $fwnames = '';
            foreach ($id_arr as $key=>$val){
                //商品
                $goods_data = Goods::find()->select('id,name,price,unit')->where(['id' => $val, 'is_del'=> 0])->asArray()->one();
                if(empty($goods_data)){
                    $this->json('400','商品不存在！');
                }
                $order_goods_model = new OrderGoods();
                $order_goods_model->order_code = $order_model->order_code;
                $order_goods_model->user_id = $order_model->user_id;
                $order_goods_model->goods_id = $goods_data['id'];
                $order_goods_model->goods_name = $goods_data['name'];
                $order_goods_model->price = $goods_data['price'];
                $order_goods_model->unit = $goods_data['unit'];
                $order_goods_model->amount = $amount_arr[$key];
                $this_total = $amount_arr[$key] * $goods_data['price'];
                $total = $total + $this_total;
                $order_goods_model->price_total = $this_total;
                $order_goods_model->is_edit = 1;
                $order_goods_model->created_at = date('Y-m-d H:i:s');
                if(!$order_goods_model->save()){
                    $this->json('401','提交失败，请重试！');
                }
                $fwnames .= $goods_data['name'].'('.$amount_arr[$key].$goods_data['unit'].'),';
            }
            $fwnames = trim($fwnames,',');
            //修改订单
            $order_model->order_total = $total + $order_model->order_total;
            $order_model->order_status = 2;
            if($order_model->save()){
                $this->addAdminLog('订单号：'.$order_model->order_code.'，修改订单，增加服务项:'.$fwnames,'表:hm_order,id:'.$order_model->id);
                $transaction->commit();
                $this->json('200','提交成功');
            }else{
                $this->json('402','提交失败，请重试！');
            }
        } catch (Exception $e) {
            //回滚
            $transaction->rollBack();
            $this->json('400','订单修改失败，请重试！');
        }


    }
}