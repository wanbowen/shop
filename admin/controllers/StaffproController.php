<?php
namespace admin\controllers;

use Yii;
use yii\data\Pagination;
use common\models\Staff;
use common\models\StaffProfession;

/*
 * 员工职业管理
 */
class StaffproController extends BaseController{

    public function init()
    {
        parent::init();
    }

    /*
     * 首页
     */
    public function actionIndex(){
        //筛选条件
        $getForm = Yii::$app->request->get();
        $query = StaffProfession::find();

        $where['is_del'] = 0;   //正常
        $query = $query->where($where);
        if (isset($getForm['name']) && $getForm['name'] != '') {      //名称
            $query = $query->andFilterWhere(['like', 'name', $getForm['name']]);
        }

        $page = (isset($getForm['page']) && $getForm['page']) ? $getForm['page'] : 1;
        $limit = (isset($getForm['limit']) && $getForm['limit']) ? $getForm['limit'] : 10;

        $allcount = $query->count();
        $pagination = new Pagination([
            'defaultPageSize' => $limit,
            'totalCount' => $allcount
        ]);
        $allData = $query->orderBy('id DESC')->offset($pagination->offset)->limit($pagination->limit)->all();

        return $this->render('index', [
            'datas'=>$allData,
            'getForm' => $getForm,//筛选条件
            'page' => $page,
            'limit' => $limit,
            'count' => $allcount
        ]);
    }

    /*
     * 添加职业
     */
    public function actionAdd(){
        return $this->render('add',[]);
    }

    /*
     * 编辑职业
     */
    public function actionEdit(){
        $id = Yii::$app->request->get('id');
        if(empty($id)){
            $this->alert('系统错误，请重试！');exit;
        }

        $data = StaffProfession::find()->where(['id' => $id, 'is_del'=> 0])->asArray()->one();
        if(empty($data)){
            $this->alert('系统错误，请重试！');exit;
        }

        return $this->render('edit',[
            'data' => $data
        ]);
    }

    /*
     * 保存数据
     */
    public function actionSave()
    {
        if(Yii::$app->request->post()){
            $form = Yii::$app->request->post();

            $record = '';
            if(isset($form['id']) && !empty($form['id'])){
                $_model = StaffProfession::find()->where(['id' => $form['id']])->one();
                $record = '编辑职业';
            }else{
                $_model = new StaffProfession();
                $_model->created_at = date('Y-m-d H:i:s');
                $record = '添加职业';
            }
            $_model->name = $form['name'];
            if($_model->save()){
                $this->addAdminLog($record,'表:hm_staff_profession,id:'.$_model->id);
                $this->alert('提交成功', '/staffpro/index');exit;
            }else{
                $this->alert('提交失败，请重试！');exit;
            }
        }
    }

    /*
     * 删除
     */
    public function actionDelete()
    {
        $id = Yii::$app->request->get('id');
        if(empty($id)){
            $this->json('400','系统错误，请重试！');
        }

        $yanzheng = Staff::find()->where(['profession_id' => $id])->count();
        if($yanzheng > 0){
            $this->json('400','此职业含有所属员工，不可删除！');
        }

        $model = StaffProfession::find()->where(['id' => $id])->one();
        $model->is_del = 1;
        $model->save();

        $this->addAdminLog('删除职业','表:hm_staff_profession,id:'.$model->id);

        $this->json('200','操作成功');
    }
}