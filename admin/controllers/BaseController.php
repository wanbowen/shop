<?php
namespace admin\controllers;

use  Yii;
use admin\models\AdminLog;
use common\components\Sms;
use common\components\Uploader;
use common\models\SmsTemplet;

class BaseController extends \yii\web\Controller{
    public function init()
    {
		$this->enableCsrfValidation = false;

		if (Yii::$app->user->isGuest) {
            return $this->goHome();
        }
    }

    /**
     * 添加操作记录
     * @param string $record 操作描述
     * @param string $data 操作库名和id
     */
    public function addAdminLog($record, $data)
    {
        $log_model = new AdminLog();
        $log_model->admin_id = Yii::$app->user->identity->id;
        $log_model->client_ip = $this->get_client_ip(0,true);
        $log_model->record = $record;
        $log_model->data = $data;
        $log_model->created_at = date('Y-m-d H:i:s');
        $log_model->save();
    }

	/**
	 * 获取客户端IP地址
	 * @param integer $type 返回类型 0 返回IP地址 1 返回IPV4地址数字
	 * @param boolean $adv 是否进行高级模式获取（有可能被伪装）
	 * @return mixed
	 */
	public function get_client_ip($type = 0,$adv=false) {
		$type       =  $type ? 1 : 0;
		static $ip  =   NULL;
		if ($ip !== NULL) return $ip[$type];
		if($adv){
			if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
				$arr    =   explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']);
				$pos    =   array_search('unknown',$arr);
				if(false !== $pos) unset($arr[$pos]);
				$ip     =   trim($arr[0]);
			}elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
				$ip     =   $_SERVER['HTTP_CLIENT_IP'];
			}elseif (isset($_SERVER['REMOTE_ADDR'])) {
				$ip     =   $_SERVER['REMOTE_ADDR'];
			}
		}elseif (isset($_SERVER['REMOTE_ADDR'])) {
			$ip     =   $_SERVER['REMOTE_ADDR'];
		}
		// IP地址合法验证
		$long = sprintf("%u",ip2long($ip));
		$ip   = $long ? array($ip, $long) : array('0.0.0.0', 0);
		return $ip[$type];
	}

	/**
     * json转换
     * @param  $ret =>状态码,$msg=>包含信息,$data=>数据,
     * @return json
     * @author ycl
     * @date 2018/4/17
     */
    public function json($ret, $msg = '', $data = array())
    {
        $dataRet = array('code' => $ret);
        $dataRet['msg'] = $msg;
        $dataRet['data'] = $data;
        $dataJson = json_encode($dataRet, JSON_UNESCAPED_SLASHES);
        echo $dataJson;die();
    }

	//检查操作权限
	public function checkrolepre()
    {
        $controller = Yii::$app->controller->id;
		$action =Yii::$app->controller->action->id;
		CheckpreController::checkpre($controller,$action);
    }

    //发送短信
    public function sendSms($tel, $content){
        //获取Sms对象
        $sms_model = new Sms(Yii::$app->params['jvtdSmsUrl'],Yii::$app->params['jvtdSmsUid'],Yii::$app->params['jvtdSmsPass']);
        //发送短信
        $res = $sms_model->SendSms($tel, $content);
        //记录短信发送
        $txt = '['.date('Y-m-d H:i:s').']'.'手机号：'.$tel.'。内容：'.$content.'【返回结果：code：'.$res['error_code'].'，msg：'.$res['error_msg'].'】';
        $this->writeLog($txt, 'sendsms');
        //返回发送结果
        if($res['error_code'] > 0){
            return true;
        }
        return false;
    }

    /**
     * ueditor编辑器 文件上传
     * @author  lvhengbin
     * @time    2019/06/04
     */
    public function actionUploadfile() {
        $config = array(
            "pathFormat" => '/upload/{yyyy}{mm}{dd}/{time}{rand:6}',    //图片保存路径
            "maxSize" => 10485760,   //上传大小限制，单位B
            "allowFiles" => [".png", ".jpg", ".jpeg", ".gif", ".bmp"]   //上传图片格式限制
        );
        $fieldName = 'upfile';
        $up = new Uploader($fieldName, $config);
        $info = $up->getFileInfo();
        $info['url'] = Yii::$app->params['kffileDomain'] . $info['url'];
        /* 返回数据 */
        return json_encode($info);
    }

    /**
     * 给服务人员发送短信模板套用
     * @author  lvhengbin
     */
    public function serverTem($staff, $date, $address, $server, $remarks)
    {
        $tem_two_data = SmsTemplet::find()->where(['type'=>2])->asArray()->one();
        $cont = $tem_two_data['content'];
        $cont = str_replace('{staff}', $staff, $cont);
        $cont = str_replace('{date}', $date, $cont);
        $cont = str_replace('{address}', $address, $cont);
        $cont = str_replace('{server}', $server, $cont);
        $cont = str_replace('{remarks}', $remarks, $cont);
        return $cont;
    }

    /**
     * 给客户发送短信模板套用
     * @author  lvhengbin
     */
    public function userTem($user, $order, $staff, $date, $staffphone, $serverphone)
    {
        $tem_two_data = SmsTemplet::find()->where(['type'=>3])->asArray()->one();
        $cont = $tem_two_data['content'];
        $cont = str_replace('{user}', $user, $cont);
        $cont = str_replace('{order}', $order, $cont);
        $cont = str_replace('{staff}', $staff, $cont);
        $cont = str_replace('{date}', $date, $cont);
        $cont = str_replace('{staffphone}', $staffphone, $cont);
        $cont = str_replace('{serverphone}', $serverphone, $cont);
        return $cont;
    }

    /**
     * 报事报修 员工 短信模板
     * @author  lvhengbin
     */
    public function repairTem($staff, $address, $problem)
    {
        $tem_data = SmsTemplet::find()->where(['type'=>4])->asArray()->one();
        $cont = $tem_data['content'];
        $cont = str_replace('{staff}', $staff, $cont);
        $cont = str_replace('{address}', $address, $cont);
        $cont = str_replace('{problem}', $problem, $cont);
        return $cont;
    }

    /**
     * 报事报修 联系人 短信模板
     * @author  lvhengbin
     */
    public function repairTem2($staff)
    {
        $tem_data = SmsTemplet::find()->where(['type'=>5])->asArray()->one();
        $cont = $tem_data['content'];
        $cont = str_replace('{staff}', $staff, $cont);
        return $cont;
    }

    /**
     * 创建目录
     * @author  mfh
     * @time    2018/1/22
     */
    public function mk_dir($dir, $mode = 0755)
    {
        if (is_dir($dir) || @mkdir($dir,$mode)) return true;
        if (!$this->mk_dir(dirname($dir),$mode)) return false;
        return @mkdir($dir,$mode);
    }

    //记录日志
    public function writeLog($txt, $filename) {
        $path = '../runtime/logs';
        if(!is_dir($path)){
            mkdir($path,0777,true);
        }
        $filename = date('Ym').'_'.$filename;
        $path = $path .'/'. $filename .'.txt';
        if(!empty($txt)) {
            file_put_contents($path, $txt . PHP_EOL, FILE_APPEND);
        }
    }

    public function alert($message,$url='')
    {
        echo "<!DOCTYPE html>
			   <html>
               <head>
				   <meta charset='UTF-8'>
                   <meta name='viewport' content='width=device-width, height=device-height, initial-scale=1,minimum-scale=1, maximum-scale=1, user-scalable=0'>
				   <link rel='stylesheet' href='/layui/css/layui.css'>
				   <script src='/layui/layui.js'></script>
               </head>
               <script type='text/javascript'>
			        layui.use('layer', function(){
					var $ = layui.jquery, layer = layui.layer;
				    layer.ready(function(){
						layer.alert('".$message."',{
						  title: '提示信息'
						  ,skin: 'layui-layer-molv'
						  ,btnAlign: 'c'
						  ,closeBtn: 0
						  ,anim: 4 //动画类型
						}, function(){
						    var url_len = '".$url."'.length;
						    if (url_len == 0) {
						        history.go(-1);
						    } else {
						       location.href = '".$url."';
						    }

						});
						});
					});
                </script>
               </html>";exit;
    }

	public function alertone($message,$url='')
    {
        echo "<!DOCTYPE html>
			   <html>
               <head>
				   <meta charset='UTF-8'>
                   <meta name='viewport' content='width=device-width, height=device-height, initial-scale=1,minimum-scale=1, maximum-scale=1, user-scalable=0'>
				   <link rel='stylesheet' href='/layui/css/layui.css'>
				   <script src='/layui/layui.js'></script>
               </head>
               <script type='text/javascript'>
			        layui.use('layer', function(){
					var $ = layui.jquery, layer = layui.layer;
				    layer.ready(function(){
						layer.alert('".$message."',{
						  title: '提示信息'
						  ,skin: 'layui-layer-molv'
						  ,btnAlign: 'c'
						  ,closeBtn: 0
						  ,anim: 4 //动画类型
						}, function(){
							layer.close(layer.index);
							var index = parent.layer.getFrameIndex(window.name); //先得到当前iframe层的索引
							window.parent.location.href='".$url."';
							parent.layer.close(index);
						});
						});
					});
                </script>
               </html>";exit;
    }
}
