<?php
namespace api\controllers;

use Yii;
use common\models\User;
use common\models\Staff;
use common\models\Order;
use common\models\OrderGoods;
use common\models\Coupon;
use common\models\CouponReceive;
use common\components\pay\JsApiPay;

/**
 * 小程序 我的订单 相关接口
 */
class OrderController extends BaseController
{
    public function init()
    {
        parent::init();
    }

    /**
     * @api {post} order/list 订单列表
     * @apiVersion 1.0.0
     * @apiName list
     * @apiGroup Order
     *
     * @apiParam {String} type (0:全部订单，1:待服务，4:待付款，5:已完成，6:已评价)
     *
     * @apiSuccessExample {json} 例子:
     *  {
     *      "api_token": "",
     *      "open_id": "ojCOA1eVWMWUaNSitcMFGl4fEvUk",
     *      "data": {
     *          "type": "0"
     *      }
     *  }
     *
     * @apiSuccess {Array} data 返回数据
     * @apiSuccess {Int} errcode 状态码
     * @apiSuccess {String} errmsg 描述信息
     *
     * @apiSuccessExample Success-Response:
     *	{
     *	    "errcode": 1000,
     *	    "errmsg": "SUCCESS",
     *	    "data": {
     *	         {
     *              "id": "1",
     *              "order_code": "201906134656561",
     *              "order_total": "160.50",
     *              "book_date": "2019-06-14 14:48:50",
     *              "order_status": "0",
     *              "goods_name": "擦玻璃"
     *          }
     *	    }
     *	}
     *
     * @apiError {Array} data 返回数据
     * @apiError {Int} errcode 状态码
     * @apiError {String} errmsg 描述信息
     *
     * @apiErrorExample Error-Response:
     *	{
     *	    "errcode": 2001,
     *	    "errmsg": "banner数据为空",
     *	    "data": {}
     *	}
    */
    public function actionList()
    {
        $main_data = $this->mainData;
        $user_id = $this->userId;
        //(0:全部订单，1:待服务，4:待付款，5:已完成，6:已评价)
        $type = isset($main_data['type']) ? $main_data['type'] : 0;

        //查询订单
        $query = Order::find()->select('id,order_code,order_total,book_date,order_status,appraisal');
        $query = $query->where(['user_id' => $user_id]);
        if(!empty($type)){
            if($type == 1){
                $query = $query->andWhere(['in', 'order_status', [0, 1, 2]]);
            } else {
                $query = $query->andWhere(['order_status' => $type - 1]);
            }
        }

        $order_data = $query->orderBy('id DESC')->asArray()->all();
        if(!empty($order_data)){
            foreach ($order_data as $key=>&$val){
                $goods = OrderGoods::find()->select('goods_name')->where(['order_code'=>$val['order_code'],'is_edit'=>0])->asArray()->one();
                $val['goods_name'] = $goods['goods_name'];
            }
        }

        $this->retJson(1000, 'SUCCESS', $order_data);
    }

    /**
     * @api {post} order/finish 服务完成
     * @apiVersion 1.0.0
     * @apiName finish
     * @apiGroup Order
     *
     * @apiParam {String} order_id 订单id
     *
     * @apiSuccessExample {json} 例子:
     *  {
     *      "api_token": "",
     *      "open_id": "ojCOA1eVWMWUaNSitcMFGl4fEvUk",
     *      "data": {
     *          "order_id": "1"
     *      }
     *  }
     *
     * @apiSuccess {Array} data 返回数据
     * @apiSuccess {Int} errcode 状态码
     * @apiSuccess {String} errmsg 描述信息
     *
     * @apiSuccessExample Success-Response:
     *	{
     *	    "errcode": 1000,
     *	    "errmsg": "SUCCESS",
     *	    "data": {}
     *	}
     *
     * @apiError {Array} data 返回数据
     * @apiError {Int} errcode 状态码
     * @apiError {String} errmsg 描述信息
     *
     * @apiErrorExample Error-Response:
     *	{
     *	    "errcode": 2001,
     *	    "errmsg": "分类数据为空",
     *	    "data": {}
     *	}
    */
    public function actionFinish()
    {
        $user_id = $this->userId;
        $main_data = $this->mainData;
        if(!isset($main_data['order_id']) || empty($main_data['order_id'])) {
            $this->retJson(2001, 'order_id不能为空！');
        }
        $order_id = $main_data['order_id'];

        $order_model = Order::find()->where(['id'=>$order_id,'user_id'=>$user_id])->one();
        if(empty($order_model)){
            $this->retJson(2002, '此订单不存在！');
        }
        if($order_model->order_status != 2){
            $this->retJson(2002, '订单状态不正确！');
        }
        $order_model->order_status = 3;
        if($order_model->save()){
            $this->retJson(1000, 'SUCCESS');
        } else {
            $this->retJson(2003, '系统错误，请重试！');
        }
    }

    /**
     * @api {post} order/appraisal 服务评价
     * @apiVersion 1.0.0
     * @apiName appraisal
     * @apiGroup Order
     *
     * @apiParam {String} order_id 订单id
     * @apiParam {String} star 星级(1-5)
     *
     * @apiSuccessExample {json} 例子:
     *  {
     *      "api_token": "",
     *      "open_id": "ojCOA1eVWMWUaNSitcMFGl4fEvUk",
     *      "data": {
     *          "order_id": "1",
     *          "star": "2",
     *      }
     *  }
     *
     * @apiSuccess {Array} data 返回数据
     * @apiSuccess {Int} errcode 状态码
     * @apiSuccess {String} errmsg 描述信息
     *
     * @apiSuccessExample Success-Response:
     *	{
     *	    "errcode": 1000,
     *	    "errmsg": "SUCCESS",
     *	    "data": {}
     *	}
     *
     * @apiError {Array} data 返回数据
     * @apiError {Int} errcode 状态码
     * @apiError {String} errmsg 描述信息
     *
     * @apiErrorExample Error-Response:
     *	{
     *	    "errcode": 2001,
     *	    "errmsg": "分类数据为空",
     *	    "data": {}
     *	}
     */
    public function actionAppraisal()
    {
        $user_id = $this->userId;
        $main_data = $this->mainData;
        if(!isset($main_data['order_id']) || empty($main_data['order_id'])) {
            $this->retJson(2001, 'order_id不能为空！');
        }
        $order_id = $main_data['order_id'];
        if(!isset($main_data['star']) || empty($main_data['star'])) {
            $this->retJson(2001, '参数不能为空！');
        }

        $order_model = Order::find()->where(['id'=>$order_id,'user_id'=>$user_id])->one();
        if(empty($order_model)){
            $this->retJson(2002, '此订单不存在！');
        }
        if($order_model->order_status != 4){
            $this->retJson(2002, '订单状态不正确！');
        }
        $order_model->order_status = 5;
        $order_model->appraisal = $main_data['star'];
        if($order_model->save()){
            $this->retJson(1000, 'SUCCESS');
        } else {
            $this->retJson(2003, '系统错误，请重试！');
        }
    }

    /**
     * @api {post} order/detail 订单详情
     * @apiVersion 1.0.0
     * @apiName detail
     * @apiGroup Order
     *
     * @apiParam {String} order_id 订单id
     *
     * @apiSuccessExample {json} 例子:
     *  {
     *      "api_token": "",
     *      "open_id": "ojCOA1eVWMWUaNSitcMFGl4fEvUk",
     *      "data": {
     *          "order_id": "1"
     *      }
     *  }
     *
     * @apiSuccess {Array} data 返回数据
     * @apiSuccess {Int} errcode 状态码
     * @apiSuccess {String} errmsg 描述信息
     *
     * @apiSuccessExample Success-Response:
     *	{
     *	    "errcode": 1000,
     *	    "errmsg": "SUCCESS",
     *	    "data": {
     * 	         "order_data": "订单数据",
     *           "goods_data": "商品数据"
     *	    }
     *	}
     *
     * @apiError {Array} data 返回数据
     * @apiError {Int} errcode 状态码
     * @apiError {String} errmsg 描述信息
     *
     * @apiErrorExample Error-Response:
     *	{
     *	    "errcode": 2001,
     *	    "errmsg": "参数不能为空",
     *	    "data": {}
     *	}
    */
    public function actionDetail()
    {
        $user_id = $this->userId;
        $main_data = $this->mainData;
        if(!isset($main_data['order_id']) || empty($main_data['order_id'])) {
            $this->retJson(2001, 'order_id不能为空！');
        }
        $order_id = $main_data['order_id'];
        $order_data = Order::find()->where(['id'=>$order_id,'user_id'=>$user_id])->asArray()->one();
        if(empty($order_data)){
            $this->retJson(2002, '此订单不存在！');
        }
        //获取服务人员
        $order_data['staff_name'] = '';
        if(!empty($order_data['staff_id'])) {
            $staff = Staff::find()->select('name')->where(['id' => $order_data['staff_id']])->asArray()->one();
            $order_data['staff_name'] = $staff['name'];
        }

        //获取此订单的商品
        $order_goods = OrderGoods::find()->select('goods_name,price,unit,amount,is_edit')->where(['order_code'=>$order_data['order_code']])->asArray()->all();
        if(empty($order_data)){
            $this->retJson(2002, '订单错误，请重试！');
        }

        $return_data['order_data'] = $order_data;
        $return_data['goods_data'] = $order_goods;

        $this->retJson(1000, 'SUCCESS', $return_data);
    }

    /**
     * @api {post} order/pay 订单支付
     * @apiVersion 1.0.0
     * @apiName pay
     * @apiGroup Order
     *
     * @apiParam {String} order_id 订单id
     * @apiParam {String} receive_id 领取的券id
     *
     * @apiSuccessExample {json} 例子:
     *  {
     *      "api_token": "",
     *      "open_id": "ojCOA1eVWMWUaNSitcMFGl4fEvUk",
     *      "data": {
     *          "order_id": "1",
     *          "receive_id": "1",
     *      }
     *  }
     *
     * @apiSuccess {Array} data 返回数据
     * @apiSuccess {Int} errcode 状态码
     * @apiSuccess {String} errmsg 描述信息
     *
     * @apiSuccessExample Success-Response:
     *	{
     *	    "errcode": 1000,
     *	    "errmsg": "SUCCESS",
     *	    "data": {
     *          "appId": "",
     *          "timeStamp": "",
     *          "nonceStr": "",
     *          "package": "",
     *          "signType": "",
     *          "paySign": "",
     *	    }
     *	}
     *
     * @apiError {Array} data 返回数据
     * @apiError {Int} errcode 状态码
     * @apiError {String} errmsg 描述信息
     *
     * @apiErrorExample Error-Response:
     *	{
     *	    "errcode": 2001,
     *	    "errmsg": "参数不能为空",
     *	    "data": {}
     *	}
     */
    public function actionPay()
    {
        $user_id = $this->userId;
        $main_data = $this->mainData;
        if(!isset($main_data['order_id']) || empty($main_data['order_id'])) {
            $this->retJson(2001, 'order_id不能为空！');
        }
        $order_id = $main_data['order_id'];
        $order_model = Order::find()->where(['id'=>$order_id,'user_id'=>$user_id])->one();
        if(empty($order_model)){
            $this->retJson(2002, '此订单不存在！');
        }
        if($order_model->order_status != 3){
            $this->retJson(2002, '订单状态不正确！');
        }
        $user_data = User::find()->select('id,name,openid')->where(['id'=>$user_id,'status'=>0])->asArray()->one();
        if(empty($user_data)){
            $this->retJson(2002, '用户不存在或被加入黑名单！');
        }

        $transaction = Yii::$app->db->beginTransaction();
        try {
            //是否使用优惠券
            $coupon_total = 0;
            if(!empty($main_data['receive_id'])){
                //查询是否使用过
                $receive_model = CouponReceive::find()->where(['id'=>$main_data['receive_id'],'user_id'=>$user_id])->one();
                if($receive_model->is_use == 1){
                    $this->retJson(2002, '此优惠券已使用！');
                }
                if($receive_model->expire_date < date('Y-m-d H:i:s')){
                    $this->retJson(2002, '此优惠券已过期！');
                }

                $coupon_id = $receive_model->coupon_id;
                $coupon = Coupon::find()->where(['id'=>$coupon_id])->asArray()->one();
                //判断使用条件
                if($coupon['restriction'] > 0){
                    if($order_model->order_total < $coupon['restriction']){
                        $this->retJson(2003, '优惠券不符合使用条件，请重试！');
                    }
                }
                $coupon_total = $coupon['money'];

                $order_model->is_coupon = 1;
                $order_model->coupon_id = $coupon_id;
                $order_model->coupon_total = $coupon_total;
                $order_model->receive_id = $main_data['receive_id'];
            } else {
                $order_model->is_coupon = 0;
                $order_model->coupon_id = 0;
                $order_model->coupon_total = 0;
            }

            //支付金额 (总额-预约金-优惠券抵扣)
            $pay_total = bcsub($order_model->order_total,$order_model->book_money,2);
            $pay_total = bcsub($pay_total,$coupon_total,2);

            $order_model->pay_total = $pay_total;
            if($pay_total <= 0){  //支付金额小于等于0，直接修改订单状态；
                $order_model->order_status = 4;
                $order_model->pay_total = 0;
                //如果使用优惠券，则直接核销优惠券
                if(!empty($order_model->receive_id)) {
                    $receive_model = CouponReceive::find()->where(['id' => $order_model->receive_id, 'user_id' => $order_model->user_id])->one();
                    $receive_model->is_use = 1;
                    $receive_model->use_date = date('Y-m-d H:i:s');
                    $receive_model->save();
                }
            }

            if(!$order_model->save()){
                $this->retJson(2020, '微信支付错误，请稍后重试！');
            }

            $transaction->commit();
            if($pay_total <= 0){  //支付金额为0，直接返回成功；
                $this->retJson(1001,'SUCCESS','订单无需支付！');
            }

            //微信支付配置信息
            $appid = Yii::$app->params['appId']; //小程序 appId
            $mchid = Yii::$app->params['mchId']; //商户号
            $mchkey = Yii::$app->params['mchKey']; //商户密钥

            //微信下单
            $pay = new JsApiPay();
            $input = new \WxPayUnifiedOrder();
            $input->SetBody("金安物业-总单支付");
            $input->SetAttach($order_model->order_code);
            $input->SetOut_trade_no($order_model->order_code.'total'.rand(100,999)); //预约金支付单号
            $input->SetTotal_fee($pay_total*100);
            $input->SetTime_start(date("YmdHis"));
            $input->SetTime_expire(date("YmdHis", time() + 600));
            $input->SetAppid($appid);
            $input->SetMch_id($mchid);
            $returnUrl = Yii::$app->params['apiDomain'].'/pay/notify'; //回调地址
            $input->SetNotify_url($returnUrl);
            $input->SetTrade_type("JSAPI");
            $input->SetOpenid($user_data['openid']);
            $order_return = \WxPayApi::unifiedOrder($input, $mchkey);
            //下单返回结果
            if(!empty($order_return) && $order_return['return_code'] == 'SUCCESS' && $order_return['result_code'] == 'SUCCESS')
            {
                $jsApiParameters = $pay->GetJsApiParameters($order_return, $mchkey);
                //下单成功，返回支付信息
                $this->retJson(1000,'SUCCESS',json_decode($jsApiParameters));
            } else {
                //微信下单失败 记录日志
                $txt = '['.date('Y-m-d H:i:s').']'.'微信下单失败：传参：'.serialize($input).'【返回结果】'.json_encode($order_return);
                $this->writeLog($txt, 'order');
                $this->retJson(2021, '微信支付错误,请稍后重试!');
            }

        } catch (\Exception $e) {
            //订单生成失败 记录日志
            $txt = '['.date('Y-m-d H:i:s').']'.'总订单支付失败：'.$e->getMessage();
            $this->writeLog($txt, 'order');

            $this->retJson(2020, '微信支付错误，请稍后重试！');
        }
    }
}
