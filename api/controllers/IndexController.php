<?php
namespace api\controllers;

use Yii;
use common\models\Goods;
use common\models\Coupon;
use common\models\Pictures;
use common\models\CouponConfig;
use common\models\CouponReceive;
use common\models\GoodsCategory;

/**
 * 小程序首页相关接口
 */
class IndexController extends BaseController
{

    public function init()
    {
        parent::init();
    }

    /**
     * @api {post} index/banner 获取首页banner图
     * @apiVersion 1.0.0
     * @apiName banner
     * @apiGroup Index
     *
     * @apiParam {String} open_id
     *
     * @apiSuccessExample {json} 例子:
     *  {
     *      "api_token": "",
     *      "open_id": "ojCOA1eVWMWUaNSitcMFGl4fEvUk",
     *      "data": {}
     *  }
     *
     * @apiSuccess {Array} data 返回数据
     * @apiSuccess {Int} errcode 状态码
     * @apiSuccess {String} errmsg 描述信息
     *
     * @apiSuccessExample Success-Response:
     *	{
     *	    "errcode": 1000,
     *	    "errmsg": "SUCCESS",
     *	    "data": {
     *	         {
     * 	            "id": "1",
     * 	            "name": "第二张",
     *              "img": "http://huamaoadmin.weiyingjia.org/upload/20190620/1561017460204160.jpg"
     *	        },
     *	    }
     *	}
     *
     * @apiError {Array} data 返回数据
     * @apiError {Int} errcode 状态码
     * @apiError {String} errmsg 描述信息
     *
     * @apiErrorExample Error-Response:
     *	{
     *	    "errcode": 2001,
     *	    "errmsg": "banner数据为空",
     *	    "data": {}
     *	}
    */
    public function actionBanner()
    {
        //banner图片
        $banner_data = Pictures::find()->select('id,name,img')->where(['is_del'=>0,'type'=>1,'status'=>0])->orderBy('id desc')->asArray()->all();
        if(empty($banner_data)){
            $this->retJson(2001, 'banner数据为空！');
        }

        foreach ($banner_data as $key=>&$val){
            $val['img'] = Yii::$app->params['fileDomain'].$val['img'];
        }

        $this->retJson(1000, 'SUCCESS', $banner_data);
    }

    /**
     * @api {post} index/bottom 获取底部图片
     * @apiVersion 1.0.0
     * @apiName bottom
     * @apiGroup Index
     *
     * @apiParam {String} open_id
     *
     * @apiSuccessExample {json} 例子:
     *  {
     *      "api_token": "",
     *      "open_id": "ojCOA1eVWMWUaNSitcMFGl4fEvUk",
     *      "data": {}
     *  }
     *
     * @apiSuccess {Array} data 返回数据
     * @apiSuccess {Int} errcode 状态码
     * @apiSuccess {String} errmsg 描述信息
     *
     * @apiSuccessExample Success-Response:
     *	{
     *	    "errcode": 1000,
     *	    "errmsg": "SUCCESS",
     *	    "data": {
     * 	         "id": "1",
     * 	         "name": "第二张",
     *           "img": "http://huamaoadmin.weiyingjia.org/upload/20190620/1561017460204160.jpg"
     *	    }
     *	}
     *
     * @apiError {Array} data 返回数据
     * @apiError {Int} errcode 状态码
     * @apiError {String} errmsg 描述信息
     *
     * @apiErrorExample Error-Response:
     *	{
     *	    "errcode": 2001,
     *	    "errmsg": "banner数据为空",
     *	    "data": {}
     *	}
     */
    public function actionBottom()
    {
        //底部图片
        $bottom_data = Pictures::find()->select('id,name,img')->where(['is_del'=>0,'type'=>2,'status'=>0])->asArray()->one();
        if(empty($bottom_data)){
            $this->retJson(2001, '数据为空！');
        }
        $bottom_data['img'] = Yii::$app->params['fileDomain'].$bottom_data['img'];

        $this->retJson(1000, 'SUCCESS', $bottom_data);
    }

    /**
     * @api {post} index/category 获取分类数据
     * @apiVersion 1.0.0
     * @apiName category
     * @apiGroup Index
     *
     * @apiParam {String} open_id
     *
     * @apiSuccessExample {json} 例子:
     *  {
     *      "api_token": "",
     *      "open_id": "ojCOA1eVWMWUaNSitcMFGl4fEvUk",
     *      "data": {}
     *  }
     *
     * @apiSuccess {Array} data 返回数据
     * @apiSuccess {Int} errcode 状态码
     * @apiSuccess {String} errmsg 描述信息
     *
     * @apiSuccessExample Success-Response:
     *	{
     *	    "errcode": 1000,
     *	    "errmsg": "SUCCESS",
     *	    "data": {
     *	         {
     * 	            "id": "1",
     * 	            "name": "金安品质服务",
     *              "son": {
     *                  {
     *                      "id": "4",
     *                      "name": "家庭清洁服务"
     *                  }
     *              }
     *	        }
     *	    }
     *	}
     *
     * @apiError {Array} data 返回数据
     * @apiError {Int} errcode 状态码
     * @apiError {String} errmsg 描述信息
     *
     * @apiErrorExample Error-Response:
     *	{
     *	    "errcode": 2001,
     *	    "errmsg": "分类数据为空",
     *	    "data": {}
     *	}
    */
    public function actionCategory()
    {
        //商品分类
        $pdata = GoodsCategory::find()->select('id,name,icon_img')->where(['is_del'=>0,'pid'=>0])->asArray()->all();
        if(empty($pdata)){
            $this->retJson(2001, '分类数据为空！');
        }
        $cate_data = [];
        foreach ($pdata as $key=>$val){
            $val['icon_img'] = !empty($val['icon_img']) ? Yii::$app->params['fileDomain'].$val['icon_img'] : '';
            $data = GoodsCategory::find()->select('id,name,icon_img')->where(['is_del'=>0,'pid'=>$val['id']])->asArray()->all();
            if(!empty($data)) {
                foreach ($data as $k=>&$v){
                    $v['icon_img'] = !empty($v['icon_img']) ? Yii::$app->params['fileDomain'].$v['icon_img'] : '';
                }
            }
            $val['son'] = !empty($data) ? $data : '';
            $cate_data[] = $val;
        }
        $this->retJson(1000, 'SUCCESS', $cate_data);
    }

    /**
     * @api {post} index/coupon 获取优惠券列表
     * @apiVersion 1.0.0
     * @apiName coupon
     * @apiGroup Index
     *
     * @apiParam {String} open_id
     *
     * @apiSuccessExample {json} 例子:
     *  {
     *      "api_token": "",
     *      "open_id": "ojCOA1eVWMWUaNSitcMFGl4fEvUk",
     *      "data": {}
     *  }
     *
     * @apiSuccess {Array} data 返回数据
     * @apiSuccess {Int} errcode 状态码
     * @apiSuccess {String} errmsg 描述信息
     *
     * @apiSuccessExample Success-Response:
     *	{
     *	    "errcode": 1000,
     *	    "errmsg": "SUCCESS",
     *	    "data": {
     *	         {
     * 	            "id": "4",
     *              "name": "20元优惠券",
     *              "money": "20",
     *              "start_at": "2019-06-09 00:00:00",
     *              "end_at": "2019-10-31 00:00:00",
     *              "restriction": "0"
     *	        },
     *	    }
     *	}
     *
     * @apiError {Array} data 返回数据
     * @apiError {Int} errcode 状态码
     * @apiError {String} errmsg 描述信息
     *
     * @apiErrorExample Error-Response:
     *	{
     *	    "errcode": 2001,
     *	    "errmsg": "参数不能为空",
     *	    "data": {}
     *	}
    */
    public function actionCoupon()
    {
        //判断优惠券开关
        $config = CouponConfig::find()->asArray()->one();
        if(empty($config) || $config['switch'] == 1){
            $this->retJson(2010, '优惠券功能关闭！');
        }
        //获取优惠券
        $coupon_data = Coupon::find()
            ->select('id,name,money,start_at,end_at,restriction')
            ->where(['is_del'=>0,'status'=>0,'is_share'=>0])
            ->andWhere(['>', 'end_at', date('Y-m-d H:i:s')])
            ->asArray()->all();
        if(empty($coupon_data)){
            $this->retJson(2001, '优惠券数据为空！');
        }
        foreach ($coupon_data as $key=>&$val){
            $val['start_at'] = substr($val['start_at'],0,10);
            $val['end_at'] = substr($val['end_at'],0,10);
        }

        $this->retJson(1000, 'SUCCESS', $coupon_data);
    }

    /**
     * @api {post} index/collect 领取优惠券
     * @apiVersion 1.0.0
     * @apiName collect
     * @apiGroup Index
     *
     * @apiParam {String} coupon_id 优惠券id
     *
     * @apiSuccessExample {json} 例子:
     *  {
     *      "api_token": "",
     *      "open_id": "ojCOA1eVWMWUaNSitcMFGl4fEvUk",
     *      "data": {
     *          "coupon_id": "1",
     *       }
     *  }
     *
     * @apiSuccess {Array} data 返回数据
     * @apiSuccess {Int} errcode 状态码
     * @apiSuccess {String} errmsg 描述信息
     *
     * @apiSuccessExample Success-Response:
     *	{
     *	    "errcode": 1000,
     *	    "errmsg": "SUCCESS",
     *	    "data": {}
     *	}
     *
     * @apiError {Array} data 返回数据
     * @apiError {Int} errcode 状态码
     * @apiError {String} errmsg 描述信息
     *
     * @apiErrorExample Error-Response:
     *	{
     *	    "errcode": 2001,
     *	    "errmsg": "参数不能为空",
     *	    "data": {}
     *	}
    */
    public function actionCollect()
    {
        $user_id = $this->userId;
        $main_data = $this->mainData;
        if(!isset($main_data['coupon_id']) || empty($main_data['coupon_id'])) {
            $this->retJson(2001, 'coupon_id不能为空！');
        }
        $coupon_id = $main_data['coupon_id'];
        //判断优惠券开关
        $config = CouponConfig::find()->asArray()->one();
        if(empty($config) || $config['switch'] == 1){
            $this->retJson(2010, '优惠券功能关闭！');
        }
        //获取优惠券
        $coupon_data = Coupon::find()
            ->where(['id'=>$coupon_id, 'is_del'=>0, 'status'=>0])
            ->andWhere(['>', 'end_at', date('Y-m-d H:i:s')])
            ->asArray()->one();
        if(empty($coupon_data)){
            $this->retJson(2002, '此优惠券不存在或已过期！');
        }
        //判断领取条件
        if($coupon_data['limit'] > 0){
            //查询已领取数量
            $re_num = CouponReceive::find()->where(['coupon_id'=>$coupon_id, 'user_id'=>$user_id])->count();
            if($re_num >= $coupon_data['limit']){
                $this->retJson(2002, '此优惠券每人只能领取'.$coupon_data['limit'].'张！');
            }
        }
        //领取优惠券
        $model = new CouponReceive();
        $model->user_id = $user_id;
        $model->coupon_id = $coupon_id;
        $model->expire_date = $coupon_data['end_at'];
        $model->created_at = date('Y-m-d H:i:s');
        if($model->save()){
            $this->retJson(1000, 'SUCCESS');
        }else{
            $this->retJson(2004, '领取失败');
        }
    }

    /**
     * @api {post} index/search 搜索
     * @apiVersion 1.0.0
     * @apiName search
     * @apiGroup Index
     *
     * @apiParam {String} keyword 服务项目关键词
     *
     * @apiSuccessExample {json} 例子:
     *  {
     *      "api_token": "",
     *      "open_id": "ojCOA1eVWMWUaNSitcMFGl4fEvUk",
     *      "data": {
     *          "keyword": "玻璃",
     *       }
     *  }
     *
     * @apiSuccess {Array} data 返回数据
     * @apiSuccess {Int} errcode 状态码
     * @apiSuccess {String} errmsg 描述信息
     *
     * @apiSuccessExample Success-Response:
     *	{
     *	    "errcode": 1000,
     *	    "errmsg": "SUCCESS",
     *	    "data": {
     *          {
     *          "id": "2",
     *          "name": "冬季玻璃清洁11月-次年三月",
     *          "category_id": "4",
     *          "price": "3",
     *          "unit": "平米",
     *          "thumbnail": "/upload/20190620/1561025532564486.jpg"
     *          },
     *      }
     *	}
     *
     * @apiError {Array} data 返回数据
     * @apiError {Int} errcode 状态码
     * @apiError {String} errmsg 描述信息
     *
     * @apiErrorExample Error-Response:
     *	{
     *	    "errcode": 2001,
     *	    "errmsg": "参数不能为空",
     *	    "data": {}
     *	}
     */
    public function actionSearch()
    {
        $main_data = $this->mainData;
        if(!isset($main_data['keyword']) || empty($main_data['keyword'])) {
            $this->retJson(2001, '关键词不能为空！');
        }
        $keyword = $main_data['keyword'];
        //查询商品服务
        $query = Goods::find()->select('id,name,category_id,price,unit,thumbnail');
        $where['is_del'] = 0;
        $where['status'] = 0;
        $query = $query->where($where);
        $query = $query->andwhere(['like', 'name', $keyword]);

        $goods_data = $query->asArray()->all();
        if(!empty($goods_data)){
            foreach ($goods_data as &$val){
                $val['thumbnail'] = Yii::$app->params['fileDomain'].$val['thumbnail'];
            }
        }else{
            $goods_data = [];
        }

        $this->retJson(1000, 'SUCCESS', $goods_data);
    }
}
