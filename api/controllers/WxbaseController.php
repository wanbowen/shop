<?php
namespace api\controllers;

use Yii;
use yii\web\Controller;
use yii\data\Pagination;
use common\models\User;
use common\components\Sms;
use common\components\Uploader;

/*
 * 前端公众号相关页面
 */
class WxbaseController extends Controller{

    public $layout = false;
    public function init()
    {
        $this->enableCsrfValidation = false;
    }

    //验证用户是否授权登陆
    public function checkLogin()
    {
        $wechat_id = Yii::$app->session['wechat_id'] ? Yii::$app->session['wechat_id'] : '';
        $user_id = Yii::$app->session['user_id'] ? Yii::$app->session['user_id'] : '';
        if (empty($wechat_id) || empty($user_id))
        {
            Yii::$app->session['backUrl'] = Yii::$app->request->url;
            $this->actionWeixinlogin();
            return;
        } else {
            //判断用户是否加入黑名单
            $user = User::find()->where(['wx_openid'=>$wechat_id])->asArray()->one();
            if($user['status'] == 1){
                $this->error('您已被加入黑名单，请联系客服！');
            }
        }
    }

    //微信公众号授权入口
    public function actionWeixinlogin()
    {
        $appid = Yii::$app->params['WxAppId'];
        $redirect_uri = 'http://'.$_SERVER['HTTP_HOST'].'/wxbase/authweixin';

        $redirect_uri = urlencode($redirect_uri);
        $url = 'https://open.weixin.qq.com/connect/oauth2/authorize?appid=' . $appid . '&redirect_uri=' . $redirect_uri
            . '&response_type=code&scope=snsapi_userinfo&state=authredirect#wechat_redirect';

        header("location:".$url);
    }

    //微信公众号授权回调
    public function actionAuthweixin()
    {
        $this->layout = '';
        $appid  = Yii::$app->params['WxAppId'];
        $secret = Yii::$app->params['WxAppSecret'];
        $code   = $_GET["code"];
        $get_token_url = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid=' . $appid . '&secret=' . $secret . '&code=' . $code . '&grant_type=authorization_code';
        //获取授权access_token
        $json_obj = $this->httpGet($get_token_url);
        if (!empty($json_obj) && isset($json_obj['access_token']) && isset($json_obj['openid'])) {
            $wechat_id = $json_obj['openid'];
            //获取用户信息
            $get_user_url = 'https://api.weixin.qq.com/sns/userinfo?access_token='.$json_obj['access_token'].'&openid='.$wechat_id.'&lang=zh_CN';
            $wx_user = $this->httpGet($get_user_url);
            if(!empty($wx_user) && isset($wx_user['nickname'])){
                $user = User::find()->where(['wx_openid'=>$wechat_id])->asArray()->one();
                Yii::$app->session['wechat_id'] = $wechat_id;
                Yii::$app->session['headimgurl'] = $wx_user['headimgurl'];
                if(!empty($user)){
                    Yii::$app->session['user_id'] = $user['id'];
                    $this->redirect(Yii::$app->session['backUrl']);
                }else{
                    //跳转注册页面
                    $this->redirect('/wechat/register');
                }
            } else {
                $this->error('登录失败');
            }
        } else {
            $this->error('登录失败');
        }
    }

    //上传文件
    public function uploadFile($tem){
        //上传文件的存放路径
        $upload_path = "../../admin/web/upload/" . date("Ymd");
        //判断文件夹是否存在，不存在创建
        if (!is_dir($upload_path)) {
            mkdir($upload_path, 0777);
        }
        //生成文件名
        $file_name = time().rand(10000,99999).'.jpg';
        $path = $upload_path . '/' . $file_name;
        $url = '/upload/' . date("Ymd") . '/' . $file_name;
        if (move_uploaded_file($tem, $path)) {
            return $url;
        }else{
            return false;
        }
    }

    //发送短信
    public function sendSms($tel, $content){
        //获取Sms对象
        $sms_model = new Sms(Yii::$app->params['jvtdSmsUrl'],Yii::$app->params['jvtdSmsUid'],Yii::$app->params['jvtdSmsPass']);
        //发送短信
        $res = $sms_model->SendSms($tel, $content);
        //记录短信发送
        $txt = '['.date('Y-m-d H:i:s').']'.'手机号：'.$tel.'。内容：'.$content.'【返回结果：code：'.$res['error_code'].'，msg：'.$res['error_msg'].'】';
        $this->writeLog($txt, 'sendsms');
        //返回发送结果
        if($res['error_code'] > 0){
            return true;
        }
        return false;
    }

    /**
     * json转换
     * @param  $ret =>状态码,$msg=>包含信息,$data=>数据,
     * @return json
     * @author lhb
     * @date 2019/06/19
     */
    public function json($ret, $msg = '', $data = array())
    {
        $dataRet = array('code' => $ret);
        $dataRet['msg'] = $msg;
        $dataRet['data'] = $data;
        $dataJson = json_encode($dataRet, JSON_UNESCAPED_SLASHES);
        echo $dataJson;die();
    }

    //CURL GET
    private function httpGet($url)
    {
        try{
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_TIMEOUT, 500);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_URL, $url);
            $res = curl_exec($curl);
            curl_close($curl);
            if(!$res)
                return false;
            else
                return json_decode($res,true);
        }
        catch(\Exception $e)
        {
            Yii::warning($e->getMessage());
            return false;
        }
    }

    /*
     * 报错页面
     * $msg string 报错提示信息
     * $url string 报错后跳转地址，可以为空，不跳转
     */
    public function error($msg, $url = '') {
        $params = array('msg'=>$msg,'url'=>$url,'status'=>false);
        echo \Yii::$app->view->renderFile('@app/views/wechat/error.php',['params'=>$params]);
        Yii::$app->end();
    }

    //记录日志
    public function writeLog($txt, $filename) {
        $path = '../runtime/logs';
        if(!is_dir($path)){
            mkdir($path,0777,true);
        }
        $filename = date('Ym').'_'.$filename;
        $path = $path .'/'. $filename .'.txt';
        if(!empty($txt)) {
            file_put_contents($path, $txt . PHP_EOL, FILE_APPEND);
        }
    }
}