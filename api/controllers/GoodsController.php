<?php
namespace api\controllers;

use Yii;
use yii\data\Pagination;
use common\models\User;
use common\models\Goods;
use common\models\Order;
use common\models\OrderGoods;
use common\models\OrderLabel;
use common\models\BookMoney;
use common\models\BookConfig;
use common\models\GoodsCategory;
use common\components\pay\JsApiPay;

/**
 * 小程序 商品服务 相关接口
 */
class GoodsController extends BaseController
{

    public function init()
    {
        parent::init();
    }

    /**
     * @api {post} goods/list 获取某个分类下所有商品服务
     * @apiVersion 1.0.0
     * @apiName list
     * @apiGroup Goods
     *
     * @apiParam {String} category_id 分类id
     * @apiParam {String} page 页码
     * @apiParam {String} limit 每页多少条数据
     *
     * @apiSuccessExample {json} 例子:
     *  {
     *      "api_token": "",
     *      "open_id": "ojCOA1eVWMWUaNSitcMFGl4fEvUk",
     *      "data": {
     *          "category_id": "1",
     *          "page": "1",
     *          "limit": "10",
     *      }
     *  }
     *
     * @apiSuccess {Array} data 返回数据
     * @apiSuccess {Int} errcode 状态码
     * @apiSuccess {String} errmsg 描述信息
     *
     * @apiSuccessExample Success-Response:
     *	{
     *	    "errcode": 1000,
     *	    "errmsg": "SUCCESS",
     *	    "data": {
     *	         "category_data": {
     *              "id": "4",
     *              "name": "家庭清洁服务",
     *              "banner_img": "http://huamaoadmin.weiyingjia.org/upload/20190620/1561008572171151.jpg"
     *          },
     *          "goods_data": {
     *              "page": 1,
     *              "limit": 10,
     *              "total": "2",
     *              "data": [
     *                  {
     *                      "id": "3",
     *                      "name": "夏季玻璃清洁4月-11月",
     *                      "category_id": "4",
     *                      "price": "2",
     *                      "unit": "平米",
     *                      "thumbnail": "http://huamaoadmin.weiyingjia.org/upload/20190620/1561025377860591.jpg"
     *                  },
     *              ]
     *          }
     *	    }
     *	}
     *
     * @apiError {Array} data 返回数据
     * @apiError {Int} errcode 状态码
     * @apiError {String} errmsg 描述信息
     *
     * @apiErrorExample Error-Response:
     *	{
     *	    "errcode": 2001,
     *	    "errmsg": "banner数据为空",
     *	    "data": {}
     *	}
    */
    public function actionList()
    {
        $main_data = $this->mainData;
        if(!isset($main_data['category_id']) || empty($main_data['category_id'])) {
            $this->retJson(2001, 'category_id不能为空！');
        }
        $category_id = $main_data['category_id'];
        $ret_data = [];
        //分类信息
        $cate_data = GoodsCategory::find()->select('id,name,banner_img')->where(['is_del'=>0,'id'=>$category_id])->asArray()->one();
        if(empty($cate_data)){
            $this->retJson(2001, '分类不存在！');
        }
        $cate_data['banner_img'] = Yii::$app->params['fileDomain'].$cate_data['banner_img'];
        $ret_data['category_data'] = $cate_data;
        //查询商品服务
        $query = Goods::find()->select('id,name,category_id,price,unit,thumbnail');
        $where['is_del'] = 0;
        $where['status'] = 0;
        $where['category_id'] = $category_id;
        $query = $query->where($where);

        $page = (isset($main_data['page']) && $main_data['page']) ? $main_data['page'] : 1;
        $limit = (isset($main_data['limit']) && $main_data['limit']) ? $main_data['limit'] : 10;

        $ret_data['goods_data']['page'] = $page;
        $ret_data['goods_data']['limit'] = $limit;

        $allcount = $query->count();
        $pagination = new Pagination([
            'defaultPageSize' => $limit,
            'totalCount' => $allcount
        ]);
        $ret_data['goods_data']['total'] = $allcount;
        $goods_data = $query->orderBy('id DESC')->offset($pagination->offset)->limit($pagination->limit)->asArray()->all();
        if(!empty($goods_data)){
            foreach ($goods_data as $key=>&$val){
                $val['thumbnail'] = Yii::$app->params['fileDomain'].$val['thumbnail'];
            }
        }
        $ret_data['goods_data']['data'] = $goods_data;

        $this->retJson(1000, 'SUCCESS', $ret_data);
    }

    /**
     * @api {post} goods/detail 获取单个商品服务详情
     * @apiVersion 1.0.0
     * @apiName detail
     * @apiGroup Goods
     *
     * @apiParam {String} goods_id 商品id
     *
     * @apiSuccessExample {json} 例子:
     *  {
     *      "api_token": "",
     *      "open_id": "ojCOA1eVWMWUaNSitcMFGl4fEvUk",
     *      "data": {
     *          "goods_id": "1"
     *      }
     *  }
     *
     * @apiSuccess {Array} data 返回数据
     * @apiSuccess {Int} errcode 状态码
     * @apiSuccess {String} errmsg 描述信息
     *
     * @apiSuccessExample Success-Response:
     *	{
     *	    "errcode": 1000,
     *	    "errmsg": "SUCCESS",
     *	    "data": {
     *	         "id": "3",
     *           "name": "夏季玻璃清洁4月-11月",
     *           "price": "2",
     *           "unit": "平米",
     *           "content": "<p><span style=\"font-size:14px;font-family:宋体\">夏季玻璃清洁</span><span style=\"font-size:14px;font-family:&#39;Calibri&#39;,sans-serif\">4</span><span style=\"font-size:14px;font-family:宋体\">月</span><span style=\"font-size:14px;font-family:&#39;Calibri&#39;,sans-serif\">-11</span><span style=\"font-size:14px;font-family:宋体\">月大优惠啊</span></p>",
     *           "detail_imgs": [
     *              "http://huamaoadmin.weiyingjia.org/upload/20190620/1561025385396495.jpg",
     *              "http://huamaoadmin.weiyingjia.org/upload/20190620/1561025389230694.jpg"
     *           ]
     *	    }
     *	}
     *
     * @apiError {Array} data 返回数据
     * @apiError {Int} errcode 状态码
     * @apiError {String} errmsg 描述信息
     *
     * @apiErrorExample Error-Response:
     *	{
     *	    "errcode": 2001,
     *	    "errmsg": "分类数据为空",
     *	    "data": {}
     *	}
    */
    public function actionDetail()
    {
        $main_data = $this->mainData;
        if(!isset($main_data['goods_id']) || empty($main_data['goods_id'])) {
            $this->retJson(2001, 'goods_id不能为空！');
        }
        $goods_id = $main_data['goods_id'];
        //获取商品
        $goods_data = Goods::find()->select('id,name,price,unit,content,detail_imgs')
            ->where(['id'=>$goods_id, 'is_del'=>0, 'status'=>0])
            ->asArray()->one();
        if(empty($goods_data)){
            $this->retJson(2001, '商品服务数据为空！');
        }
        if(!empty($goods_data['detail_imgs'])) {
            $detail_imgs = $goods_data['detail_imgs'];
            $img_arr = explode(';', $detail_imgs);
            $detail_img_arr = [];
            foreach ($img_arr as $key => $val) {
                $img = Yii::$app->params['fileDomain'].$val;
                $detail_img_arr[] = $img;
            }
            $goods_data['detail_imgs'] = $detail_img_arr;
        }

        $this->retJson(1000, 'SUCCESS', $goods_data);
    }

    /**
     * @api {post} goods/review 获取商品所有评价
     * @apiVersion 1.0.0
     * @apiName review
     * @apiGroup Goods
     *
     * @apiParam {String} goods_id 商品id
     *
     * @apiSuccessExample {json} 例子:
     *  {
     *      "api_token": "",
     *      "open_id": "ojCOA1eVWMWUaNSitcMFGl4fEvUk",
     *      "data": {
     *          "goods_id": "1"
     *      }
     *  }
     *
     * @apiSuccess {Array} data 返回数据
     * @apiSuccess {Int} errcode 状态码
     * @apiSuccess {String} errmsg 描述信息
     *
     * @apiSuccessExample Success-Response:
     *	{
     *	    "errcode": 1000,
     *	    "errmsg": "SUCCESS",
     *	    "data": {
     *	         {
     * 	            "mobile": "15510778583",
     *              "created_at": "2019-06-13 14:52:24",
     *              "appraisal": "3"
     *	        },
     *	    }
     *	}
     *
     * @apiError {Array} data 返回数据
     * @apiError {Int} errcode 状态码
     * @apiError {String} errmsg 描述信息
     *
     * @apiErrorExample Error-Response:
     *	{
     *	    "errcode": 2001,
     *	    "errmsg": "参数不能为空",
     *	    "data": {}
     *	}
    */
    public function actionReview()
    {
        $main_data = $this->mainData;
        if(!isset($main_data['goods_id']) || empty($main_data['goods_id'])) {
            $this->retJson(2001, 'goods_id不能为空！');
        }
        $goods_id = $main_data['goods_id'];
        $review_data = [];
        //获取此商品所在订单
        $order_goods = OrderGoods::find()->select('order_code')->where(['goods_id'=>$goods_id])->groupBy('order_code')->asArray()->all();
        if(!empty($order_goods)){
            foreach ($order_goods as $key=>$val) {
                $order = Order::find()->select('mobile,created_at,appraisal')->where(['order_code'=>$val['order_code']])->asArray()->one();
                $order['mobile'] = substr_replace($order['mobile'],'****',3,4);
                $review_data[] = $order;
            }
        }

        $this->retJson(1000, 'SUCCESS', $review_data);
    }

    /**
     * @api {post} goods/reserve 立即预约
     * @apiVersion 1.0.0
     * @apiName reserve
     * @apiGroup Goods
     *
     * @apiParam {String} goods_id 商品id
     *
     * @apiSuccessExample {json} 例子:
     *  {
     *      "api_token": "",
     *      "open_id": "ojCOA1eVWMWUaNSitcMFGl4fEvUk",
     *      "data": {
     *          "goods_id": "1",
     *       }
     *  }
     *
     * @apiSuccess {Array} data 返回数据
     * @apiSuccess {Int} errcode 状态码
     * @apiSuccess {String} errmsg 描述信息
     *
     * @apiSuccessExample Success-Response:
     *	{
     *	    "errcode": 1000,
     *	    "errmsg": "SUCCESS",
     *	    "data": {
     *          "goods_data": "商品数据",
     *          "user_data": "用户数据",
     *          "book_config": "预约时间和开关",
     *          "lable_data": "标签数据",
     *          "book_money": "预约金额",
     *      }
     *	}
     *
     * @apiError {Array} data 返回数据
     * @apiError {Int} errcode 状态码
     * @apiError {String} errmsg 描述信息
     *
     * @apiErrorExample Error-Response:
     *	{
     *	    "errcode": 2001,
     *	    "errmsg": "参数不能为空",
     *	    "data": {}
     *	}
    */
    public function actionReserve()
    {
        $user_id = $this->userId;
        $main_data = $this->mainData;
        if(!isset($main_data['goods_id']) || empty($main_data['goods_id'])) {
            $this->retJson(2001, 'goods_id不能为空！');
        }
        $goods_id = $main_data['goods_id'];
        $ret_data = [];
        //获取商品
        $goods_data = Goods::find()->select('id,name,price,unit')
            ->where(['id'=>$goods_id, 'is_del'=>0, 'status'=>0])
            ->asArray()->one();
        if(empty($goods_data)){
            $this->retJson(2001, '商品服务数据为空！');
        }
        $ret_data['goods_data'] = $goods_data;
        //获取用户信息
        $user_data = User::find()->select('id,name,mobile,address')->where(['id'=>$user_id,'status'=>0])->asArray()->one();
        if(empty($user_data)){
            $this->retJson(2010, '用户不存在或被加入黑名单！');
        }
        $ret_data['user_data'] = $user_data;
        //获取预约时间配置
        $book_config = BookConfig::find()->asArray()->one();
        if(!empty($book_config)){
            if($book_config['switch'] == 1){
                $book_config['days'] = '';
                $book_config['today'] = '';
                $book_config['start_at'] = '';
                $book_config['end_at'] = '';
            } else {
                $days = $book_config['days'] - 1;
                $book_config['days'] = date("Y-m-d", strtotime("+$days day"));
                $book_config['this_day'] = date("Y-m-d");
                $book_config['today'] = date("Y-m-d");
                $book_config['start_at'] = substr($book_config['start_at'], 0, 5);
                $book_config['end_at'] = substr($book_config['end_at'], 0, 5);
                //判断当前时间距离今天最晚截至时间是否超过4个小时
                $this_time = strtotime(date("Y-m-d").' '.$book_config['end_at']) - (time() + 4*3600);
                $book_config['is_today'] = 1;
                if($this_time <= 0){ //如果小于4个小时，预约时间从明天开始
                    $book_config['is_today'] = 0;
                    $book_config['today'] = date("Y-m-d", strtotime("+1 day"));
                } else { //如果大于4个小时，计算今天的预约开始结束时间
                    $book_config['today_start_at'] = date("H:i", (time() + 4*3600));
                    $book_config['today_end_at'] = $book_config['end_at'];
                }
            }
        }else{
            $book_config['switch'] = 1;
            $book_config['days'] = '';
            $book_config['today'] = '';
        }
        $ret_data['book_config'] = $book_config;
        //获取标签
        $lable_data = OrderLabel::find()->select('id,name')->where(['is_del'=>0])->asArray()->all();
        $ret_data['lable_data'] = !empty($lable_data) ? $lable_data : '';
        //预约金额
        $book_money = BookMoney::find()->select('money')->asArray()->one();
        $ret_data['book_money'] = !empty($book_money['money']) ? $book_money['money'] : '15';

        $this->retJson(1000, 'SUCCESS', $ret_data);
    }

    /**
     * @api {post} goods/confirmorder 确认下单
     * @apiVersion 1.0.0
     * @apiName confirmorder
     * @apiGroup Goods
     *
     * @apiParam {String} goods_id 商品id
     * @apiParam {String} amount 商品数量
     * @apiParam {String} mobile 联系人电话
     * @apiParam {String} address 联系人地址
     * @apiParam {String} book_date 预约日期
     * @apiParam {String} book_time 预约时间
     * @apiParam {String} remarks 用户备注
     *
     * @apiSuccessExample {json} 例子:
     *  {
     *      "api_token": "",
     *      "open_id": "ojCOA1eVWMWUaNSitcMFGl4fEvUk",
     *      "data": {
     *          "goods_id": "1",
     *          "amount": "1",
     *          "mobile": "18101362546",
     *          "address": "人民路2号",
     *          "book_date": "2019-06-12",
     *          "book_time": "15:00:00",
     *          "remarks": "干活勤快",
     *       }
     *  }
     *
     * @apiSuccess {Array} data 返回数据
     * @apiSuccess {Int} errcode 状态码
     * @apiSuccess {String} errmsg 描述信息
     *
     * @apiSuccessExample Success-Response:
     *	{
     *	    "errcode": 1000,
     *	    "errmsg": "SUCCESS",
     *	    "data": {
     *          "appId": "",
     *          "timeStamp": "",
     *          "nonceStr": "",
     *          "package": "",
     *          "signType": "",
     *          "paySign": "",
     *      }
     *	}
     *
     * @apiError {Array} data 返回数据
     * @apiError {Int} errcode 状态码
     * @apiError {String} errmsg 描述信息
     *
     * @apiErrorExample Error-Response:
     *	{
     *	    "errcode": 2001,
     *	    "errmsg": "参数不能为空",
     *	    "data": {}
     *	}
     */
    public function actionConfirmorder()
    {
        $user_id = $this->userId;
        $main_data = $this->mainData;
        if(!isset($main_data['goods_id']) || empty($main_data['goods_id'])) {
            $this->retJson(2001, 'goods_id不能为空！');
        }
        $goods_id = $main_data['goods_id'];
        if(empty($main_data['amount'])){
            $this->retJson(2001, '商品数量不能为空！');
        }
        if(empty($main_data['mobile']) || empty($main_data['address'])){
            $this->retJson(2001, '联系人信息不能为空！');
        }

        $user_data = User::find()->select('id,name,openid')->where(['id'=>$user_id,'status'=>0])->asArray()->one();
        if(empty($user_data)){
            $this->retJson(2002, '用户不存在或被加入黑名单！');
        }
        //预约时间
        $book_date = '';
        if(!empty($main_data['book_date']) && !empty($main_data['book_time'])){
            //获取预约时间配置
            $book_config = BookConfig::find()->asArray()->one();
            if(!empty($book_config) && $book_config['switch'] == 0) {
                $book_date = $main_data['book_date'] .' '.$main_data['book_time'];
                //判断预约时间是否正确
                $days = $book_config['days'] - 1;
                $end_days = date("Y-m-d", strtotime("+$days day"));
                $start_day= date("Y-m-d");
                if(strtotime($book_date) < strtotime(date("Y-m-d H:i:s"))){ //预约时间小于当前时间
                    $this->retJson(2003, '操作超时，请重新选择预约时间！');
                }
                if(strtotime($main_data['book_date']) == strtotime($start_day)){ //预约日期是当天
                    $book_str = strtotime($book_date);
                    $this_time = $book_str - (time() + 4*3540); //是否比当前时间大4个小时
                    $end_time = $book_str - strtotime($start_day.' '.$book_config['end_at']); //是否大于当天最晚预约时间
                    if($this_time < 0 || $end_time > 0){
                        $this->retJson(2004, '操作超时，请重新选择预约时间！');
                    }
                }
                if(strtotime($book_date) > strtotime($end_days.' '.$book_config['end_at'])){  //预约时间大于最晚预约时间
                    $this->retJson(2005, '操作超时，请重新选择预约时间！');
                }
            }
        }

        //获取商品
        $goods_data = Goods::find()->select('id,name,price,unit')
            ->where(['id'=>$goods_id, 'is_del'=>0, 'status'=>0])
            ->asArray()->one();
        if(empty($goods_data)){
            $this->retJson(2002, '商品服务不存在！');
        }

        //生成订单
        $transaction = Yii::$app->db->beginTransaction();
        try {
            $order_model = new Order();
            $order_model->order_code = date('YmdHis').rand(10000,99999);
            //用户信息
            $order_model->user_id = $user_id;
            $order_model->mobile = $main_data['mobile'];
            $order_model->address = $main_data['address'];
            //预约时间
            $order_model->book_date = $book_date;
            //预约金额
            $book_money = BookMoney::find()->select('money')->asArray()->one();
            $order_model->book_money = !empty($book_money['money']) ? $book_money['money'] : '15';
            //订单总额
            $order_model->order_total = $main_data['amount'] * $goods_data['price'];
            //用户备注
            $order_model->remarks = $main_data['remarks'];
            $order_model->order_status = 6; //未支付预约金(支付成功，在支付回调中改为 0 待派单状态)
            $order_model->created_at = date('Y-m-d H:i:s');
            if(!$order_model->save()){
                $transaction->rollBack();
                $this->retJson(2010, '下单失败，请重试！');
            }
            //保存订单商品信息
            $order_goods_model = new OrderGoods();
            $order_goods_model->order_code = $order_model->order_code;
            $order_goods_model->user_id = $user_id;
            $order_goods_model->goods_id = $goods_data['id'];
            $order_goods_model->goods_name = $goods_data['name'];
            $order_goods_model->price = $goods_data['price'];
            $order_goods_model->unit = $goods_data['unit'];
            $order_goods_model->amount = $main_data['amount'];
            $order_goods_model->price_total = $main_data['amount'] * $goods_data['price'];
            $order_goods_model->created_at = date('Y-m-d H:i:s');
            if(!$order_goods_model->save()){
                $transaction->rollBack();
                $this->retJson(2011, '下单失败，请重试！');
            }
            $transaction->commit();

            //微信支付配置信息
            $appid = Yii::$app->params['appId']; //小程序 appId
            $mchid = Yii::$app->params['mchId']; //商户号
            $mchkey = Yii::$app->params['mchKey']; //商户密钥
            //微信下单
            $pay = new JsApiPay();
            $input = new \WxPayUnifiedOrder();
            $input->SetBody("金安物业-预约金支付");
            $input->SetAttach($order_model->order_code);
            $input->SetOut_trade_no($order_model->order_code.'book'.rand(100,999)); //预约金支付单号
            $input->SetTotal_fee(intval($order_model->book_money*100));
            $input->SetTime_start(date("YmdHis"));
            $input->SetTime_expire(date("YmdHis", time() + 600));
            $input->SetAppid($appid);
            $input->SetMch_id($mchid);
            $returnUrl = Yii::$app->params['apiDomain'].'/pay/booknotify'; //回调地址
            $input->SetNotify_url($returnUrl);
            $input->SetTrade_type("JSAPI");
            $input->SetOpenid($user_data['openid']);
            $order_return = \WxPayApi::unifiedOrder($input, $mchkey);
            //下单返回结果
            if(!empty($order_return) && $order_return['return_code'] == 'SUCCESS' && $order_return['result_code'] == 'SUCCESS')
            {
                $jsApiParameters = $pay->GetJsApiParameters($order_return, $mchkey);
                //下单成功，返回支付信息
                $this->retJson(1000,'SUCCESS',json_decode($jsApiParameters));
            } else {
                //微信下单失败 记录日志
                $txt = '['.date('Y-m-d H:i:s').']'.'微信下单失败：传参：'.serialize($input).'【返回结果】'.json_encode($order_return);
                $this->writeLog($txt, 'order');
                $this->retJson(2021, '微信支付错误,请稍后重试!');
            }
        } catch (\Exception $e) {
            //订单生成失败 记录日志
            $txt = '['.date('Y-m-d H:i:s').']'.'订单生成失败：'.$e->getMessage();
            $this->writeLog($txt, 'order');

            $this->retJson(2020, '下单失败，请重试！');
        }
    }
}
