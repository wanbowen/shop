<?php
namespace api\controllers;

use Yii;
use yii\web\Controller;
use common\models\User;
use common\components\Sms;
use common\components\ToolHelper;

header ( 'Access-Control-Allow-Origin:*' );

class BaseController extends Controller{

    public $userId;
    public $mainData;
    public $layout = false;
    public function init()
    {
        $this->enableCsrfValidation = false;
        //获取请求参数
        $tool = new ToolHelper();
        $requestData = $tool->getRequest();
        if(!empty($requestData['api_token']) && !empty($requestData['open_id'])){
            $key = Yii::$app->params['key'];
            $new_token = strtoupper(md5($requestData['open_id'].$key));
            if ($requestData['api_token'] == $new_token) {
                //获取请求地址，判断是否是需要用户数据的地址，进行区别判断
                $uri = $_SERVER['REQUEST_URI'];
                $apiUrl = ['/index/collect','/goods/reserve','/goods/confirmorder','/order/list','/order/finish','/order/appraisal','/order/detail','/order/pay','/user/info','/user/bill','/user/coupon','/user/income','/user/getqrcode','/user/editinfo','/user/avatar'];
                if(in_array($uri, $apiUrl)) {
                    $user = User::find()->where(['openid' => $requestData['open_id']])->asArray()->one();
                    if (!empty($user) && !empty($user['mobile']) && $user['status'] == 0) {
                        $this->userId = $user['id'];
                        $this->mainData = !empty($requestData['data']) ? $requestData['data'] : [];
                    } else {
                        $this->retJson(2003, '用户不存在或被加入黑名单！');
                    }
                }
            } else {
                $this->retJson(2002, 'api_token不正确！');
            }
        } else {
            $this->retJson(2001, 'api_token或open_id不能为空！');
        }
	}

    //上传文件
    public function uploadFile($tem){
        //上传文件的存放路径
        $upload_path = "../../admin/web/upload/" . date("Ymd");
        //判断文件夹是否存在，不存在创建
        if (!is_dir($upload_path)) {
            mkdir($upload_path, 0777);
        }
        //生成文件名
        $file_name = time().rand(10000,99999).'.jpg';
        $path = $upload_path . '/' . $file_name;
        $url = '/upload/' . date("Ymd") . '/' . $file_name;
        if (move_uploaded_file($tem, $path)) {
            return $url;
        }else{
            return false;
        }
    }

    /**
     * 接口返回
     * @author  lvhengbin
     * @access public
     * @param string $code 返回错误码
     * @param string $msg 返回错误描述
     * @param array $data 返回数据
     * @return object json数据
     * @date    2019/05/24
     */
    public function retJson($code, $msg = '', $data = array())
    {
        $dataRet = [];
        $dataRet['errcode'] = $code;
        $dataRet['errmsg'] = $msg;
        $dataRet['data'] = $data;
        $dataJson = json_encode($dataRet, JSON_UNESCAPED_SLASHES);
        echo  $dataJson;
        exit();
    }

    /**
     * 发送短信
     * @author  lvhengbin
     * @access public
     * @param string $tel 手机号
     * @param string $content 短信内容
     * @return boolean
     * @date    2019/05/24
     */
    public function sendSms($tel, $content){
        //获取Sms对象
        $sms_model = new Sms(Yii::$app->params['jvtdSmsUrl'],Yii::$app->params['jvtdSmsUid'],Yii::$app->params['jvtdSmsPass']);
        //发送短信
        $res = $sms_model->SendSms($tel, $content);
        //记录短信发送
        $txt = '['.date('Y-m-d H:i:s').']'.'手机号：'.$tel.'。内容：'.$content.'【返回结果：code：'.$res['error_code'].'，msg：'.$res['error_msg'].'】';
        $this->writeLog($txt, 'sendsms');
        //返回发送结果
        if($res['error_code'] > 0){
            return true;
        }
        return false;
    }

    //记录日志
    public function writeLog($txt, $filename) {
        $path = '../runtime/logs';
        if(!is_dir($path)){
            mkdir($path,0777,true);
        }
        $filename = date('Ym').'_'.$filename;
        $path = $path .'/'. $filename .'.txt';
        if(!empty($txt)) {
            file_put_contents($path, $txt . PHP_EOL, FILE_APPEND);
        }
    }
}
