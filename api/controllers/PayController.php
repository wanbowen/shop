<?php
namespace api\controllers;

use Yii;
use yii\web\Controller;
use common\models\User;
use common\models\Order;
use common\models\UserShare;
use common\models\Coupon;
use common\models\CouponShare;
use common\models\CouponReceive;
use common\components\Sms;
use common\models\WorkHours;
use common\models\SmsTemplet;

/**
 * 支付回调相关
 * @author  lvhengbin
 * @date    2019/06/27
 */
class PayController extends Controller
{
    public function init()
    {
        $this->enableCsrfValidation = false;
        $this->layout = false;
    }

    /**
     * 总单 微信支付回调
     * @author  lvhengbin
     * @access  public
     * @date   2019/06/27
     */
    public function actionNotify()
    {
        $callback_string = file_get_contents('php://input');
        //接收内容无，返回FAIL
        if (empty($callback_string))
        {
            echo "FAIL";exit;
        }
        $params = simplexml_load_string($callback_string, 'SimpleXMLElement', LIBXML_NOCDATA);
        $order_code = $params->attach;
        $transaction_id = $params->transaction_id;
        $total_fee = $params->total_fee;    //订单金额

        $order_model = Order::find()->where(['order_code'=>$order_code])->one();
        //查询不到订单，返回FAIL
        if (empty($order_model))
        {
            echo "FAIL";exit;
        }
        //已经修改过订单状态，返回SUCCESS
        if(!empty($order_model->transaction_id))
        {
            echo "<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>";
            exit;
        }
        //金额是否正确
        if($total_fee == ($order_model->pay_total * 100)){
            $order_model->order_status = 4;
        }else{
            //记录日志
            $txt = '['.date('Y-m-d H:i:s').']'.'支付金额不正确，单号:'.$order_code.'，实际支付金额(单位分)：'.$total_fee;
            $this->writeLog($txt, 'order');
            $order_model->order_status = 8; //订单金额有问题
        }

        $order_model->pay_at = date('Y-m-d H:i:s');
        $order_model->transaction_id = $transaction_id;
        if($order_model->save()) {
            //核销优惠券
            if(!empty($order_model->receive_id)) {
                $receive_model = CouponReceive::find()->where(['id' => $order_model->receive_id, 'user_id' => $order_model->user_id])->one();
                $receive_model->is_use = 1;
                $receive_model->use_date = date('Y-m-d H:i:s');
                $receive_model->save();
            }

            echo "<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>";
            exit;
        } else {
            //修改订单状态失败 记录日志
            $txt = '['.date('Y-m-d H:i:s').']'.'支付回调修改订单状态失败：'.json_encode($order_model->getErrors());
            $this->writeLog($txt, 'order');

            echo "FAIL";exit;
        }
    }

    /**
     * 预约金 微信支付回调
     * @author  lvhengbin
     * @access  public
     * @date   2019/06/27
    */
    public function actionBooknotify()
    {
        $callback_string = file_get_contents('php://input');
        //接收内容无，返回FAIL
        if (empty($callback_string))
        {
            echo "FAIL";exit;
        }
        $params = simplexml_load_string($callback_string, 'SimpleXMLElement', LIBXML_NOCDATA);
        $order_code = $params->attach;
        $transaction_id = $params->transaction_id;
        $total_fee = $params->total_fee;    //订单金额

        $order_model = Order::find()->where(['order_code'=>$order_code])->one();
        //查询不到订单，返回FAIL
        if (empty($order_model))
        {
            echo "FAIL";exit;
        }
        //已经修改过订单状态，返回SUCCESS
        if(!empty($order_model->book_transaction_id))
        {
            echo "<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>";
            exit;
        }
        //金额是否正确
        if($total_fee == ($order_model->book_money * 100)){
            $order_model->order_status = 0;
        }else{
            //记录日志
            $txt = '['.date('Y-m-d H:i:s').']'.'支付金额(预约金)不正确，单号:'.$order_code.'，实际支付金额(单位分)：'.$total_fee;
            $this->writeLog($txt, 'order');
            $order_model->order_status = 7; //预付金额有问题
        }

        $order_model->pay_book_at = date('Y-m-d H:i:s');
        $order_model->book_transaction_id = $transaction_id;
        if($order_model->save()) {
            //如果此用户是通过别人推荐注册的，需要给推荐人发一张优惠券
            $share = UserShare::find()->where(['user_id'=>$order_model->user_id,'is_send'=>0])->asArray()->one();
            if(!empty($share)){
                //查询推荐时所发放的优惠券配置
                $coupon_share = CouponShare::find()->asArray()->one();
                if(!empty($coupon_share['coupon_id'])){
                    $coupon = Coupon::find()->where(['id'=>$coupon_share['coupon_id'],'is_del'=>0])->asArray()->one();
                    if(!empty($coupon)) {
                        //发放优惠券
                        $model = new CouponReceive();
                        $model->user_id = $share['origin_user_id'];
                        $model->origin_user_id = $share['user_id'];
                        $model->coupon_id = $coupon_share['coupon_id'];
                        $model->expire_date = $coupon['end_at'];
                        $model->created_at = date('Y-m-d H:i:s');
                        $model->save();
                    }
                }
            }
            //判断当前下单时间是否在工作时间内
            $hours_data = WorkHours::find()->asArray()->one();
            if(!empty($hours_data)){
                $start = strtotime(date('Y-m-d').' '.$hours_data['start'].':00');
                $end = strtotime(date('Y-m-d').' '.$hours_data['end'].':00');
                $now = time();
                if($start > $now || $now > $end){
                    $mobile = $order_model->mobile;
                    //短信模板
                    $tem_data = SmsTemplet::find()->where(['type'=>6])->asArray()->one();
                    $cont = $tem_data['content'];
                    $cont = str_replace('{start}', $hours_data['start'], $cont);
                    $sms_msg = str_replace('{end}', $hours_data['end'], $cont);
                    $this->sendSms($mobile, $sms_msg);
                }
            }

            echo "<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>";
            exit;
        } else {
            //修改订单状态失败 记录日志
            $txt = '['.date('Y-m-d H:i:s').']'.'支付回调修改订单状态失败(预约金)：'.json_encode($order_model->getErrors());
            $this->writeLog($txt, 'order');

            echo "FAIL";exit;
        }
    }

    /**
     * 发送短信
     * @author  lvhengbin
     * @access public
     * @param string $tel 手机号
     * @param string $content 短信内容
     * @return boolean
     * @date    2019/05/24
     */
    public function sendSms($tel, $content){
        //获取Sms对象
        $sms_model = new Sms(Yii::$app->params['jvtdSmsUrl'],Yii::$app->params['jvtdSmsUid'],Yii::$app->params['jvtdSmsPass']);
        //发送短信
        $res = $sms_model->SendSms($tel, $content);
        //记录短信发送
        $txt = '['.date('Y-m-d H:i:s').']'.'手机号：'.$tel.'。内容：'.$content.'【返回结果：code：'.$res['error_code'].'，msg：'.$res['error_msg'].'】';
        $this->writeLog($txt, 'sendsms');
        //返回发送结果
        if($res['error_code'] > 0){
            return true;
        }
        return false;
    }

    //记录日志
    public function writeLog($txt, $filename) {
        $path = '../runtime/logs';
        if(!is_dir($path)){
            mkdir($path,0777,true);
        }
        $filename = date('Ym').'_'.$filename;
        $path = $path .'/'. $filename .'.txt';
        if(!empty($txt)) {
            file_put_contents($path, $txt . PHP_EOL, FILE_APPEND);
        }
    }
}
