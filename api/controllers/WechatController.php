<?php
namespace api\controllers;

use Yii;
use yii\data\Pagination;
use common\models\User;
use common\models\Code;
use common\models\Repair;
use common\models\Materials;
use common\models\Suggestion;
use common\models\SmsTemplet;
use common\components\WeixinHelper;

/*
 * 前端公众号相关页面
 */
class WechatController extends WxbaseController{

    public function init()
    {
        parent::init();
    }

    /*
     * 注册页面
     */
    public function actionRegister()
    {
        if(Yii::$app->request->post()){
            $form = Yii::$app->request->post();
            $mobile = $form['mobile'];
            $reg = "/^1[3|4|5|6|7|8|9]\d{9}$/";
            $wechat_id = Yii::$app->session['wechat_id'] ? Yii::$app->session['wechat_id'] : '';

            if (empty($wechat_id)) {
                $this->json('401', '用户已过期,请重新授权');
            }
            if (!preg_match($reg, $mobile)) {
                $this->json('402', '手机号格式不正确');
            }
            if (empty($form['code'])) {
                $this->json('401', '验证码不能为空');
            }
            $code_data = Code::find()->where(['tel' => $mobile])->orderBy('id desc')->asArray()->one();
            if(empty($code_data) || $code_data['code'] != $form['code']){
                $this->json('402', '验证码不正确');
            }
            if (empty($form['name'])) {
                $this->json('401', '姓名不能为空');
            }
            if (empty($form['address'])) {
                $this->json('401', '地址不能为空');
            }

            $user_model = User::find()->where(['mobile'=>$mobile])->one();
            if(!empty($user_model)){
                $user_model->avatar = Yii::$app->session['headimgurl'];
                $user_model->wx_openid = $wechat_id;
                if ($user_model->save()) {
                    $this->json('200', '注册成功!');
                } else {
                    $this->json('403', '注册失败!');
                }
            }else {
                $user_model = new User();
                $user_model->name = $form['name'];
                $user_model->mobile = $mobile;
                $user_model->address = $form['address'];
                $user_model->avatar = Yii::$app->session['headimgurl'];
                $user_model->wx_openid = $wechat_id;
                $user_model->created_at = date('Y-m-d H:i:s');
                if ($user_model->save()) {
                    $this->json('200', '注册成功!');
                } else {
                    $this->json('403', '注册失败!');
                }
            }
        } else {
            $back_url = Yii::$app->session['backUrl'];
            return $this->render('register', ['back_url'=>$back_url]);
        }
    }

    /*
     * 获取验证码
     */
    public function actionGetCode()
    {
        $mobile = Yii::$app->request->post('mobile');
        $wechat_id = Yii::$app->session['wechat_id'] ? Yii::$app->session['wechat_id'] : '';
        $reg = "/^1[3|4|5|6|7|8|9]\d{9}$/";

        if (empty($wechat_id)) {
            $this->json('401', '用户已过期,请重新授权');
        }
        if (!preg_match($reg, $mobile)) {
            $this->json('402', '手机号格式不正确');
        }

        $code_num = mt_rand(100000, 999999);
        $tem_data = SmsTemplet::find()->where(['type'=>1])->asArray()->one();
        $cont = $tem_data['content'];
        $sms_msg = str_replace('{code}', $code_num, $cont);
        $res = $this->sendSms($mobile, $sms_msg);
        if($res) {
            $code_data = Code::find()->where(['tel' => $mobile])->orderBy('id desc')->asArray()->one();
            if(empty($code_data)){
                $code_model = new Code();
                $code_model->tel = $mobile;
                $code_model->code = (string)$code_num;
                $code_model->addtime = time();
                if($code_model->save()){
                    $this->json('200','发送成功!');
                }else{
                    $this->json('405','发送失败!');
                }
            }else{
                $code_model = Code::updateAll(['code' => $code_num,'addtime' =>time()],['tel' => $mobile]);
                if($code_model > 0){
                    $this->json('200','发送成功!');
                }else{
                    $this->json('406','发送失败!');
                }
            }
        } else {
            $this->json('400', '发送失败！');
        }
    }

    /*
     * 报事报修
     */
    public function actionRepair()
    {
        //提交数据
        if(Yii::$app->request->post()){
            $form = Yii::$app->request->post();
            if(empty($form['name']) || empty($form['contact']) || empty($form['contact_tel'])){
                $this->json('401','参数不能为空!');
            }
            //处理图片
            $imgs = '';
            if(!empty($_FILES)){
                foreach ($_FILES['file']['tmp_name'] as $tem) {
                    $img = $this->uploadFile($tem);
                    if($img){
                        $imgs .= $img . ';';
                    }
                }
                $imgs = trim($imgs,';');
            }

            //保存数据
            $model = new Repair();
            $model->repair_code = date('YmdHis').rand(1000,9999);
            $model->name = $form['name'];
            $model->contact = $form['contact'];
            $model->contact_tel = $form['contact_tel'];
            $model->position = $form['position'];
            $model->content = $form['content'];
            $model->detail_imgs = $imgs;
            $model->created_at = date('Y-m-d H:i:s');
            if ($model->save()) {
                $this->json('200', '提交成功!');
            } else {
                $this->json('402', '提交失败!');
            }
        }
        return $this->render('repair', []);
    }

    /*
     * 投诉建议
     */
    public function actionSuggest()
    {
        //提交数据
        if(Yii::$app->request->post()){
            $user_id = Yii::$app->session['user_id'] ? Yii::$app->session['user_id'] : '';
            if (empty($user_id)) {
                $this->json('401', '用户已过期,请重新授权');
            }

            $title = Yii::$app->request->post('title');
            $content = Yii::$app->request->post('content');
            if(empty($title) || empty($content)){
                $this->json('401','标题和内容不能为空!');
            }
            //插入数据
            $model = new Suggestion();
            $model->title = $title;
            $model->content = $content;
            $model->user_id = $user_id;
            $model->created_at = date('Y-m-d H:i:s');
            if ($model->save()) {
                $this->json('200', '提交成功!');
            } else {
                $this->json('402', '提交失败!');
            }
        }
        //判断用户是否登陆
        $this->checkLogin();

        return $this->render('suggest', []);
    }

    /*
     * 获取投诉建议
     */
    public function actionGetSuggest()
    {
        $user_id = Yii::$app->session['user_id'] ? Yii::$app->session['user_id'] : '';
        $data = Suggestion::find()->where(['is_del' => 0, 'user_id'=>$user_id])->orderBy('id desc')->asArray()->all();
        foreach ($data as $key=>&$val){
            $val['reply'] = !empty($val['reply']) ? $val['reply'] : '';
            $val['reply_at'] = !empty($val['reply_at']) ? $val['reply_at'] : '';
        }
        $this->json('200', 'SUCCESS', $data);
    }

    /*
     * 企业介绍
     */
    public function actionIntroduction()
    {
        $model = Materials::find()->where(['type' => 1])->one();

        $content = !empty($model) ? $model->content : '';
        return $this->render('introduction', [
            'content' => $content,
        ]);
    }

    /*
     * 客服电话
     */
    public function actionServe()
    {
        $model = Materials::find()->where(['type' => 5])->one();

        $content = !empty($model) ? $model->content : '';
        return $this->render('serve', [
            'content' => $content,
        ]);
    }

    /*
     * 企业动态
     */
    public function actionTrend()
    {
        $title = "企业动态";
        return $this->render('list',['title'=>$title, 'type'=>2]);
    }

    /*
     * 项目信息
     */
    public function actionProject()
    {
        $title = "项目信息";
        return $this->render('list',['title'=>$title, 'type'=>3]);
    }

    /*
     * 生活小百科
     */
    public function actionLife()
    {
        $title = "生活小百科";
        return $this->render('list',['title'=>$title, 'type'=>4]);
    }

    /*
     * 获取数据列表
     */
    public function actionGetList()
    {
        $type = Yii::$app->request->get('type');
        $page = Yii::$app->request->get('page');
        if(empty($type)){
            $this->json('401', '参数错误,请重试！');
        }

        $query = Materials::find()->select('id,title,status,created_at');
        $where['is_del'] = 0;
        $where['status'] = 0;
        $where['type'] = $type;
        $query = $query->where($where);

        $limit = 10;
        $allcount = $query->count();
        $pagination = new Pagination([
            'defaultPageSize' => $limit,
            'totalCount' => $allcount
        ]);
        $ma_data = $query->orderBy('id DESC')->offset($pagination->offset)->limit($pagination->limit)->asArray()->all();

        $ret_data['type'] = $type;
        $ret_data['total'] = $allcount;
        $ret_data['data'] = $ma_data;

        $this->json('200', 'SUCCESS', $ret_data);
    }

    /*
     * 图文详情
     */
    public function actionDetail()
    {
        $id = Yii::$app->request->get('id');
        if(empty($id)){
            $this->error('系统错误，请重试！');
        }

        $data = Materials::find()->where(['id'=>$id, 'is_del'=>0, 'status'=>0])->asArray()->one();
        if(empty($data)){
            $this->error('此文章不存在或已删除！');
        }

        $title = '';
        switch ($data['type']){
            case 2:
                $title = '企业动态';
                break;
            case 3:
                $title = '项目信息';
                break;
            case 4:
                $title = '生活小百科';
                break;
        }

        return $this->render('detail',[
            'data'=>$data,
            'title'=>$title
        ]);
    }
}