<?php
namespace api\controllers;

use Yii;
use yii\web\Controller;
use common\models\User;
use common\models\UserShare;
use common\models\Code;
use common\components\Sms;
use common\models\SmsTemplet;
use common\components\ToolHelper;
use common\components\WeixinHelper;

/**
 * 接口登陆相关处理
 * @author  lvhengbin
 * @date    2019/05/24
 */
class TokenController extends Controller
{
    protected $appid;
    protected $appsecret;
    protected $key;
    public function init()
    {
        $this->enableCsrfValidation = false;
        $this->layout = false;
        $this->appid = Yii::$app->params['appId'];
        $this->appsecret = Yii::$app->params['appSecret'];
        $this->key = Yii::$app->params['key'];
    }

    /**
     * @api {post} token/gettoken 获取token
     * @apiVersion 1.0.0
     * @apiName gettoken
     * @apiGroup Token
     *
     * @apiParam {String} code 微信小程序获取的code
     *
     * @apiSuccessExample {json} 例子:
     *  {
     *      "api_token": "",
     *      "data": {
     *          "code": "safasfsafaf",
     *      }
     *  }
     *
     * @apiSuccess {Array} data 返回数据 is_register 是否注册 1，已注册 0，未注册
     * @apiSuccess {Int} errcode 状态码
     * @apiSuccess {String} errmsg 描述信息
     *
     * @apiSuccessExample Success-Response:
     *	{
     *	    "errcode": 1000,
     *	    "errmsg": "SUCCESS",
     *	    "data": {
     *	         {
     * 	            "api_token": "1",
     * 	            "open_id": "1",
     * 	            "is_register": "1",
     *	        }
     *	    }
     *	}
     *
     * @apiError {Array} data 返回数据
     * @apiError {Int} errcode 状态码
     * @apiError {String} errmsg 描述信息
     *
     * @apiErrorExample Error-Response:
     *	{
     *	    "errcode": 2001,
     *	    "errmsg": "参数不能为空",
     *	    "data": {}
     *	}
    */
    public function actionGettoken()
    {
        $tool = new ToolHelper();
        $data = $tool->getRequest();
        if(!empty($data['data'])){
            $code = $data['data']['code'];
            $wx = new WeixinHelper($this->appid, $this->appsecret);
            $wxret = $wx->jscode2session($code);
            if($wxret){
                if(isset($wxret['openid']) && !empty($wxret['openid'])){
                    $openid = $wxret['openid'];
                    $user = User::find()->where(['openid'=>$openid])->asArray()->one();

                    $api_token = strtoupper(md5($openid.$this->key));
                    $data['api_token'] = $api_token;
                    $data['open_id'] = $openid;
                    if(!empty($user)){
                        $data['is_register'] = 1;
                    }else{
                        $data['is_register'] = 0;
                    }
                    $tool->retJson(1000, 'SUCCESS', $data);
                }else{
                    $tool->retJson(2011, $wxret['errmsg']);
                }
            }else{
                $tool->retJson(2010, '请求微信失败');
            }
        }else{
            $tool->retJson(2001, '参数不能为空');
        }
    }

    /**
     * @api {post} token/register 用户注册
     * @apiVersion 1.0.0
     * @apiName register
     * @apiGroup Token
     *
     * @apiParam {String} open_id 微信小程序的 open_id
     * @apiParam {String} mobile 手机号
     * @apiParam {String} code 验证码
     * @apiParam {String} name 姓名
     * @apiParam {String} address 地址
     * @apiParam {String} avatar 用户头像
     * @apiParam {String} from_user_id 如果是通过分享的链接注册的需要携带来源用户id
     *
     * @apiSuccessExample {json} 例子:
     *  {
     *      "api_token": "",
     *      "open_id": "",
     *      "data": {
     *          "mobile": "18101362445",
     *          "code": "254741",
     *          "name": "张三",
     *          "address": "热敏路",
     *          "avatar": "",
     *          "from_user_id": "",
     *      }
     *  }
     *
     * @apiSuccess {Array} data 返回数据
     * @apiSuccess {Int} errcode 状态码
     * @apiSuccess {String} errmsg 描述信息
     *
     * @apiSuccessExample Success-Response:
     *	{
     *	    "errcode": 1000,
     *	    "errmsg": "SUCCESS",
     *	    "data": {}
     *	}
     *
     * @apiError {Array} data 返回数据
     * @apiError {Int} errcode 状态码
     * @apiError {String} errmsg 描述信息
     *
     * @apiErrorExample Error-Response:
     *	{
     *	    "errcode": 2001,
     *	    "errmsg": "参数不能为空",
     *	    "data": {}
     *	}
     */
    public function actionRegister()
    {
        $tool = new ToolHelper();
        $data = $tool->getRequest();
        // 记录日志
        $txt = '['.date('Y-m-d H:i:s').']注册数据接收内容：'.json_encode($data);
        $this->writeLog($txt, 'data');

        if(!empty($data['data'])){
            $form = $data['data'];

            $mobile = isset($form['mobile']) && !empty($form['mobile']) ? $form['mobile'] : '';
            $reg = "/^1[3|4|5|6|7|8|9]\d{9}$/";

            if (!isset($data['open_id']) || empty($data['open_id'])) {
                $tool->retJson(2001, '用户open_id不能为空');
            }
            $open_id = $data['open_id'];
            if (!preg_match($reg, $mobile)) {
                $tool->retJson(2001, '手机号格式不正确');
            }
            if (!isset($form['code']) || empty($form['code'])) {
                $tool->retJson(2001, '验证码不能为空');
            }

            $code_data = Code::find()->where(['tel' => $mobile])->orderBy('id desc')->asArray()->one();
            if(empty($code_data) || $code_data['code'] != $form['code']){
                $tool->retJson(2001, '验证码不正确');
            }
            if (!isset($form['name']) || empty($form['name'])) {
                $tool->retJson(2001, '姓名不能为空');
            }
            if (!isset($form['address']) || empty($form['address'])) {
                $tool->retJson(2001, '地址不能为空');
            }

            $user_model = User::find()->where(['mobile'=>$mobile])->one();
            if(!empty($user_model)){
                $tool->retJson(2001, '已注册！');
            }
            $user_model = new User();
            $user_model->name = $form['name'];
            $user_model->mobile = $mobile;
            $user_model->address = $form['address'];
            $user_model->avatar = isset($form['avatar']) && !empty($form['avatar']) ? $form['avatar'] : '';
            $user_model->openid = $open_id;
            $user_model->created_at = date('Y-m-d H:i:s');
            if ($user_model->save()) {
                if(!empty($form['from_user_id'])){
                    // 记录日志
                    $txt = '['.date('Y-m-d H:i:s').']分享人id：'.$form['from_user_id'];
                    $this->writeLog($txt, 'data');
                    //如果是通过别人分享注册的
                    $user_share_model = new UserShare();
                    $user_share_model->user_id = $user_model->id;
                    $user_share_model->origin_user_id = $form['from_user_id'];
                    $user_share_model->created_at = date('Y-m-d H:i:s');
                    if ($user_share_model->save()) {
                        $txt = '['.date('Y-m-d H:i:s').']保存成功：';
                        $this->writeLog($txt, 'data');
                        $tool->retJson(1000, '注册成功!');
                    } else {
                        $txt = '['.date('Y-m-d H:i:s').']保存失败：'.json_encode($user_share_model->getErrors());
                        $this->writeLog($txt, 'data');
                    }
                }
                $tool->retJson(1000, '注册成功!');
            } else {
                $tool->retJson(2004, '注册失败，请重试！');
            }

        }else{
            $tool->retJson(2001, '参数不能为空');
        }
    }

    /**
     * @api {post} token/getcode 获取验证码
     * @apiVersion 1.0.0
     * @apiName getcode
     * @apiGroup Token
     *
     * @apiParam {String} mobile 手机号
     *
     * @apiSuccessExample {json} 例子:
     *  {
     *      "api_token": "",
     *      "open_id": "",
     *      "data": {
     *          "mobile": "18101362445"
     *      }
     *  }
     *
     * @apiSuccess {Array} data 返回数据
     * @apiSuccess {Int} errcode 状态码
     * @apiSuccess {String} errmsg 描述信息
     *
     * @apiSuccessExample Success-Response:
     *	{
     *	    "errcode": 1000,
     *	    "errmsg": "SUCCESS",
     *	    "data": {
     *	         {
     * 	            "code": "245671",
     *	        }
     *	    }
     *	}
     *
     * @apiError {Array} data 返回数据
     * @apiError {Int} errcode 状态码
     * @apiError {String} errmsg 描述信息
     *
     * @apiErrorExample Error-Response:
     *	{
     *	    "errcode": 2001,
     *	    "errmsg": "参数不能为空",
     *	    "data": {}
     *	}
     */
    public function actionGetcode()
    {
        $tool = new ToolHelper();
        $data = $tool->getRequest();
        if(!empty($data['data'])){
            $form = $data['data'];
            $open_id = isset($data['open_id']) && !empty($data['open_id']) ? $data['open_id'] : '';
            if(empty($open_id)){
                $tool->retJson(2001, 'open_id参数不能为空');
            }

            $mobile = isset($form['mobile']) && !empty($form['mobile']) ? $form['mobile'] : '';
            $reg = "/^1[3|4|5|6|7|8|9]\d{9}$/";

            if (!preg_match($reg, $mobile)) {
                $tool->retJson(2001, '手机号格式不正确');
            }

            //查询手机号是否已经绑定过
            $user_model = User::find()->where(['mobile'=>$mobile])->one();
            if(!empty($user_model)){
                $user_model->openid = $open_id;
                $user_model->save();
                $tool->retJson(2010, '手机号已注册绑定');
            }

            $code_num = mt_rand(100000, 999999);
            $tem_data = SmsTemplet::find()->where(['type'=>1])->asArray()->one();
            $cont = $tem_data['content'];
            $sms_msg = str_replace('{code}', $code_num, $cont);
            $res = $this->sendSms($mobile, $sms_msg);
            if($res) {
                $code_data = Code::find()->where(['tel' => $mobile])->orderBy('id desc')->asArray()->one();
                if(empty($code_data)){
                    $code_model = new Code();
                    $code_model->tel = $mobile;
                    $code_model->code = (string)$code_num;
                    $code_model->addtime = time();
                    if($code_model->save()){
                        $tool->retJson(1000,'SUCCESS');
                    }else{
                        $tool->retJson(2004,'发送失败!');
                    }
                }else{
                    $code_model = Code::updateAll(['code' => $code_num,'addtime' =>time()],['tel' => $mobile]);
                    if($code_model > 0){
                        $tool->retJson(1000,'SUCCESS');
                    }else{
                        $tool->retJson(2004,'发送失败!');
                    }
                }
            } else {
                $tool->retJson(2004,'发送失败!');
            }
        }else{
            $tool->retJson(2001, '参数不能为空');
        }
    }

    /**
     * @api {post} token/getmap 获取地理位置
     * @apiVersion 1.0.0
     * @apiName getmap
     * @apiGroup Token
     *
     * @apiParam {String} location 经纬度坐标
     *
     * @apiSuccessExample {json} 例子:
     *  {
     *      "api_token": "",
     *      "open_id": "",
     *      "data": {
     *          "location": "116.507862,39.903683"
     *      }
     *  }
     *
     * @apiSuccess {Array} data 返回数据
     * @apiSuccess {Int} errcode 状态码
     * @apiSuccess {String} errmsg 描述信息
     *
     * @apiSuccessExample Success-Response:
     *	{
     *	    "errcode": 1000,
     *	    "errmsg": "SUCCESS",
     *	    "data": {
     *	        "province": "北京市",
     *          "city": [],
     *          "address": "北京市朝阳区高碑店镇国粹苑艺术品交易中心"
     *	    }
     *	}
     *
     * @apiError {Array} data 返回数据
     * @apiError {Int} errcode 状态码
     * @apiError {String} errmsg 描述信息
     *
     * @apiErrorExample Error-Response:
     *	{
     *	    "errcode": 2001,
     *	    "errmsg": "参数不能为空",
     *	    "data": {}
     *	}
     */
    public function actionGetmap()
    {
        $tool = new ToolHelper();
        $data = $tool->getRequest();
        if(!empty($data['data'])){
            $form = $data['data'];
            $location = isset($form['location']) && !empty($form['location']) ? $form['location'] : '';
            if (empty($location)) {
                $tool->retJson(2001, '参数不能为空');
            }
            //调用高德地图api
            $key = Yii::$app->params['amapKey'];
            $url = "https://restapi.amap.com/v3/geocode/regeo?location=$location&key=$key&radius=1000";
            $res = $tool->httpGet($url);
            if($res['status'] == 1){
                $retdata['province'] = $res['regeocode']['addressComponent']['province'];
                $retdata['city'] = $res['regeocode']['addressComponent']['city'];
                $retdata['address'] = $res['regeocode']['formatted_address'];

                $tool->retJson(1000, 'SUCCESS', $retdata);
            }else{
                $tool->retJson(2002, '获取位置错误：'.$res['info']);
            }
        }else{
            $tool->retJson(2001, '参数不能为空');
        }
    }

    /**
     * 发送短信
     * @author  lvhengbin
     * @access public
     * @param string $tel 手机号
     * @param string $content 短信内容
     * @return boolean
     * @date    2019/05/24
     */
    public function sendSms($tel, $content){
        //获取Sms对象
        $sms_model = new Sms(Yii::$app->params['jvtdSmsUrl'],Yii::$app->params['jvtdSmsUid'],Yii::$app->params['jvtdSmsPass']);
        //发送短信
        $res = $sms_model->SendSms($tel, $content);
        //记录短信发送
        $txt = '['.date('Y-m-d H:i:s').']'.'手机号：'.$tel.'。内容：'.$content.'【返回结果：code：'.$res['error_code'].'，msg：'.$res['error_msg'].'】';
        $this->writeLog($txt, 'sendsms');
        //返回发送结果
        if($res['error_code'] > 0){
            return true;
        }
        return false;
    }

    //记录日志
    public function writeLog($txt, $filename) {
        $path = '../runtime/logs';
        if(!is_dir($path)){
            mkdir($path,0777,true);
        }
        $filename = date('Ym').'_'.$filename;
        $path = $path .'/'. $filename .'.txt';
        if(!empty($txt)) {
            file_put_contents($path, $txt . PHP_EOL, FILE_APPEND);
        }
    }
}
