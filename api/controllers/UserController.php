<?php
namespace api\controllers;

use Yii;
use common\models\Code;
use common\models\User;
use common\models\Order;
use common\models\OrderGoods;
use common\models\Coupon;
use common\models\CouponReceive;
use common\components\WeixinHelper;

/**
 * 小程序 用户信息 相关接口
 */
class UserController extends BaseController
{
    public function init()
    {
        parent::init();
    }

    /**
     * @api {post} user/info 我的信息
     * @apiVersion 1.0.0
     * @apiName info
     * @apiGroup User
     *
     * @apiParam {String} open_id
     *
     * @apiSuccessExample {json} 例子:
     *  {
     *      "api_token": "",
     *      "open_id": "ojCOA1eVWMWUaNSitcMFGl4fEvUk",
     *      "data": {}
     *  }
     *
     * @apiSuccess {Array} data 返回数据
     * @apiSuccess {Int} errcode 状态码
     * @apiSuccess {String} errmsg 描述信息
     *
     * @apiSuccessExample Success-Response:
     *	{
     *	    "errcode": 1000,
     *	    "errmsg": "SUCCESS",
     *	    "data": {
     *	         {
     *              "id": "1",
     *              "name": "",
     *              "mobile": "",
     *              "address": "",
     *              "avatar": "",
     *          }
     *	    }
     *	}
     *
     * @apiError {Array} data 返回数据
     * @apiError {Int} errcode 状态码
     * @apiError {String} errmsg 描述信息
     *
     * @apiErrorExample Error-Response:
     *	{
     *	    "errcode": 2001,
     *	    "errmsg": "banner数据为空",
     *	    "data": {}
     *	}
     */
    public function actionInfo()
    {
        $user_id = $this->userId;

        $user_data = User::find()->where(['id'=>$user_id,'status'=>0])->asArray()->one();
        if(empty($user_data)){
            $this->retJson(2002, '用户不存在或被加入黑名单！');
        }

        if(strpos($user_data['avatar'],'upload') !== false){
            $user_data['avatar'] = Yii::$app->params['fileDomain'].$user_data['avatar'];
        }

        $this->retJson(1000, 'SUCCESS', $user_data);
    }

    /**
     * @api {post} user/bill 我的账单
     * @apiVersion 1.0.0
     * @apiName bill
     * @apiGroup User
     *
     * @apiParam {String} open_id
     *
     * @apiSuccessExample {json} 例子:
     *  {
     *      "api_token": "",
     *      "open_id": "ojCOA1eVWMWUaNSitcMFGl4fEvUk",
     *      "data": {}
     *  }
     *
     * @apiSuccess {Array} data 返回数据
     * @apiSuccess {Int} errcode 状态码
     * @apiSuccess {String} errmsg 描述信息
     *
     * @apiSuccessExample Success-Response:
     *	{
     *	    "errcode": 1000,
     *	    "errmsg": "SUCCESS",
     *	    "data": {
     *	         {
     *              "id": "1",
     *              "order_code": "201906134656561",
     *              "order_total": "160.50",
     *              "created_at": "2019-06-15 14:48:58",
     *              "order_status": "0",
     *              "goods_name": "擦玻璃"
     *          }
     *	    }
     *	}
     *
     * @apiError {Array} data 返回数据
     * @apiError {Int} errcode 状态码
     * @apiError {String} errmsg 描述信息
     *
     * @apiErrorExample Error-Response:
     *	{
     *	    "errcode": 2001,
     *	    "errmsg": "banner数据为空",
     *	    "data": {}
     *	}
    */
    public function actionBill()
    {
        $user_id = $this->userId;
        //查询订单
        $query = Order::find()->select('id,order_code,order_total,order_status,created_at');
        $where['user_id'] = $user_id;
        $query = $query->where($where);
        $query = $query->andWhere(['in', 'order_status', [4, 5]]);
        $order_data = $query->orderBy('id DESC')->asArray()->all();

        if(!empty($order_data)){
            foreach ($order_data as $key=>&$val){
                $goods = OrderGoods::find()->select('goods_name')->where(['order_code'=>$val['order_code'],'is_edit'=>0])->asArray()->one();
                $val['goods_name'] = $goods['goods_name'];
            }
        }

        $this->retJson(1000, 'SUCCESS', $order_data);
    }

    /**
     * @api {post} user/coupon 我的优惠券
     * @apiVersion 1.0.0
     * @apiName coupon
     * @apiGroup User
     *
     * @apiParam {String} type 1：未使用，2：已使用，3：已失效
     *
     * @apiSuccessExample {json} 例子:
     *  {
     *      "api_token": "",
     *      "open_id": "ojCOA1eVWMWUaNSitcMFGl4fEvUk",
     *      "data": {
     *          "type": "1"
     *      }
     *  }
     *
     * @apiSuccess {Array} data 返回数据
     * @apiSuccess {Int} errcode 状态码
     * @apiSuccess {String} errmsg 描述信息
     *
     * @apiSuccessExample Success-Response:
     *	{
     *	    "errcode": 1000,
     *	    "errmsg": "SUCCESS",
     *	    "data": {
     *	         {
     *              "name": "20元优惠券",
     *              "money": "20",
     *              "start_at": "2019-06-09 00:00:00",
     *              "end_at": "2019-10-31 00:00:00",
     *              "restriction": "0",
     *              "receive_id": "1",
     *              "coupon_id": "4"
     *          }
     *	    }
     *	}
     *
     * @apiError {Array} data 返回数据
     * @apiError {Int} errcode 状态码
     * @apiError {String} errmsg 描述信息
     *
     * @apiErrorExample Error-Response:
     *	{
     *	    "errcode": 2001,
     *	    "errmsg": "banner数据为空",
     *	    "data": {}
     *	}
    */
    public function actionCoupon()
    {
        $main_data = $this->mainData;
        $user_id = $this->userId;
        //(1：未使用，2：已使用，3：已失效)
        $type = isset($main_data['type']) ? $main_data['type'] : 1;
        //查询优惠券
        $query = CouponReceive::find()->select('id,coupon_id');
        $query = $query->where(['user_id'=>$user_id]);
        if($type == 1){
            $query = $query->andWhere(['is_use'=>0]);
            $query = $query->andWhere(['>', 'expire_date', date('Y-m-d H:i:s')]);
        } elseif($type == 2){
            $query = $query->andWhere(['is_use'=>1]);
        } elseif($type == 3){
            $query = $query->andWhere(['is_use'=>0]);
            $query = $query->andWhere(['<=', 'expire_date', date('Y-m-d H:i:s')]);
        }

        $coupon_data = $query->orderBy('id DESC')->asArray()->all();
        $ret_data = [];
        if(!empty($coupon_data)){
            foreach ($coupon_data as $key=>&$val){
                $coupon = Coupon::find()->select('name,money,start_at,end_at,restriction')->where(['id'=>$val['coupon_id'],'is_del'=>0])->asArray()->one();
                if(!empty($coupon)) {
                    $coupon['start_at'] = substr($coupon['start_at'],0,10);
                    $coupon['end_at'] = substr($coupon['end_at'],0,10);
                    $coupon['receive_id'] = $val['id'];
                    $coupon['coupon_id'] = $val['coupon_id'];
                    $ret_data[] = $coupon;
                }
            }
        }

        $this->retJson(1000, 'SUCCESS', $ret_data);
    }

    /**
     * @api {post} user/income 我的收益
     * @apiVersion 1.0.0
     * @apiName income
     * @apiGroup User
     *
     * @apiParam {String} open_id
     *
     * @apiSuccessExample {json} 例子:
     *  {
     *      "api_token": "",
     *      "open_id": "ojCOA1eVWMWUaNSitcMFGl4fEvUk",
     *      "data": {}
     *  }
     *
     * @apiSuccess {Array} data 返回数据
     * @apiSuccess {Int} errcode 状态码
     * @apiSuccess {String} errmsg 描述信息
     *
     * @apiSuccessExample Success-Response:
     *	{
     *	    "errcode": 1000,
     *	    "errmsg": "SUCCESS",
     *	    "data": {
     *	         {
     *              "coupon_id": "4",
     *              "mobile": "18233308510",
     *              "name": "成亮",
     *              "income": "20元优惠券",
     *              "date": "2019-06-20 15:43:25"
     *          }
     *	    }
     *	}
     *
     * @apiError {Array} data 返回数据
     * @apiError {Int} errcode 状态码
     * @apiError {String} errmsg 描述信息
     *
     * @apiErrorExample Error-Response:
     *	{
     *	    "errcode": 2001,
     *	    "errmsg": "banner数据为空",
     *	    "data": {}
     *	}
     */
    public function actionIncome()
    {
        $user_id = $this->userId;
        //查询优惠券
        $query = CouponReceive::find()->select('id,coupon_id,origin_user_id,created_at');
        $query = $query->where(['user_id'=>$user_id]);
        $query = $query->andWhere(['>', 'origin_user_id', 0]);

        $coupon_data = $query->orderBy('id DESC')->asArray()->all();

        $ret_data = [];
        if(!empty($coupon_data)){
            foreach ($coupon_data as $key=>$val){
                $coupon = Coupon::find()->select('name')->where(['id'=>$val['coupon_id']])->asArray()->one();
                $user = User::find()->select('mobile,name')->where(['id'=>$val['origin_user_id']])->asArray()->one();
                $arr['coupon_id'] = $val['coupon_id'];
                $arr['mobile'] = $user['mobile'];
                $arr['name'] = $user['name'];
                $arr['income'] = $coupon['name'];
                $arr['date'] = $val['created_at'];

                $ret_data[] = $arr;
            }
        }

        $this->retJson(1000, 'SUCCESS', $ret_data);
    }

    /**
     * @api {post} user/getqrcode 生成二维码
     * @apiVersion 1.0.0
     * @apiName getqrcode
     * @apiGroup User
     *
     * @apiParam {String} page 下程序页面路径
     *
     * @apiSuccessExample {json} 例子:
     *  {
     *      "api_token": "",
     *      "open_id": "ojCOA1eVWMWUaNSitcMFGl4fEvUk",
     *      "data": {
     *          "page": "pages/share"
     *      }
     *  }
     *
     * @apiSuccess {Array} data 返回数据
     * @apiSuccess {Int} errcode 状态码
     * @apiSuccess {String} errmsg 描述信息
     *
     * @apiSuccessExample Success-Response:
     *	{
     *	    "errcode": 1000,
     *	    "errmsg": "SUCCESS",
     *	    "data": {
     *          "url": "http://huamaoadmin.weiyingjia.org/upload/20190620/1561017460204160.jpg",
     *      }
     *	}
     *
     * @apiError {Array} data 返回数据
     * @apiError {Int} errcode 状态码
     * @apiError {String} errmsg 描述信息
     *
     * @apiErrorExample Error-Response:
     *	{
     *	    "errcode": 2001,
     *	    "errmsg": "参数不能为空",
     *	    "data": {}
     *	}
     */
    public function actionGetqrcode()
    {
        $main_data = $this->mainData;
        $user_id = $this->userId;

        if(!isset($main_data['page']) || empty($main_data['page'])) {
            $this->retJson(2001, 'page不能为空！');
        }

        $user_model = User::find()->select('id,name,openid')->where(['id'=>$user_id,'status'=>0])->one();
        if(empty($user_model)){
            $this->retJson(2002, '用户不存在或被加入黑名单！');
        }
        if(empty($user_model->share_qrcode)){
            $qr = $this->getQrcode($main_data['page'], $user_id);
            if(!$qr){
                $this->retJson(2002, '生成二维码失败，请重试！');
            }
            $user_model->share_qrcode = $qr;
            $user_model->save();
        }
        $url = Yii::$app->params['fileDomain'].$user_model->share_qrcode;

        $this->retJson(1000, 'SUCCESS！', ['url'=>$url]);
    }

    /**
     * @api {post} user/editinfo 用户修改信息
     * @apiVersion 1.0.0
     * @apiName editinfo
     * @apiGroup User
     *
     * @apiParam {String} mobile 手机号
     * @apiParam {String} code 验证码
     * @apiParam {String} name 姓名
     * @apiParam {String} address 地址
     *
     * @apiSuccessExample {json} 例子:
     *  {
     *      "api_token": "",
     *      "open_id": "ojCOA1eVWMWUaNSitcMFGl4fEvUk",
     *      "data": {
     *          "mobile": "18101362445",
     *          "code": "254741",
     *          "name": "张三",
     *          "address": "热敏路",
     *      }
     *  }
     *
     * @apiSuccess {Array} data 返回数据
     * @apiSuccess {Int} errcode 状态码
     * @apiSuccess {String} errmsg 描述信息
     *
     * @apiSuccessExample Success-Response:
     *	{
     *	    "errcode": 1000,
     *	    "errmsg": "SUCCESS",
     *	    "data": {}
     *	}
     *
     * @apiError {Array} data 返回数据
     * @apiError {Int} errcode 状态码
     * @apiError {String} errmsg 描述信息
     *
     * @apiErrorExample Error-Response:
     *	{
     *	    "errcode": 2001,
     *	    "errmsg": "参数不能为空",
     *	    "data": {}
     *	}
     */
    public function actionEditinfo()
    {
        $main_data = $this->mainData;
        $user_id = $this->userId;
        //查询用户
        $user_model = User::find()->where(['id'=>$user_id])->one();
        if(empty($user_model)){
            $this->retJson(2001, '用户不存在');
        }

        $mobile = $main_data['mobile'];
        $reg = "/^1[3|4|5|6|7|8|9]\d{9}$/";
        if (!preg_match($reg, $mobile)) {
            $this->retJson(2001, '手机号格式不正确');
        }
        if($mobile != $user_model->mobile) {    //用户修改了手机号，必须获取验证码
            if (empty($main_data['code'])) {
                $this->retJson(2001, '验证码不能为空');
            }
            $code_data = Code::find()->where(['tel' => $mobile])->orderBy('id desc')->asArray()->one();
            if (empty($code_data) || $code_data['code'] != $main_data['code']) {
                $this->retJson(2001, '验证码不正确');
            }
        }
        if (empty($main_data['name'])) {
            $this->retJson(2001, '姓名不能为空');
        }
        if (empty($main_data['address'])) {
            $this->retJson(2001, '地址不能为空');
        }

        $user_model->name = $main_data['name'];
        $user_model->mobile = $mobile;
        $user_model->address = $main_data['address'];
        if ($user_model->save()) {
            $this->retJson(1000, '提交成功!');
        } else {
            $this->retJson(2004, '提交失败，请重试！');
        }
    }

    /**
     * @api {post} user/avatar 用户修改头像
     * @apiVersion 1.0.0
     * @apiName avatar
     * @apiGroup User
     *
     * @apiParam {String} file 文件名
     *
     * @apiSuccessExample {json} 例子:
     *  {
     *      "api_token": "",
     *      "open_id": "ojCOA1eVWMWUaNSitcMFGl4fEvUk",
     *      "data": {
     *          "file": "",
     *      }
     *  }
     *
     * @apiSuccess {Array} data 返回数据
     * @apiSuccess {Int} errcode 状态码
     * @apiSuccess {String} errmsg 描述信息
     *
     * @apiSuccessExample Success-Response:
     *	{
     *	    "errcode": 1000,
     *	    "errmsg": "SUCCESS",
     *	    "data": {
     *          "url": "http://huamaoadmin.weiyingjia.org/upload/20190620/1561017460204160.jpg",
     *      }
     *	}
     *
     * @apiError {Array} data 返回数据
     * @apiError {Int} errcode 状态码
     * @apiError {String} errmsg 描述信息
     *
     * @apiErrorExample Error-Response:
     *	{
     *	    "errcode": 2001,
     *	    "errmsg": "参数不能为空",
     *	    "data": {}
     *	}
     */
    public function actionAvatar()
    {
        $user_id = $this->userId;

        //处理图片
        $imgs = '';
        if(!empty($_FILES)){
            if (!empty($_FILES['file']['tmp_name'])) {
                $img = $this->uploadFile($_FILES['file']['tmp_name']);
                if($img){
                    $imgs = $img;
                }
            }
        }
        if(empty($imgs)){
            $this->retJson(2001, '请上传头像');
        }

        $user_model = User::find()->where(['id'=>$user_id])->one();
        $user_model->avatar = $imgs;
        if ($user_model->save()) {
            $url = Yii::$app->params['fileDomain'].$user_model->avatar;
            $this->retJson(1000, 'SUCCESS！', ['url'=>$url]);
        } else {
            $this->retJson(2004, '提交失败，请重试！');
        }
    }

    //获取小程序二维码
    private function getQrcode($page,$user_id)
    {
        $appid = Yii::$app->params['appId'];
        $secret = Yii::$app->params['appSecret'];
        $wx = new WeixinHelper($appid, $secret);
        $acesstoken = $wx->getToken();

        $url = 'https://api.weixin.qq.com/wxa/getwxacodeunlimit?access_token='.$acesstoken;
        //页面路径
        $urldata['page'] = $page;
        //提交参数
        $urldata['scene'] = 'from_user_id='.$user_id;
        //二维码宽度
        $urldata['width'] = 430;
        //自动配置线条颜色
        $urldata['auto_color'] = false;
        //是否需要透明底色
        $urldata['is_hyaline'] = false;
        $jsondata = json_encode($urldata);
        $code = $wx->httpPost($url,$jsondata);

        $txt = '生成二维码返回结果：'.json_encode($code);
        $this->writeLog($txt, 'wxcode');

        if(!empty($code) && !is_array($code)){
            //保存地址
            $imgDir = "../../admin/web/upload/" . date("Ymd");
            //判断文件夹是否存在，不存在创建
            if (!is_dir($imgDir)) {
                mkdir($imgDir, 0700);
            }
            //生成文件名
            $file_name = time().rand(10000,99999).'.jpg';
            $newFilePath = $imgDir . '/' . $file_name;
            $newFile = fopen($newFilePath,"w"); //打开文件准备写入
            $is_ok = fwrite($newFile,$code); //写入二进制流到文件
            fclose($newFile); //关闭文件
            if($is_ok != false){
                return '/upload/' . date("Ymd") . '/' . $file_name;
            }
        } else {
            return false;
        }
    }
}