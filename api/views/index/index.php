<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="renderer" content="webkit">
    <title>盒子有奖</title>
    <link rel="stylesheet" href="/css/reset.css">
    <link rel="stylesheet" type="text/css" href="/css/index.css">
    <link rel="stylesheet" type="text/css" href="/css/demo.css" />
    <link rel="stylesheet" type="text/css" href="/css/tabs.css" />
    <link rel="stylesheet" type="text/css" href="/css/tabstyles.css" />
    <!-- <script src="/js/modernizr.custom.js"></script> -->
    <style type="text/css">
        .typeUl{
            z-index: 9999;
        }
        .typeUlist li, .erJiLie li{
            z-index: 9999;
        }
    </style>
</head>
<body>
<div class="indexCon">
    <section>
        <div class="tabs tabs-style-bar">
            <nav class="nav">
                <ul>
                    <li>
                        <table></table>
                        <a href="#section-bar-1">
                            <img src="/img/logo.png" style="width:216px;height: 68px;margin-top: 36px;">
                        </a>
                    </li>
                    <li><a href="#section-bar-2"><span>广告形式</span></a></li>
                    <li><a href="#section-bar-3"><span id="gotoTF">投放流程</span></a></li>
                    <li><a href="#section-bar-4"><span id="gotoRZ">入驻流程</span></a></li>
                    <li><a href="#section-bar-5"><span>电商团购</span></a></li>
                    <li><a href="#section-bar-6"><span>合作案例</span></a></li>
                    <li><a href="#section-bar-7"><span>联系我们</span></a></li>
                </ul>
            </nav>
            <div class="content-wrap" id="index">
                <!-- 首页 -->
                <section id="section-bar-1">
                    <div class="index">
                        <ul class="typeList">
                            <li>
                                <img src="/img/banner1.png">
                                <img src="/img/1.png">
                                <span>广告主WEB端<br>需求提交:用户画像</span>
                            </li>
                            <li>
                                <img src="/img/banner2.png">
                                <img src="/img/2.png">
                                <span>电商受众匹配、<br>发货周期确认</span>
                            </li>
                            <li>
                                <img src="/img/banner3.png">
                                <img src="/img/3.png">
                                <span>订单成立、印刷品设<br>计、印刷厂制作</span>
                            </li>
                            <li>
                                <img src="/img/banner4.png">
                                <img src="/img/4.png">
                                <span>投放产品到发货仓库,<br>电商仓库精准打包分发</span>
                            </li>
                            <li>
                                <img src="/img/banner5.png">
                                <img src="/img/5.png">
                                <span>快递员送货上门,消费<br>者拆封包发现DM单</span>
                            </li>
                            <li>
                                <img src="/img/banner6.png">
                                <img src="/img/6.png">
                                <span>消费者扫描二维码,填<br>写电话,领取奖励</span>
                            </li>
                        </ul>
                        <div class="fontQr">
                            <div class="onlineADV" style="padding: 0px 23px 0px 0px;">
                                <p>电商广告</p>
                                <p>精准分发平台</p>
                            </div>
                            <div class="btnTwo">
                                <!-- <img src="/img/b150_64.png"> -->
                                <span class="guangGaoTF" style="cursor: pointer;">广告投放</span>
                                <!-- <img src="/img/g150_64.png"> -->
                                <span class="dianShangRZ" style="cursor: pointer;">电商入驻</span>
                            </div>
                            <div class="qrTwo">
                                <p>
                                    <img src="/img/qr1.png">
                                    <span>广告业务微信</span>
                                </p>
                                <p style="margin-left: 60px;">
                                    <img src="/img/qr2.png">
                                    <span>电商业务微信</span>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="luoKuan">
                        <?= $luokuan ?>
                    </div>
                </section>
                <!-- 广告形式 -->
                <section id="section-bar-2">
                    <div class="title">
                        <span class="borShow"></span>
                        <p class="titText">广告投放形式</p>
                        <span class="borShow"></span>
                    </div>
                    <div class="advStyle">
                        <img src="<?= $pages['广告形式']['img_url']?>" style="width:100%;">
                    </div>
                    <div class="luoKuan">
                        <?= $luokuan ?>
                    </div>
                </section>
                <!-- 投放流程 -->
                <section id="section-bar-3">
                    <div class="title">
                        <span class="borShow"></span>
                        <p class="titText">广告投放流程</p>
                        <span class="borShow"></span>
                    </div>
                    <div class="advProcess">
                        <img src="<?= $pages['投放流程']['img_url']?>" style="width:100%;">
                    </div>
                    <div class="luoKuan">
                        <?= $luokuan ?>
                    </div>
                </section>
                <!-- 入驻流程 -->
                <section id="section-bar-4">
                    <div  class="showTit">
                        <div class="title">
                            <span class="borShow"></span>
                            <p class="titText">电商合作流程</p>
                            <span class="borShow"></span>
                        </div>
                        <p class="clickBtn" style="display: block;">点击入驻</p>
                    </div>
                    <div class="dsCooPro">
                        <img src="<?= $pages['入驻流程']['img_url']?>" style="width: 100%;">
                    </div>
                    <div class="dsCooInfo">
                        <div class="closeImg">
                            <img src="/img/close.png" class="closeInfo">
                        </div>
                        <div class="fontTit">电商入驻信息填写</div>
                        <div class="warnShow">
                            <span>提交后会有客服人员与您联系，核实信息。</span>
                            <span>填写过程中有任何疑问可拨打<b style="color:#000;font-weight: normal;">13718413989</b>进行询问。</span>
                        </div>
                        <form action="/index/create" method="post" onsubmit="return validData()">
                            <div class="inputBox">
                                <span class="star">*</span>
                                <p class="infoWrite">
                                    <span style="width: 80px;">店铺名称:</span>
                                    <input type="text" name="shop_name" class="infoFont sName" style="width: 327px;height:35px;margin: auto;">
                                </p>
                            </div>
                            <div class="inputTwoBox">
                                <span class="star" style="color:transparent;">*</span>
                                <p class="infoTwoWrite" style="margin-right: 20px;">
                                    <span class="name">法人姓名:</span>
                                    <input type="text" name="legal_person" class="infoFont fRName">
                                </p>
                                <p class="infoTwoWrite">
                                    <span class="name">联系电话:</span>
                                    <input type="text" name="legal_mobile" class="infoFont fRPhone" style="font-size: 14px;">
                                </p>
                            </div>
                            <div class="uploadImg">
                                <span class="star" style="color:transparent;">*</span>
                                <span>营业执照副本:</span>
                                <div class="container imgList">
                                    <div class="imgBox">
                                        <img id="add" class="add" src="/img/add.png"/>
                                        <input id="file1" name="license_file" class="inputFile" accept="image/*" type="file"/>
                                    </div>
                                </div>
                            </div>
                            <div class="inputTwoBox">
                                <span class="star">*</span>
                                <p class="infoTwoWrite" style="margin-right: 20px;">
                                    <span style="width:100px;">执行人姓名:</span>
                                    <input type="text" name="transactor" class="infoFont zXRName" style="width:80px;">
                                </p>
                                <p class="infoTwoWrite">
                                    <span class="name">联系电话:</span>
                                    <input type="text" name="transactor_mobile" class="infoFont zXRPhone" style="font-size: 14px;">
                                </p>
                            </div>
                            <div class="inputBox">
                                <span class="star">*</span>
                                <p class="infoWrite">
                                    <span>发货电脑系统:</span>
                                    <input type="text" name="system" class="infoFont fHSystem">
                                </p>
                            </div>
                            <div class="inputBox">
                                <span class="star">*</span>
                                <p class="infoWrite checkChoo">
                                    <span>是否有线上社群:</span>
                                    <span>是</span>
                                    <input type="radio" value="1" checked name="have_group" id="sQYes">
                                    <span>否</span>
                                    <input type="radio" value="2" name="have_group" id="sQNo">
                                </p>
                            </div>
                            <div class="inputBox">
                                <span class="star">*</span>
                                <p class="infoWrite checkChoo">
                                    <span>是否愿意在群内做分销:</span>
                                    <span>是</span>
                                    <input type="radio" value="1" checked name="if_distribution" id="fXYes">
                                    <span>否</span>
                                    <input type="radio" value="2" name="if_distribution" id="fXNo">
                                </p>
                            </div>
                            <div class="inputBox">
                                <span class="star">*</span>
                                <p class="infoWrite">
                                    <span style="width:155px;">常用的发货包装品类:</span>
                                    <select name="pack" class="select1">
                                        <option value="纸箱">纸箱</option>
                                        <option value="气泡袋信封袋">气泡袋信封袋</option>
                                        <option value="飞机盒">飞机盒</option>
                                        <option value="其他">其他</option>
                                    </select>
                                    <!-- <img src="/img/bootom.png" class="bootomIng"> -->
                                </p>
                            </div>
                            <div class="inputTwoBox">
                                <span class="star">*</span>
                                <p class="infoTwoWrite" style="margin-right: 20px;">
                                    <span style="width:43px;">尺寸:</span>
                                    <input type="text" name="size" class="infoFont size" style="width:130px;">
                                </p>
                                <p class="infoTwoWrite">
                                    <span style="width: 45px;">数量:</span>
                                    <input type="text" name="number" class="infoFont count" style="width: 114px;" onkeyup="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'')}else{this.value=this.value.replace(/\D/g,'')}" onafterpaste="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'0')}else{this.value=this.value.replace(/\D/g,'')}" />
                                    <span style="width: 27px;">/月</span>
                                </p>
                            </div>
                            <div class="inputBox">
                                <span class="star" style="color:transparent;">*</span>
                                <p class="infoWrite">
                                    <span style="width:400px;">经营所在地:</span>
                                    <select name="province_manage" id="province" class="selsct_xjgdzzt">
                                        <option value="">请选择</option>
                                    </select>
                                    <select name="city_manage" id="city" class="selsct_xjgdzzt">
                                        <option value="">请选择</option>
                                    </select>
                                    <select name="area_manage" id="town" class="selsct_xjgdzzt">
                                        <option value="">请选择</option>
                                    </select>
                                </p>
                            </div>
                            <div class="inputBox">
                                <span class="star" style="color:transparent;">*</span>
                                <p class="infoWrite">
                                    <span style="width:140px;">经营所在地详细地址:</span>
                                    <input type="text" name="address_manage" class="xiangxi_dizhi jYDetailArea" placeholder="请填写详细地址">
                                </p>
                            </div>
                            <div class="inputBox">
                                <span class="star">*</span>
                                <p class="infoWrite">
                                    <span style="width:400px;">仓库所在地:</span>
                                    <select name="province_warehouse" id="province1" class="selsct_xjgdzzt">
                                        <option value="请选择">请选择</option>
                                    </select>
                                    <select name="city_warehouse" id="city1" class="selsct_xjgdzzt">
                                        <option value="请选择">请选择</option>
                                    </select>
                                    <select name="area_warehouse" id="town1" class="selsct_xjgdzzt">
                                        <option value="请选择">请选择</option>
                                    </select>
                                </p>
                            </div>
                            <div class="inputBox">
                                <span class="star">*</span>
                                <p class="infoWrite">
                                    <span style="width:140px;">仓库所在地详细地址:</span>
                                    <input type="text" name="address_warehouse" class="xiangxi_dizhi cKDetailArea" placeholder="请填写详细地址">
                                </p>
                            </div>
                            <div id="typeCon" style="margin-left:0px;">
                            </div>
                            <div id='typeListCon' style="margin-left:0px;">
                            </div>
                            <div class="inputBox">
                                <span class="star" style="color:transparent;">*</span>
                                <p class="addType">点击添加品类</p>
                                <input id="inputType" class="inputboxYIncangyu" type="text" name="inputboxYIncangyu"  value="" />
                            </div>
                            <div class="submitBox">
                                <!-- <p class="submitBtn">提交</p> -->
                                <input type="hidden" name="license_img" id="license_img" >
                                <input type="submit" value="提交" class="submitBtn"/>
                            </div>
                        </form>
                        <!-- <iframe id="is_iframe" name="the_iframe" style="display:none;"></iframe> -->
                    </div>
                    <div class="luoKuan">
                        <?= $luokuan ?>
                    </div>
                </section>
                <!-- 电商团购 -->
                <section id="section-bar-5">
                    <div class="title">
                        <span class="borShow"></span>
                        <p class="titText">电商团购</p>
                        <span class="borShow"></span>
                    </div>
                    <div class="dsTeamBuy">
                        <img src="<?= $pages['电商团购']['img_url']?>"  style="width: 100%;">
                    </div>
                    <div class="luoKuan">
                        <?= $luokuan ?>
                    </div>
                </section>
                <!-- 合作案例 -->
                <section id="section-bar-6">
                    <div class="title">
                        <span class="borShow"></span>
                        <p class="titText">合作案例</p>
                        <span class="borShow"></span>
                    </div>
                    <div class="cooTemplate">
                        <ul class="temList">
                            <?php
                            foreach ($case as $v){
                                ?>
                                <li>
                                    <p><img style="width: 186px;height: 146px;" src="<?= $v['img_url']?>" ></p>
                                    <p><?= $v['title']?></p>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                    <div class="luoKuan">
                        <?= $luokuan ?>
                    </div>
                </section>
                <!-- 联系我们 -->
                <section id="section-bar-7">
                    <div class="title">
                        <span class="borShow"></span>
                        <p class="titText">联系我们</p>
                        <span class="borShow"></span>
                    </div>
                    <div class="contactWe">
                        <div class="contantCon">
                            <p class="fontBold">联系我们</p>
                            <p>官网网址 : www.heziyoujiang.com</p>
                            <p>官方邮箱 : SC@xindaoguanggao.com</p>
                            <p>广告投放 : 李先生 15230350008</p>
                            <p>电商合作 : 崔先生 13718413989</p>
                            <p>印刷合作 : 010-86464568</p>
                            <p class="borderBox"></p>
                            <p class="fontBold">盒子有奖简介</p>
                            <p style="line-height: 30px;">盒子有奖（BingoBox）是北京信道广告有限公司旗下的电商精准广告投放模块。

                                盒子有奖项目依托全网上万家垂直电商及当当网、亚马逊等电商平台，通过16大类和48小类电商品类，形成了月发货3000万件以上的电商广告精准分发平台。

                                北京信道广告有限公司成立于2016年，作为新型的精准营销公司，直接以帮助广告主精准获客为目的，将用户画像与用户需求进行细分，形成消费者需求标签，进而帮广告主精准匹配有消费需求的目标受众，并锁定有共同特性的，相关度高的小范围受众，搭建线上、线下的推广矩阵，形成了CPM/CPT/CPC/CPA/CPS等多样的业务模式 ，帮助广告主降低获客成本！

                                </p>
                            <p class="fontBold">业务宗旨</p>
                            <p>精准曝光、精准引流，精准转化。
                                让有用的信息在正确的时间出现在正确的地方！</p>
                        </div>
                    </div>
                    <div class="luoKuan">
                        <?= $luokuan ?>
                    </div>
                </section>
            </div>
        </div>
    </section>

</div>
</body>
<script type="text/javascript" src="/js/jquery-2.1.0.js"></script>
<script src="/js/cbpFWTabs.js"></script>
<script src="/js/area.js"></script>
<script src="/js/select.js"></script>
<script>
    /** tab切换 **/
    (function() {
        [].slice.call( document.querySelectorAll( '.tabs' ) ).forEach( function( el ) {
            new CBPFWTabs( el );
        });
    })();
    /** 添加营业执照副本 **/
    var imgObj=[];
    $('.container').delegate('.inputFile','change',function(){
        var _this=$(this);
        var _thisId=_this.attr('id');
        var imgBgSrc=_this.parent().find('.add').attr('id');
        // console.log(imgBgSrc);
        var reads= new FileReader();
        // console.log(reads);
        f=document.getElementById(_thisId).files[0];
        reads.readAsDataURL(f);
        reads.onload=function (e) {
            //this.result就是返回的base64 url地址
            // console.log(this.result);
            imgObj[_this.parent().index()]=this.result;
            document.getElementById(imgBgSrc).src=this.result;
            $("#license_img").val(this.result)
        }
    })
    /**  不能同时选中是否按钮控制 **/
    $("#sQYes").on("click",function(){
        $("#sQNo").removeAttr("checked");
    });
    $("#sQNo").on("click",function(){
        $("#sQYes").removeAttr("checked");
    });
    $("#fXYes").on("click",function(){
        $("#fXNo").removeAttr("checked");
    });
    $("#fXNo").on("click",function(){
        $("#fXYes").removeAttr("checked");
    });
    /** 点击添加品类 **/
    var index = 1,arr=[{'index':index}]
    function chooseType(){
        var html = `<div class="inputBox">
						<span class="star" style="margin-left: 0px;">*</span>
						<p class="infoWrite" style="position: relative;">
							<span style="width:100px;">主营商品品类:</span>
							<span class="chooseType chooseT" style="width:302px;" id="removeClass">
                                <span class="html1"></span>
                                <span class="html2"></span>
                                <span class="html3"></span>
								<input type="text" name="writeType" placeholder="请输入商品品类" class="writeType"/>
                                <img src="/img/bootom.png" class="bootomIng">
							</span>
						</p>
                        <p>
                            <span class="delType" style="width: 60px;display:  inline-block;margin: 5px 0px -5px 5px;">删除此类</span>
                        </p>
								<!-- 一级内容 -->
            <div class="typeUl">
            <ul class="typeUlist">
            <!-- 图书音像一级 -->
            <li class='books' id="17">
            图书音像
            </li>
            <!-- 女装内衣一级 -->
            <li class="womenClo" id="18">
            女装内衣
            </li>
            <!-- 男装/运动户外一级-->
            <li class="menClo" id="19">
            男装/运动户外
            </li>
            <!-- 女鞋/男鞋/箱包一级 -->
            <li class="shoesB" id="20">
            女鞋/男鞋/箱包
            </li>
            <!-- 美妆/个人护理一级 -->
            <li class="cosmetics" id="5">
            美妆/个人护理
            </li>
            <!-- 腕表/眼镜/珠宝饰品一级 -->
            <li class="watch" id="6">
            腕表/眼镜/珠宝饰品
            </li>
            <!-- 手机/数码/电脑办公一级 -->
            <li class="phoneSm" id="7">
            手机/数码/电脑办公
            </li>
            <!-- 母婴玩具一级 -->
            <li class="toys"  id="8">
            母婴玩具
            </li>
            <!-- 零食/茶酒/进口食品一级 -->
            <li class="food" id="9">
            零食/茶酒/进口食品
            </li>
            <!-- 生鲜水果一级 -->
            <li class="freshF" id="10">
            生鲜水果
            </li>
            <!-- 大家电/生活电器一级 -->
            <li class="electric"  id="11">
            大家电/生活电器
            </li>
            <!-- 家具建材一级 -->
            <li class="furniture" id="12">
            家具建材
            </li>
            <!-- 汽车/配件/用品一级 -->
            <li class="autocar" id="13">
            汽车/配件/用品
            </li>
            <!-- 家纺/家饰/鲜花一级 -->
            <li class="life" id="14">
            家纺/家饰/鲜花
            </li>
            <!-- 医药保健一级 -->
            <li class="drug" id="15">
            医药保健
            </li>
            <!-- 厨具/收纳/宠物一级 -->
            <li class="Kitchen" id="16">
            厨具/收纳/宠物
            </li>
            <li class="Kitchen" id="30">
            其他
            </li>
            </ul>
            </div>
            <!-- 二级内容 -->
            <div class='erjiMain' style="position: absolute;left: 200px; top:30px;z-index:9999">
            <!-- 图书音像二级 -->
            <ul class="booksTwo erJiLie" id="17-1">
            <li id='a'>
            <span>儿童读物</span>
            <img src="/img/right.png" alt="">
            </li>
            <li id='b'>
            <!-- 图书音像二级 -->
            <span>畅销小说</span>
            <img src="/img/right.png" alt="">
            </li>
            <li id='c'>
            <!-- 图书音像二级 -->
            <span>文学文艺</span>
            <img src="/img/right.png" alt="">
            </li>
            <li id='d'>
            <!-- 图书音像二级 -->
            <span>社科生活</span>
            <img src="/img/right.png" alt="">
            </li>
            <li id='e'>
            <!-- 图书音像二级 -->
            <span>育儿百科</span>
            <img src="/img/right.png" alt="">
            </li>
            <li id='f'>
            <!-- 图书音像二级 -->
            <span>学习考试</span>
            <img src="/img/right.png" alt="">
            </li>
            <li id='g'>
            <!-- 图书音像二级 -->
            <span>教材教辅</span>
            <img src="/img/right.png" alt="">
            </li>
            <li id='h'>
            <!-- 图书音像二级 -->
            <span>经管励志</span>
            <img src="/img/right.png" alt="">
            </li>
            <li id='i'>
            <!-- 图书音像二级 -->
            <span>大牌乐器</span>
            <img src="/img/right.png" alt="">
            </li>
            </ul>
            <!-- 女装内衣二级 -->
            <ul class="womenYF erJiLie" id="18-1">
            <li>当季流行</li>
            <li>精选上装</li>
            <li>浪漫裙装</li>
            <li>女士下装</li>
            <li>特色女装</li>
            <li>文胸塑身</li>
            <li>家居服</li>
            <li>内裤背心</li>
            </ul>
            <!-- 男装/运动户外二级 -->
            <ul class="menHW erJiLie" id="19-1">
            <li>当季流行</li>
            <li>男士外套</li>
            <li>男士内搭</li>
            <li>男士裤装</li>
            <li>特色男装</li>
            <li>运动服</li>
            <li>户外用品</li>
            <li>运动用品</li>
            </ul>
            <!-- 女鞋/男鞋/箱包二级 -->
            <ul class="shoeBag erJiLie" id="20-1">
            <li>推荐女鞋</li>
            <li>潮流男鞋</li>
            <li>女单鞋</li>
            <li>特色鞋</li>
            <li>潮流女包</li>
            <li>精品男包</li>
            <li>功能箱包</li>
            </ul>
            <!-- 美妆/个人护理二级 -->
            <ul class="meiZ erJiLie" id="5-1">
            <li>护肤品</li>
            <li>彩妆</li>
            <li>男士护肤</li>
            <li>肤质推选</li>
            <li>美发护发</li>
            <li>口腔护理</li>
            <li>身体女性</li>
            </ul>
            <!-- 腕表/眼镜/珠宝饰品二级 -->
            <ul class="shouShi erJiLie" id="6-1">
            <li>黄金首饰</li>
            <li>钻石彩宝</li>
            <li>珍珠玉翠</li>
            <li>潮流饰品</li>
            <li>腕表</li>
            <li>眼镜</li>
            <li>烟具</li>
            </ul>
            <!-- 手机/数码/电脑办公二级 -->
            <ul class="shuMa erJiLie" id="7-1">
            <li>热门手机</li>
            <li>特色手机</li>
            <li>电脑整机</li>
            <li>智能数码</li>
            <li>游戏组装</li>
            <li>硬件储存</li>
            <li>摄影摄像</li>
            <li>影音娱乐</li>
            <li>办公文教</li>
            <li>数码配件</li>
            </ul>
            <!-- 母婴玩具二级 -->
            <ul class="muYin erJiLie" id="8-1">
            <li>童鞋</li>
            <li>车床用品</li>
            <li>洗护</li>
            <li>玩具</li>
            <li>天猫动漫</li>
            <li>奶粉</li>
            <li>纸尿裤</li>
            <li>孕产</li>
            </ul>
            <!-- 零食/茶酒/进口食品二级 -->
            <ul class="lingShi erJiLie" id="9-1">
            <li>进口食品</li>
            <li>休闲零食</li>
            <li>酒类</li>
            <li>茶叶</li>
            <li>乳品冲饮</li>
            <li>粮油速食</li>
            <li>生鲜水果</li>
            </ul>
            <!-- 生鲜水果二级 -->
            <ul class="fruits erJiLie" id="10-1">
            <li>新鲜蔬菜</li>
            <li>冰激凌</li>
            <li>蛋类</li>
            <li>肉类</li>
            <li>海鲜水产</li>
            <li>新鲜水果</li>
            <li>精选干货</li>
            </ul>
            <!-- 大家电/生活电器二级 -->
            <ul class="jiaDian erJiLie" id="11-1">
            <li>平板电视</li>
            <li>空调</li>
            <li>冰箱</li>
            <li>洗衣机</li>
            <li>厨房大电</li>
            <li>热水器</li>
            <li>中式厨房</li>
            <li>西式厨房</li>
            <li>生活电器</li>
            <li>个护健康</li>
            <li>精品推荐</li>
            </ul>
            <ul class="jiaJu erJiLie" id="12-1">
            <li>成套家具</li>
            <li>客厅餐厅</li>
            <li>卧室家具</li>
            <li>书房儿童</li>
            <li>家装主材</li>
            <li>厨房卫浴</li>
            <li>灯饰照明</li>
            <li>五金工具</li>
            <li>全屋定制</li>
            <li>装修设计</li>
            </ul>
            <ul class="car erJiLie" id="13-1">
            <li>整车</li>
            <li>坐垫脚垫</li>
            <li>基友轮胎</li>
            <li>电子导航</li>
            <li>车载电器</li>
            <li>维修保养</li>
            <li>美容清洗</li>
            <li>汽车装饰</li>
            <li>安全自驾</li>
            <li>外饰改装</li>
            <li>汽车服务</li>
            </ul>
            <ul class="jiaFang erJiLie" id="14-1">
            <li>床上用品</li>
            <li>居家布艺</li>
            <li>家居饰品</li>
            <li>鲜花绿植</li>
            </ul>
            <ul class="yiYao erJiLie" id="15-1">
            <li>保健品</li>
            <li>滋补品</li>
            <li>医药</li>
            <li>医疗器械</li>
            <li>隐形眼镜</li>
            <li>医疗服务</li>
            </ul>
            <ul class="chuJu erJiLie" id="16-1">
            <li>厨房烹饪</li>
            <li>餐饮具</li>
            <li>居家礼品</li>
            <li>纸品清洁</li>
            <li>宠物用品</li>
            </ul>
            </div>
            <div class='sanjiMain' style="position: absolute;top: 30px;right:-165px;z-index:9999;" >
            <ul class="booksET sanJiLie" id="a-1">
            <li>畅销童书</li>
            <li>绘本</li>
            <li>儿童文学</li>
            <li>启蒙认知</li>
            <li>益智游戏</li>
            <li>幼儿科普</li>
            <li>童话书</li>
            </ul>
            <ul class="bookXS sanJiLie" id="b-1">
            <li>都市情感</li>
            <li>穿越架空</li>
            <li>畅销小说</li>
            <li>外国文学</li>
            <li>科幻小说</li>
            <li>影视同期</li>
            </ul>	
            <ul class="booksWX sanJiLie"  id="c-1">
            <li>畅销书</li>
            <li>散文随笔</li>
            <li>文学</li>
            <li>传记</li>
            <li>涂色</li>
            <li>字帖</li>
            <li>现当代文学</li>
            <li>诺贝尔</li>
            <li>色铅笔</li>
            </ul>
            <ul class="booksSK sanJiLie" id="d-1">
            <li>烘焙</li>
            <li>旅游</li>
            <li>菜谱</li>
            <li>美容/美体</li>
            <li>时尚</li>
            <li>家居装修</li>
            <li>两性健康</li>
            <li>两性</li>
            <li>减肥</li>
            </ul>
            <ul class="booksYE sanJiLie" id="e-1">
            <li>家庭教育</li>
            <li>幼儿情绪管理</li>
            <li>能力培养</li>
            <li>胎教</li>
            <li>幼儿心理</li>
            <li>孕产妇保健</li>
            </ul>
            <ul class="booksStu sanJiLie" id="f-1">
            <li>公务员</li>
            <li>托福</li>
            <li>职称英语</li>
            <li>建造师考试</li>
            <li>注册会计师</li>
            <li>四六级英语</li>
            <li>考研</li>
            </ul>
            <ul class="booksJC sanJiLie" id="g-1">
            <li>中学教辅</li>
            <li>新课标</li>
            <li>中小学作文</li>
            <li>本科/研究生教材</li>
            <li>高职教材</li>
            <li>外语学习</li>
            </ul>
            <ul class="booksJY sanJiLie" id="h-1">
            <!-- 图书音像三级 -->
            <li>管理学</li>
            <li>金融与投资</li>
            <li>励志</li>
            <li>投资与理财</li>
            <li>金融学</li>
            <li>股市</li>
            <li>女性励志</li>
            <li>营销</li>
            </ul>
            <ul class="booksYQ sanJiLie" id="i-1">
            <li>乐器</li>
            <li>钢琴</li>
            <li>智能钢琴</li>
            <li>数码钢琴</li>
            <li>电子琴</li>
            <li>吉他</li>
            <li>尤克里里</li>
            <li>古筝</li>
            <li>萨克斯风</li>
            <li>小提琴</li>
            <li>儿童乐器</li>
            <li>口琴</li>
            <li>电吉他</li>
            <li>民谣吉他</li>
            </ul>
            </div>	
            </div>
            <div class="inputTwoBox">
            <span class="star">*</span>
            <p class="infoTwoWrite" style="margin-right: 20px;">
            <span class="name">月发货量:</span>
            <input type="number" name="send_number[]" class="infoFont faHuoCount" onBlur="if(this.value<0){this.value=0;alert('月发货量请输入正整数')}">
            </p>
            <p class="infoTwoWrite">
            <span style="width:82px;">用户年龄:</span>
            <select name="age[]" class="select2">
            <option>0-3</option>
            <option>4-6</option>
            <option>7-12</option>
            <option>13-15</option>
            <option>16-18</option>
            <option>19-22</option>
            <option>23-26</option>
            <option>27-40</option>
            <option>40-60</option>
            <option>60以上</option>
            </select>
            </p>
            </div>`	
            $('#typeListCon').append(html)
            }
    (()=>{
        chooseType()
    })()
    $(".addType").on('click',function(){
        arr.push({'index':++index})
        chooseType();
        console.log(arr)
    })
    /** 选择品类列表 **/
    $('body').delegate('.chooseType','click',function(e){
        e.stopPropagation();  
        $('.typeUl').hide();
        $(".erJiLie").hide();
        $(".sanJiLie").hide();
        $(".erJiLie li").css("background","#fff");
        $(".erJiLie li").css("color","#000");
        $(".sanJiLie li").css("background","#fff");
        $(".sanJiLie li").css("color","#000");
        $(this).parents('.inputBox').find('.typeUl').show();
    })
    $('body').delegate('.typeUlist li','click',function(e){
        e.stopPropagation();
        $(".erJiLie").hide();
        var erJiID = $(this).attr("id");
        if(erJiID == 30){
            $(this).parents('.inputBox').find(".writeType").show();
            $('.typeUl').hide();
            $(".erJiLie").hide();
            $(".sanJiLie").hide();
            $(this).parents('.inputBox').find('.html1').html("其他");
            $(this).parents('.inputBox').find('.html2').html('');
            $(this).parents('.inputBox').find('.html3').html('');
            $(this).parents('.inputBox').find("#removeClass").removeClass("chooseType")   
        }
        var erJiShowID = "#"+ erJiID +"-1";
        $(this).parents('.inputBox').find(erJiShowID).show();
        $(erJiShowID).siblings().hide();
        $(this).parents('.inputBox').find('.html1').html($(this).text());
        $(this).parents('.inputBox').find('.html2').html('');
        $(this).parents('.inputBox').find('.html3').html('');
        $(".sanJiLie").hide();
        $(this).css("background","#4A90E2")
        $(this).css("color","#fff")
        $(this).siblings().css("background","#fff")
        $(this).siblings().css("color","#000")
    })
    $('body').delegate('.writeType','blur',function(e){
        e.stopPropagation();
        if($(this).val() !=""){
            $(this).parents('.inputBox').find('.html2').html($(this).val());
            $(this).parents('.inputBox').find('.html2').css({"width":"245px","white-space": "nowrap","text-overflow": " ellipsis",'overflow':'hidden'})
            $(this).hide();
        }
    })
    $('body').delegate('.erJiLie li','click',function(e){
        e.stopPropagation();
        var sanjiID = $(this).attr("id");
        var sanJiShowID = "#"+ sanjiID +"-1";
        $(this).parents('.inputBox').find('.html2').html($(this).text());
        if(sanjiID == undefined){
            $('.typeUl').hide();
            $(".erJiLie").hide();
            $(".sanJiLie").hide();
        }else{
            $(".sanJiLie").hide();
            $(this).parents('.inputBox').find(sanJiShowID).show();
        }
        $(this).css("background","#4A90E2")
        $(this).css("color","#fff")
        $(this).siblings().css("background","#fff")
        $(this).siblings().css("color","#000")
    })
    $('body').delegate('.sanJiLie li','click',function(e){
        e.stopPropagation();
        $(this).parents('.inputBox').find('.html3').html($(this).text());
        $('.typeUl').hide();
        $(".erJiLie").hide();
        $(".sanJiLie").hide();
        $(this).css("background","#4A90E2")
        $(this).css("color","#fff")
        $(this).siblings().css("background","#fff")
        $(this).siblings().css("color","#000")
    })
    /** 点击关闭品类选择操作  */
    $('body').click(function(e) {
       if(e.target.id != 'chooseType')
          if ( $('.typeUl').is(':visible') ) {
             $('.typeUl').hide();
             $(".erJiLie").hide();
             $(".sanJiLie").hide();
          }
    })
    /** 点击删除品类操作  */
    $('body').delegate('.delType','click',function(e){
        e.stopPropagation();
       // alert($(".delType").length)
        if($(".delType").length ==1){
            alert("不能再删了哦！")
        }else if($(".delType").length >1){
            $(this).parent().parent().next().remove();
            $(this).parent().parent().remove();
        }
    })
    /** 点击入驻控制显示隐藏  */
    $(".clickBtn").on('click',function(){
        $(".showTit").hide();
        $(".dsCooPro").hide();
        $(".dsCooInfo").show();
    })
    $(".closeInfo").on('click',function(){
        $(".showTit").show();
        $(".dsCooPro").show();
        $(".dsCooInfo").hide();
    })

    /**  点击提交事件 */
    $('body').delegate('.submitBtn','click',function(){
        $('.html1').text().replace(/(<br[^>]*>| |\s*)/g,'');
        var arr=[];
        var firstHtml='';
        var secondHtml='';
        var thirdHtml='';
        var sumhHtml='';
        for(var i=0;i<$('.chooseT').length;i++){
            if($('.chooseT').eq(i).find('.html1').text()!=''){
                firstHtml=$('.chooseT').eq(i).find('.html1').text().replace(/(<br[^>]*>| |\s*)/g,'');
            }
            if($('.chooseT').eq(i).find('.html2').text()!=''){
                secondHtml=$('.chooseT').eq(i).find('.html2').text().replace(/(<br[^>]*>| |\s*)/g,'');
            }
            if($('.chooseT').eq(i).find('.html3').text()!=''){
                thirdHtml=$('.chooseT').eq(i).find('.html3').text().replace(/(<br[^>]*>| |\s*)/g,'');
                sumhHtml=firstHtml+','+secondHtml+','+thirdHtml
                arr.push(sumhHtml);
            }else{
                sumhHtml=firstHtml+','+secondHtml
                arr.push(sumhHtml);
            }
}
        console.log(arr);
        var arrStr = arr.join("&");
        console.log("品类数组字符串",arrStr)
        $("#inputType").val(arrStr);
        // console.log($("#inputType").val(arrStr))

    })
    /**  表单提交前验证 */
    function validData(){
        /**  验证店铺名称 **/
        var storeName = $(".sName").val()
        if(storeName == ""){
            alert("请输入店铺名称")
            return false
        }
        /** 验证法人联系电话 **/
        var faRenPhone = $(".fRPhone").val()
        var reg=/^1[34578]\d{9}$/;
        if(faRenPhone !==""){
            if(!reg.test(faRenPhone)){
                alert("法人手机号格式不正确，请重新输入！")
                return false
            }
        }
        /** 验证执行人姓名 **/
        var zhiXingRenName = $(".zXRName").val();
        if(zhiXingRenName  == ""){
            alert("请输入执行人姓名")
            return false
        }
        /** 验证执行人联系电话 **/
        var zhiXingPhone = $(".zXRPhone").val();
        if(zhiXingPhone  == ""){
            alert("请输入执行人联系电话")
            return false
        }
        var reg1=/^1[34578]\d{9}$/;
        if(!reg1.test(zhiXingPhone)){
            alert("执行人手机号格式不正确，请重新输入！")
            return false
        }
        /** 验证发货电脑系统 **/
        var faHuoSystem = $(".fHSystem").val();
        if(faHuoSystem  == ""){
            alert("请输入发货电脑系统")
            return false
        }
        /** 验证尺寸 **/
        var size = $(".size").val();
        if(size  == ""){
            alert("请输入尺寸")
            return false
        }
        /** 验证数量 **/
        var count = $(".count").val();
        if(count  == ""){
            alert("请输入数量")
            return false
        }
        /** 验证仓库地址 **/
        var province_warehouse = $("#province1").val();
        var city_warehouse = $("#city1").val();
        var area_warehouse = $("#town1").val();
        if(province_warehouse  == "请选择" || city_warehouse == "请选择" || area_warehouse == "请选择"){
            alert("请选择完整的仓库地址信息")
            return false
        }
        /** 验证仓库地详细地址 **/
        var cKDetailArea = $(".cKDetailArea").val();
        if(cKDetailArea  == ""){
            alert("请输入仓库地详细地址")
            return false
        }
        /** 验证主营品类是否添加 **/
        if($("#inputType").val().length==0){
            alert("请点击添加品类，添加商品品类")
            return false
        }
        for(var i=0;i<$('.html1').length;i++){
            if($('.html1').eq(i).text()==''){
                alert('请选择主营商品品类');
                return false;
            }
        }
        for(var i=0;i<$('.faHuoCount').length;i++){
            if($('.faHuoCount').eq(i).val()==''){
                alert('请输入月发货量');
                return false;
            }
        }
        alert("数据已提交")
    }
    /** 点击首页广告投放、电商入住按钮跳转至对应页面 **/
    $('.guangGaoTF').on("click",function(){ 
        $("#gotoTF").trigger('click')
    })
    $('.dianShangRZ').click(function(){ 
        $("#gotoRZ").trigger('click')
        $(".clickBtn").trigger('click')
    })
</script>
</html>