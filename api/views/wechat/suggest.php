<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no,viewport-fit=cover">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no" />
	<title>投诉建议</title>
	<link rel="stylesheet" type="text/css" href="/css/style.css" />
	<link rel="stylesheet" type="text/css" href="/css/suggestion.css" />
</head>

<body>
	<div class="tab-box">
		<div class="tab" id="tab">
			<span class="tab-item active">提交意见</span>
			<span class="tab-item">我的意见</span>
		</div>
	</div>
	<div class="tab-list">
		<div class="tab-container">
			<div class="suggest-box">
				<div class="suggest-title">
					<div>建议标题：</div>
					<input type="text" placeholder="请输入标题" id="title">
				</div>
				<div class="suggest-content">
					<textarea placeholder="请输入建议内容" id="content" maxlength="500" class="textarea"></textarea>
				</div>
			</div>
			<div class="btn">
				<button class="submit" onclick="submit()">提交</button>
			</div>
		</div>
		<div class="tab-container"  style="display: none;">
            <div id="data-box">

            </div>
		</div>
	</div>

	<script type="text/javascript" src="/js/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="/layer_mobile/layer.js"></script>
	<script type="text/javascript" src="/js/common.js"></script>
	<script>
		var tabItem = document.querySelectorAll('#tab span'),
		    div = document.querySelectorAll('.tab-container');

        getList();

		for (var i = 0; i < tabItem.length; i++) {
			tabItem[i].idx = i;
			tabItem[i].onclick = function () {
				console.log(div[this.idx])
				for(var j=0;j<div.length;j++){
					div[j].style.display = 'none';
					tabItem[j].classList.remove('active');
				}
				div[this.idx].style.display = 'block';
				this.classList.add('active')
			}
		}
		//提交建议
		function submit() {
            var title = $("#title").val();
            if(title == ''){
                layMsg('请输入标题！');return false;
            }
            var content = $("#content").val();
            if(content == ''){
                layMsg('请输入内容！');return false;
            }
            layLoad();
            $.post('/wechat/suggest',{'title':title,'content':content},function (res) {
                layer.closeAll();
                if(res.code == '200'){
                    layAlertback('提交成功！', function () {
                        window.location.href=window.location.href+"?id="+10000*Math.random();
                    });
                }else{
                    layAlert(res.msg);
                }
            },'json')
        }

        //获取历史建议
        function getList() {
            $.post('/wechat/get-suggest',{},function (res) {
                if(res.code == '200'){
                    var data = res.data;
                    if(data.length > 0) {
                        var htm = '';
                        $.each(data, function (x,y) {
                            htm += '<div class="my-suggest"><div class="my-title">'+y.title+'</div>';
                            htm += '<div class="my-suggest-content">'+y.content+'</div>';
                            htm += '<div class="suggest-time"><div>'+y.created_at+'</div></div></div>';
                            htm += '<div class="reply-box"><div class="reply"><div style="color: #48CA41;">回复：</div>';
                            htm += '<div class="reply-content">'+y.reply+'</div></div>';
                            htm += '<div class="reply-time"><div>'+y.reply_at+'</div></div></div>';
                        });
                        $("#data-box").append(htm);
                    } else {
                        $("#data-box").append('<div style="text-align: center;line-height: 1rem;">暂无数据</div>');
                    }
                }
            },'json')
        }
	</script>
</body>

</html>