<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no,viewport-fit=cover">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no" />
	<title>报事报修</title>
	<link rel="stylesheet" type="text/css" href="/css/style.css" />
	<style>
		body {
			background: #F2F2F2;
		}
		.message-wrapper {
			width: 100%;
			background: #fff;
		}
		.mess-item {
			width: 100%;
			height: .5rem;

			font-size: .28rem;
			color: #333333;
			display: flex;
			/* justify-content: center; */
			align-items: center;
			padding-top: .5rem;
		}
		.mess-item input {
			border: 0;
			outline: none;
            width: 70%;
		}
		.message-box {
			width: 90%;
			border-bottom: 1px solid #e4e4e4;
			margin: 0 auto;
		}
		.description-wrapper {
			width: 100%;
			height: 2.4rem;
			background: #fff;
			margin-top: .3rem;
		}
		.description-title {
			width: 94%;
			border-bottom: 1px solid #e4e4e4;
			height: .7rem;
			display: flex;
			margin: 0 auto;
			font-size: .28rem;
			color: #333333;
			align-items: center;
		}
		.description-title input {
			border: 0;
			outline: none;
		}
		.des-content {
			text-align: center;
            padding: 0 0.3rem;
		}
		.textarea {
			border: 0;
			outline: none;
			margin-top: .2rem;
            width: 100%;
            height: 1.3rem;
		}
		.img-upload img {
			width: 1.2rem;
			height: 1.2rem;
			margin-top: .2rem;
			margin-left: .2rem;
		}
		.imgChoose01,.imgChoose02,.imgChoose03 {
			display: none;
		}
        .submitBtn {
            width: 6rem;
            height: .88rem;
            background: #48CA41;
            border-radius: 44px;
            font-size: .28rem;
            color: #FFFFFF;
            letter-spacing: 0.45px;
            text-align: center;
            border: 0;
            outline: none;
        }
        .btn-wrapper{
            text-align: center;
        }
	</style>
</head>

<body>
	<div>
        <div style="height: .3rem;"></div>
		<div class="message-wrapper">
			<div class="message-box">
				<div class="mess-item">
					<div>项目信息：</div>
					<input type="text" id="name">
				</div>
			</div>
			<div class="message-box">
				<div class="mess-item">
					<div>联系人：</div>
					<input type="text" id="contact">
				</div>
			</div>
			<div class="message-box">
				<div class="mess-item">
					<div>电话号码：</div>
					<input type="num" maxlength="11" id="contact_tel" />
				</div>
			</div>
			<div class="message-box">
				<div class="mess-item">
					<div>具体位置：</div>
					<input type="text" id="position">
				</div>
			</div>
			<div class="message-box" style="border-bottom:0;">
				<div class="mess-item" style="height:.1rem;">

				</div>
			</div>
		</div>
		<div class="description-wrapper">
			<div class="description-title">
				<div>问题描述：</div>
				<input type="text">
			</div>
			<div class="des-content">
				<textarea class="textarea" name="" id="content" placeholder="请输入问题"></textarea>
			</div>
		</div>
		<div class="img-upload">
			<!-- 图片上传 -->
			<input type="file" class="imgChoose01" id="file01" name="image" accept="image/*" onchange="previewImg01(this);" />
			<img src="/tpl/imgs/img-upload.png" alt="" id="img01">
			<input type="file" class="imgChoose02" id="file02" name="image" accept="image/*" onchange="previewImg02(this);" />
			<img src="/tpl/imgs/img-upload.png" alt="" id="img02">
			<input type="file" class="imgChoose03" id="file03" name="image" accept="image/*" onchange="previewImg03(this);" />
			<img src="/tpl/imgs/img-upload.png" alt="" id="img03">
		</div>
        <div style="height: 1rem;"></div>
        <div class="btn-wrapper">
            <button class="submitBtn" onclick="submit()">提交</button>
        </div>
	</div>
    <script type="text/javascript" src="/js/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="/layer_mobile/layer.js"></script>
	<script type="text/javascript" src="/js/common.js"></script>
	<script>
        
        //提交数据
        function submit() {
            var name = $("#name").val();
            if(name == ''){
                layMsg('请输入项目信息！');return false;
            }
            var contact = $("#contact").val();
            if(contact == ''){
                layMsg('请输入联系人！');return false;
            }
            var contact_tel = $("#contact_tel").val();
            if(contact_tel == ''){
                layMsg('请输入手机号！');return false;
            }
            //正则验证手机号
            var reg = /^1[3|4|5|6|7|8|9]\d{9}$/;
            if (!reg.test(contact_tel)) {
                layMsg('请输入正确的手机号');return false;
            }
            var position = $("#position").val();
            if(position == ''){
                layMsg('请输入位置信息！');return false;
            }
            var content = $("#content").val();

            var formData = new FormData();
            formData.append("name", name);
            formData.append("contact", contact);
            formData.append("contact_tel", contact_tel);
            formData.append("position", position);
            formData.append("content", content);
            formData.append("file[]", document.getElementById("file01").files[0]);
            formData.append("file[]", document.getElementById("file02").files[0]);
            formData.append("file[]", document.getElementById("file03").files[0]);

            layLoad();
            $.ajax({
                url: "/wechat/repair",
                data: formData,
                type: "POST",
                dataType: 'JSON',
                contentType: false,//这里
                processData: false,//这两个一定设置为false
                success: function (res) {
                    layer.closeAll();
                    if(res.code == '200'){
                        layAlertback('我们已经收到您的反馈！', function () {
                            window.location.href=window.location.href+"?id="+10000*Math.random();
                        });
                    }else{
                        layAlert(res.msg);
                    }
                }
            });
        }

		//图片上传
		$('#img01').click(function () {
			$('.imgChoose01').click();
		});
		$('#img02').click(function () {
			$('.imgChoose02').click();
		});
		$('#img03').click(function () {
			$('.imgChoose03').click();
		});
		//第一张图
		function previewImg01(input) {
			if (input.files && input.files[0]) {
				var file = input.files[0],
					reader = new FileReader();
				if (!input['value'].match(/.jpg|.JPG|.gif|.png|.bmp/i)) {　　//判断上传文件格式
					return alert("上传的图片格式不正确，请重新选择")
				}
				reader.readAsDataURL(file);
				reader.onload = function (e) {
					var result = this.result;
					//回显图片
					document.getElementById('img01').setAttribute('src',result);//回显图片
				}
			}
		}
		//第二张图
		function previewImg02(input) {
			if (input.files && input.files[0]) {
				var file = input.files[0],
					reader = new FileReader();
				if (!input['value'].match(/.jpg|.JPG|.gif|.png|.bmp/i)) {　　//判断上传文件格式
					return alert("上传的图片格式不正确，请重新选择")
				}
				reader.readAsDataURL(file);
				reader.onload = function (e) {
					var result = this.result;
					//回显图片
					document.getElementById('img02').setAttribute('src',result);//回显图片
				}
			}
		}
		//第三张图
		function previewImg03(input) {
			if (input.files && input.files[0]) {
				var file = input.files[0],
					reader = new FileReader();
				if (!input['value'].match(/.jpg|.JPG|.gif|.png|.bmp/i)) {　　//判断上传文件格式
					return alert("上传的图片格式不正确，请重新选择")
				}
				reader.readAsDataURL(file);
				reader.onload = function (e) {
					var result = this.result;
					//回显图片
					document.getElementById('img03').setAttribute('src',result);//回显图片
				}
			}
		}
	</script>
</body>

</html>