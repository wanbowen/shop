<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no,viewport-fit=cover">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no" />
	<title>客服电话</title>
	<link rel="stylesheet" type="text/css" href="/css/style.css" />
	<style>
        .editor{
            box-sizing: border-box;
            background-color: #fff;
            width: 100%;
            height: auto;
            padding: 0.3rem;
        }
        .cont{
            width: 100%;
        }
        .cont img{
            width: 100%;
        }
	</style>
</head>

<body>
	<div class="editor">
        <div class="cont"><?=$content?></div>
    </div>
	<script type="text/javascript" src="/js/jquery-1.11.2.min.js"></script>
	<script type="text/javascript" src="/js/common.js"></script>
	<script>

	</script>
</body>

</html>