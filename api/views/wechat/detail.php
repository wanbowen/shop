<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no,viewport-fit=cover">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no" />
	<title><?=$title?></title>
	<link rel="stylesheet" type="text/css" href="/css/style.css" />
	<style>
        .editor{
            box-sizing: border-box;
            background-color: #fff;
            width: 100%;
            height: auto;
            padding: 0.3rem;
        }
        .title{
            width: 100%;
            font-size: .4rem;
            font-weight: bold;
        }
        .time{
            padding: .3rem 0 .5rem 0;
            color: #949494;
        }
        .cont{
            width: 100%;
        }
        .cont img{
            width: 100%;
        }
	</style>
</head>

<body>
	<div class="editor">
        <div class="title"><?=$data['title']?></div>
        <div class="time"><?=$data['created_at']?></div>
        <div class="cont"><?=$data['content']?></div>
    </div>
	<script type="text/javascript" src="/js/jquery-1.11.2.min.js"></script>
	<script type="text/javascript" src="/js/common.js"></script>
	<script>

	</script>
</body>

</html>