<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no,viewport-fit=cover">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta name="format-detection" content="telephone=no" />
		<title>金安物业</title>
		<link rel="stylesheet" type="text/css" href="/css/style.css"/>
        <script type="text/javascript" src="/js/jquery-1.11.2.min.js"></script>
        <script type="text/javascript" src="/js/common.js"></script>
        <style>
            .weui_msg{
                padding-top: 1rem;
                text-align: center;
            }
            .weui_msg .weui_icon_area {
                margin-bottom: 0.8rem;
            }
            .weui_icon_area img{
                width: 2.5rem;
            }
            .weui_msg .weui_text_area {
                padding: 0 0.5rem;
                font-size: 0.4rem;
            }
        </style>
	</head>
	<body>
        <div class="weui_msg">
            <div class="weui_icon_area">
                <img src="/tpl/imgs/waring.png" />
            </div>
            <div class="weui_text_area">
                <h4 class="weui_msg_title"><?php echo $params['msg']; ?></h4>
            </div>
        </div>

        <script>

        </script>
	</body>
</html>
