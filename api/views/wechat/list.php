<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no,viewport-fit=cover">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="format-detection" content="telephone=no" />
    <title><?=$title?></title>
    <link rel="stylesheet" type="text/css" href="/css/style.css" />
    <style>
        body {
            background: #F2F2F2;
            width: 100%;
            height: auto;
        }
        .news-item {
            background: #fff;
            font-size: .32rem;
            color: #333333;
            letter-spacing: 0.52px;
        }
        .bottom-line {
            width: 86%;
            padding: .4rem;
            border-bottom: 1px solid #E4E4E4;
            margin: 0 auto;
        }
        .time {
            font-size: .24rem;
            color: #999999;
            margin-top: .2rem;
        }
    </style>
</head>

<body>
    <div style="height: 0.2rem"></div>
    <div class="news-list">
        <div id="data-box">

        </div>
        <!-- 底部空白 -->
        <div class="news-item">
            <div class="bottom-line" id="bottom" style="text-align: center;">
              
            </div>
        </div>
    </div>
    <script type="text/javascript" src="/js/jquery-1.11.2.min.js"></script>
    <script type="text/javascript" src="/js/common.js"></script>
    <script type="text/javascript" src="/layer_mobile/layer.js"></script>
    <script>
        var type = "<?=$type?>";
        var page = 1;
        var flag = true;
        
        $(function () {
            getList(1);
        });
        
        function getList() {
            if(!flag) return false;
            layLoad();
            flag = false;
            $.get('/wechat/get-list',{'type':type,'page':page},function (res) {
                layer.closeAll();
                if(res.code == '200'){
                    var data = res.data.data;
                    if(data.length > 0) {
                        var htm = '';
                        $.each(data, function (x,y) {
                            htm += '<div class="news-item" onclick="detail('+y.id+')"><div class="bottom-line">';
                            htm += '<div>'+y.title+'</div>';
                            htm += '<div class="time">'+y.created_at+'</div>';
                            htm += '</div></div>';
                        });
                        if(data.length >= 10){
                            flag = true;
                            page++;
                        }
                        $("#data-box").append(htm);
                    }else{
                        $("#bottom").html('暂无数据');
                    }
                }else{
                    layAlert(res.msg);
                }
            },'json')
        }
        //查看详情
        function detail(id) {
            window.location.href = '/wechat/detail?id='+id;
        }


        //--------------上拉加载更多---------------

        //滚动事件触发
        window.onscroll = function() {
            if(getScrollTop() + getClientHeight() >= getScrollHeight()-5) {
                getList();
            }
        };
        //获取滚动条当前的位置
        function getScrollTop() {
            var scrollTop = 0;
            if(document.documentElement && document.documentElement.scrollTop) {
                scrollTop = document.documentElement.scrollTop;
            } else if(document.body) {
                scrollTop = document.body.scrollTop;
            }
            return scrollTop;
        }
        //获取当前可视范围的高度
        function getClientHeight() {
            var clientHeight = 0;
            if(document.body.clientHeight && document.documentElement.clientHeight) {
                clientHeight = Math.min(document.body.clientHeight, document.documentElement.clientHeight);
            } else {
                clientHeight = Math.max(document.body.clientHeight, document.documentElement.clientHeight);
            }
            return clientHeight;
        }
        //获取文档完整的高度
        function getScrollHeight() {
            return Math.max(document.body.scrollHeight, document.documentElement.scrollHeight);
        }
        //-----------------结束--------------------
    </script>
</body>

</html>