<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no,viewport-fit=cover">
		<meta name="apple-mobile-web-app-capable" content="yes">
		<meta name="apple-mobile-web-app-status-bar-style" content="black">
		<meta name="format-detection" content="telephone=no" />
		<title>金安物业</title>
		<link rel="stylesheet" type="text/css" href="/css/style.css"/>
        <link rel="stylesheet" type="text/css" href="/css/register.css" />
        <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.min.js"></script>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/vant@2.0/lib/index.css">
        <style>
            body {
                background: #ffffff;
            }
        </style>
	</head>
	<body>
        <div class="main">
            <div class="word">绑定信息</div>
            <div class="form-item">
                <div>手机号</div>
                <input type="tel" name="phone" maxlength="11" id="mobile"/>
            </div>
            <div class="form-item">
                <div>验证码</div>
                <input type="tel" maxlength="6" id="code">
                <button class="codeBtn" id="sendCode" onclick="getCode()">获取验证码</button>
            </div>
            <div class="form-item">
                <div>姓名</div>
                <input type="text" maxlength="5" id="name">
            </div>
            <div class="form-item">
                <div>地址</div>
                <input type="text" id="address">
            </div>
            <button class="btn" onclick="submit()">确定</button>
        </div>

        <script type="text/javascript" src="/js/jquery-1.11.2.min.js"></script>
        <script type="text/javascript" src="/layer_mobile/layer.js"></script>
        <script type="text/javascript" src="/js/common.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/vant@2.0/lib/vant.min.js"></script>
        <script>
            var back_url = "<?=$back_url?>";
            function submit() {
                var mobile = $("#mobile").val();
                if(mobile == ''){
                    vant.Toast('请输入手机号！');return false;
                }
                //正则验证手机号
                var reg = /^1[3|4|5|6|7|8|9]\d{9}$/;
                if (!reg.test(mobile)) {
                    vant.Toast('请输入正确的手机号');return false;
                }
                var code = $("#code").val();
                if(code == ''){
                    vant.Toast('请输入验证码！');return false;
                }
                var name = $("#name").val();
                if(name == ''){
                    vant.Toast('请输入姓名！');return false;
                }
                var address = $("#address").val();
                if(address == ''){
                    vant.Toast('请输入地址！');return false;
                }
                layLoad();
                $.post('/wechat/register',{'mobile':mobile,'code':code,'name':name,'address':address},function (res) {
                    layer.closeAll();
                    if(res.code == '200'){
                        layAlertback('注册成功！', function () {
                            window.location.href = back_url;
                        });
                    }else{
                        layAlert(res.msg);
                    }
                },'json')
            }

            function getCode() {
                var mobile = $("#mobile").val();
                if(mobile == ''){
                    vant.Toast('请输入手机号');return false;
                }
                //正则验证手机号
                var reg = /^1[3|4|5|6|7|8|9]\d{9}$/;
                if (!reg.test(mobile)) {
                    vant.Toast('请输入正确的手机号');return false;
                }
                layLoad();
                $.post('/wechat/get-code',{'mobile':mobile},function (res) {
                    layer.closeAll();
                    if(res.code == '200'){
                        // 倒计时
                        let count = 60;
                        const countDown = setInterval(() => {
                            if (count === 0) {
                                $('#sendCode').text('重新发送').removeAttr('disabled');
                                $('#sendCode').css({
                                    background: '#48CA41',
                                    color: '#fff',
                                });
                                clearInterval(countDown);
                            } else {
                                $('#sendCode').attr('disabled', true);
                                $('#sendCode').css({
                                    background: '#d8d8d8',
                                    color: '#707070',
                                });
                                $('#sendCode').text(count + '秒');
                            }
                            count--;
                        }, 1000);
                    }else{
                        layAlert(res.msg);
                    }
                },'json')
            }
        </script>
	</body>
</html>
