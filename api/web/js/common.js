/*手机自适应设置*/
window.onload=window.onresize=window.onscroll=function(){	
	fontSize();
	pageShow();
};

function pageShow(){
	var oBox = document.getElementsByTagName('body')[0];
	oBox.style.visibility = 'visible';
}

function fontSize(){
	document.documentElement.style.fontSize = 100*(document.documentElement.clientWidth/750)+'px';
}

//layer_mobile 弹窗
function layMsg(msg) {
    layer.open({
        content: msg,
        skin: 'msg',
        time: 2
    });
    $(".layui-m-layer .layui-m-layer-msg").css('bottom','0');
}
//layer_mobile 确认弹窗
function layAlert(msg) {
    layer.open({
        content: msg,
		style: 'width: 240px;height: 150px;',
        btn: '确定'
    });
	$(".layui-m-layermain .layui-m-layercont").css('padding','40px 30px');
}
//layer_mobile 带回调确认弹窗
function layAlertback(msg, func) {
    layer.open({
        content: msg,
        style: 'width: 240px;height: 150px;',
        btn: '确定',
        shadeClose: false,
        yes: func
    });
    $(".layui-m-layermain .layui-m-layercont").css('padding','40px 30px');
}
//layer_mobile 加载中
function layLoad() {
    layer.open({
        type: 2,
        shadeClose: false
    });
}