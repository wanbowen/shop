define({ "api": [
  {
    "type": "post",
    "url": "goods/confirmorder",
    "title": "确认下单",
    "version": "1.0.0",
    "name": "confirmorder",
    "group": "Goods",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "goods_id",
            "description": "<p>商品id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "amount",
            "description": "<p>商品数量</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mobile",
            "description": "<p>联系人电话</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>联系人地址</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "book_date",
            "description": "<p>预约日期</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "book_time",
            "description": "<p>预约时间</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "remarks",
            "description": "<p>用户备注</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "例子:",
          "content": "{\n    \"api_token\": \"\",\n    \"open_id\": \"ojCOA1eVWMWUaNSitcMFGl4fEvUk\",\n    \"data\": {\n        \"goods_id\": \"1\",\n        \"amount\": \"1\",\n        \"mobile\": \"18101362546\",\n        \"address\": \"人民路2号\",\n        \"book_date\": \"2019-06-12\",\n        \"book_time\": \"15:00:00\",\n        \"remarks\": \"干活勤快\",\n     }\n}",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "\t{\n\t    \"errcode\": 1000,\n\t    \"errmsg\": \"SUCCESS\",\n\t    \"data\": {\n         \"appId\": \"\",\n         \"timeStamp\": \"\",\n         \"nonceStr\": \"\",\n         \"package\": \"\",\n         \"signType\": \"\",\n         \"paySign\": \"\",\n     }\n\t}",
          "type": "json"
        }
      ],
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n    \"errcode\": 2001,\n    \"errmsg\": \"参数不能为空\",\n    \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/GoodsController.php",
    "groupTitle": "Goods"
  },
  {
    "type": "post",
    "url": "goods/detail",
    "title": "获取单个商品服务详情",
    "version": "1.0.0",
    "name": "detail",
    "group": "Goods",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "goods_id",
            "description": "<p>商品id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "例子:",
          "content": "{\n    \"api_token\": \"\",\n    \"open_id\": \"ojCOA1eVWMWUaNSitcMFGl4fEvUk\",\n    \"data\": {\n        \"goods_id\": \"1\"\n    }\n}",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "\t{\n\t    \"errcode\": 1000,\n\t    \"errmsg\": \"SUCCESS\",\n\t    \"data\": {\n\t         \"id\": \"3\",\n          \"name\": \"夏季玻璃清洁4月-11月\",\n          \"price\": \"2\",\n          \"unit\": \"平米\",\n          \"content\": \"<p><span style=\\\"font-size:14px;font-family:宋体\\\">夏季玻璃清洁</span><span style=\\\"font-size:14px;font-family:&#39;Calibri&#39;,sans-serif\\\">4</span><span style=\\\"font-size:14px;font-family:宋体\\\">月</span><span style=\\\"font-size:14px;font-family:&#39;Calibri&#39;,sans-serif\\\">-11</span><span style=\\\"font-size:14px;font-family:宋体\\\">月大优惠啊</span></p>\",\n          \"detail_imgs\": [\n             \"http://huamaoadmin.weiyingjia.org/upload/20190620/1561025385396495.jpg\",\n             \"http://huamaoadmin.weiyingjia.org/upload/20190620/1561025389230694.jpg\"\n          ]\n\t    }\n\t}",
          "type": "json"
        }
      ],
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n    \"errcode\": 2001,\n    \"errmsg\": \"分类数据为空\",\n    \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/GoodsController.php",
    "groupTitle": "Goods"
  },
  {
    "type": "post",
    "url": "goods/list",
    "title": "获取某个分类下所有商品服务",
    "version": "1.0.0",
    "name": "list",
    "group": "Goods",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "category_id",
            "description": "<p>分类id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "page",
            "description": "<p>页码</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "limit",
            "description": "<p>每页多少条数据</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "例子:",
          "content": "{\n    \"api_token\": \"\",\n    \"open_id\": \"ojCOA1eVWMWUaNSitcMFGl4fEvUk\",\n    \"data\": {\n        \"category_id\": \"1\",\n        \"page\": \"1\",\n        \"limit\": \"10\",\n    }\n}",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "\t{\n\t    \"errcode\": 1000,\n\t    \"errmsg\": \"SUCCESS\",\n\t    \"data\": {\n\t         \"category_data\": {\n             \"id\": \"4\",\n             \"name\": \"家庭清洁服务\",\n             \"banner_img\": \"http://huamaoadmin.weiyingjia.org/upload/20190620/1561008572171151.jpg\"\n         },\n         \"goods_data\": {\n             \"page\": 1,\n             \"limit\": 10,\n             \"total\": \"2\",\n             \"data\": [\n                 {\n                     \"id\": \"3\",\n                     \"name\": \"夏季玻璃清洁4月-11月\",\n                     \"category_id\": \"4\",\n                     \"price\": \"2\",\n                     \"unit\": \"平米\",\n                     \"thumbnail\": \"http://huamaoadmin.weiyingjia.org/upload/20190620/1561025377860591.jpg\"\n                 },\n             ]\n         }\n\t    }\n\t}",
          "type": "json"
        }
      ],
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n    \"errcode\": 2001,\n    \"errmsg\": \"banner数据为空\",\n    \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/GoodsController.php",
    "groupTitle": "Goods"
  },
  {
    "type": "post",
    "url": "goods/reserve",
    "title": "立即预约",
    "version": "1.0.0",
    "name": "reserve",
    "group": "Goods",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "goods_id",
            "description": "<p>商品id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "例子:",
          "content": "{\n    \"api_token\": \"\",\n    \"open_id\": \"ojCOA1eVWMWUaNSitcMFGl4fEvUk\",\n    \"data\": {\n        \"goods_id\": \"1\",\n     }\n}",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "\t{\n\t    \"errcode\": 1000,\n\t    \"errmsg\": \"SUCCESS\",\n\t    \"data\": {\n         \"goods_data\": \"商品数据\",\n         \"user_data\": \"用户数据\",\n         \"book_config\": \"预约时间和开关\",\n         \"lable_data\": \"标签数据\",\n         \"book_money\": \"预约金额\",\n     }\n\t}",
          "type": "json"
        }
      ],
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n    \"errcode\": 2001,\n    \"errmsg\": \"参数不能为空\",\n    \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/GoodsController.php",
    "groupTitle": "Goods"
  },
  {
    "type": "post",
    "url": "goods/review",
    "title": "获取商品所有评价",
    "version": "1.0.0",
    "name": "review",
    "group": "Goods",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "goods_id",
            "description": "<p>商品id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "例子:",
          "content": "{\n    \"api_token\": \"\",\n    \"open_id\": \"ojCOA1eVWMWUaNSitcMFGl4fEvUk\",\n    \"data\": {\n        \"goods_id\": \"1\"\n    }\n}",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "\t{\n\t    \"errcode\": 1000,\n\t    \"errmsg\": \"SUCCESS\",\n\t    \"data\": {\n\t         {\n\t            \"mobile\": \"15510778583\",\n             \"created_at\": \"2019-06-13 14:52:24\",\n             \"appraisal\": \"3\"\n\t        },\n\t    }\n\t}",
          "type": "json"
        }
      ],
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n    \"errcode\": 2001,\n    \"errmsg\": \"参数不能为空\",\n    \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/GoodsController.php",
    "groupTitle": "Goods"
  },
  {
    "type": "post",
    "url": "index/banner",
    "title": "获取首页banner图",
    "version": "1.0.0",
    "name": "banner",
    "group": "Index",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "open_id",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "例子:",
          "content": "{\n    \"api_token\": \"\",\n    \"open_id\": \"ojCOA1eVWMWUaNSitcMFGl4fEvUk\",\n    \"data\": {}\n}",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "\t{\n\t    \"errcode\": 1000,\n\t    \"errmsg\": \"SUCCESS\",\n\t    \"data\": {\n\t         {\n\t            \"id\": \"1\",\n\t            \"name\": \"第二张\",\n             \"img\": \"http://huamaoadmin.weiyingjia.org/upload/20190620/1561017460204160.jpg\"\n\t        },\n\t    }\n\t}",
          "type": "json"
        }
      ],
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n    \"errcode\": 2001,\n    \"errmsg\": \"banner数据为空\",\n    \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/IndexController.php",
    "groupTitle": "Index"
  },
  {
    "type": "post",
    "url": "index/bottom",
    "title": "获取底部图片",
    "version": "1.0.0",
    "name": "bottom",
    "group": "Index",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "open_id",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "例子:",
          "content": "{\n    \"api_token\": \"\",\n    \"open_id\": \"ojCOA1eVWMWUaNSitcMFGl4fEvUk\",\n    \"data\": {}\n}",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "\t{\n\t    \"errcode\": 1000,\n\t    \"errmsg\": \"SUCCESS\",\n\t    \"data\": {\n\t         \"id\": \"1\",\n\t         \"name\": \"第二张\",\n          \"img\": \"http://huamaoadmin.weiyingjia.org/upload/20190620/1561017460204160.jpg\"\n\t    }\n\t}",
          "type": "json"
        }
      ],
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n    \"errcode\": 2001,\n    \"errmsg\": \"banner数据为空\",\n    \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/IndexController.php",
    "groupTitle": "Index"
  },
  {
    "type": "post",
    "url": "index/category",
    "title": "获取分类数据",
    "version": "1.0.0",
    "name": "category",
    "group": "Index",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "open_id",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "例子:",
          "content": "{\n    \"api_token\": \"\",\n    \"open_id\": \"ojCOA1eVWMWUaNSitcMFGl4fEvUk\",\n    \"data\": {}\n}",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "\t{\n\t    \"errcode\": 1000,\n\t    \"errmsg\": \"SUCCESS\",\n\t    \"data\": {\n\t         {\n\t            \"id\": \"1\",\n\t            \"name\": \"金安品质服务\",\n             \"son\": {\n                 {\n                     \"id\": \"4\",\n                     \"name\": \"家庭清洁服务\"\n                 }\n             }\n\t        }\n\t    }\n\t}",
          "type": "json"
        }
      ],
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n    \"errcode\": 2001,\n    \"errmsg\": \"分类数据为空\",\n    \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/IndexController.php",
    "groupTitle": "Index"
  },
  {
    "type": "post",
    "url": "index/collect",
    "title": "领取优惠券",
    "version": "1.0.0",
    "name": "collect",
    "group": "Index",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "coupon_id",
            "description": "<p>优惠券id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "例子:",
          "content": "{\n    \"api_token\": \"\",\n    \"open_id\": \"ojCOA1eVWMWUaNSitcMFGl4fEvUk\",\n    \"data\": {\n        \"coupon_id\": \"1\",\n     }\n}",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "{\n    \"errcode\": 1000,\n    \"errmsg\": \"SUCCESS\",\n    \"data\": {}\n}",
          "type": "json"
        }
      ],
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n    \"errcode\": 2001,\n    \"errmsg\": \"参数不能为空\",\n    \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/IndexController.php",
    "groupTitle": "Index"
  },
  {
    "type": "post",
    "url": "index/coupon",
    "title": "获取优惠券列表",
    "version": "1.0.0",
    "name": "coupon",
    "group": "Index",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "open_id",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "例子:",
          "content": "{\n    \"api_token\": \"\",\n    \"open_id\": \"ojCOA1eVWMWUaNSitcMFGl4fEvUk\",\n    \"data\": {}\n}",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "\t{\n\t    \"errcode\": 1000,\n\t    \"errmsg\": \"SUCCESS\",\n\t    \"data\": {\n\t         {\n\t            \"id\": \"4\",\n             \"name\": \"20元优惠券\",\n             \"money\": \"20\",\n             \"start_at\": \"2019-06-09 00:00:00\",\n             \"end_at\": \"2019-10-31 00:00:00\",\n             \"restriction\": \"0\"\n\t        },\n\t    }\n\t}",
          "type": "json"
        }
      ],
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n    \"errcode\": 2001,\n    \"errmsg\": \"参数不能为空\",\n    \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/IndexController.php",
    "groupTitle": "Index"
  },
  {
    "type": "post",
    "url": "index/search",
    "title": "搜索",
    "version": "1.0.0",
    "name": "search",
    "group": "Index",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "keyword",
            "description": "<p>服务项目关键词</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "例子:",
          "content": "{\n    \"api_token\": \"\",\n    \"open_id\": \"ojCOA1eVWMWUaNSitcMFGl4fEvUk\",\n    \"data\": {\n        \"keyword\": \"玻璃\",\n     }\n}",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "\t{\n\t    \"errcode\": 1000,\n\t    \"errmsg\": \"SUCCESS\",\n\t    \"data\": {\n         {\n         \"id\": \"2\",\n         \"name\": \"冬季玻璃清洁11月-次年三月\",\n         \"category_id\": \"4\",\n         \"price\": \"3\",\n         \"unit\": \"平米\",\n         \"thumbnail\": \"/upload/20190620/1561025532564486.jpg\"\n         },\n     }\n\t}",
          "type": "json"
        }
      ],
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n    \"errcode\": 2001,\n    \"errmsg\": \"参数不能为空\",\n    \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/IndexController.php",
    "groupTitle": "Index"
  },
  {
    "type": "post",
    "url": "order/appraisal",
    "title": "服务评价",
    "version": "1.0.0",
    "name": "appraisal",
    "group": "Order",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "order_id",
            "description": "<p>订单id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "star",
            "description": "<p>星级(1-5)</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "例子:",
          "content": "{\n    \"api_token\": \"\",\n    \"open_id\": \"ojCOA1eVWMWUaNSitcMFGl4fEvUk\",\n    \"data\": {\n        \"order_id\": \"1\",\n        \"star\": \"2\",\n    }\n}",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "{\n    \"errcode\": 1000,\n    \"errmsg\": \"SUCCESS\",\n    \"data\": {}\n}",
          "type": "json"
        }
      ],
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n    \"errcode\": 2001,\n    \"errmsg\": \"分类数据为空\",\n    \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/OrderController.php",
    "groupTitle": "Order"
  },
  {
    "type": "post",
    "url": "order/detail",
    "title": "订单详情",
    "version": "1.0.0",
    "name": "detail",
    "group": "Order",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "order_id",
            "description": "<p>订单id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "例子:",
          "content": "{\n    \"api_token\": \"\",\n    \"open_id\": \"ojCOA1eVWMWUaNSitcMFGl4fEvUk\",\n    \"data\": {\n        \"order_id\": \"1\"\n    }\n}",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "\t{\n\t    \"errcode\": 1000,\n\t    \"errmsg\": \"SUCCESS\",\n\t    \"data\": {\n\t         \"order_data\": \"订单数据\",\n          \"goods_data\": \"商品数据\"\n\t    }\n\t}",
          "type": "json"
        }
      ],
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n    \"errcode\": 2001,\n    \"errmsg\": \"参数不能为空\",\n    \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/OrderController.php",
    "groupTitle": "Order"
  },
  {
    "type": "post",
    "url": "order/finish",
    "title": "服务完成",
    "version": "1.0.0",
    "name": "finish",
    "group": "Order",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "order_id",
            "description": "<p>订单id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "例子:",
          "content": "{\n    \"api_token\": \"\",\n    \"open_id\": \"ojCOA1eVWMWUaNSitcMFGl4fEvUk\",\n    \"data\": {\n        \"order_id\": \"1\"\n    }\n}",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "{\n    \"errcode\": 1000,\n    \"errmsg\": \"SUCCESS\",\n    \"data\": {}\n}",
          "type": "json"
        }
      ],
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n    \"errcode\": 2001,\n    \"errmsg\": \"分类数据为空\",\n    \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/OrderController.php",
    "groupTitle": "Order"
  },
  {
    "type": "post",
    "url": "order/list",
    "title": "订单列表",
    "version": "1.0.0",
    "name": "list",
    "group": "Order",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>(0:全部订单，1:待服务，4:待付款，5:已完成，6:已评价)</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "例子:",
          "content": "{\n    \"api_token\": \"\",\n    \"open_id\": \"ojCOA1eVWMWUaNSitcMFGl4fEvUk\",\n    \"data\": {\n        \"type\": \"0\"\n    }\n}",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "\t{\n\t    \"errcode\": 1000,\n\t    \"errmsg\": \"SUCCESS\",\n\t    \"data\": {\n\t         {\n             \"id\": \"1\",\n             \"order_code\": \"201906134656561\",\n             \"order_total\": \"160.50\",\n             \"book_date\": \"2019-06-14 14:48:50\",\n             \"order_status\": \"0\",\n             \"goods_name\": \"擦玻璃\"\n         }\n\t    }\n\t}",
          "type": "json"
        }
      ],
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n    \"errcode\": 2001,\n    \"errmsg\": \"banner数据为空\",\n    \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/OrderController.php",
    "groupTitle": "Order"
  },
  {
    "type": "post",
    "url": "order/pay",
    "title": "订单支付",
    "version": "1.0.0",
    "name": "pay",
    "group": "Order",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "order_id",
            "description": "<p>订单id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "receive_id",
            "description": "<p>领取的券id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "例子:",
          "content": "{\n    \"api_token\": \"\",\n    \"open_id\": \"ojCOA1eVWMWUaNSitcMFGl4fEvUk\",\n    \"data\": {\n        \"order_id\": \"1\",\n        \"receive_id\": \"1\",\n    }\n}",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "\t{\n\t    \"errcode\": 1000,\n\t    \"errmsg\": \"SUCCESS\",\n\t    \"data\": {\n         \"appId\": \"\",\n         \"timeStamp\": \"\",\n         \"nonceStr\": \"\",\n         \"package\": \"\",\n         \"signType\": \"\",\n         \"paySign\": \"\",\n\t    }\n\t}",
          "type": "json"
        }
      ],
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n    \"errcode\": 2001,\n    \"errmsg\": \"参数不能为空\",\n    \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/OrderController.php",
    "groupTitle": "Order"
  },
  {
    "type": "post",
    "url": "token/getcode",
    "title": "获取验证码",
    "version": "1.0.0",
    "name": "getcode",
    "group": "Token",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mobile",
            "description": "<p>手机号</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "例子:",
          "content": "{\n    \"api_token\": \"\",\n    \"open_id\": \"\",\n    \"data\": {\n        \"mobile\": \"18101362445\"\n    }\n}",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "{\n    \"errcode\": 1000,\n    \"errmsg\": \"SUCCESS\",\n    \"data\": {\n         {\n            \"code\": \"245671\",\n        }\n    }\n}",
          "type": "json"
        }
      ],
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n    \"errcode\": 2001,\n    \"errmsg\": \"参数不能为空\",\n    \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/TokenController.php",
    "groupTitle": "Token"
  },
  {
    "type": "post",
    "url": "token/getmap",
    "title": "获取地理位置",
    "version": "1.0.0",
    "name": "getmap",
    "group": "Token",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "location",
            "description": "<p>经纬度坐标</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "例子:",
          "content": "{\n    \"api_token\": \"\",\n    \"open_id\": \"\",\n    \"data\": {\n        \"location\": \"116.507862,39.903683\"\n    }\n}",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "\t{\n\t    \"errcode\": 1000,\n\t    \"errmsg\": \"SUCCESS\",\n\t    \"data\": {\n\t        \"province\": \"北京市\",\n         \"city\": [],\n         \"address\": \"北京市朝阳区高碑店镇国粹苑艺术品交易中心\"\n\t    }\n\t}",
          "type": "json"
        }
      ],
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n    \"errcode\": 2001,\n    \"errmsg\": \"参数不能为空\",\n    \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/TokenController.php",
    "groupTitle": "Token"
  },
  {
    "type": "post",
    "url": "token/gettoken",
    "title": "获取token",
    "version": "1.0.0",
    "name": "gettoken",
    "group": "Token",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>微信小程序获取的code</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "例子:",
          "content": "{\n    \"api_token\": \"\",\n    \"data\": {\n        \"code\": \"safasfsafaf\",\n    }\n}",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "{\n    \"errcode\": 1000,\n    \"errmsg\": \"SUCCESS\",\n    \"data\": {\n         {\n            \"api_token\": \"1\",\n            \"open_id\": \"1\",\n            \"is_register\": \"1\",\n        }\n    }\n}",
          "type": "json"
        }
      ],
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据 is_register 是否注册 1，已注册 0，未注册</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n    \"errcode\": 2001,\n    \"errmsg\": \"参数不能为空\",\n    \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/TokenController.php",
    "groupTitle": "Token"
  },
  {
    "type": "post",
    "url": "token/register",
    "title": "用户注册",
    "version": "1.0.0",
    "name": "register",
    "group": "Token",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "open_id",
            "description": "<p>微信小程序的 open_id</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mobile",
            "description": "<p>手机号</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>验证码</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>姓名</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>地址</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "avatar",
            "description": "<p>用户头像</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "from_user_id",
            "description": "<p>如果是通过分享的链接注册的需要携带来源用户id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "例子:",
          "content": "{\n    \"api_token\": \"\",\n    \"open_id\": \"\",\n    \"data\": {\n        \"mobile\": \"18101362445\",\n        \"code\": \"254741\",\n        \"name\": \"张三\",\n        \"address\": \"热敏路\",\n        \"avatar\": \"\",\n        \"from_user_id\": \"\",\n    }\n}",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "{\n    \"errcode\": 1000,\n    \"errmsg\": \"SUCCESS\",\n    \"data\": {}\n}",
          "type": "json"
        }
      ],
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n    \"errcode\": 2001,\n    \"errmsg\": \"参数不能为空\",\n    \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/TokenController.php",
    "groupTitle": "Token"
  },
  {
    "type": "post",
    "url": "user/avatar",
    "title": "用户修改头像",
    "version": "1.0.0",
    "name": "avatar",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "file",
            "description": "<p>文件名</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "例子:",
          "content": "{\n    \"api_token\": \"\",\n    \"open_id\": \"ojCOA1eVWMWUaNSitcMFGl4fEvUk\",\n    \"data\": {\n        \"file\": \"\",\n    }\n}",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "\t{\n\t    \"errcode\": 1000,\n\t    \"errmsg\": \"SUCCESS\",\n\t    \"data\": {\n         \"url\": \"http://huamaoadmin.weiyingjia.org/upload/20190620/1561017460204160.jpg\",\n     }\n\t}",
          "type": "json"
        }
      ],
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n    \"errcode\": 2001,\n    \"errmsg\": \"参数不能为空\",\n    \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/UserController.php",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "user/bill",
    "title": "我的账单",
    "version": "1.0.0",
    "name": "bill",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "open_id",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "例子:",
          "content": "{\n    \"api_token\": \"\",\n    \"open_id\": \"ojCOA1eVWMWUaNSitcMFGl4fEvUk\",\n    \"data\": {}\n}",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "\t{\n\t    \"errcode\": 1000,\n\t    \"errmsg\": \"SUCCESS\",\n\t    \"data\": {\n\t         {\n             \"id\": \"1\",\n             \"order_code\": \"201906134656561\",\n             \"order_total\": \"160.50\",\n             \"created_at\": \"2019-06-15 14:48:58\",\n             \"order_status\": \"0\",\n             \"goods_name\": \"擦玻璃\"\n         }\n\t    }\n\t}",
          "type": "json"
        }
      ],
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n    \"errcode\": 2001,\n    \"errmsg\": \"banner数据为空\",\n    \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/UserController.php",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "user/coupon",
    "title": "我的优惠券",
    "version": "1.0.0",
    "name": "coupon",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "type",
            "description": "<p>1：未使用，2：已使用，3：已失效</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "例子:",
          "content": "{\n    \"api_token\": \"\",\n    \"open_id\": \"ojCOA1eVWMWUaNSitcMFGl4fEvUk\",\n    \"data\": {\n        \"type\": \"1\"\n    }\n}",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "\t{\n\t    \"errcode\": 1000,\n\t    \"errmsg\": \"SUCCESS\",\n\t    \"data\": {\n\t         {\n             \"name\": \"20元优惠券\",\n             \"money\": \"20\",\n             \"start_at\": \"2019-06-09 00:00:00\",\n             \"end_at\": \"2019-10-31 00:00:00\",\n             \"restriction\": \"0\",\n             \"receive_id\": \"1\",\n             \"coupon_id\": \"4\"\n         }\n\t    }\n\t}",
          "type": "json"
        }
      ],
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n    \"errcode\": 2001,\n    \"errmsg\": \"banner数据为空\",\n    \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/UserController.php",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "user/editinfo",
    "title": "用户修改信息",
    "version": "1.0.0",
    "name": "editinfo",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "mobile",
            "description": "<p>手机号</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "code",
            "description": "<p>验证码</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "name",
            "description": "<p>姓名</p>"
          },
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "address",
            "description": "<p>地址</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "例子:",
          "content": "{\n    \"api_token\": \"\",\n    \"open_id\": \"ojCOA1eVWMWUaNSitcMFGl4fEvUk\",\n    \"data\": {\n        \"mobile\": \"18101362445\",\n        \"code\": \"254741\",\n        \"name\": \"张三\",\n        \"address\": \"热敏路\",\n    }\n}",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "{\n    \"errcode\": 1000,\n    \"errmsg\": \"SUCCESS\",\n    \"data\": {}\n}",
          "type": "json"
        }
      ],
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n    \"errcode\": 2001,\n    \"errmsg\": \"参数不能为空\",\n    \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/UserController.php",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "user/getqrcode",
    "title": "生成二维码",
    "version": "1.0.0",
    "name": "getqrcode",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "page",
            "description": "<p>下程序页面路径</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "例子:",
          "content": "{\n    \"api_token\": \"\",\n    \"open_id\": \"ojCOA1eVWMWUaNSitcMFGl4fEvUk\",\n    \"data\": {\n        \"page\": \"pages/share\"\n    }\n}",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "\t{\n\t    \"errcode\": 1000,\n\t    \"errmsg\": \"SUCCESS\",\n\t    \"data\": {\n         \"url\": \"http://huamaoadmin.weiyingjia.org/upload/20190620/1561017460204160.jpg\",\n     }\n\t}",
          "type": "json"
        }
      ],
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n    \"errcode\": 2001,\n    \"errmsg\": \"参数不能为空\",\n    \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/UserController.php",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "user/income",
    "title": "我的收益",
    "version": "1.0.0",
    "name": "income",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "open_id",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "例子:",
          "content": "{\n    \"api_token\": \"\",\n    \"open_id\": \"ojCOA1eVWMWUaNSitcMFGl4fEvUk\",\n    \"data\": {}\n}",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "\t{\n\t    \"errcode\": 1000,\n\t    \"errmsg\": \"SUCCESS\",\n\t    \"data\": {\n\t         {\n             \"coupon_id\": \"4\",\n             \"mobile\": \"18233308510\",\n             \"name\": \"成亮\",\n             \"income\": \"20元优惠券\",\n             \"date\": \"2019-06-20 15:43:25\"\n         }\n\t    }\n\t}",
          "type": "json"
        }
      ],
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n    \"errcode\": 2001,\n    \"errmsg\": \"banner数据为空\",\n    \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/UserController.php",
    "groupTitle": "User"
  },
  {
    "type": "post",
    "url": "user/info",
    "title": "我的信息",
    "version": "1.0.0",
    "name": "info",
    "group": "User",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "String",
            "optional": false,
            "field": "open_id",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "例子:",
          "content": "{\n    \"api_token\": \"\",\n    \"open_id\": \"ojCOA1eVWMWUaNSitcMFGl4fEvUk\",\n    \"data\": {}\n}",
          "type": "json"
        },
        {
          "title": "Success-Response:",
          "content": "\t{\n\t    \"errcode\": 1000,\n\t    \"errmsg\": \"SUCCESS\",\n\t    \"data\": {\n\t         {\n             \"id\": \"1\",\n             \"name\": \"\",\n             \"mobile\": \"\",\n             \"address\": \"\",\n             \"avatar\": \"\",\n         }\n\t    }\n\t}",
          "type": "json"
        }
      ],
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Success 200",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      }
    },
    "error": {
      "fields": {
        "Error 4xx": [
          {
            "group": "Error 4xx",
            "type": "Array",
            "optional": false,
            "field": "data",
            "description": "<p>返回数据</p>"
          },
          {
            "group": "Error 4xx",
            "type": "Int",
            "optional": false,
            "field": "errcode",
            "description": "<p>状态码</p>"
          },
          {
            "group": "Error 4xx",
            "type": "String",
            "optional": false,
            "field": "errmsg",
            "description": "<p>描述信息</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Error-Response:",
          "content": "{\n    \"errcode\": 2001,\n    \"errmsg\": \"banner数据为空\",\n    \"data\": {}\n}",
          "type": "json"
        }
      ]
    },
    "filename": "controllers/UserController.php",
    "groupTitle": "User"
  }
] });
