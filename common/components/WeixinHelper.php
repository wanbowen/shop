<?php
namespace common\components;
use Yii;
/*
 * api
 * 微信相关接口
 */
class WeixinHelper{
    static $appid;
    static $appsecret;
    public function __construct($appid,$appsecret)
    {
        self::$appid = $appid;
        self::$appsecret = $appsecret;
    }
    /*
     * 微信小程序登陆
     */
    public static function jscode2session($code)
    {
        $appid      = self::$appid;
        $appsecret  = self::$appsecret;

        if(!empty($code)) {
            $url = "https://api.weixin.qq.com/sns/jscode2session?appid=$appid&secret=$appsecret&js_code=$code&grant_type=authorization_code";
            $res = self::httpGet($url);
            if (!$res){
                return false;
            } else {
                return $res;
            }
        } else {
            return false;
        }
    }
    /*
     * 获取access_token
     */
    public static function getToken()
    {
        $appid      = self::$appid;
        $appsecret  = self::$appsecret;
        $cacheToken = Yii::$app->cache->get('accesstoken');
        if(empty($cacheToken))
        {
            $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=$appid&secret=$appsecret";
            $res = self::httpGet($url);
            if (!$res)
                return false;
            else
               $access_token = $res['access_token'];
            if ($access_token)
            {
                Yii::$app->cache->set('accesstoken',$access_token,7200);
            }
            else
                return false;
        }
        else
        {
           $access_token = $cacheToken;
        }
        return $access_token;
    }
    /*
     * 二次获取access_token
     */
    public static function getTokenTwo()
    {
        $appid      = self::$appid;
        $appsecret  = self::$appsecret;
        $url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=$appid&secret=$appsecret";
        $res = self::httpGet($url);
        if (!$res)
            return false;
        else
            $access_token = $res['access_token'];
            if ($access_token)
            {
                Yii::$app->cache->set($appid.'token',$access_token,7200);
            }
            else
                return false;
        return $access_token;
    }
    /*
     * 调用jssdk
     * 参数配置
     */
    public static function getSignPackage()
    {
        $jsapiTicket = self::getJsApiTicket();
        $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
        $url = "$protocol$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
        $timestamp = time();
        $nonceStr  = self::createNonceStr();
        // 这里参数的顺序要按照 key 值 ASCII 码升序排序
        $string = "jsapi_ticket=$jsapiTicket&noncestr=$nonceStr&timestamp=$timestamp&url=$url";
        $signature = sha1($string);
        $signPackage = array(
            "appId"     => self::$appid,
            "nonceStr"  => $nonceStr,
            "timestamp" => $timestamp,
            "url"       => $url,
            "signature" => $signature,
            "rawString" => $string
        );
        return $signPackage;
    }
    /*
     * 生成菜单
     */
    public function creteMenu($data)
    {
        if(empty($data))
        {
            return false;
        }
        else 
        {
            $token  = self::getToken();
            $url    = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=$token";
            //var_dump($data);
            $result = self::httpPost($url, $data);
            if(!empty($result['errcode']) && ($result['errcode'] == "42001" || $result['errcode'] == "40001"))
            {
                $token  = self::getTokenTwo();
                $result = self::httpPost($url, $data);
            }
            return $result;
        }
    }
    
    /*
     * 获取用户信息
     *
     */
    public static function getUserInfo($wechat_id)
    {
        $token  = self::getToken();
        $url    = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=$token&openid=$wechat_id&lang=zh_CN";
        $res    = self::httpGet($url);
        if(isset($res['errcode']) && ($res['errcode'] == "42001" || $res['errcode'] == "40001"))
        {
            $token  = self::getTokenTwo();
            $res    = self::httpGet($url);
        }
        return $res;
    }
    /*
     * 判断是否关注
     */
    public static function isSubscribe($wechat_id)
    {
        if (empty($wechat_id))
            return false;
        $access_token = self::getToken();
        $url = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=$access_token&openid=$wechat_id&lang=zh_CN";
        $res = self::httpGet($url);
        if(isset($res['errcode']) && ($res['errcode'] == "42001" || $res['errcode'] == "40001"))
        {
            $token  = self::getTokenTwo();
            $res    = self::httpGet($url);
        }
        if (!$res)
            return false;
        else
        {
            if(isset($res['subscribe']) && $res['subscribe'] == '1')
                return true;
            else 
                return false;
        }
    }
    /*
     * 获得随机字符
     */
    private static function createNonceStr($length = 16) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        $str = "";
        for ($i = 0; $i < $length; $i++) {
            $str .= substr($chars, mt_rand(0, strlen($chars) - 1), 1);
        }
        return $str;
    }
    /*
     * 取票据
     */
    private function getJsApiTicket() 
    {
        $appid       = self::$appid;
        $cacheTicket = Yii::$app->cache->get($appid.'ticket');
        if(empty($cacheTicket))
        {
            $accessToken = self::getToken();
            $url = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?type=jsapi&access_token=$accessToken";
            $res = self::httpGet($url);
            if(isset($res['errcode']) && ($res['errcode'] == "42001" || $res['errcode'] == "40001"))
            {
                $accessToken  = self::getTokenTwo();
                $res    = self::httpGet($url);
            }
            if (!$res)
                return false;
            else
               $ticket = $res['ticket'];
            if ($ticket) 
            {
                 Yii::$app->cache->set($appid.'ticket',$ticket,7200);
            }
        } 
        else
        {
           $ticket = $cacheTicket ;
        }
        return $ticket;
    }
    /*
     * 发送模板消息
     *
     */
    public static function sentMsg($template)
    {
        $access_token  = self::getToken();
        $url    = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=$access_token";
        $res    = self::httpPost($url,$template);
        if(isset($res['errcode']) && ($res['errcode'] == "42001" || $res['errcode'] == "40001" || $res['errcode'] == "40012"))
        {
            $access_token  = self::getTokenTwo();
            $res    = self::httpPost($url,$template);
        }
        return $res;
    }
    /*
     * 生成二维码
     */
    public static function GetQrcode($sn)
    {
        $access_token  = self::getToken();
        $url    = "https://api.weixin.qq.com/cgi-bin/qrcode/create?access_token=$access_token";
        $data = '{"action_name": "QR_LIMIT_STR_SCENE", "action_info": {"scene": {"scene_str": "'.$sn.'"}}}';
        $res = self::httpPost($url, $data);
        if(isset($res['errcode']) && ($res['errcode'] == "42001" || $res['errcode'] == "40001" || $res['errcode'] == "40012"))
        {
            $access_token  = self::getTokenTwo();
            $res    = self::httpPost($url,$data);
        }
        return $res;
    }
    public static function add_media($urls,$type,$videovalue=null)
    {
        $access_token  = self::getToken();
        $url    = "https://api.weixin.qq.com/cgi-bin/material/add_material?access_token=$access_token&type=$type";
        if (class_exists ( '\CURLFile' )) {//关键是判断curlfile,官网推荐php5.5或更高的版本使用curlfile来实例文件
            $filedata = array (
                'media' => new \CURLFile (realpath($urls), 'image/jpeg' )
            );
        } else {
            $filedata = array (
                'media' => '@' . realpath($urls)
            );
        }
        if($type == 'video' && $videovalue != '')
        {
            $filedata['description']= '{"title":"'.$videovalue['title'].'","introduction":"'.$videovalue['introduction'].'"}';
        }
        //var_dump($filedata);die;
        $res = self::upload($url, $filedata);
        if(isset($res['errcode']) && ($res['errcode'] == "42001" || $res['errcode'] == "40001" || $res['errcode'] == "40012"))
        {
            $access_token  = self::getTokenTwo();
            $res    = self::upload($url,$filedata);
        }
        return $res;
    }
    public static function upload($url, $filedata) {
        $curl = curl_init ();
        if (class_exists ( '/CURLFile' )) {//php5.5跟php5.6中的CURLOPT_SAFE_UPLOAD的默认值不同
            curl_setopt ( $curl, CURLOPT_SAFE_UPLOAD, true );
        } else {
            if (defined ( 'CURLOPT_SAFE_UPLOAD' )) {
                curl_setopt ( $curl, CURLOPT_SAFE_UPLOAD, false );
            }
        }
        curl_setopt ( $curl, CURLOPT_URL, $url );
        curl_setopt ( $curl, CURLOPT_SSL_VERIFYPEER, FALSE );
        curl_setopt ( $curl, CURLOPT_SSL_VERIFYHOST, FALSE );
        if (! empty ( $filedata )) {
            curl_setopt ( $curl, CURLOPT_POST, 1 );
            curl_setopt ( $curl, CURLOPT_POSTFIELDS, $filedata );
        }
        curl_setopt ( $curl, CURLOPT_RETURNTRANSFER, 1 );
        $output = curl_exec ( $curl );
        curl_close ( $curl );
        if(!$output){
            return false;
        }
        return json_decode($output, true);
    
    }
    /*
     * curl
     *
     * */
    public static function httpPost($url, $data){
        try
        {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
            $response = curl_exec($ch);
            curl_close($ch);
            if(!$response){
                return false;
            }
            return json_decode($response, true);
        }
        catch (\Exception $e)
        {
            Yii::warning($e->getMessage());
            return false;
        }
    }
    public static function httpGet($url)
    {
        try{
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_TIMEOUT, 500);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_URL, $url);
            $res = curl_exec($curl);
            curl_close($curl);
            if(!$res)
                return false;
            else
                return json_decode($res,true);
        }
        catch(\Exception $e)
        {
            Yii::warning($e->getMessage());
            return false;
        }
         
    }
}