<?php
namespace common\components\pay;
use common\models\PayConfig;
require_once('WxPay.Api.php');
use Yii;
use common\models\RefundLog;


class RefundHelper { 
    public static function refund($order_id, $transaction_id,$price,$refund_price,$openid)
    {
        $payInfo = PayConfig::find()->where(['token'=>'67lodqckx21487835545'])->asArray()->one();
            if(empty($payInfo) || empty($payInfo['appid']) || empty($payInfo['appsecret']) || empty($payInfo['mchid'])  || empty($payInfo['key']))
                {
                    return 'false';
                    exit;
                }
        if(!empty($transaction_id) && !empty($price)){
            $total_fee      = $price*100;
            $refund_fee     = $refund_price*100;
            $input = new \WxPayRefund();
            $input->SetAppid($payInfo['appid']);
            //$input->SetMch_id($payInfo['mchid'])z;
            //$input->SetSign($payInfo['key']);
            $input->SetTransaction_id($transaction_id);
            $input->SetTotal_fee($total_fee);
            $input->SetRefund_fee($refund_fee);
            $input->SetOut_refund_no(\WxPayConfig::MCHID.date("YmdHis"));
            $input->SetOp_user_id($openid);
            $returnData = \WxPayApi::refund($input,$payInfo['key']);
            if (!empty($returnData))
            {
                if ($returnData['return_msg'] == 'OK'&& 
                    isset($returnData['result_code']) &&
                    $returnData['return_code']== 'SUCCESS')
                {                   
                    return 'success';
                }
                else 
                {
                    return 'false';
                }
            }        
        }
        return 'false';
    }
    
}
