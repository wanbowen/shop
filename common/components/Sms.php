<?php
namespace common\components;
use Yii;

/**
 * 发短信(聚通达短信通道)
 * @author  lvhengbin
 * @date    2019/06/13
 */
class Sms {

    private $url; // $url string 发送短信的接口 例如 http://ip:8090/jtdsms/smsSend.do
    private $uid; // $uid string 用户的id
    private $pass; // $pass string 用户的密码

    public function __construct($url,$uid,$pass){
        // $url string 发送短信的接口 例如 http://ip:8090/jtdsms/smsSend.do
        $this->url = $url;
        $this->uid = $uid;
        $this->pass = $pass;
    }

    /*
     * 发送短信
     * $mobile 要发送的手机号
     * $content 要发送的内容
     */
    public function SendSms($mobile,$content){
        //处理密码先md5加密再转大写
        $pass = strtoupper(md5($this->pass));
        //组装数组
        $data = array(
            'uid' => $this->uid,
            'password' =>$pass ,
            'mobile' => $mobile,//多条例子 xxxx,xxxx
            'encode' => 'utf8',
            'content' => base64_encode($content),//把内容进行base64转换
            'encodeType' => 'base64',
            'cid' => '',// 唯一标识，选填，如果不填系统自动生成作为当前批次的唯一标识
            'extNumber' => '',// 扩展 选填
            'schtime' => ''// 定时时间，选填，格式2008-06-09 12:00:00
        );
        $request = $this->httpPost($data);
        return $request;
    }

    /*
     * php post提交数据
     */
    private  function httpPost($data){
        // 启动一个CURL会话
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL,$this->url);//接口地址
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; MSIE 5.01; Windows NT 5.0)');
        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curl, CURLOPT_AUTOREFERER, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded'
        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        // 执行操作
        $response_body = curl_exec($curl);
        //捕抓异常
        $error_msg = '';
        if($response_body > 0){
            $error_msg = '发送成功';
        }else{
            $error_msg = $this->errorMsg($response_body);
        }
        // 关闭CURL会话
        curl_close($curl);

        $response["error_code"] = $response_body;//请求接口返回的数据 大于0代表成功，否则根据返回值查找错误
        $response["error_msg"] = $error_msg;//curl post 提交发生的错误
        return $response;
    }

    /*
     * php post提交数据
     */
    private  function errorMsg($code){
        $msg = '';
        switch ((int)$code) {
            case -2:
                $msg = '参数不能为空（必填项有空值现象）';
                break;
            case -3:
                $msg = '账户信息验证不通过';
                break;
            case -4:
                $msg = '余额不足';
                break;
            case -5:
                $msg = '参数格式不正确';
                break;
            case -6:
                $msg = '内容太长';
                break;
            case -8:
                $msg = '扩展长度不正确（咨询运营是否开启扩展权限以及扩展码）';
                break;
            case -9:
                $msg = 'Ip地址未授权（联系运营绑定ip地址）';
                break;
            case -990:
                $msg = '未知异常（联系技术人员）';
                break;
        }
        return $msg;
    }
}