<?php
namespace common\components;
use Yii;

/**
 * 常用工具和相关数据处理
 * @author  lvhengbin
 * @date    2019/05/24
 */
class ToolHelper {
    /**
     * 获取接口请求数据
     * @author  lvhengbin
     * @access public
     * @return array 数据
     * @date    2019/05/24
     */
    public function getRequest() {
        $result = file_get_contents("php://input");
        $result = json_decode($result,true);
        if(!empty($result) && isset($result['api_token'])){
            return $result;
        }else{
            if(!empty($_POST) || !empty($_FILES)){
                return $_POST;
            }
            $this->retJson(2000, '请求失败,请重试');
        }
    }

    /**
     * 接口返回
     * @author  lvhengbin
     * @access public
     * @param string $code 返回错误码
     * @param string $msg 返回错误描述
     * @param array $data 返回数据
     * @return object json数据
     * @date    2019/05/24
     */
    public function retJson($code, $msg = '', $data = array())
    {
        $dataRet = [];
        $dataRet['errcode'] = $code;
        $dataRet['errmsg'] = $msg;
        $dataRet['data'] = $data;
        $dataJson = json_encode($dataRet, JSON_UNESCAPED_SLASHES);
        echo  $dataJson;
        exit();
    }

    //curl
    public function httpGet($url)
    {
        try{
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_TIMEOUT, 500);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($curl, CURLOPT_URL, $url);
            $res = curl_exec($curl);
            curl_close($curl);
            if(!$res)
                return false;
            else
                return json_decode($res,true);
        }
        catch(\Exception $e)
        {
            Yii::warning($e->getMessage());
            return false;
        }

    }
}