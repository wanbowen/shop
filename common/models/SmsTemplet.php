<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "hm_sms_templet".
 *
 * @property int $id 序号
 * @property int $type 模板类型(1:验证码模板,2:员工端模板,3:客户端模板)
 * @property string $content 模板内容
 * @property string $created_at 创建时间
 * @property string $updated_at 更新时间
 */
class SmsTemplet extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hm_sms_templet';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type'], 'integer'],
            [['content'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'content' => 'Content',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
