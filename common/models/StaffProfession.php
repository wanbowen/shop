<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "hm_staff_profession".
 *
 * @property int $id 序号
 * @property string $name 职业名称
 * @property int $is_del 0.正常 1.已删除
 * @property string $created_at 创建时间
 * @property string $updated_at 更新时间
 */
class StaffProfession extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hm_staff_profession';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['is_del'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'is_del' => 'Is Del',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
