<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "hm_s_address".
 *
 * @property int $id
 * @property int $user_id 用户id
 * @property string $name 收件人
 * @property string $mobile 用户手机号
 * @property string $address 用户地址
 * @property string $created_at 创建时间
 * @property int $is_del 0.正常 1.已删除
 * @property int $is_defu 0.未默认 1.默认收货地址
 */
class SAddress extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hm_s_address';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'is_del', 'is_defu'], 'integer'],
            [['created_at'], 'safe'],
            [['name'], 'string', 'max' => 50],
            [['mobile'], 'string', 'max' => 11],
            [['address'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => '用户id',
            'name' => '收件人',
            'mobile' => '用户手机号',
            'address' => '用户地址',
            'created_at' => '创建时间',
            'is_del' => '0.正常 1.已删除',
            'is_defu' => '0.未默认 1.默认收货地址',
        ];
    }
}
