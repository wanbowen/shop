<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "hm_code".
 *
 * @property string $id 验证码id
 * @property string $tel 手机号
 * @property string $code 验证码
 * @property int $addtime 添加时间
 */
class Code extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hm_code';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['addtime'], 'required'],
            [['addtime'], 'integer'],
            [['tel'], 'string', 'max' => 11],
            [['code'], 'string', 'max' => 7],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tel' => 'Tel',
            'code' => 'Code',
            'addtime' => 'Addtime',
        ];
    }
}
