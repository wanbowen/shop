<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "hm_s_goods_category".
 *
 * @property int $id 序号
 * @property string $name 分类名称
 * @property string $icon_img 分类图标
 * @property int $is_del 0.正常 1.已删除
 * @property string $created_at 创建时间
 * @property string $deleted_at 删除时间
 * @property string $updated_at 更新时间
 */
class SGoodsCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hm_s_goods_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['is_del'], 'integer'],
            [['created_at', 'deleted_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 50],
            [['icon_img'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '序号',
            'name' => '分类名称',
            'icon_img' => '分类图标',
            'is_del' => '0.正常 1.已删除',
            'created_at' => '创建时间',
            'deleted_at' => '删除时间',
            'updated_at' => '更新时间',
        ];
    }
}
