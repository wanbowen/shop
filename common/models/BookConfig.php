<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "hm_book_config".
 *
 * @property int $id 序号
 * @property string $start_at 预约开始时间
 * @property string $end_at 预约截至时间
 * @property int $days 预约天数限制(n天内可预约,包含当天)
 * @property int $switch 预约服务开关 0.开启 1.关闭
 * @property string $updated_at 更新时间
 */
class BookConfig extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hm_book_config';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['days', 'switch'], 'integer'],
            [['updated_at'], 'safe'],
            [['start_at', 'end_at'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'start_at' => 'Start At',
            'end_at' => 'End At',
            'days' => 'Days',
            'switch' => 'Switch',
            'updated_at' => 'Updated At',
        ];
    }
}
