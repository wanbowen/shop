<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "hm_work_hours".
 *
 * @property int $id
 * @property string $start 开始时间
 * @property string $end 结束时间
 * @property string $updated_at 更新时间
 */
class WorkHours extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hm_work_hours';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['updated_at'], 'safe'],
            [['start', 'end'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'start' => 'Start',
            'end' => 'End',
            'updated_at' => 'Updated At',
        ];
    }
}
