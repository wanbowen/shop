<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "hm_user_share".
 *
 * @property int $id 序号
 * @property int $user_id 注册用户id
 * @property int $origin_user_id 来源用户id
 * @property int $is_send 是否已发放优惠券 0.未发放 1.已发放
 * @property string $created_at 创建时间
 * @property string $updated_at 更新时间
 */
class UserShare extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hm_user_share';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'origin_user_id', 'is_send'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'origin_user_id' => 'Origin User ID',
            'is_send' => 'Is Send',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
