<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "hm_repair".
 *
 * @property int $id 序号
 * @property string $repair_code 报修单号
 * @property string $name 项目名称
 * @property string $contact 联系人
 * @property string $contact_tel 联系人电话
 * @property string $position 具体位置
 * @property string $content 问题描述
 * @property int $staff_id 接单人id
 * @property int $status 0.待派单 1.已派单 2.已完成
 * @property int $is_del 0.正常 1.已删除
 * @property string $created_at 创建时间
 * @property string $reply_at 派单时间
 * @property string $finish_at 完成时间
 * @property string $deleted_at 删除时间
 */
class Repair extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hm_repair';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content'], 'required'],
            [['content'], 'string'],
            [['staff_id', 'status', 'is_del'], 'integer'],
            [['created_at', 'reply_at', 'finish_at', 'deleted_at'], 'safe'],
            [['repair_code', 'contact', 'contact_tel'], 'string', 'max' => 32],
            [['name'], 'string', 'max' => 128],
            [['position'], 'string', 'max' => 200],
            [['repair_code'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'repair_code' => 'Repair Code',
            'name' => 'Name',
            'contact' => 'Contact',
            'contact_tel' => 'Contact Tel',
            'position' => 'Position',
            'content' => 'Content',
            'staff_id' => 'Staff ID',
            'status' => 'Status',
            'is_del' => 'Is Del',
            'created_at' => 'Created At',
            'reply_at' => 'Reply At',
            'finish_at' => 'Finish At',
            'deleted_at' => 'Deleted At',
        ];
    }
}
