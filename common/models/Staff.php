<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "hm_staff".
 *
 * @property int $id 序号
 * @property int $admin_id 管理员id
 * @property string $name 姓名
 * @property int $age 年龄
 * @property int $sex 性别(0.男 1.女)
 * @property int $mobile 手机号
 * @property int $profession_id 职业id
 * @property string $address 地址
 * @property int $is_del 0.正常 1.已删除
 * @property string $created_at 创建时间
 * @property string $updated_at 更新时间
 */
class Staff extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hm_staff';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['admin_id', 'age', 'sex', 'mobile', 'profession_id', 'is_del'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 50],
            [['address'], 'string', 'max' => 200],
            [['admin_id', 'profession_id'], 'unique', 'targetAttribute' => ['admin_id', 'profession_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'admin_id' => 'Admin ID',
            'name' => 'Name',
            'age' => 'Age',
            'sex' => 'Sex',
            'mobile' => 'Mobile',
            'profession_id' => 'Profession ID',
            'address' => 'Address',
            'is_del' => 'Is Del',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
