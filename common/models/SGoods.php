<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "hm_s_goods".
 *
 * @property int $id 序号
 * @property string $name 商品名称
 * @property int $category_id 分类id
 * @property string $content 内容
 * @property int $stock 库存
 * @property int $price 商品价格(分)
 * @property string $thumbnail 缩略图
 * @property int $goods_imgs_id 商品图片表id
 * @property int $status 0.上架中 1.已下架
 * @property int $is_recommend 0.未推荐 1.推荐
 * @property int $is_del 0.正常 1.已删除
 * @property string $created_at 创建时间
 * @property string $deleted_at 删除时间
 * @property string $updated_at 更新时间
 * @property int $is_specs 0.无规格 1.有规格
 */
class SGoods extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hm_s_goods';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'stock', 'price',  'status', 'is_recommend', 'is_del', 'is_specs'], 'integer'],
            [['content'], 'string'],
            [['created_at', 'deleted_at', 'updated_at'], 'safe'],
            [['name', 'thumbnail'], 'string', 'max' => 100],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '序号',
            'name' => '商品名称',
            'category_id' => '分类id',
            'content' => '内容',
            'stock' => '库存',
            'price' => '商品价格(分)',
            'thumbnail' => '缩略图',
            'status' => '0.上架中 1.已下架',
            'is_recommend' => '0.未推荐 1.推荐',
            'is_del' => '0.正常 1.已删除',
            'created_at' => '创建时间',
            'deleted_at' => '删除时间',
            'updated_at' => '更新时间',
            'is_specs' => '0.无规格 1.有规格',
        ];
    }
}
