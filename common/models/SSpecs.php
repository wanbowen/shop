<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "hm_s_specs".
 *
 * @property int $id
 * @property int $goods_id 商品id
 * @property string $name 规格名称
 * @property int $stock 库存
 * @property int $price 商品价格(分)
 * @property int $is_del 0.正常 1.已删除
 * @property string $created_at 创建时间
 */
class SSpecs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hm_s_specs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'name'], 'required'],
            [['id', 'goods_id', 'stock', 'price', 'is_del'], 'integer'],
            [['created_at'], 'safe'],
            [['name'], 'string', 'max' => 100],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'goods_id' => '商品id',
            'name' => '规格名称',
            'stock' => '库存',
            'price' => '商品价格(分)',
            'is_del' => '0.正常 1.已删除',
            'created_at' => '创建时间',
        ];
    }
}
