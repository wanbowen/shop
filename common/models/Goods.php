<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "hm_goods".
 *
 * @property int $id 序号
 * @property string $name 商品名称
 * @property int $category_id 分类id
 * @property string $price 商品价格
 * @property string $unit 价格单位(例：平米，次，个)
 * @property string $content 内容
 * @property string $thumbnail 缩略图
 * @property string $detail_imgs 详情图片
 * @property int $status 0.上架中 1.已下架
 * @property int $is_del 0.正常 1.已删除
 * @property string $created_at 创建时间
 * @property string $deleted_at 删除时间
 * @property string $updated_at 更新时间
 */
class Goods extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hm_goods';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'status', 'is_del'], 'integer'],
            [['price'], 'number'],
            [['content', 'detail_imgs'], 'string'],
            [['created_at', 'deleted_at', 'updated_at'], 'safe'],
            [['name', 'thumbnail'], 'string', 'max' => 100],
            [['unit'], 'string', 'max' => 50],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'category_id' => 'Category ID',
            'price' => 'Price',
            'unit' => 'Unit',
            'content' => 'Content',
            'thumbnail' => 'Thumbnail',
            'detail_imgs' => 'Detail Imgs',
            'status' => 'Status',
            'is_del' => 'Is Del',
            'created_at' => 'Created At',
            'deleted_at' => 'Deleted At',
            'updated_at' => 'Updated At',
        ];
    }
}
