<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "hm_order_goods".
 *
 * @property int $id 序号
 * @property string $order_code 订单号
 * @property int $user_id 用户id
 * @property int $goods_id 商品id
 * @property string $price 商品金额
 * @property int $amount 商品数量
 * @property string $price_total 商品金额合计
 * @property int $is_edit 是否是修改订单添加 0.不是 1.是
 * @property string $created_at 创建时间
 * @property string $updated_at 更新时间
 */
class OrderGoods extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hm_order_goods';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'goods_id', 'amount', 'is_edit'], 'integer'],
            [['price', 'price_total'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['order_code'], 'string', 'max' => 32],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_code' => 'Order Code',
            'user_id' => 'User ID',
            'goods_id' => 'Goods ID',
            'price' => 'Price',
            'amount' => 'Amount',
            'price_total' => 'Price Total',
            'is_edit' => 'Is Edit',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
