<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "hm_order_label".
 *
 * @property int $id 序号
 * @property string $name 标签名称
 * @property int $sort 排序
 * @property string $created_at 创建时间
 */
class OrderLabel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hm_order_label';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sort'], 'integer'],
            [['created_at'], 'safe'],
            [['name'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'sort' => 'Sort',
            'created_at' => 'Created At',
        ];
    }
}
