<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "hm_coupon_share".
 *
 * @property int $id 序号
 * @property int $coupon_id 优惠券id
 * @property string $updated_at 更新时间
 */
class CouponShare extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hm_coupon_share';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['coupon_id'], 'integer'],
            [['updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'coupon_id' => 'Coupon ID',
            'updated_at' => 'Updated At',
        ];
    }
}
