<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "hm_goods_unit".
 *
 * @property int $id 序号
 * @property string $name 单位名称
 * @property string $updated_at 更新时间
 */
class GoodsUnit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hm_goods_unit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['updated_at'], 'safe'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'updated_at' => 'Updated At',
        ];
    }
}
