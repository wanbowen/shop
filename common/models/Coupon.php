<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "hm_coupon".
 *
 * @property int $id 序号
 * @property string $name 优惠券名称
 * @property string $money 优惠券金额
 * @property string $start_at 开始时间
 * @property string $end_at 截至时间
 * @property int $restriction 优惠券使用条件(0表示无限制，大于0表示使用最低金额)
 * @property int $limit 优惠券领取限制(0表示无限制，大于0表示最多领取张数)
 * @property int $status 0.上架中 1.已下架
 * @property int $is_del 0.正常 1.已删除
 * @property string $created_at 创建时间
 * @property string $deleted_at 删除时间
 * @property string $updated_at 更新时间
 */
class Coupon extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hm_coupon';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['money'], 'number'],
            [['start_at', 'end_at', 'created_at', 'deleted_at', 'updated_at'], 'safe'],
            [['restriction', 'limit', 'status', 'is_del'], 'integer'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'money' => 'Money',
            'start_at' => 'Start At',
            'end_at' => 'End At',
            'restriction' => 'Restriction',
            'limit' => 'Limit',
            'status' => 'Status',
            'is_del' => 'Is Del',
            'created_at' => 'Created At',
            'deleted_at' => 'Deleted At',
            'updated_at' => 'Updated At',
        ];
    }
}
