<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "hm_goods_category".
 *
 * @property int $id 序号
 * @property string $pid 父级id
 * @property string $name 分类名称
 * @property string $banner_img 分类banner图
 * @property int $is_del 0.正常 1.已删除
 * @property string $created_at 创建时间
 * @property string $deleted_at 删除时间
 * @property string $updated_at 更新时间
 */
class GoodsCategory extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hm_goods_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pid', 'is_del'], 'integer'],
            [['created_at', 'deleted_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 50],
            [['banner_img'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'pid' => 'Pid',
            'name' => 'Name',
            'banner_img' => 'Banner Img',
            'is_del' => 'Is Del',
            'created_at' => 'Created At',
            'deleted_at' => 'Deleted At',
            'updated_at' => 'Updated At',
        ];
    }
}
