<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "hm_user".
 *
 * @property int $id 序号
 * @property string $name 用户姓名
 * @property int $mobile 用户手机号
 * @property string $address 用户地址
 * @property string $avatar 用户头像
 * @property string $openid 小程序openid
 * @property string $unionid 微信unionid
 * @property string $wx_openid 公众号openid
 * @property int $status 0.正常 1.黑名单
 * @property string $created_at 创建时间
 * @property string $updated_at 更新时间
 */
class User extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hm_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 50],
            [['address', 'avatar'], 'string', 'max' => 200],
            [['openid', 'unionid', 'wx_openid'], 'string', 'max' => 64],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'mobile' => 'Mobile',
            'address' => 'Address',
            'avatar' => 'Avatar',
            'openid' => 'Openid',
            'unionid' => 'Unionid',
            'wx_openid' => 'Wx Openid',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
