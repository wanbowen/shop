<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "hm_coupon_config".
 *
 * @property int $id 序号
 * @property int $switch 优惠券开关 0.开启 1.关闭
 * @property string $updated_at 更新时间
 */
class CouponConfig extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hm_coupon_config';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['switch'], 'integer'],
            [['updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'switch' => 'Switch',
            'updated_at' => 'Updated At',
        ];
    }
}
