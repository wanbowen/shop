<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "hm_s_pictures".
 *
 * @property int $id 序号
 * @property string $img 图片地址
 * @property int $status 0.启用 1.禁用
 * @property int $is_del 0.正常 1.已删除
 * @property string $created_at 创建时间
 * @property string $deleted_at 删除时间
 * @property string $updated_at 更新时间
 */
class SPictures extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hm_s_pictures';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'is_del'], 'integer'],
            [['created_at', 'deleted_at', 'updated_at'], 'safe'],
            [['img'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '序号',
            'img' => '图片地址',
            'status' => '0.启用 1.禁用',
            'is_del' => '0.正常 1.已删除',
            'created_at' => '创建时间',
            'deleted_at' => '删除时间',
            'updated_at' => '更新时间',
        ];
    }
}
