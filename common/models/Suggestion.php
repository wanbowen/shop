<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "hm_suggestion".
 *
 * @property int $id 序号
 * @property string $title 标题
 * @property string $content 建议内容
 * @property int $staff_id 建议提交人id
 * @property string $reply 回复内容
 * @property int $status 0.未回复 1.已反馈
 * @property int $is_del 0.正常 1.已删除
 * @property string $created_at 创建时间
 * @property string $reply_at 回复时间
 * @property string $deleted_at 删除时间
 */
class Suggestion extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hm_suggestion';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'status', 'is_del'], 'integer'],
            [['created_at', 'reply_at', 'deleted_at'], 'safe'],
            [['title'], 'string', 'max' => 128],
            [['content', 'reply'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'content' => 'Content',
            'user_id' => 'User ID',
            'reply' => 'Reply',
            'status' => 'Status',
            'is_del' => 'Is Del',
            'created_at' => 'Created At',
            'reply_at' => 'Reply At',
            'deleted_at' => 'Deleted At',
        ];
    }
}
