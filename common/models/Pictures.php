<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "hm_pictures".
 *
 * @property int $id 序号
 * @property int $type 1.首页banner图 2.首页底部图 3.分类banner图
 * @property string $name 图片名称
 * @property string $url 图片跳转链接
 * @property string $img 图片地址
 * @property string $desc 图片描述
 * @property int $sorts 排序
 * @property int $status 0.启用 1.禁用
 * @property int $is_del 0.正常 1.已删除
 * @property string $created_at 创建时间
 * @property string $deleted_at 删除时间
 * @property string $updated_at 更新时间
 */
class Pictures extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hm_pictures';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'sorts', 'status', 'is_del'], 'integer'],
            [['created_at', 'deleted_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 100],
            [['url', 'img', 'desc'], 'string', 'max' => 200],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'name' => 'Name',
            'url' => 'Url',
            'img' => 'Img',
            'desc' => 'Desc',
            'sorts' => 'Sorts',
            'status' => 'Status',
            'is_del' => 'Is Del',
            'created_at' => 'Created At',
            'deleted_at' => 'Deleted At',
            'updated_at' => 'Updated At',
        ];
    }
}
