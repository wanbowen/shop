<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "hm_book_money".
 *
 * @property string $money 金额
 * @property string $updated_at 更新时间
 */
class BookMoney extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hm_book_money';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['money'], 'number'],
            [['updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'money' => 'Money',
            'updated_at' => 'Updated At',
        ];
    }
}
