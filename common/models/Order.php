<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "hm_order".
 *
 * @property int $id 序号
 * @property string $order_code 订单号
 * @property int $user_id 用户id
 * @property string $mobile 联系电话
 * @property string $address 联系地址
 * @property string $start_at 预约开始时间
 * @property string $end_at 预约截至时间
 * @property string $book_money 预约金额
 * @property string $order_total 订单总额
 * @property string $pay_total 支付总额
 * @property int $is_coupon 是否用券 0.不用 1.使用
 * @property int $coupon_id 优惠券id
 * @property string $coupon_total 优惠券抵扣金额
 * @property int $order_status 订单状态 0.待服务(待派单) 1.已派单 3.待付款 4.已付款(待评价) 5.已评价(已完成)
 * @property string $remarks 用户备注
 * @property string $label_ids 标签项
 * @property int $staff_id 接单人id
 * @property string $order_remarks 订单备注
 * @property string $created_at 下单时间
 * @property string $updated_at 更新时间
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hm_order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'is_coupon', 'coupon_id', 'order_status', 'staff_id'], 'integer'],
            [['book_date', 'created_at', 'updated_at'], 'safe'],
            [['book_money', 'order_total', 'pay_total', 'coupon_total'], 'number'],
            [['order_code', 'mobile'], 'string', 'max' => 32],
            [['address'], 'string', 'max' => 200],
            [['remarks', 'order_remarks'], 'string', 'max' => 500],
            [['label_ids'], 'string', 'max' => 50],
            [['order_code'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_code' => 'Order Code',
            'user_id' => 'User ID',
            'mobile' => 'Mobile',
            'address' => 'Address',
            'start_at' => 'Start At',
            'end_at' => 'End At',
            'book_money' => 'Book Money',
            'order_total' => 'Order Total',
            'pay_total' => 'Pay Total',
            'is_coupon' => 'Is Coupon',
            'coupon_id' => 'Coupon ID',
            'coupon_total' => 'Coupon Total',
            'order_status' => 'Order Status',
            'remarks' => 'Remarks',
            'label_ids' => 'Label Ids',
            'staff_id' => 'Staff ID',
            'order_remarks' => 'Order Remarks',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
