<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "hm_coupon_receive".
 *
 * @property int $id 序号
 * @property int $user_id 用户id
 * @property int $origin_user_id 来源用户id
 * @property int $coupon_id 优惠券id
 * @property string $expire_date 优惠券到期时间
 * @property int $is_use 是否使用 0.未使用 1.已使用
 * @property string $use_date 优惠券使用时间
 * @property string $created_at 创建时间
 * @property string $updated_at 更新时间
 */
class CouponReceive extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hm_coupon_receive';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'origin_user_id', 'coupon_id', 'is_use'], 'integer'],
            [['expire_date', 'use_date', 'created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'origin_user_id' => 'Origin User ID',
            'coupon_id' => 'Coupon ID',
            'expire_date' => 'Expire Date',
            'is_use' => 'Is Use',
            'use_date' => 'Use Date',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
