<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "hs_s_goods_img".
 *
 * @property int $id
 * @property string $url 图片路径
 * @property int $goods_id 商品id
 * @property int $is_del 0.正常 1.已删除
 * @property string $created_at 创建时间
 */
class SGoodsImg extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hm_s_goods_img';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id'], 'required'],
            [['id', 'goods_id', 'is_del'], 'integer'],
            [['created_at'], 'safe'],
            [['url'], 'string', 'max' => 100],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'url' => '图片路径',
            'goods_id' => '商品id',
            'is_del' => '0.正常 1.已删除',
            'created_at' => '创建时间',
        ];
    }
}
