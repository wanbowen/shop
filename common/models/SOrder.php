<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "hm_s_order".
 *
 * @property int $id 序号
 * @property string $order_code 订单号
 * @property int $user_id 用户id
 * @property int $address_id 地址表id
 * @property string $order_total 订单总额
 * @property string $pay_total 支付总额
 * @property int $order_status 订单状态 0.已取消,1.待付款 2.已付款 3.已发货 4.已完成 5.退款中  6.退款完成
 * @property string $remarks 用户备注
 * @property string $pay_at 总单支付时间
 * @property string $transaction_id 总单支付时 微信订单号
 * @property string $created_at 下单时间
 * @property string $updated_at 更新时间
 * @property int $goods_id 商品id
 * @property string $express_code 快递号
 * @property int $self_status 1快递 0自取
 */
class SOrder extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hm_s_order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'address_id', 'order_status', 'goods_id', 'self_status'], 'integer'],
            [['address_id'], 'required'],
            [['order_total', 'pay_total'], 'number'],
            [['pay_at', 'created_at', 'updated_at'], 'safe'],
            [['order_code', 'express_code'], 'string', 'max' => 32],
            [['remarks'], 'string', 'max' => 500],
            [['transaction_id'], 'string', 'max' => 40],
            [['order_code'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '序号',
            'order_code' => '订单号',
            'user_id' => '用户id',
            'address_id' => '地址表id',
            'order_total' => '订单总额',
            'pay_total' => '支付总额',
            'order_status' => '订单状态 0.已取消,1.待付款 2.已付款 3.已发货 4.已完成 5.退款中  6.退款完成',
            'remarks' => '用户备注',
            'pay_at' => '总单支付时间',
            'transaction_id' => '总单支付时 微信订单号',
            'created_at' => '下单时间',
            'updated_at' => '更新时间',
            'goods_id' => '商品id',
            'express_code' => '快递号',
            'self_status' => '1快递 0自取',
        ];
    }
}
