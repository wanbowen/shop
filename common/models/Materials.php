<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "hm_materials".
 *
 * @property int $id 序号
 * @property int $type 1.企业介绍 2.企业动态 3.项目信息 4.生活小百科 5.客服电话
 * @property string $title 标题
 * @property string $content 内容
 * @property int $status 0.正常 1.禁用
 * @property int $is_del 0.正常 1.已删除
 * @property string $created_at 创建时间
 * @property string $updated_at 更新时间
 * @property string $deleted_at 删除时间
 */
class Materials extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'hm_materials';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'status', 'is_del'], 'integer'],
            [['content'], 'string'],
            [['created_at', 'updated_at', 'deleted_at'], 'safe'],
            [['title'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'title' => 'Title',
            'content' => 'Content',
            'status' => 'Status',
            'is_del' => 'Is Del',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'deleted_at' => 'Deleted At',
        ];
    }
}
